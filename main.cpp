
/**
 *      main
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include <ctime>
#include <getopt.h>

#include "bu/BUMasterPush.h"
#include "bu/BUMasterPull.h"
#include "ru/RUMasterPush.h"
#include "ru/RUMasterPull.h"
#include "em/EMMaster.h"
#include "ru/RUMaster.h"

#define MPI_TRANSPORT 0
#define VERBS_TRANSPORT 1
#define TCP_TRANSPORT 2
#define UDP_TRANSPORT 3
#define LIBFABRIC_TRANSPORT 4

#if TRANSPORT == MPI_TRANSPORT
    #include "transport/MPI/MPICore.h"
    #include "transport/MPI/MPIEMListener.h"
    #include "transport/MPI/MPIEMConsumer.h"
    #include "transport/MPI/MPIReadoutUnit.h"
    #include "transport/MPI/MPIBuilderUnit.h"
    #define TRANSPORT_NAMESPACE MPI_transport
#elif TRANSPORT == VERBS_TRANSPORT
    #include "transport/verbs/VerbsCore.h"
    #include "transport/verbs/VerbsEMListener.h"
    #include "transport/verbs/VerbsEMConsumer.h"
    #include "transport/verbs/VerbsReadoutUnit.h"
    #include "transport/verbs/VerbsBuilderUnit.h"
    #define TRANSPORT_NAMESPACE Verbs
#elif TRANSPORT == TCP_TRANSPORT
    #include "transport/TCP/TCPCore.h"
    #include "transport/TCP/TCPEMListener.h"
    #include "transport/TCP/TCPEMConsumer.h"
    #include "transport/TCP/tcpRU.h"
    #include "transport/TCP/tcpBU.h"
    #define TRANSPORT_NAMESPACE TCP
#elif TRANSPORT == UDP_TRANSPORT
    #include "transport/UDP/UDPCore.h"
    #include "transport/UDP/UDPEMListener.h"
    #include "transport/UDP/UDPEMConsumer.h"
    #include "transport/UDP/udpRU.h"
    #include "transport/UDP/udpBU.h"
    #define TRANSPORT_NAMESPACE UDP
#elif TRANSPORT == LIBFABRIC_TRANSPORT
    #include "transport/libfabric/LibFabricCore.h"
    #include "transport/libfabric/LibFabricEMListener.h"
    #include "transport/libfabric/LibFabricEMConsumer.h"
    #include "transport/libfabric/LibFabricReadoutUnit.h"
    #include "transport/libfabric/LibFabricBuilderUnit.h"
    #define TRANSPORT_NAMESPACE LibFabric
#else
    #error "Unsupported transport, please check your TRANSPORT variable"
#endif

#include "generator/Generator.h"
#include "transport/shmem/MemcpyComm.h"
#include "transport/shmem/ShmemBuilderUnit.h"
#include "transport/shmem/ShmemReadoutUnit.h"
#include "transport/bypass/BypassBuilderUnit.h"
#include "transport/bypass/BypassReadoutUnit.h"
#include "common/Logger.h"
#include <pthread.h>

namespace Core { Core::ConfigurationLoader* Configuration = new TRANSPORT_NAMESPACE::ConfigurationLoader(); }
namespace TRANSPORT_NAMESPACE { TRANSPORT_NAMESPACE::ConfigurationLoader* Configuration = dynamic_cast<TRANSPORT_NAMESPACE::ConfigurationLoader*>(Core::Configuration); }

const char cstHelpMsg[] = "\
Usage: %s [OPTIONS]\n\
Available options: \n\
   -h              Show help message\n\
   -p              Enable the profiling level\n\
   -S              Use PUSH approach\n\
   -L              Use PULL approach\n\
   -e              Use ethernet mode\n\
   -i              Use infiniband mode\n\
   -f <freq>       Set the trigger frequency\n\
   -s <size>       Set the event size\n\
   -n <nb>         Set the number of readout and builder per node\n\
   -c <credits>    Set the initial credit number for builders\n\
   -d <level>      Set the debug level (1-4)\n\
   -l <id>         Setup the logfile to use (0,1)\n\
   -r <id>         Print to std::cout (0,1)\n\
   -a <file.json>  Setup the configure file to use\n";

void showHelp(const char * exePath)
{
	fprintf(stderr,cstHelpMsg,exePath);
}

void * deadlockKiller(void * arg)
{
	sleep(2 * Core::Configuration->timeout);
	fprintf(stderr,"Get deadlock timeout, exiting to not eat ressources !");
	exit(1);
	return NULL;
}

int main(int argc, char *argv[])
{
    // Initialize MPI machinery and load configuration.

    // Get params
    int c;
    while ((c = getopt(argc, argv, "t:f:s:c:hd:pl:r:n:B:R:SLa:ei")) != -1)
        switch (c){
        case 'f': Core::Configuration->triggerFrequency = atoi(optarg); break;
        case 'c': Core::Configuration->initialCredits = atoi(optarg); break;
        case 's': Core::Configuration->eventSize = atoi(optarg); break;
        case 'd': Core::Configuration->logLevel = atoi(optarg); break;
        case 'p': Core::Configuration->activateLogLevel(Logger::_PROFILE); break;
        case 'l': Core::Configuration->logToFile = atoi(optarg); break;
        case 'r': Core::Configuration->logToStd = atoi(optarg); break;
        case 'S': Core::Configuration->protocol = Core::PUSH; break;
        case 'L': Core::Configuration->protocol = Core::PULL; break;
        case 't': Core::Configuration->timeout = atoi(optarg); break;
        case 'n':
            Core::Configuration->setBuilders(atoi(optarg));
            Core::Configuration->setReadouts(atoi(optarg));
            break;
        case 'B': Core::Configuration->setBuilders(atoi(optarg)); break;
        case 'R': Core::Configuration->setReadouts(atoi(optarg)); break;
        case 'a' :
            if(!Core::Configuration->loadConfigFile(optarg)){
                ERROR << "Unable to load a valid configuration file. Aborting" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case 'e' : Core::Configuration->interface = Core::ETHERNET; break;
        case 'i' : Core::Configuration->interface = Core::INFINIBAND; break;
        //case 'I' : TRANSPORT_NAMESPACE::Configuration->rankID = atoi(optarg); break;
        case 'h':
        case '?': showHelp(argv[0]);
            return -1;
            break;
        }


    TRANSPORT_NAMESPACE::Configuration->initialize(argc, argv);

    Core::Configuration->setConfiguration();

    TRANSPORT_NAMESPACE::Configuration->parseConfiguration();

    //start deadlock killer in case of trouble
    if (Core::Configuration->timeout > 0)
    {
        pthread_t th;
        pthread_create(&th,NULL,deadlockKiller,NULL);
    }

    MON << TRANSPORT_NAMESPACE::Configuration->name << " - rank " << TRANSPORT_NAMESPACE::Configuration->rankID << " started" << std::endl;

    // Event Manager processes
    if (TRANSPORT_NAMESPACE::Configuration->isEMListener()){
        // EventManagerListener
        // Also spawns shmem communicators
        MON << "main: " << Core::Configuration->name << " Event Manager Listener started" << std::endl;

        EMTransportCoreListener* transport = new TRANSPORT_NAMESPACE::EMListener();
        EMListenerMaster* eml;

        if (Core::Configuration->protocol == Core::PUSH)
            eml = new EMListenerMasterPush(Core::Configuration->trigger_sqName, transport);
        else if (Core::Configuration->protocol == Core::PULL)
            eml = new EMListenerMasterPull(Core::Configuration->trigger_sqName, transport);
        
        eml->run();
    }

    else if (TRANSPORT_NAMESPACE::Configuration->isEMConsumer()){
        // EventManagerConsumer
        MON << "main: " << Core::Configuration->name << " Event Manager Consumer started" << std::endl;
        Logger::logger(0) << "main: Configuration" << std::endl
            << (Core::Configuration->protocol == Core::PUSH ? " PUSH architecture" : " PULL architecture") << std::endl
            << " Frequency: " << Core::Configuration->triggerFrequency << std::endl
            << " Event request per cycle: " << Core::Configuration->eventSize << std::endl
            << "  Per-event data size: " << (Core::Configuration->eventSize * Core::Configuration->dataFragmentSize) / (1024.f * 1024.f) << " MB" << std::endl
            << " Initial credits: " << Core::Configuration->initialCredits << std::endl 
            << "  level: " << Core::Configuration->logLevel
            << ", " << Core::Configuration->mapLogLevels[Core::Configuration->logLevel] << std::endl
            << std::endl;
        
        if (Core::Configuration->isConfigurationValid()){
            EMTransportCoreConsumer* transport = new TRANSPORT_NAMESPACE::EMConsumer();
            EMConsumerMaster* emc;

            if (Core::Configuration->protocol == Core::PUSH)
                emc = new EMConsumerMasterPush(Core::Configuration->trigger_sqName, transport, Core::Configuration->triggerFrequency, Core::Configuration->eventSize);
            if (Core::Configuration->protocol == Core::PULL)
                emc = new EMConsumerMasterPull(Core::Configuration->trigger_sqName, transport, Core::Configuration->triggerFrequency, Core::Configuration->eventSize);
            
            emc->run();
        }
    }

    // Builder Unit
    else if (TRANSPORT_NAMESPACE::Configuration->is(TRANSPORT_NAMESPACE::BUILDER_UNIT)){
        // Hard coded wait for EventManager to initialize
        sleep(2);

        MON << "main: " << Core::Configuration->name << " Builder Unit started" << std::endl;

        BUTransportCore* butransport = new TRANSPORT_NAMESPACE::BuilderUnit();
        BUTransportCore* shmem_butransport = butransport;
        BUMaster* bu;

        if (Core::Configuration->protocol == Core::PUSH)
            bu = new BUMasterPush(butransport, shmem_butransport, Core::Configuration->initialCredits, Core::Configuration->eventSize);
        if (Core::Configuration->protocol == Core::PULL)
            bu = new BUMasterPull(butransport, shmem_butransport, Core::Configuration->initialCredits, Core::Configuration->eventSize);

        bu->run();
    }

    // Readout Unit
    else if (TRANSPORT_NAMESPACE::Configuration->is(TRANSPORT_NAMESPACE::READOUT_UNIT)){
        // Hard coded wait for EventManager to initialize
        sleep(2);

        // Start (some) generator!
        MON << "main: " << Core::Configuration->name << " Readout Unit started" << std::endl;

        Generator g(Core::Configuration->triggerFrequency);
        pthread_t * genThread = startThreadWithReference<Generator>(g);
        
        RUTransportCore* rutransport = new TRANSPORT_NAMESPACE::ReadoutUnit();
        RUMaster* ru;

        if (Core::Configuration->protocol == Core::PUSH)
            ru = new RUMasterPush(rutransport, Core::Configuration->initialCredits * Core::Configuration->numnodes);
        else if (Core::Configuration->protocol == Core::PULL)
            ru = new RUMasterPull(rutransport, Core::Configuration->initialCredits * Core::Configuration->numnodes);
        
        ru->run();
        
        //stop the generator thread
        g.stop();
        pthread_join(*genThread,NULL);
    }

    TRANSPORT_NAMESPACE::Configuration->finalize();
    return 0;
}
