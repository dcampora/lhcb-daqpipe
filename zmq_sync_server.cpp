#include "zmq_handshake/zmq_handshake.h"
#include "extern-deps/jsoncpp/dist/json/json.h"
#include <iostream>
#include "common/Logging.h"
#include <getopt.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include "transport/config/ConfigurationCore.h"

int main(int argc, char **argv){
    char c;
    char *file_name;
    std::ifstream config_file;
    std::string sync_server_port;
    std::string pub_server_port;
    Core::interface_type interface;
    int num_proc_per_node = 2;
    int num_procs;
    std::map<std::string, std::vector<int>> host_name_rank_id;

    Json::Value config_root;
    Json::Value int_type;
    Json::Value hosts;
    Json::Reader file_reader;
    int rank_id;

    while ((c = getopt(argc, argv, "ha:ein:B:R:")) != -1)
        switch (c){
        case 'e' : interface = Core::ETHERNET ; break;
        case 'i' : interface = Core::INFINIBAND ; break;
        case 'n' : num_proc_per_node += (2 * (atoi(optarg)-1)); break;
        case 'B' : num_proc_per_node += (atoi(optarg)-1); break;
        case 'R' : num_proc_per_node += (atoi(optarg)-1); break;
        case 'a' : file_name = optarg; break;
        case 'h' :
        case '?' : std::cerr << "Usage: " << argv[0]
                            << " [-h -n <numReadoutsBuildersPerNode> -R <numReadoutPerNode> -B <numBuildePerNode>]"
                            <<  "-a <json_config_file> -e (ethernet mode) -i (infiniband mode)"
                            << std::endl;
            return -1;
            break;
        }

    config_file.open(file_name);
    if(!config_file.is_open()){
        std::cerr << file_name << ": error opening config file" << std::endl;
        return (EXIT_FAILURE);
    }else{
        // TODO fix this issue is no checking for the integrity of the json file
       if(!(file_reader.parse(config_file, config_root))){
           std::cerr << file_name << ": error invalid json file" << std::endl;
           return (EXIT_FAILURE);
       }
    }


    switch (interface) {
    case Core::INFINIBAND:
        int_type = config_root["infiniband capabitilies"];
        break;
    case Core::ETHERNET:
        int_type = config_root["ethernet capabitilies"];
        break;
    default:
        break;
        std::cerr << "VerbsConfigurationLoader::initializeIPs: Undefined interface type aborting" << std::endl;
        exit(EXIT_FAILURE);
    }
    hosts = int_type["hosts"];


    rank_id = 0;
    // em listener and consumer
    host_name_rank_id[hosts[0].asString()].push_back(rank_id++);
    host_name_rank_id[hosts[0].asString()].push_back(rank_id++);
    host_name_rank_id[hosts[0].asString()].push_back(rank_id++);
    host_name_rank_id[hosts[0].asString()].push_back(rank_id++);
    // all the RUs BUs
    for (int k = 1 ; k<hosts.size() ; k++){
        rank_id += 2;
        for (int proc=0 ; proc < num_proc_per_node ; proc++){
            host_name_rank_id[hosts[k].asString()].push_back(rank_id++);
        }
    }

    for (auto host : host_name_rank_id){
        for (auto rank: host.second){
            std::cerr << "host " << host.first << " rank " << rank << std::endl;
        }
    }

    if(interface == Core::ETHERNET){
        num_procs = config_root.get("number of ethernet nodes", 0).asInt() * num_proc_per_node + 2;
    }else if(interface == Core::INFINIBAND){
        num_procs = config_root.get("number of infiniband nodes", 0).asInt() * num_proc_per_node + 2;
    }

    sync_server_port = config_root.get("sync server port", "").asString();
    pub_server_port = config_root.get("pub server port", "").asString();


    zmq_handshake_pub(num_procs, pub_server_port, sync_server_port, host_name_rank_id);


    return (EXIT_SUCCESS);



}
