######################################################
#            PROJECT  : lhcb-daqpipe
#            DATE     : 07/2015
#            AUTHOR   : Daniel Campora  - CERN
#                       Sébastien Valat - CERN
######################################################

######################################################
add_library(RUMaster STATIC MetaDataProcessor.cpp MetaReader.cpp RUMaster.cpp RUMasterPull.cpp RUMasterPush.cpp)
