
/**
 *      MetaDataProcessor
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      Movember, 2014
 *      CERN
 */

#ifndef MDPROCESSOR
#define MDPROCESSOR 1

#include <assert.h>
#include <limits>
#include <stdlib.h>
#include <malloc.h>
#include <vector>
#include <algorithm>
#include <list>
#include "../common/Logging.h"
#include "../common/GenStructures.h"
#include "../common/NamedPipe.h"
#include "../common/Timer.h"
#include "../transport/common/RUTransportCore.h"
#include "../transport/config/ConfigurationCore.h"
#include "MetaReader.h"

class MetaReader;

class MetaDataProcessor {
private:
    // Generator communication machinery
    struct metaStructure {
        metaStructure() {
            previous_initialized = false;
            current_initialized = false;
            previousMeta = -1;
            lastGeneratedMeta = -1;
            currentMeta = 0;
        }

        bool previous_initialized;
        bool current_initialized;
        unsigned int previousMeta;
        unsigned int currentMeta;
        unsigned int lastGeneratedMeta;
        unsigned int startPointer;
        unsigned int endPointer;
        std::string name;
    };

    struct dataStructure {
        dataStructure(){
            currentData = 0;
        }

        unsigned int currentData;
        unsigned int startPointer;
        unsigned int endPointer;
        std::string name;
    };

    void getMeta(Metadata* mpointer, metaStructure& mstruct, unsigned int startingEvent, unsigned int requestedEvents);
    void getData(Metadata* mpointer, metaStructure& mstruct, dataStructure& dstruct, unsigned int startingEvent, unsigned int requestedEvents);

    bool metaContainsEvent(Metadata& metadata, unsigned int eid);
    bool metaContainsEvent(Metadata& metadata, unsigned int eid, unsigned int previousLastEID);

protected:
    // Meta and data structures, for pointer calculation
    std::vector<metaStructure> metaStructures;
    std::vector<dataStructure> dataStructures;

    // generatedEvents and processed queues, communication with generator
    std::vector<NamedPipe<int>> qGenerated;
    NamedPipe<int> qruProcessed;

    unsigned int nqueues;

    unsigned int lastOrderedFinishedEvent;
    std::vector<unsigned int> finishedEvents;

    unsigned int generatedEvents;
    unsigned int currentEvent;
    unsigned int processed_events;

    unsigned int metaReservedElems;

    std::map<std::string, int> descriptors;

    std::list<int> activeRequests_eid;
    std::map<unsigned int, unsigned int> id_size_map;

    // MetaReader singleton
    MetaReader* metaReader;
    
    //thread handler
    pthread_t * thread;

public:
    std::map<std::string, void*> pmems;
    char** sendPointers;

    MetaDataProcessor();
    void readSHMem();

    void setEIDSize(unsigned int eid, unsigned int size);
    void selectMetaAndData(unsigned int startingEvent, unsigned int requestedEvents);
    void createFragmentComposition(FragmentComposition& fc);
    void communicateFinished(unsigned int finishedEventID);
    void updateCurrentPointers(unsigned int processedEvents);
    void stopThread(void);
	void registerMemory(RUTransportCore & transport);
};

#endif
