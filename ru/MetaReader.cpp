
/**
 *      MetaReader
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#include "MetaReader.h"

MetaReader::MetaReader(MetaDataProcessor* mdp_instance) :
    mdp(mdp_instance) {}

void MetaReader::run(){
    DEBUG << "MetaReader: Started! :D" << std::endl;
    
    this->hasToStop = false;

    while(hasToStop == false){
        mdp->readSHMem();
        usleep(1);
    }
}

void MetaReader::stop(void)
{
    this->hasToStop = true;
}
