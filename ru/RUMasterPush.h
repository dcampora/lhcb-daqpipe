
/**
 *      Readout Unit Master Push
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#include "RUMaster.h"

class RUMasterPush : public RUMaster
{
public:
    RUMasterPush(RUTransportCore* transport, unsigned int allocateNEvents) : 
        RUMaster(transport, allocateNEvents){}
    
    virtual bool mainLoop(int slot);
    virtual void run();
};
