
/**
 *      Readout Unit Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "RUMaster.h"

/**
 * Initialize the two generator queues, and the two meta / data
 * structures to hold their progress process.
 */
RUMaster::RUMaster(RUTransportCore* transport, unsigned int allocateNEvents)
    : transport(transport), allocateNEvents(allocateNEvents) {

    mdp = new MetaDataProcessor();
    fc = (FragmentComposition*) malloc(allocateNEvents * sizeof(FragmentComposition));

    showWaitingMessage = true;

    sendTimer.start();
}

/**
 * Use the proper transport to prepareSendEvent.
 * orderedIssuedEvents usage is described in sendEvent.
 * 
 * @param fc            
 * @param destinationBU 
 */
void RUMaster::prepareSendEvent(FragmentComposition* fc, unsigned int destinationBU){

    transport->prepareSendEvent(fc, destinationBU);

    // Insert in order
    bool inserted = false;
    for (auto ac_it = activeRequests_eid.begin(); ac_it != activeRequests_eid.end(); ++ac_it){
        if ((*ac_it) > fc->eventID){
            activeRequests_eid.insert(ac_it, fc->eventID);
            inserted = true;
            break;
        }
    }
    if (!inserted)
        activeRequests_eid.push_back(fc->eventID);
}

/**
 * sendEvent - Tests shmem and transport sendEvent.
 * The order is very important. Testing one or the other first implies
 * if the first checked is too quick, then the other will sit waiting for
 * a while... and therefore exceed the need for a sequential finish.
 *
 * Hereby the need of a std::list to check in order of issue.
 * This could also be left to the transport itself. Oh well.
 * 
 * @param  eventID 
 * @return         
 */
bool RUMaster::sendEvent(unsigned int& eventID){
    auto it = activeRequests_eid.begin();
    bool eventSent = false;

    // The app arbitrarily hangs because of the Generator not having enough room.
    // In function of the configured data buffer size, event number per eventID and
    // the distance between the first and last event in activeRequestList, we will
    // require the first event to finish, or check all in order
    unsigned int eventsDistance = (unsigned) abs(activeRequests_eid.back() - activeRequests_eid.front());
    if ((eventsDistance + 3) * Core::Configuration->eventSize * Core::Configuration->dataFragmentSize >= Core::Configuration->RUDataBufferSize){
        if (showWaitingMessage){
            WARNING << "RUMaster::sendEvent Waiting on evID " << activeRequests_eid.front() << std::endl;
            WARNING << "RUMaster last evID " << activeRequests_eid.back() << std::endl;
            WARNING << "RUMaster::sendEvent evDist " << eventsDistance << ", databufsize " << Core::Configuration->RUDataBufferSize
                << ", databuf current util " << (eventsDistance + 1) * Core::Configuration->eventSize * Core::Configuration->dataFragmentSize << std::endl;
            showWaitingMessage = false;
        }

        eventSent = transport->sendEvent(*it);
    }
    else {
        for (it = activeRequests_eid.begin(); it != activeRequests_eid.end(); it++){
            if (transport->sendEvent(*it)){
                eventSent = true;
                break;
            }
        }
    }

    // If using a non-guaranteed delivery mechanism, we have a max number of send attempts
    if (Core::Configuration->unreliableTransport &&
        sendTimer.getElapsedTime() > Core::Configuration->maxRUSendAttempts){
        transport->discardEvent();
        exit(EXIT_FAILURE);
    }

    if (eventSent){
        sendTimer.flush();
        sendTimer.start();
        eventID = *it;
        activeRequests_eid.erase(it);
        showWaitingMessage = true;
    }
    
    return eventSent;
}
