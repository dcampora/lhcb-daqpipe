
/**
 *      Readout Unit Master Pull
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#include <utility>
#include "RUMaster.h"

class RUMasterPull : public RUMaster
{
private:
    std::map<int, std::pair<int, int> > map_eid_size_bu;
    std::list<int> eids;
    int tolerance;
    Timer timeout;

public:
    RUMasterPull(RUTransportCore* transport, unsigned int allocateNEvents) : 
        RUMaster(transport, allocateNEvents){
        tolerance = allocateNEvents * 3;
    }
    
    void pullRequestReceive(int& eventID, int& size, int& destinationBU);
    virtual bool mainLoop(int slot);
    virtual void run();
};
