
/**
 *      Readout Unit Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef RUMASTER
#define RUMASTER 1

#include <assert.h>
#include <limits>
#include <stdlib.h>
#include <malloc.h>
#include <vector>
#include <algorithm>
#include <list>
#include "../common/Logging.h"
#include "../common/GenStructures.h"
#include "../common/NamedPipe.h"
#include "../common/Timer.h"
#include "../transport/common/RUTransportCore.h"
#include "../transport/config/ConfigurationCore.h"

#include "MetaDataProcessor.h"

class RUMaster {
protected:
    RUTransportCore* transport;
    MetaDataProcessor* mdp;
    
    unsigned int allocateNEvents;
    std::list<unsigned int> activeRequests_eid;
    FragmentComposition* fc;
    bool showWaitingMessage;
    std::map<unsigned int, unsigned int> eventID_slot;

    Timer ttotal, twait, tlisten, torder, tmeta, tfragment, tsetup, tprepare;
    Timer sendTimer, stuck_timer;

    RUMaster(RUTransportCore* transport, unsigned int allocateNEvents);

    void prepareSendEvent(FragmentComposition* fc, unsigned int destinationBU);
    bool sendEvent(unsigned int& eventID);

public:
    virtual bool mainLoop(int slot) = 0;
    virtual void run() = 0;
};

#endif
