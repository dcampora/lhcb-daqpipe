
/**
 *      MetaReader
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#ifndef METAREADER
#define METAREADER 1

#include "../common/pthreadHelper.h"
#include "../common/Logging.h"

class MetaDataProcessor;

// Simple reader thread
class MetaReader {
private:
    MetaDataProcessor* mdp;
    bool hasToStop;
    
public:
    MetaReader(MetaDataProcessor* mdp_instance);
    void run();
    void stop(void);
};

#endif

#include "MetaDataProcessor.h"
