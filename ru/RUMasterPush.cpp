
/**
 *      Readout Unit Master Push
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#include "RUMasterPush.h"

bool RUMasterPush::mainLoop(int slot){
    int size, eventID, destinationBU;

    tlisten.start();
    //if get false, then this is exit signal
    if (transport->EventListen(size, eventID, destinationBU) == false)
		return false;
    tlisten.stop();

    // Communicate to MetaDataProcessor what eid - size we got to process
    mdp->setEIDSize(eventID, size);

    // Prepare data
    tmeta.start();
    // TODO: Hack. eventID is multiEventID here...
    mdp->selectMetaAndData(eventID * size, size);
    tmeta.stop();
    eventID_slot[eventID] = slot;
    
    tfragment.start();
    mdp->createFragmentComposition(fc[slot]);
    fc[slot].eventID = eventID;
    tfragment.stop();

    // Don't send data fragments to same node.
    // This is to prevent mem-related bottlenecks.
    // Since in the final system we'll have ~500 nodes,
    // this effect will be negligible.
    if(Core::Configuration->builderInSameNode(destinationBU)){
        fc[slot].datablocks = 0;
    }

    if (Core::Configuration->logMetadata){
        DEBUG << "RUMasterPush: Printing metadata of event " << eventID << std::endl;
        for (int i=0; i<fc[slot].metablocks; ++i){
            Logger::printMeta(mdp->sendPointers[i], fc[slot].metasizes[i]);
        }
    }

    // Send!
    INFO << "RUMasterPush: prepare and setupSendEvent for eid " << eventID << " ..." << std::endl;
    tprepare.start();

    prepareSendEvent(&(fc[slot]), destinationBU);
    tprepare.stop();

    if (Core::Configuration->logFC && Core::Configuration->logLevel >= 4){
        INFO << "Printing Fragment Composition" << std::endl;
        Logger::printFC(&(fc[slot]));
    }

    tsetup.start();
    for (int i=0; i<fc[slot].metablocks; ++i)
        transport->setupSendEvent(mdp->sendPointers[i], fc[slot].metasizes[i], eventID, destinationBU);
    for (int i=0; i<fc[slot].datablocks; ++i)
        transport->setupSendEvent(mdp->sendPointers[i + fc[slot].metablocks], fc[slot].datasizes[i], eventID, destinationBU);
    tsetup.stop();
	
	return true;
}

void RUMasterPush::run(){
    // Print every 5.0 seconds monitoring info
    float printPeriod = 5.0f;

    Timer tperiod;
    tperiod.start();
    transport->initialize(allocateNEvents, mdp->pmems);

    mdp->registerMemory(*transport);
    
    // Send 'allocateNEvents' times
    for (int i=0; i<allocateNEvents; ++i){
        mainLoop(i);
    }

    // Start the main loop
    unsigned int counter = 0;
    unsigned int finishedEventID;
    while(true){
        ttotal.start();

        twait.start();
        while(!sendEvent(finishedEventID)){
            usleep(1);
        }
        finishedEventID = counter;
        twait.stop();

        INFO << "RUMasterPush: send " << counter++ << " (evID " << finishedEventID << ") completed!" << std::endl;

        // Communicate to Generator we just finished transmitting that event
        torder.start();
        mdp->communicateFinished(finishedEventID);
        torder.stop();

        // Grab slot and run
        int slot = eventID_slot[finishedEventID];
        eventID_slot.erase(finishedEventID);

        //check if get exit signal
        if (mainLoop(slot) == false)
        {
            DEBUG << "RUMasterPush: Get ReadoutUnit exit" << std::endl;
            break;
        }
        ttotal.stop();

        // Profiling
        if (Core::Configuration->logActivated[Logger::_PROFILE])
            if (tperiod.getElapsedTime() > printPeriod){
                // Timer ttotal, twait, torder, tmeta, tfragment, tsetup;
                PROFILE << "RUMasterPush: " << Core::Configuration->name << " timers: " << std::endl
                    << std::setw(10) << "ttotal " << ttotal.get() << std::endl
                    << std::setw(10) << "twait " << twait.get() << " (" << 100.f * twait.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tlisten " << tlisten.get() << " (" << 100.f * tlisten.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "torder " << torder.get() << " (" << 100.f * torder.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tmeta " << tmeta.get() << " (" << 100.f * tmeta.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tfragment " << tfragment.get() << " (" << 100.f * tfragment.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tsetup " << tsetup.get() << " (" << 100.f * tsetup.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tprepare " << tprepare.get() << " (" << 100.f * tprepare.get() / ttotal.get() << "\%)" << std::endl << std::endl;

                tperiod.flush();
                tperiod.start();
            }
    }
    
    //stop child threads
    mdp->stopThread();
}

