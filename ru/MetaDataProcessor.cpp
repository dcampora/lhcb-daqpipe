
/**
 *      MetaDataProcessor
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      Movember, 2014
 *      CERN
 */

#include "MetaDataProcessor.h"

#define WRITE_TO_SQ(X) while(X == -1) usleep(1)

/**
 * Initialize the two generator queues, and the two meta / data
 * structures to hold their progress process.
 */
MetaDataProcessor::MetaDataProcessor() : 
    qruProcessed(NamedPipe<int>(Core::Configuration->RUProcessedQName, Core::Configuration->RUQueueElements)),
    qGenerated({NamedPipe<int>(Core::Configuration->RUgpuGeneratedQName1, Core::Configuration->RUQueueElements),
        NamedPipe<int>(Core::Configuration->RUgpuGeneratedQName2, Core::Configuration->RUQueueElements)})
    {

    nqueues = 2;

    for (int i=0; i<nqueues; ++i){
        metaStructures.emplace_back(metaStructure());
        dataStructures.emplace_back(dataStructure());
    }
    metaStructures[0].name = std::string(Core::Configuration->RUMeta1Name);
    metaStructures[1].name = std::string(Core::Configuration->RUMeta2Name);
    dataStructures[0].name = std::string(Core::Configuration->RUData1Name);
    dataStructures[1].name = std::string(Core::Configuration->RUData2Name);

    for (int i=0; i<nqueues; ++i){
        openSharedMem(metaStructures[i].name, Core::Configuration->RUMetaBufferSize, descriptors[metaStructures[i].name], pmems[metaStructures[i].name]);
        openSharedMem(dataStructures[i].name, Core::Configuration->RUDataBufferSize, descriptors[dataStructures[i].name], pmems[dataStructures[i].name]);
    }

    currentEvent = 0;
    generatedEvents = 0;
    lastOrderedFinishedEvent = (unsigned int) Core::Configuration->startingEventID - 1;

    sendPointers = (char**) malloc(MAX_RU_FRAGMENTS * sizeof(char*));
    metaReservedElems = Core::Configuration->RUMetaBufferSize / sizeof(Metadata);

    processed_events = 0;

    metaReader = new MetaReader(this);
    thread = startThreadWithReference<MetaReader>(metaReader);
}

/**
 * Update metapointers by reading the shared queues from the Generator.
 */
void MetaDataProcessor::readSHMem(){
    // Read from meta1 and meta2, and increment counters
    int newGenerated;
    for (int i=0; i<nqueues; ++i){
        while(qGenerated[i].read(newGenerated) != -1){
//            INFO << "new generated "  << newGenerated << std::endl;
            generatedEvents += newGenerated;
            metaStructures[i].lastGeneratedMeta = (metaStructures[i].lastGeneratedMeta + 1) % metaReservedElems;
            metaStructures[i].current_initialized = true;
        }
    }
}

void MetaDataProcessor::registerMemory ( RUTransportCore& transport )
{
    for (int i=0; i<nqueues; ++i){
        transport.registerSegment(pmems[metaStructures[i].name],Core::Configuration->RUMetaBufferSize);
        transport.registerSegment(pmems[dataStructures[i].name],Core::Configuration->RUDataBufferSize);
    }
}

/**
 * Fills in eid_size map
 * @param eid  
 * @param size 
 */
void MetaDataProcessor::setEIDSize(unsigned int eid, unsigned int size){
    id_size_map[eid] = size;
}

/**
 * Get the meta we would need to send requestedEvents, from currentEvent.
 * This method updates the variables passed by reference, which act as
 * updated pointers to the requested data, as specified in startingEvent and requestedEvents.
 * 
 * @param mpointer        
 * @param meta            
 * @param startingEvent   
 * @param requestedEvents 
 */
void MetaDataProcessor::getMeta(Metadata* mpointer, MetaDataProcessor::metaStructure& mstruct,
    unsigned int startingEvent, unsigned int requestedEvents){
    
    // We can't really do anything until it's initialized
    while (!mstruct.current_initialized){
        usleep(1);
    }

    unsigned int lastEvent = startingEvent + requestedEvents;
    unsigned int mid = mstruct.currentMeta;
    unsigned int previousLastEID = 0;
    if (mstruct.previous_initialized)
        previousLastEID = mpointer[mstruct.previousMeta].lastEID;

    INFO << "MetaDataProcessor: getMeta Requested " << requestedEvents << " events starting with " << startingEvent << " in the metadata" << std::endl;

    bool found1 = false, found2 = false;
    while(!found1 || !found2){
        if (!found1 && metaContainsEvent(mpointer[mid], startingEvent, previousLastEID) ){
            mstruct.startPointer = mid;
            found1 = true;
        }

        if (!found2 && metaContainsEvent(mpointer[mid], lastEvent, previousLastEID) ){
            mstruct.endPointer = mid;
            found2 = true;
        }

        if( !found1 || !found2 ) {
            // We need to wait for MetaReader to generate more
            Timer t_stuck;
            t_stuck.start();
            while (mid == mstruct.lastGeneratedMeta){
                if(t_stuck.getElapsedTime() > 10){
                    WARNING << "MetaDataProcessor: getMeta waiting on metadata generation for longer than expected (backpressure)" << std::endl;
                    t_stuck.flush();
                    t_stuck.start();
                }
                usleep(1);
            }
            
            previousLastEID = mpointer[mid].lastEID;
            mid = (mid + 1) % metaReservedElems;
        }
    }

    DEBUG << "MetaDataProcessor: getMeta finished; updated startPointer " << mstruct.startPointer << ", endPointer " << mstruct.endPointer << std::endl;
}

/**
 * Returns whether the passed metadata contains the eid requested.
 * @param  metadata 
 * @param  eid      
 * @return          
 */
bool MetaDataProcessor::metaContainsEvent(Metadata& metadata, unsigned int eid){
    return ((eid - metadata.firstEID) <= (metadata.lastEID - metadata.firstEID));
}

/**
 * Overload using the previousLastEID.
 * @param  metadata        
 * @param  eid             
 * @param  previousLastEID 
 * @return                 
 */
bool MetaDataProcessor::metaContainsEvent(Metadata& metadata, unsigned int eid, unsigned int previousLastEID){
    return ((eid - previousLastEID) <= (metadata.lastEID - previousLastEID));
}

/**
 * Returns the beginning of the last data pointer which needs sending.
 * @param  mpointer      
 * @param  lastMetaIndex 
 * @return               
 */
void MetaDataProcessor::getData(Metadata* mpointer, MetaDataProcessor::metaStructure& mstruct,
    MetaDataProcessor::dataStructure& dstruct, unsigned int startingEvent, unsigned int requestedEvents){

    DEBUG << "MetaDataProcessor getData: seeking data pointers" << std::endl;

    // To make this a bit safer, initialize start and end pointers
    dstruct.startPointer = mpointer[mstruct.startPointer].entries[0].eid_pointer;
    // TODO: This should be the last eid_pointer
    dstruct.endPointer = mpointer[mstruct.endPointer].entries[0].eid_pointer;

    // These vectors are a bit of magic to write things in a compact way.
    std::vector<bool> found {false, false};
    std::vector<unsigned int> metaStartEndPointer {mstruct.startPointer, mstruct.endPointer};
    std::vector<unsigned int*> dpointers {&dstruct.startPointer, &dstruct.endPointer};
    std::vector<unsigned int> v_requestedEvent {startingEvent, startingEvent + requestedEvents};
    std::vector<std::string> info_message {"starting", "ending"};

    for (int i=0; i<nqueues; ++i){
        if (metaContainsEvent(mpointer[metaStartEndPointer[i]], v_requestedEvent[i])){
            for (int j=0; j<mpointer[metaStartEndPointer[i]].length; ++j){
                // Note the >= (it may be in between)
                if (mpointer[metaStartEndPointer[i]].entries[j].eid_lsb == v_requestedEvent[i]){
                    *(dpointers[i]) = mpointer[metaStartEndPointer[i]].entries[j].eid_pointer;
                    found[i] = true;
                    DEBUG << "MetaDataProcessor getData: Found " << info_message[i] << " data pointer " << *(dpointers[i]) << std::endl;
                    break;
                }
            }
        }
        else {
            INFO << "MetaDataProcessor: " << info_message[i] << " event is not held by " << info_message[i] << " metadata."
                 << " Assuming it's in other meta chunk." << std::endl;
            found[i] = true;
        }
    }

    for (bool f : found){
        if (!f){
            ERROR << "MetaDataProcessor getData: Didn't find one of the data pointers. "
                << "data_start " << dstruct.startPointer << " data_end " << dstruct.endPointer << std::endl;
            break;
        }
    }
}

/**
 * Update meta and data pointers, according to the requested events.
 * 
 * @param startingEvent   
 * @param requestedEvents 
 * @post  meta and data pointers (startPointer, endPointer) are updated.
 */
void MetaDataProcessor::selectMetaAndData(unsigned int startingEvent, unsigned int requestedEvents){
    // Using the info in meta and data, select the chunks
    // which will be sent. This assumes _contiguous_ data chunks.
    // 
    // They asked for *requestedEvents* events, and we are in *currentEvent*

    DEBUG << "MetaDataProcessor selectMetaAndData: Starting event " << startingEvent << ", requested " << requestedEvents << std::endl;

    std::vector<Metadata*> mpointers;
    for (int i=0; i<nqueues; ++i){
        // Get meta pointers
        mpointers.push_back((Metadata*) pmems[metaStructures[i].name]);

        // Update meta and data
        getMeta(mpointers[i], metaStructures[i], startingEvent, requestedEvents);
        getData(mpointers[i], metaStructures[i], dataStructures[i], startingEvent, requestedEvents);

        DEBUG << "MetaDataProcessor selectMetaAndData: Found meta pointers " << metaStructures[i].startPointer << ", " << metaStructures[i].endPointer << ";"
            << " data pointers " << dataStructures[i].startPointer << ", " << dataStructures[i].endPointer << std::endl;
    }
}

/**
 * Updates the current pointers currentEvent, currentMeta1, currentMeta2 according
 * to the processed events
 * 
 * @param processedEvents 
 */
void MetaDataProcessor::updateCurrentPointers(unsigned int processedEvents){
    currentEvent += processedEvents;

    std::vector<Metadata*> mpointers;
    for (int i=0; i<nqueues; ++i){
        // Get meta pointers
        mpointers.push_back((Metadata*) pmems[metaStructures[i].name]);

        unsigned int m = metaStructures[i].currentMeta;
        unsigned int previousMeta = metaStructures[i].previousMeta;
        bool previous_initialized = metaStructures[i].previous_initialized;

        unsigned int previousLastEID = 0;
        if (metaStructures[i].previous_initialized)
            previousLastEID = mpointers[i][previousMeta].lastEID;

        while(true){
            if( metaContainsEvent((mpointers[i])[m], currentEvent, previousLastEID) ){
                metaStructures[i].currentMeta = m;
                metaStructures[i].previousMeta = previousMeta;
                metaStructures[i].previous_initialized = previous_initialized;
                break;
            }

            // This should never happen
            if (m == metaStructures[i].lastGeneratedMeta)
                ERROR << "MetaDataProcessor updateCurrentPointers: Latest generatedEvents meta reached, this shouldn't happen" << std::endl;
            
            previousMeta = m;
            previous_initialized = true;
            m = (m + 1) % metaReservedElems;
        }

        DEBUG << "MetaDataProcessor updateCurrentPointers: Updated queue " << i
            << " to previousMeta " << metaStructures[i].previousMeta << ","
            << " currentMeta " << metaStructures[i].currentMeta << std::endl;
    }
}

/**
 * This function generates fragment compositions out of the existing
 * and already updated metadata pointers (m1_start, m1_end, m2_start, m2_end)
 * and data pointers (d1_start, d1_end, d2_start, d2_end).
 *
 * @param fc Where it will be stored
 */
void MetaDataProcessor::createFragmentComposition(FragmentComposition& fc){
    // Initialization
    unsigned int index = 0, dindex = 0;
    
    // By definition, at least we will send nqueues meta and data blocks (the current ones)
    // We initialize the meta and data sizes to 0
    fc.metablocks = nqueues;
    fc.datablocks = 0;
    for (int i=0; i<nqueues; ++i){
        fc.metasizes[i] = sizeof(FragmentComposition); // Default
    }

    // sendPointers is a variable that holds the pointers where meta / data is located.
    // Used in conjunction with meta/data sizes, pointers and size are calculated.
    // sendPointers is initialized succesively below.
    
    // Meta sendPointers and size calculation
    std::vector<Metadata*> mpointers;
    for (int i=0; i<nqueues; ++i){
        // Get meta pointers
        mpointers.push_back((Metadata*) pmems[metaStructures[i].name]);

        sendPointers[dindex++] = (char*) &(mpointers[i][metaStructures[i].startPointer]);
        if (metaStructures[i].startPointer > metaStructures[i].endPointer){
            fc.metablocks++;
            fc.metasizes[index++] = (metaReservedElems - metaStructures[i].startPointer) * sizeof(FragmentComposition);
            fc.metasizes[index++] = (metaStructures[i].endPointer + 1) * sizeof(FragmentComposition);
            sendPointers[dindex++] = (char*) mpointers[i];
        }
        else if (metaStructures[i].endPointer != metaStructures[i].startPointer)
            fc.metasizes[index++] = (metaStructures[i].endPointer - metaStructures[i].startPointer + 1) * sizeof(FragmentComposition);
    }

    // Data sendPointers and size calculation
    // 
    // Data treatment
    // Pointers to datas are included in the metadatas. The starting / ending
    // pointer should be calculated as a relative index to the data_start pointers
    // from those, but currently it's calculated using a fixed dataFragmentSize instead.
    //
    // TODO: Update this whenever we implement a different Generator.
    index = 0;
    for (int i=0; i<nqueues; ++i){

        if (dataStructures[i].startPointer > dataStructures[i].endPointer){
            fc.datablocks += 2;
            sendPointers[dindex++] = (char*) pmems[dataStructures[i].name] + dataStructures[i].startPointer;
            sendPointers[dindex++] = (char*) pmems[dataStructures[i].name];
            fc.datasizes[index++] = Core::Configuration->RUDataBufferSize - (dataStructures[i].startPointer + 1) * Core::Configuration->dataFragmentSize;
            fc.datasizes[index++] = (dataStructures[i].endPointer + 1) * Core::Configuration->dataFragmentSize;
        }
        else if (dataStructures[i].endPointer != dataStructures[i].startPointer){
            fc.datablocks += 1;
            sendPointers[dindex++] = (char*) pmems[dataStructures[i].name] + dataStructures[i].startPointer;
            fc.datasizes[index++] = (dataStructures[i].endPointer - dataStructures[i].startPointer + 1) * Core::Configuration->dataFragmentSize;
        }
    }
    
    for (auto i=0; i<fc.datablocks; ++i){
        const auto size = fc.datasizes[i];
        if (size > 100000000 || size < 0){
            ERROR << "MetaDataProcessor::createFragmentComposition: datasize out of bounds " << size << std::endl;
        }
    }
}

/**
 * Communicates back to the processed queue.
 * 
 * @param finishedEventID [description]
 */
void MetaDataProcessor::communicateFinished(unsigned int finishedEventID){
    // We rely on id_size_map being filled accordingly in the implementation
    // of the protocol

    finishedEvents.push_back(finishedEventID);
    std::sort(finishedEvents.begin(), finishedEvents.end());
    unsigned int size_events = 0;

    if (finishedEvents[0] == lastOrderedFinishedEvent+1){
        // Find last consecutive event finished, keep counter of how many
        // events were finished
        unsigned int fevents = 0;
        unsigned int prevLastOrderedFinishedEvent = lastOrderedFinishedEvent;
        auto it = finishedEvents.begin();
        for (; it != finishedEvents.end(); ++it){
            if (*it == lastOrderedFinishedEvent+1) ++lastOrderedFinishedEvent;
            else break;
        }

        // We send back from lastOrderedFinishedEvent until evcounter
        for (unsigned int i=(prevLastOrderedFinishedEvent+1); i != (lastOrderedFinishedEvent+1); ++i){
            DEBUG << "MetaDataProcessor communicateFinished: size of eid " << i << ": " << id_size_map[i] << std::endl;
            size_events += id_size_map[i];
            id_size_map.erase(i);
        }

        processed_events += size_events;

        WRITE_TO_SQ( qruProcessed.write( size_events ) );
        
        // There is a bit of desorganization. "event_no" means a different
        // thing in the generator and in the general EB (multi-event VS event).
        // This code is correct though.
        updateCurrentPointers(size_events);
        INFO << "MetaDataProcessor: Written " << size_events << std::endl;

        // Erase from vector
        finishedEvents.erase(finishedEvents.begin(), it);

        INFO << "MetaDataProcessor: lastOrderedFinishedEvent " << lastOrderedFinishedEvent << std::endl;
    }
}

void MetaDataProcessor::stopThread(void)
{
    metaReader->stop();
    pthread_join(*thread,NULL);
}
