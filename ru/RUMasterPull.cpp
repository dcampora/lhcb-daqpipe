
/**
 *      Readout Unit Master Pull
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#include "RUMasterPull.h"

void RUMasterPull::pullRequestReceive(int& eventID, int& size, int& destinationBU){
    // std::map<int, std::pair<int, int> > map_eid_size_bu;
    // std::list<int> eids;

    while(true){
        if (eids.empty()){
            // Get pull request
            transport->pullRequestReceive(eventID, size, destinationBU);
            eids.push_back(eventID);
            eids.sort();
            map_eid_size_bu[eventID] = std::make_pair(size, destinationBU);
        }
        else {
            // If we have pull requests, get the one in the front
            eventID = eids.front();
            size = map_eid_size_bu[eids.front()].first;
            destinationBU = map_eid_size_bu[eids.front()].second;

            eids.pop_front();
            map_eid_size_bu.erase(eventID);
            timeout.flush();
            timeout.start();
            return;
        }

        // TODO: Rethink
        // if (!eids.empty() && eids.front() < lastOrderedFinishedEvent + tolerance){
        //     eventID = eids.front();
        //     size = map_eid_size_bu[eids.front()].first;
        //     destinationBU = map_eid_size_bu[eids.front()].second;

        //     eids.pop_front();
        //     map_eid_size_bu.erase(eventID);
        //     timeout.flush();
        //     timeout.start();
        //     return;
        // }
        // else {
        //     if (!eids.empty()){
        //         DEBUG << "RUMasterPull: eids front " << eids.front() << ", eids last " << eids.back()
        //             << ", lastOrderedFinishedEvent " << lastOrderedFinishedEvent << std::endl;
                
        //         if (eids.front() < lastOrderedFinishedEvent + tolerance * 10){
        //             WARNING << "RUMasterPull: Popping out first event (" << eids.front() << ") - buffer-out" << std::endl;
        //             map_eid_size_bu.erase(eids.front());
        //             eids.pop_front();
        //         }
        //     }
        //     else {
        //         transport->pullRequestReceive(eventID, size, destinationBU);
        //         eids.push_back(eventID);
        //         eids.sort();
        //         map_eid_size_bu[eventID] = std::make_pair(size, destinationBU);
        //     }
        // }
    }
}

bool RUMasterPull::mainLoop(int slot){
    int eventID, size, destinationBU;

    // Get a pull request (non-blocking)
    // while (transport->pullRequestReceive(eventID, size, destinationBU) == 0);
    pullRequestReceive(eventID, size, destinationBU);

    DEBUG << "RUMasterPull: Received PULL request for eid " << eventID << " destination bu " << destinationBU << std::endl;

    // Communicate to MetaDataProcessor what eid - size we got to process
    mdp->setEIDSize(eventID, size);
    eventID_slot[eventID] = slot;

    // Prepare data
    tmeta.start();
    // Hack. eventID is multiEventID here...
    mdp->selectMetaAndData(eventID * size, size);
    tmeta.stop();
    
    tfragment.start();
    mdp->createFragmentComposition(fc[slot]);
    fc[slot].eventID = eventID;
    tfragment.stop();

    // Don't send data fragments to same node.
    // This is to prevent mem-related bottlenecks.
    // Since in the final system we'll have ~500 nodes,
    // this effect will be neglegible.
    if(Core::Configuration->builderInSameNode(destinationBU)){
        fc[slot].datablocks = 0;
    }

    if (Core::Configuration->logMetadata){
        DEBUG << "Printing metadata of event " << eventID << std::endl;
        for (int i=0; i<fc[slot].metablocks; ++i){
            Logger::printMeta(mdp->sendPointers[i], fc[slot].metasizes[i]);
        }
    }

    // Send!
    INFO << "RUMaster: prepare and setupSendEvent for " << eventID << "..." << std::endl;
    tsetup.start();

    prepareSendEvent(&(fc[slot]), destinationBU);

    if (Core::Configuration->logFC && Core::Configuration->logLevel >= 4){
        INFO << "Printing Fragment Composition" << std::endl;
        Logger::printFC(&(fc[slot]));
    }

    for (int i=0; i<fc[slot].metablocks; ++i)
        transport->setupSendEvent(mdp->sendPointers[i], fc[slot].metasizes[i], eventID, destinationBU);
    for (int i=0; i<fc[slot].datablocks; ++i)
        transport->setupSendEvent(mdp->sendPointers[i + fc[slot].metablocks], fc[slot].datasizes[i], eventID, destinationBU);
    tsetup.stop();
	
	return true;
}

void RUMasterPull::run(){
    float printPeriod = 5.0f; // Print every 5.0 seconds monitoring info

    Timer tperiod;
    tperiod.start();

    DEBUG << "RUMasterPull: Initializing transport" << std::endl;
    transport->initialize(allocateNEvents, mdp->pmems);

    // Send 'allocateNEvents' times
    for (int i=0; i<allocateNEvents; ++i){
        mainLoop(i);
    }
    
    mdp->registerMemory(*transport);

    // Start the main loop
    unsigned int counter = 0;
    unsigned int finishedEventID;
    while(true){
        ttotal.start();

        twait.start();
        while(!sendEvent(finishedEventID)) usleep(1);
        twait.stop();

        INFO << "RUMasterPull: send " << counter++ << " (evID " << finishedEventID << ") completed!" << std::endl;

        // Communicate to Generator we just finished transmitting that event
        torder.start();
        mdp->communicateFinished(finishedEventID);
        torder.stop();

        // Grab slot and run
        int slot = eventID_slot[finishedEventID];
        eventID_slot.erase(finishedEventID);

        mainLoop(slot);
        ttotal.stop();

        // if(stuck_timer.getElapsedTime()>1){
        //     MON << "RUMasterPull: generatedEvents consumed " << generatedEvents - processed_events << std::endl;
        //     stuck_timer.flush();
        //     stuck_timer.start();
        // }

        // Profiling
        if (Core::Configuration->logActivated[Logger::_PROFILE])
            if (tperiod.getElapsedTime() > printPeriod){
                // Timer ttotal, twait, torder, tmeta, tfragment, tsetup;
                PROFILE << "RUMasterPull: " << Core::Configuration->name << " timers: " << std::endl
                    << std::setw(10) << "ttotal " << ttotal.get() << std::endl
                    << std::setw(10) << "twait " << twait.get() << " (" << 100.f * twait.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tlisten " << tlisten.get() << " (" << 100.f * tlisten.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "torder " << torder.get() << " (" << 100.f * torder.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tmeta " << tmeta.get() << " (" << 100.f * tmeta.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tfragment " << tfragment.get() << " (" << 100.f * tfragment.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(10) << "tsetup " << tsetup.get() << " (" << 100.f * tsetup.get() / ttotal.get() << "\%)" << std::endl << std::endl;

                tperiod.flush();
                tperiod.start();
            }
    }
}

