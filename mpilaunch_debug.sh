#!/bin/bash

declare -A hostname_display

hostname_display[lab01]=localhost:10.0
hostname_display[lab02]=localhost:10.0
hostname_display[lab09]=localhost:10.0
hostname_display[lab10]=localhost:11.0
hostname_display[lab11]=localhost:10.0
hostname_display[lab12]=localhost:10.0
hostname_display[lab13]=localhost:11.0
hostname_display[lab14]=localhost:11.0
hostname_display[lab16]=localhost:10.0
hostname_display[lab17]=localhost:10.0
hostname_display[lab18]=localhost:11.0
hostname_display[lab19]=localhost:11.0
hostname_display[hltb0101]=localhost:11.0
hostname_display[hltb0102]=localhost:10.0
hostname_display[hltb0103]=localhost:10.0

if [ ${hostname_display[$HOSTNAME]} ]
then
  # echo $HOSTNAME is associated with ${hostname_bind[$HOSTNAME]}
  export DISPLAY=${hostname_display[$HOSTNAME]}
fi

$*
