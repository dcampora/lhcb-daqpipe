
/**
 *      SharedQueue
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "SharedQueue.h"

/**
 * Queue for one producer / one consumer
 *
 * OpenMPI: Use pthread_mutex in allocateSize to avoid race conditions,
 * since we don't know what their implementation of malloc
 * is doing.
 */

SharedQueue::SharedQueue(int size){
    _initialize();
    allocateSize(size);
}

void SharedQueue::_initialize(){
    _nwritten = 0;
    _nread = 0;
    _polls = 0;
}

void SharedQueue::allocateSize(int size){
    _size = size;
    _array = (int*) malloc(_size * sizeof(int));
    // _array = (int*) mmap(NULL, _size * sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    // if (_array == MAP_FAILED) ERROR << "SharedQueue: " << strerror(errno) << std::endl;

    _producerPointer = _array;
    _consumerPointer = _array;
}

/**
 * Writes a value onto the SharedQueue.
 * @param  x Value to write.
 * @return   -1 if unsuccesful, 0 if succesful.
 */
int SharedQueue::write(int x){
    // The substraction naturally takes care of overflows
    if (_nwritten - _nread < _size){
        *_producerPointer = x;
        if ((++_producerPointer) >= _array + _size)
            _producerPointer = _array;

        ++_nwritten;
        return 0;
    }
    return -1;
}

/**
 * Reads a value from the SharedQueue.
 * @return -1 if unsuccesful, the value otherwise.
 * Note: The accepted values on the SharedQueue are positive.
 */
int SharedQueue::read(int& rval){
    // std::cout << "polls: " << _polls << std::endl;
    _polls++;
    if (_nwritten > _nread){
        // DEBUG << "(written, read) - (" << _nwritten << ", " << _nread << ") after " << _polls << " polls" << std::endl;
        // DEBUG << "mem locs " << std::hex << &_nwritten << ", " << &_nread << std::endl;
        rval = *_consumerPointer;
        if (++_consumerPointer >= _array + _size)
            _consumerPointer = _array;

        ++_nread;
        return 0;
    }
    return -1;
}

int SharedQueue::getWritten(){
    return _nwritten;
}