/**
 *      NamedPipe
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      April, 2015
 *      CERN
 */

template<class T>
NamedPipe<T>::NamedPipe(const std::string& sharedName, int numElements,bool skipUserName) : size(numElements) {
    if(sizeof(T) > PIPE_BUF){
        std::cerr << "WARNING the size of the specified object excedes the size that can be atomically"
                  <<  "written into a pipe using more producers/consumers may produce race conditions"
                  << std::endl;
    }
    if (sharedName.length() != 0){
        //get user name to add to the path (will create /tmp/daqpipe-{USER} to store the named pipe files)
        std::string dirName = std::string("/tmp/daqpipe-")+getenv("USER");

        //create the directory if needed (or will exist with EEXIST)
        int error = mkdir(dirName.c_str(),S_IRUSR|S_IWUSR|S_IXUSR);
        assumeArg(error == 0 || errno == EEXIST,"Fail to create the directory to store named pipes : %1")
            .arg(dirName)
            .end();

        _sharedName = dirName + "/" + sharedName;
        // TODO this should not be done by the library
        error = mkfifo(_sharedName.c_str(), 0755);
        if (error == -1){
            if(errno != EEXIST){
                std::cerr << "NamedPipe: " << strerror(errno) << " " << _sharedName << std::endl;
            }
        }

        // TODO this is potentially unsafe the queue should open read only or write only
        descriptor = open(_sharedName.c_str(), O_RDWR | O_NONBLOCK);

        if(descriptor == -1){
            std::cerr << "NamedPipe: " << strerror(errno) << " " << _sharedName << std::endl;
        }
    }
}

template<class T>
NamedPipe<T>::~NamedPipe(){
    //std::cerr << "NamedPipe: destroing " << _sharedName << std::endl;
    //unclose(descriptor);
    //unlink(_sharedName.c_str());
}

/**
 * Writes a value onto the SharedQueue.
 * @param  x Value to write.
 * @return   -1 if unsuccesful, 0 if succesful.
 */
template<class T>
int NamedPipe<T>::write(T x){
    int ret_val = 0;

    if((int)(::write(descriptor, &x, sizeof(T))) < (int)sizeof(T)){
        ret_val = -1;
    }

    return ret_val;
}

/**
 * Reads a value from the SharedQueue.
 * @return -1 if unsuccesful, the value otherwise.
 * Note: The accepted values on the SharedQueue are positive.
 */
template<class T>
int NamedPipe<T>::read(T& r){
    int ret_val = 0;

    if((int)(::read(descriptor, &r, sizeof(T))) < (int)sizeof(T)){
        ret_val = -1;
    }

    return ret_val;
}

template<class T>
std::string NamedPipe<T>::getSharedName(){
    return _sharedName;
}
