
/**
 *      Logger, based on,
 *      http://www.drdobbs.com/cpp/a-lightweight-logger-for-c/240147505?pgno=1
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef LOGGER
#define LOGGER 1

#include <sstream>
#include <iostream>
#include <iomanip>
#include <map>
#include "GenStructures.h"

namespace Logger {
enum logLevels {
    _PROFILE=10,
    _DEBUG=5,
    _INFO=4,
    _MON=3,
    _WARNING=2,
    _ERROR=1
};

std::ostream& logger(int requestedLogLevel);

}

#endif

// It's necessarily outside the ifndef
#include "../transport/config/ConfigurationCore.h"
