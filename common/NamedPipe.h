
/**
 *      NamedPipe
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      April, 2015
 *      CERN
 */

#ifndef NAMED_PIPE
#define NAMED_PIPE 1

#include <iostream>
#include "stdlib.h"
#include "Shmem.h"
#include <mutex>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <linux/limits.h>
#include "Debug.hpp"

template<class T>
class NamedPipe
{
protected:
    int descriptor;
    std::string _sharedName;
    // this member is not in use is just for compatibility with the Shmem_SharedQueue
    int size;

public:
    NamedPipe(const std::string& sharedName, int numElements,bool skipUserName = false);
    ~NamedPipe();
    std::string getSharedName();
    int write(T x);
    int read(T& r);
};

#include "NamedPipe_impl.h"

#endif

