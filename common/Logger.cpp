
/**
 *      Logger
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "Logger.h"

std::ostream& Logger::logger(int requestedLogLevel){
    if ((Core::Configuration->logToFile || Core::Configuration->logToStd) &&
        (Core::Configuration->logLevel >= requestedLogLevel || 
         Core::Configuration->logActivated[requestedLogLevel])){
        
        std::string pre = requestedLogLevel == 0 ? "" : "[" + Core::Configuration->mapLogLevels[requestedLogLevel] + "]: ";

        // Dispatcher
        if (Core::Configuration->logToFile && Core::Configuration->logToStd){
            Core::Configuration->logfilestd << pre;
            return Core::Configuration->logfilestd;
        }
        else if (Core::Configuration->logToFile){
            Core::Configuration->logfile << pre;
            return Core::Configuration->logfile;
        }
        else {
            std::cout << pre;
            return std::cout;
        }
    }
    else {
        return Core::Configuration->discardStream;
        // Logger::discard.str("");
        // Logger::discard.clear();
        // return Logger::discard;
    }
}
