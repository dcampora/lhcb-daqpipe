
/**
 *      LockShmem_SharedQueue
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      February, 2013
 *      CERN
 */

/**
 * Queue for multiple producers / multiple consumers
 */

/**
 * Writes a value onto the SharedQueue.
 * @param  x Value to write.
 * @return   -1 if unsuccesful, 0 if succesful.
 */
template<class T>
int LockShmem_SharedQueue<T>::write(T x){
    // The substraction naturally takes care of overflows
    pthread_mutex_lock(&write_mutex);
    if ( ((unsigned int) (*this->nwritten - *this->nread)) < this->size){
        // ++this->polls;
        // DEBUG << "LockShmem: Write " << this->polls << std::endl;
        // DEBUG << "LockShmem_SharedQueue: WRITE w " << *this->nwritten << ", r " << *this->nread << std::endl;
        *this->producerPointer = x;
        if ((++this->producerPointer) >= this->ipmem + this->size)
            this->producerPointer = this->ipmem;
        ++*this->nwritten;
        pthread_mutex_unlock(&write_mutex);
        return 0;
    }
    pthread_mutex_unlock(&write_mutex);
    return -1;
}

/**
 * Reads a value from the SharedQueue.
 * @return -1 if unsuccesful, the value otherwise.
 * Note: The accepted values on the SharedQueue are positive.
 */
template<class T>
int LockShmem_SharedQueue<T>::read(T& r){
    // std::cout << "polls: " << _polls << std::endl;
    pthread_mutex_lock(&read_mutex);
    // DEBUG << "LockShmem_SharedQueue: READ w " << *this->nwritten << ", r " << *this->nread << std::endl;
    if ((*this->nwritten - *this->nread) > 0){
        r = *this->consumerPointer;
        if (++this->consumerPointer >= this->ipmem + this->size)
            this->consumerPointer = this->ipmem;
        ++*this->nread;
        pthread_mutex_unlock(&read_mutex);
        return 0;
    }
    pthread_mutex_unlock(&read_mutex);
    return -1;
}
