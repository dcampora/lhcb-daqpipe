
/**
 *      shmem my way - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "Debug.hpp"
#include "Shmem.h"

int openSharedMem(std::string& sharedName, size_t size, int& descriptor, void*& pmem){
    int flags = O_CREAT | O_RDWR;
    mode_t perms = S_IRWXU | S_IRWXG | S_IRWXO;

    //prefix with daqpipe-{USER}
    std::string filename = sharedName + std::string("-daqpipe-")+getenv("USER");

    //check size
    assumeArg(filename.size() < 255,"Shared filename is too large, get %1, must be lower than %2")
        .arg(filename.size())
        .arg(255)//ideally it must be NAME_MAX from manpage
        .end();

    //open
    descriptor = shm_open(filename.c_str(), flags, perms);
    if (descriptor == -1){
        ERROR << "Shmem::openSharedMem: shm_open error"  << std::endl;
        return -1;
    }

    if (ftruncate(descriptor, size) == -1){
        ERROR << "Shmem::openSharedMem: ftruncate error"  << std::endl;
        return -1;
    }

    /* Map shared memory object */
    pmem = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, 0);
    if (pmem == MAP_FAILED){
        ERROR << "Shmem::openSharedMem: " << strerror(errno) << std::endl;
        return -1;
    }

    return 0;
}

int closedSharedMem(int descriptor){
    return close(descriptor);
}

int deleteSharedMem(std::string& sharedName){
    //prefix with daqpipe-{USER}
    std::string filename = sharedName + std::string("-daqpipe-")+getenv("USER");

    return shm_unlink(filename.c_str());
}
