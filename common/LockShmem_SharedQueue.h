
/**
 *      LockShmem_SharedQueue
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      February, 2013
 *      CERN
 */

#ifndef LOCK_SHMEM_SHARED_QUEUE
#define LOCK_SHMEM_SHARED_QUEUE 1

/**
 * Queue with thread synchronization.
 * https://www.quantnet.com/threads/c-multithreading-in-boost.10028/
 */

#include "Shmem_SharedQueue.h"
#include "Logging.h"

/**
 * Queue for one producer / one consumer.
 */
template<class T>
class LockShmem_SharedQueue : public Shmem_SharedQueue<T>
{
private:
    pthread_mutex_t write_mutex;
    pthread_mutex_t read_mutex;

public:
    LockShmem_SharedQueue(std::string& sharedName, int numElements) : 
        Shmem_SharedQueue<T>(sharedName, numElements) {
            pthread_mutex_init(&write_mutex, NULL);
            pthread_mutex_init(&read_mutex, NULL);
        }
    int write(T x);
    int read(T& r);
};

#include "LockShmem_SharedQueue_impl.h"

#endif

