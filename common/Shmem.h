
/**
 *      shmem my way
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef SHARED_MEMORY_MY_WAY
#define SHARED_MEMORY_MY_WAY 1

#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h> /* For O_* constants */
#include <unistd.h>
#include <errno.h>
#include <string>
#include <iostream>
#include <cstring> 
#include "Logging.h"

/**
 * Opens a shared memory object (POD), determined
 * by the name and size.
 * @param  sharedName
 * @param  size       
 * @param  descriptor    descriptor of the object, if succesful.
 * @param  pmem         pointer to memory, if succesful.
 * @return            0 if success, -1 otherwise.
 */
int openSharedMem(std::string& sharedName, size_t size, int& descriptor, void*& pmem);

/**
 * Closes the shared memory object.
 * @param  descriptor 
 * @return            
 */
int closedSharedMem(int descriptor);

/**
 * Deletes the shared memory object from /dev/shm fs.
 * @param  sharedName 
 * @return            
 */
int deleteSharedMem(std::string& sharedName);

#endif
