
/**
 *      Shmem_SharedQueue
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef SHMEM_SHARED_QUEUE
#define SHMEM_SHARED_QUEUE 1

/**
 * Queue with thread synchronization.
 * https://www.quantnet.com/threads/c-multithreading-in-boost.10028/
 */

#include <iostream>
#include "stdlib.h"
#include "Shmem.h"

/**
 * Queue for one producer / one consumer.
 */
template<class T>
class Shmem_SharedQueue
{
protected:
    T *producerPointer, *consumerPointer, *ipmem;
    int *nread, *nwritten;
    void* pmem;
    int descriptor;
    unsigned int size, polls;
    std::string sharedName;

public:
    Shmem_SharedQueue(std::string& sharedName, int numElements);
    ~Shmem_SharedQueue();
    int write(T x);
    int read(T& r);

    int getRead();
    int getWritten();
    int* getArray();
};

#include "Shmem_SharedQueue_impl.h"

#endif

