
#ifndef LOGGING
#define LOGGING 1

#include "Logger.h"
#include <list>

#define PROFILE Logger::logger(Logger::_PROFILE)
#define DEBUG   Logger::logger(Logger::_DEBUG)
#define INFO    Logger::logger(Logger::_INFO)
#define MON     Logger::logger(Logger::_MON)
#define WARNING Logger::logger(Logger::_WARNING)
#define ERROR   Logger::logger(Logger::_ERROR)

namespace Logger {

void printFC(FragmentComposition* fc);
void printMeta(char* metapointer, int size);

template<class T>
void printIterable(T& t);

};

#endif 
