
/**
 *      Timer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#include "Timer.h"

Timer::Timer() : _append(0), timeDiff(0) {}

void Timer::start(){
    clock_gettime(CLOCK_REALTIME, &tstart);
}

void Timer::stop(){
    clock_gettime(CLOCK_REALTIME, &tend);
    timeDiff += (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
}

void Timer::flush(){
    timeDiff = 0;
}

float Timer::getElapsedTime(){
    clock_gettime(CLOCK_REALTIME, &tend);
    timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
    return timeDiff / 1000000000.0f;
}

long long int Timer::getElapsedTimeLong(){
    clock_gettime(CLOCK_REALTIME, &tend);
    timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
    return timeDiff;
}

float Timer::get(){
    return timeDiff / 1000000000.0f;
}

long long int Timer::getLong(){
    return timeDiff;
}
