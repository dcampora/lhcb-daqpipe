
/**
 *      Timer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#ifndef DP_TIMER 
#define DP_TIMER 1

#include <ctime>

#define GET_TIME(__TIMER__, __FOO__) __TIMER__.start(); __FOO__; __TIMER__.stop()

class Timer {
private:
    struct timespec tstart, tend;
    long long int timeDiff;
    bool _append;

public:
    Timer();

    void start();
    void stop();
    void flush();

    /**
     * Gets the elapsed time since tstart
     * @return
     */
    float getElapsedTime();
    long long int getElapsedTimeLong();

    /**
     * Gets the accumulated time in timeDiff
     * @return
     */
    float get();
    long long int getLong();
};

#endif
