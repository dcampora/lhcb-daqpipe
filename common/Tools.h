
/* 
 * File:   Tools.h
 * Author: dcampora
 *
 * Created on August 23, 2012
 */

#ifndef TOOLS_H
#define	TOOLS_H

#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

class Tools {
public:
    /**
     * @brief mk_dir make a directory and all the parent directories with default permissions
     * @param name path of the new directory
     * @return true on success false on error check errno
     */
    inline static bool mk_dir(std::string name){
       return mk_dir(name, 0755);
    }

    /**
     * @brief mk_dir make a directory and all the parent directories with custom permissions
     * @param name path of the new directory
     * @param permissions permissions used for the creation of the new directory
     * @return true on success false on error check errno
     */
    static bool mk_dir(std::string name, mode_t permissions){
        struct stat st = {0};
        std::vector<std::string> sub_paths;
        bool ret_val = true;
        size_t current_separator, last_separator;

        last_separator = 0;
        current_separator = name.find('/');
        while (current_separator != std::string::npos){
           if(current_separator - last_separator > 1){
               sub_paths.push_back(name.substr(0, current_separator));
           }
           last_separator = current_separator;
           current_separator = name.find('/', last_separator+1);
        }

        if (last_separator != name.length() - 1){
            sub_paths.push_back(name);
        }

        for (std::vector<std::string>::iterator sub_path = sub_paths.begin() ;
             sub_path != sub_paths.end() && ret_val ; sub_path++){
            if (stat(sub_path->c_str(), &st) == -1){
                if (mkdir(sub_path->c_str(), permissions) < 0) {
                    if (errno != EEXIST){
                        ret_val = false;
                    }
                }
            }
        }


        return ret_val;
    }
    
    /** Returns a string with the contents of T t */
    template <class T>
    static std::string toString(T t){
        std::stringstream ss;
        std::string s;
        ss << t;
        ss >> s;
        return s;
    }
    
    /** Transforms a given number to binary format */
    template <class T>
    static std::string num2bin(T& a) {
        std::string s;
        char* cp = reinterpret_cast<char*>(&a);
        for (int i=(sizeof(T)-1); i>=0; i--){
            for (int j=7; j>=0; j--){
                // LE
                int temp = (cp[i] >> j) & 1;
                s += Tools::toString<int>(temp);
                if (!(j % 4) && !(i==(sizeof(T)-1) && j==7)) s += ' ';
            }
        }
        return s;
    }
    
    /** Prints a number of bits from the specified bitfield */
    template <class T>
    static std::string bitfield2bin(T& a, int num_bits_to_print) {
        std::string s;
        for (int i=num_bits_to_print - 1 /*(sizeof(T) * 8) - 1*/; i>=0; i--){
            int temp = (a >> i) & 1;
            s += Tools::toString<int>(temp);
            if (!(i % 4)) s += ' ';
        }
        return s;
    }
    
    /** Converts from vector<string> to argc, argv (returns argc, argv is a parameter) */
    static int vec2arg(std::vector<std::string>& v, char**& argv){
        int argc = v.size();
        argv = (char**) malloc((argc + 1) * sizeof(char*));
        for(int i=0; i<v.size(); i++){
            argv[i] = const_cast<char*>(v[i].c_str());
        }
        argv[argc] = NULL;
        
        return argc;
    }
    
    /** Converts from int32 to string */
    static std::string inttostr(int addr32){
        char* addrc = (char*)(&addr32);
        
        // TODO: test inet_ntoa
        
        return Tools::toString<int>( abs((int)addrc[0]) ) + "." +
               Tools::toString<int>( abs((int)addrc[1]) ) + "." +
               Tools::toString<int>( abs((int)addrc[2]) ) + "." +
               Tools::toString<int>( abs((int)addrc[3]) );
    }

    static bool fileExists(const std::string& filename){
        struct stat buf;
        if (stat(filename.c_str(), &buf) != -1)
            return true;
        return false;
    }
};

#endif	/* TOOLS_H */

