
/**
 *      Logging
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      February, 2013
 *      CERN
 */

#include "Logging.h"

void Logger::printFC(FragmentComposition* fc){
    if (Core::Configuration->logFC){
        INFO << "FragmentComposition - #meta " << fc->metablocks << ", #data " << fc->datablocks << std::endl;
        Logger::logger(0) << "metasizes: ";
        if (fc->metablocks > 10)
            ERROR << "number of metas is too huge (" << fc->metablocks << ")" << std::endl;
        for (int i=0; i<fc->metablocks; ++i){
            Logger::logger(0) << fc->metasizes[i] << ", ";
        }
        Logger::logger(0) << std::endl;
        Logger::logger(0) << "datasizes: ";
        if (fc->datablocks > 10)
            ERROR << "number of datas is too huge (" << fc->datablocks << ")" << std::endl;
        for (int i=0; i<fc->datablocks; ++i){
            Logger::logger(0) << fc->datasizes[i] << ", ";
        }
        Logger::logger(0) << std::endl;
    }
}

void Logger::printMeta(char* pointer, int size){
    if (Core::Configuration->logMetadata){
        // Supposing the beginning pointer is a meta pointer...
        Metadata* metapointer = (Metadata*) &(pointer[0]);

        Logger::logger(0) << "Meta block starting with eid #" << metapointer->firstEID
            << ", size " << size << std::endl << std::hex;
        int j=0;
        for (int i=0; i<size; ++i){
            Logger::logger(0) << std::setw(8) << std::setfill('0') << ((int) pointer[i]);
            if ((++j % 8) == 0) Logger::logger(0) << std::endl;
        }
        Logger::logger(0) << std::dec << std::endl;
    }
}

template<class T>
void Logger::printIterable(T& t){
    if (Core::Configuration->logLevel >= 4){
        INFO << "Iterable contents: ";
        for (typename T::iterator it = t.begin(); it != t.end(); it++){
            Logger::logger(0) << *it << ", ";
        }
        Logger::logger(0) << std::endl;
    }
}
