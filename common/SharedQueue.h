
/**
 *      SharedQueue
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef SHARED_QUEUE
#define SHARED_QUEUE 1

/**
 * Queue with thread synchronization.
 */

#include <iostream>
#include <sys/mman.h>
#include <errno.h>
#include <cstring> 
#include "stdlib.h"
#include "Logging.h"
#include "../transport/config/ConfigurationCore.h"

/**
 * Queue for one producer / one consumer.
 */
class SharedQueue
{
private:
    int *_array, *_producerPointer, *_consumerPointer;
    unsigned int _nwritten, _nread, _size, _polls;
    void _initialize();

public:
    SharedQueue(int size);
    void allocateSize(int size);
    int write(int x);
    int read(int& rval);

    int getWritten();
};

#endif
