
struct CreditAnnounce {
    int numCredits;
};

struct EventRequest {
    int size;
    int eid;
    int destBU;
};

struct EventAssign {
    int eid;
    int size;
};

struct FragmentComposition {
    int metablocks, datablocks;
    int metasizes[5];
    int datasizes[5];
};

#define MAX_EVENTIDS_METADATA 1024

struct MetadataEntry {
    short eid_lsb : 16;
    short eid_pointer : 16; // The "pointer" here refers to the element in the array
};

struct Metadata {
    short length : 16;
    short noEvents : 16;
    unsigned int firstEID;
    unsigned int lastEID;
    char* startingPointer;
    MetadataEntry entries[MAX_EVENTIDS_METADATA];
};
