#!/bin/bash

declare -A hostname_bind

hostname_bind[lab09]=1
hostname_bind[lab10]=1
hostname_bind[lab11]=1
hostname_bind[lab12]=1
hostname_bind[lab13]=0
hostname_bind[lab14]=1

if [ ${hostname_bind[$HOSTNAME]} ]
then
  # echo $HOSTNAME is associated with ${hostname_bind[$HOSTNAME]}
  numactl --cpunodebind=${hostname_bind[$HOSTNAME]} --membind=${hostname_bind[$HOSTNAME]} $*
else
  # echo $HOSTNAME is not associated - 0
  numactl --cpunodebind=0 --membind=0 $*
fi
