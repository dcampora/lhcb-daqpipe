
/**
 *      Data generator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#ifndef DAQ_GENERATOR
#define DAQ_GENERATOR 1

#include <map>
#include <unistd.h>
#include <math.h>
#include <cstdlib>
#include <cstring>
#include "../common/NamedPipe.h"
#include "../common/Logging.h"
#include "../common/GenStructures.h"
#include "../common/Timer.h"

struct pieceOfData {
  char blockSize [4096];
};

class Generator {
private:
    int pagesize;
    int numElements, eventsToReport, sizeOfEvent_meta, sizeOfEvent_data, frequency,
        metaReservedElems, dataReservedElems;
    
    std::string meta1Name, meta2Name, data1Name, data2Name;
    NamedPipe<int> qgpuGenerated1, qgpuGenerated2, qruProcessed;
    
    std::map<std::string, void*> pmems;
    std::map<std::string, int> descriptors;
    std::map<std::string, int> currentElem;
    std::map<std::string, char*> currentPointer;

    Metadata* currentMetadataPointer;
    int* currentDataPointer;
    int currentEID;

    int reportEach, reportCounter;

    volatile pieceOfData someData;
    
    bool hasToStop;

public:
    Generator(int frequency);

    int generateMeta(NamedPipe<int>*& sh_queue);
    void run();
    void stop();
};


#endif
