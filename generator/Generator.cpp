
/**
 *      Data generator implementation
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#include "Generator.h"

#define WRITE_TO_SQ(X) while(X == -1) usleep(1);

Generator::Generator(int frequency) : frequency(frequency),
    qgpuGenerated1(NamedPipe<int>(Core::Configuration->RUgpuGeneratedQName1, Core::Configuration->RUQueueElements)),
    qgpuGenerated2(NamedPipe<int>(Core::Configuration->RUgpuGeneratedQName2, Core::Configuration->RUQueueElements)),
    qruProcessed(NamedPipe<int>(Core::Configuration->RUProcessedQName, Core::Configuration->RUQueueElements)),
    meta1Name(std::string(Core::Configuration->RUMeta1Name)),
    meta2Name(std::string(Core::Configuration->RUMeta2Name)),
    data1Name(std::string(Core::Configuration->RUData1Name)),
    data2Name(std::string(Core::Configuration->RUData2Name)) {

    // Reserve meta and data
    // note: sizeof(struct meta), and sizeof(data)
    // pagesize = sysconf(_SC_PAGESIZE); // Do we need this?

    for (int i=0; i<4096; ++i)
      someData.blockSize[i] = (char) i;

    // Default parameters
    metaReservedElems = Core::Configuration->RUMetaBufferSize / sizeof(Metadata);
    dataReservedElems = Core::Configuration->RUDataBufferSize / Core::Configuration->dataFragmentSize;

    DEBUG << "Generator: We have space for " << metaReservedElems << " meta elems and "
        << dataReservedElems << " data elems" << std::endl;

    openSharedMem(meta1Name, Core::Configuration->RUMetaBufferSize, descriptors[meta1Name], pmems[meta1Name]);
    openSharedMem(meta2Name, Core::Configuration->RUMetaBufferSize, descriptors[meta2Name], pmems[meta2Name]);
    openSharedMem(data1Name, Core::Configuration->RUDataBufferSize, descriptors[data1Name], pmems[data1Name]);
    openSharedMem(data2Name, Core::Configuration->RUDataBufferSize, descriptors[data2Name], pmems[data2Name]);

    // Start pointers
    currentElem[meta1Name] = 0;
    currentElem[meta2Name] = 0;
    currentElem[data1Name] = 0;
    currentElem[data2Name] = 0;
    currentEID = (Core::Configuration->startingEventID * (Core::Configuration->eventSize));

    // Debug purposes
     reportEach = 10000;
//    reportEach = 1000000;
    reportCounter = reportEach;
    hasToStop = false;
}

void Generator::stop()
{
    this->hasToStop = true;
}

void Generator::run(){
    int newConsumed;
    INFO << "Generator: Started" << std::endl;
    DEBUG << "Generator: starting pointers: meta1 " << std::hex << (long long int) pmems[meta1Name] << ", meta2 " << (long long int) pmems[meta2Name]
        << ", data1 " << (long long int) pmems[data1Name] << ", data2 " << (long long int) pmems[data2Name] << std::dec << std::endl;

    // By default:
    //  40 B for metadata size
    //  1 KB for event fragment size
    int period = (1.0f / frequency) * 1000000.0f;
    unsigned int generableEvents = Core::Configuration->RUDataBufferSize / Core::Configuration->eventFragmentSize;

    unsigned int generated = 0;
    unsigned int consumed =  0;

    Timer t_print, t_stuck;

    NamedPipe<int>* sh_queue;

    // TODO: Debug
    while(hasToStop == false){
        // Generate every period
        // usleep(period);
        if(t_print.getElapsedTime() > 1){
        //    DEBUG << "generator: generated - consumed " << (generated - consumed) << std::endl;
            t_print.start();
            t_print.flush();
        }

        if ((qruProcessed.read(newConsumed)) != -1){
            consumed += newConsumed;
//            DEBUG << "new consumed " << newConsumed << std::endl;
        }

        t_stuck.flush();
        t_stuck.start();

        // We need room for the new events we want to generate.
        // Has the RU consumed enough?
        bool printMessage = true;
        while ((generated - consumed) >= generableEvents - MAX_EVENTIDS_METADATA){
            if (printMessage)
                INFO << "Generator: We have in total room for " << generableEvents << " events,"
                      << " but " << (generated - consumed) << " are waiting to be consumed." << std::endl;

            // Update consumed
            while( (qruProcessed.read(newConsumed)) != -1 ){
                consumed += newConsumed;
                DEBUG << "Generator: newConsumed " << newConsumed << std::endl;
            }

            if(t_stuck.getElapsedTime() > 10){
             //   ERROR << "generator stuck" << std::endl;
                t_stuck.start();
                t_stuck.flush();
            }
            
            if (hasToStop)
            {
                DEBUG << "Generator : Has stop message" << std::endl;
                break;
            }

            usleep(1);
            printMessage = false;
        }

        int newGenerated = generateMeta(sh_queue);
        generated += newGenerated;

//        DEBUG << "new generated " << newGenerated << std::endl;
        // Generates events, and communicates new events in the
        // corresponding queue
        WRITE_TO_SQ( sh_queue->write( newGenerated ) );

        if (generated > reportCounter){
//            DEBUG << "Generator: Generated " << generated << " events" << std::endl;
//            reportCounter += reportEach;
        }
    }
}

/**
 * @pre We can generate a maximum of MAX_EVENTIDS_METADATA (ie. there's space
 *      both in meta and data pointers free, ie. already consumed or not used yet).
 */
int Generator::generateMeta(NamedPipe<int>*& sh_queue){
    // Choose randomly the location where this meta will be generated
    // Note: This generator generates data in a straightforward, simple
    // way. Two meta and data buffers are kept, one is chosen randomly and
    // a chunk is generated, and then sent to the queue. There is no
    // interleaved entries therein, but as long as the RU doesn't assume
    // this then it should be fine.
    std::string metaName, dataName;
    switch(rand() % 2){
        case 0: metaName = meta1Name; dataName = data1Name; sh_queue = &qgpuGenerated1; break;
        case 1: metaName = meta2Name; dataName = data2Name; sh_queue = &qgpuGenerated2; }

    Metadata* metadataPointer = (Metadata*) pmems[metaName];
    char* dataPointer = (char*) pmems[dataName];
    int& melem = currentElem[metaName];
    int& delem = currentElem[dataName];

    // Generate between 18 and 28 events
    // (more) real generation of random number
    // float r = ((float) rand()) / (((float)RAND_MAX) + 1); // Random number [0,1)
    // int r10 = floor(r * 11.0f); // Random number [0,10]
    int nevents = (MAX_EVENTIDS_METADATA - 10) + (rand() % 11);
//    int nevents = MAX_EVENTIDS_METADATA;

    // short length : 16;
    // short noEvents : 16;
    // int firstEID;
    // int lastEID;
    // char* startingPointer;
    // int entries[28];
    metadataPointer[melem].length = (short) nevents;
    metadataPointer[melem].noEvents = (short) nevents;
    metadataPointer[melem].firstEID = currentEID;
    metadataPointer[melem].lastEID = currentEID + nevents - 1;
    metadataPointer[melem].startingPointer = dataPointer + delem;

    for (int i=0; i<nevents; ++i){
        metadataPointer[melem].entries[i].eid_lsb = currentEID + i;
        metadataPointer[melem].entries[i].eid_pointer = delem;

        // TODO: Several caveats of this:
        //  It reads and writes (it should only write)
        //  It slows down the application (perhaps have another thread write... we would need a queue to generate at a rate in that case)
        // TODO: This is considering the Core::Configuration->dataFragmentSize is 1024.
        // if ((delem % 4) == 0){
        //   memcpy(&dataPointer[delem * Core::Configuration->dataFragmentSize], (void const *) &someData.blockSize[0], 4096);
        // }
        delem = (delem + 1) % dataReservedElems;

        // for (int j=0; j<Core::Configuration->dataFragmentSize; ++j){
        //   dataPointer[delem * Core::Configuration->dataFragmentSize + j] = (char) rand();
        // }

        // Deprecated
        // If it's aligned, it's fine
        // delem = (delem + Core::Configuration->dataFragmentSize) % Core::Configuration->RUDataBufferSize;
    }

    // Print out the written metadata
    // DEBUG << "Generator: metadata " << metaName << " (" << std::hex << metadataPointer + melem << std::dec
    //       << "), elem " << melem << std::endl
    //       << metadataPointer[melem].length << ", " << metadataPointer[melem].noEvents << ", "
    //       << metadataPointer[melem].firstEID << ", " << metadataPointer[melem].lastEID << ", "
    //       << std::hex << (int*) metadataPointer[melem].startingPointer << std::dec << std::endl;

    // Increase the general pointers
    melem = (melem + 1) % metaReservedElems;
    currentEID += nevents;

    return nevents;
}
