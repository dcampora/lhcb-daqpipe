#!/bin/bash

server="lab09"
server_ip="192.168.6.35"
client="lab10"

server_exe="/home/fpisani/verb_test/simple_send_rec/simple_server"
server_opt="-p 123"
client_exe="/home/fpisani/verb_test/simple_send_rec/simple_client"
client_opt="-a $server_ip -n $2 -p 123"
server_for="for i in `seq 1 $1`; do $server_exe $server_opt\$i & ; done"

trap 'killall -9 `basename "$server_exe"`; exit' SIGHUP SIGINT SIGTERM
# spawning servers
for i in `seq 1 $1`; do
	echo $i
	sleep 0.1
	$server_exe $server_opt$i > /dev/null&
done
cat
