#!/bin/bash

spawner_path="/home/fpisani/verb_test/simple_send_rec/simple_client -a 192.168.6.35 -p 1235"
file_out=$1

echo -n > $file_out
for k in `seq 1 $2`
do
	echo -n "$k	" >> $file_out
	$spawner_path -n $k > /dev/null 2>> $file_out 
done
	
