#include "simple_verbs_lib.h"

Message::Message(){

}

Message::~Message(){

}

Message::Message(message_type type, message_payload payload){
    _type = type;
    _payload = payload;
}

message_type& Message::get_type(){
    return _type;
}

message_payload& Message::get_payload(){
    return _payload;
}

void Message::set_type(message_type type){
    _type = type;
}

void Message::set_payload(message_payload payload){
    _payload = payload;
}

void Message::send_message(message_type type, message_payload payload, struct connection *conn){
    _type = type;
    _payload = payload;

    this->send_message(conn);
}

void Message::send_message(struct connection *conn){
    struct ibv_send_wr wr, *bad_wr = NULL;
    struct ibv_sge sge;

    memset(&wr, 0, sizeof(wr));

    wr.wr_id = (uintptr_t)conn;
    wr.opcode = IBV_WR_SEND;
    wr.sg_list = &sge;
    wr.num_sge = 1;
    wr.send_flags = IBV_SEND_SIGNALED;

    sge.addr = (uintptr_t)this;
    sge.length = sizeof(Message);
    sge.lkey = conn->send_mr->lkey;

    check_nz(ibv_post_send(conn->qp, &wr, &bad_wr), "on_connection ibv_post_send", true);

}

void Message::send_mr(struct connection *conn){
    _type = MSG_MR;
    memcpy(&(_payload.mr), conn->rdma_remote_mr, sizeof(struct ibv_mr));

    this->send_message(conn);
}

void init_buffer_content(void *buffer){
    int index;
    int index_max;
    int *int_buffer;
    int_buffer = (int *)buffer;

    index_max = BUFFER_SIZE/sizeof(int);

    for(index=0 ; index<index_max ; index++){
        //printf("index %d\n", index);
        *(int_buffer+index) = index;
    }
}

void printf_buffer_content(void *buffer){
    int index;
    int index_max;
    int *int_buffer;
    int_buffer = (int *)buffer;

    index_max = BUFFER_SIZE/sizeof(int);

    for(index=0 ; index<index_max ; index++){
        //printf("index %d\n", index);
        printf("%d\n",*(int_buffer+index));
    }
}

void check_alloc(const void* array, const char *message, bool fatal){
    if(array==NULL){
        perror(message);
        if(fatal){
            exit(EXIT_FAILURE);
        }
    }
}

void check_nz(int value, const char *message, bool fatal){
    if(value != 0){
        perror(message);
        if(fatal){
            exit(EXIT_FAILURE);
        }
    }
}

void build_context(struct ibv_context *verbs, context *ctx, int connection_number) {
    // TODO multiple device operation
    //    if (*ctx != NULL) {
    //        if ((*ctx)->ctx != verbs){
    //            fprintf(stderr, "Error: cannot handle events in more than one context.\n");
    //            exit(EXIT_FAILURE);
    //        }

    //        /* context already created no further action needed */
    //        return;
    //    }
    static struct ibv_srq *srq = NULL;
    static struct ibv_comp_channel *comp_channel = NULL;
    static struct ibv_cq *cq = NULL;


    if (srq == NULL){
        ctx->ctx = verbs;

        ctx->pd = ibv_alloc_pd(ctx->ctx);
        check_alloc(ctx->pd, "build_context ibv_alloc_pd ctx->pd", true);

        struct ibv_srq_init_attr srq_attr;
        memset(&srq_attr, 0, sizeof (srq_attr));
        srq_attr.attr.max_wr = 10;
        srq_attr.attr.max_sge = 1;
        srq = ibv_create_srq(ctx->pd, &srq_attr);
        check_alloc(srq, "build_context ibv_create_srq:", true);

        comp_channel = ibv_create_comp_channel(srq->context);
        check_alloc(comp_channel, "build_context ibv_alloc_pd ctx->comp_channel", true);

        check_nz(pthread_create(&(ctx->cq_poller_thread), NULL, poll_cq, ctx), "build_context pthread_create", true);

        //ctx->cq = ibv_create_cq(ctx->ctx, 10, NULL, comp_channel, 0); /* cqe=10 is arbitrary */
        //check_alloc(cq, "build_context ibv_alloc_pd ctx->cq", true);



    //    struct ibv_srq_init_attr srq_attr;
    //    memset(&srq_attr, 0, sizeof (srq_attr));
    //    srq_attr.attr.max_wr = ctx->max_wr;
    //    srq_attr.attr.max_sge = 1;
    //    ctx->srq = ibv_create_srq(ctx->pd, NULL, &srq_attr);
    //    check_alloc(ctx->srq, "build_context ibv_create_srq:", true);

        ctx->srq = srq;
        ctx->comp_channel = comp_channel;
    }

    ctx->connections[connection_number]->cq = ibv_create_cq(verbs, 10, NULL, ctx->comp_channel, 0); /* cqe=10 is arbitrary */
    check_alloc(ctx->connections[connection_number]->cq, "build_context ibv_alloc_pd ctx->cq", true);
    //ctx->cq = cq;
    printf("ctx cq %p\n", ctx->connections[connection_number]->cq);

    check_nz(ibv_req_notify_cq(ctx->connections[connection_number]->cq, 0), "build_context ibv_req_notify_cq", true);

}

void build_qp_attr(struct ibv_qp_init_attr *qp_attr, struct connection *conn) {
    memset(qp_attr, 0, sizeof(*qp_attr));

    qp_attr->send_cq = conn->cq;
    qp_attr->recv_cq = conn->cq;
    qp_attr->qp_type = IBV_QPT_RC;

    qp_attr->cap.max_send_wr = 50;
    qp_attr->cap.max_recv_wr = 50;
    qp_attr->cap.max_send_sge = 1;
    qp_attr->cap.max_recv_sge = 1;
}

int on_connect_request(struct rdma_cm_id *id, struct context *ctx, std::map <unsigned int, int> &mapIPID){
    struct ibv_qp_init_attr qp_attr;
    struct rdma_conn_param cm_params;
    struct connection *conn;
    std::map<unsigned int,int>::iterator it;
    int connection_id = 0;


    conn = (struct connection*) malloc(sizeof(struct connection));
    check_alloc(conn, "on_connect_request malloc conn", true);


    it = mapIPID.find(((struct sockaddr_in *) &id->route.addr.dst_addr)->sin_addr.s_addr);
    if(it == mapIPID.end()){
        fprintf(stderr, "Connection from undefined device\n");
        return(-1);
    }else{
        connection_id = it->second;
    }

    ctx->connections[connection_id] = conn;

    printf("received connection request number %d.\n", connection_id);

    printf("building context\n");
    build_context(id->verbs, ctx, connection_id);
    build_qp_attr(&qp_attr, ctx->connections[connection_id]);

    check_nz(rdma_create_qp(id, ctx->pd, &qp_attr), "on_connect_request rdma_create_qp", true);

    conn->connection_number = connection_id;
    conn->local_done = false;
    conn->remote_done = false;
    conn->stop_gone = false;

    conn->client_addr = (struct sockaddr*) malloc(sizeof(struct sockaddr));

    *conn->client_addr = id->route.addr.dst_addr;

    printf("connection from ip %s\n",  id->route.addr.dst_addr.sa_data);
    id->context = conn;
    conn->qp = id->qp;
    printf("conn qp handle %u\n", conn->qp->handle);

    register_memory(conn, ctx);

    memset(&cm_params, 0, sizeof(cm_params));
    check_nz(rdma_accept(id, &cm_params), "on_connect_request rdma_accept", true);
    /* required to receive the message from the client */
    post_receives(conn);
    // TODO not so elegant try to find a better way
    post_receives(conn);

    connection_id++;
    return 0;
}

void on_completion(struct ibv_wc *wc) {
    extern int verbose;
    struct connection *conn = (struct connection *)(uintptr_t)wc->wr_id;

    if(verbose >= 1){
        printf("connection number:%d\n", conn->connection_number);
    }

    if (wc->status != IBV_WC_SUCCESS){
        fprintf(stderr, "on_completion: status is %s.\n", ibv_wc_status_str(wc->status));
        exit(EXIT_FAILURE);
    }

    if (wc->opcode & IBV_WC_RECV) {
        if(conn->recv_msg->get_type() == MSG_MR){
            // TODO more efficient way to do this probably a copy is not needed
            printf("MR received\n");
            memcpy(&(conn->peer_mr), &(conn->recv_msg->get_payload().mr), sizeof(conn->peer_mr));
            if(conn->my_type == CLIENT){
                conn->send_msg->send_mr(conn);
            }
            post_receives(conn);
            int k;
            conn->packet_left = conn->total_packets;
            printf("start time a\n");
            clock_gettime(CLOCK_MONOTONIC, &(conn->start));
            for(k=0 ; k<10 && (conn->packet_left)>0 ; k++){
                rdma_write(conn);
                //(conn->packet_left)--;
            }
        }else if (conn->recv_msg->get_type() == MSG_END){
            printf("END msg received\n");
            //printf("received message: %s\n", conn->rdma_remote_buff);
            printf("received message:\n");
            if(verbose >= 1){
                //printf_buffer_content(conn->rdma_remote_buff);
            }
            printf("last :%d\n", *((int *)(conn->rdma_remote_buff) + BUFFER_SIZE/sizeof(int) -1));
            conn->remote_done = true;
            if(conn->my_type == CLIENT){
                printf("trasmission disconnecting\n");
                //rdma_disconnect(conn->id);
            }else if(conn->local_done || ((conn->packet_left) <= 0)){
                printf("server done\n");
                //clock_gettime(CLOCK_MONOTONIC, &(conn->stop));
                if(!conn->stop_gone){
                    printf("stop time\n");
                    clock_gettime(CLOCK_MONOTONIC, &(conn->stop));
                    conn->stop_gone = true;
                }
                message_payload tmp_payload;
                tmp_payload.num_packets=0;
                conn->send_msg->send_message(MSG_END, tmp_payload, conn);
            }

        }else if(conn->recv_msg->get_type() == MSG_NUM_PACK){
            conn->total_packets = conn->recv_msg->get_payload().num_packets;
            printf("received num_packets from client sending %d packets\n", conn->total_packets);
            post_receives(conn);
            conn->send_msg->send_mr(conn);
        }else{
            //printf("received message: %s\n", conn->rdma_remote_buff);
            post_receives(conn);
        }

    }else if (wc->opcode == IBV_WC_SEND) {
        printf("message send completed successfully.\n");
        if(conn->send_msg->get_type() == MSG_NUM_PACK){
            printf("num pack sent sending mr\n");
        }
    }else if (wc->opcode == IBV_WC_RDMA_WRITE) {
        if(verbose >= 1){
            printf("rdma write completed successfully.\n");
        }

        (conn->packet_left)--;

        if((conn->packet_left <= 0) && (!conn->stop_gone)){
            printf("stop time\n");
            clock_gettime(CLOCK_MONOTONIC, &(conn->stop));
            conn->stop_gone = true;
        }


        if(((conn->my_type == SERVER && conn->remote_done) || (conn->my_type == CLIENT && conn->local_done)) && (conn->packet_left <= 0)){
            if(verbose >= 1){
                printf("rdma_done_rcv\n");
            }

            if(conn->my_type == SERVER){
                printf("server\n");
            }else{
                printf("client\n");
            }

            printf("sending end message\n");
            message_payload tmp_payload;
            tmp_payload.num_packets=0;
            usleep(1);
            conn->send_msg->send_message(MSG_END, tmp_payload, conn);
        }

        if((conn->packet_left) - 10 >= 0){
            rdma_write(conn);
            //(conn->packet_left)--;
        }else{
            conn->local_done = true;
//            if(!conn->stop_gone){
//                printf("stop time b\n");
//                clock_gettime(CLOCK_MONOTONIC, &(conn->stop));
//                conn->stop_gone = true;
//            }
        }

        if(verbose >= 1){
            printf("packet_left %d\n", conn->packet_left);
        }
    }
}

void * poll_cq(void *ctx) {
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    struct context *contx;
    extern pthread_mutex_t poll_cq_mtx;
    static int events_to_ack = 0;
    static int max_events_to_ack = 20;
    //pthread_mutex_t tmp;

    contx = ((struct context *)ctx);

    while (1) {
        check_nz(ibv_get_cq_event(contx->comp_channel, &cq, &ctx_ret), "poll_cq ibv_get_cq_event", true);
        //tmp=poll_cq_mtx;
        //pthread_mutex_lock(&poll_cq_mtx);
        while (ibv_poll_cq(cq, 1, &wc)){
            on_completion(&wc);
        }
        //events_to_ack++;
        //if(events_to_ack == max_events_to_ack){
            ibv_ack_cq_events(cq, 1);
            //events_to_ack = 0;
        //}
        check_nz(ibv_req_notify_cq(cq, 0), "poll_cq ibv_req_notify_cq", true);
        //pthread_mutex_unlock(&poll_cq_mtx);

        //poll_cq_mtx = tmp;

    }

    return NULL;
}

void register_memory(struct connection *conn, struct context *ctx) {
    extern int verbose;
    //conn->send_msg = (message *)malloc(sizeof(message));
    conn->send_msg = new Message();
    check_alloc(conn->send_msg, "register_memory conn->send_msg malloc", true);
    conn->recv_msg = new Message();
    check_alloc(conn->recv_msg, "register_memory conn->recv_msg malloc", true);

    conn->send_mr = ibv_reg_mr( ctx->pd, conn->send_msg, sizeof(Message), IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(conn->send_mr, "register_memory conn->send_mr ibv_reg_mr", true);

    conn->recv_mr = ibv_reg_mr( ctx->pd, conn->recv_msg, sizeof(Message), IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(conn->recv_mr, "register_memory conn->recv_mr ibv_reg_mr", true);

    conn->rdma_local_buff = malloc(BUFFER_SIZE);
    check_alloc(conn->rdma_local_buff, "register_memory conn->rdma_local_buff malloc", true);
    init_buffer_content(conn->rdma_local_buff);

    conn->rdma_remote_buff = malloc(BUFFER_SIZE);
    check_alloc(conn->rdma_remote_buff, "register_memory conn->rdma_remote_buff malloc", true);

    conn->rdma_local_mr = ibv_reg_mr( ctx->pd, conn->rdma_local_buff, BUFFER_SIZE,
                                      IBV_ACCESS_LOCAL_WRITE);
    check_alloc(conn->rdma_local_mr, "register_memory conn->rdma_local_mr ibv_reg_mr", true);

    if(verbose >= 1){
        printf("allocated local ");
        printf_mr(conn->rdma_local_mr);
    }

    conn->rdma_remote_mr = ibv_reg_mr( ctx->pd, conn->rdma_remote_buff, BUFFER_SIZE,
                                       IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(conn->rdma_remote_mr, "register_memory conn->rdma_remote_mr ibv_reg_mr", true);

    if(verbose >= 1){
        printf("allocated remote ");
        printf_mr(conn->rdma_remote_mr);
    }

}

void printf_mr(const struct ibv_mr *mr){
    printf("memory region\n" "context: %p\n" "pd: %p\n" "addr: %p\n" "length: %lu\n" "handle: %u\n" "lkey: %u\n" "rkey: %u\n",
           mr->context, mr->pd, mr->addr, mr->length, mr->handle, mr->lkey, mr->rkey);
}

int on_connection(void *context){
    struct connection *conn = (struct connection *)context;

    if(conn->my_type == CLIENT){
        printf("sending number of packets to the server\n");
        message_payload tmp_payload;
        tmp_payload.num_packets = conn->total_packets;
        conn->send_msg->send_message(MSG_NUM_PACK, tmp_payload, conn);
    }else{
        //printf("connected sending memory region\n");
        //conn->send_msg->send_mr(conn);
    }

    return 0;
}

int on_addr_resolved(struct rdma_cm_id *id, struct context *ctx, std::map <unsigned int, int> &mapIPID) {
    struct ibv_qp_init_attr qp_attr;
    struct connection *conn;
    std::map<unsigned int,int>::iterator it;
    int connection_id = 0;

    conn = new connection;
    check_alloc(conn, "on_addr_resolved malloc conn", true);

    it = mapIPID.find(((struct sockaddr_in *) &id->route.addr.dst_addr)->sin_addr.s_addr);
    if(it == mapIPID.end()){
        fprintf(stderr, "Connection from undefined device\n");
        return(-1);
    }else{
        connection_id = it->second;
    }

    ctx->connections[connection_id] = conn;
    printf("address resolved.\n");

    build_context(id->verbs, ctx, connection_id);
    build_qp_attr(&qp_attr, ctx->connections[connection_id]);

    check_nz(rdma_create_qp(id, ctx->pd, &qp_attr), "on_addr_resolved rdma_create_qp", true);

    id->context = conn;

    conn->connection_number = connection_id;

    conn->local_done = false;
    conn->remote_done = false;
    conn->stop_gone = false;

    conn->id = id;
    conn->qp = id->qp;

    register_memory(conn, ctx);
    post_receives(conn);

    check_nz(rdma_resolve_route(id, TIMEOUT_IN_MS), "on_addr_resolved rdma_resolve_route", true);

    return 0;
}

int on_route_resolved(struct rdma_cm_id *id){
    struct rdma_conn_param cm_params;

    printf("route resolved.\n");
    memset(&cm_params, 0, sizeof(cm_params));
    check_nz(rdma_connect(id, &cm_params), "on_route_resolved rdma_connect", true);

    return 0;
}

void send_msg(struct connection *conn){
    struct ibv_send_wr wr, *bad_wr = NULL;
    struct ibv_sge sge;

    memset(&wr, 0, sizeof(wr));

    printf("sending message\n");

    wr.wr_id = (uintptr_t)conn;
    wr.opcode = IBV_WR_SEND;
    wr.sg_list = &sge;
    wr.num_sge = 1;
    wr.send_flags = IBV_SEND_SIGNALED;

    sge.addr = (uintptr_t)conn->send_msg;
    sge.length = sizeof(Message);
    sge.lkey = conn->send_mr->lkey;

    check_nz(ibv_post_send(conn->qp, &wr, &bad_wr), "on_connection ibv_post_send", true);
}

void rdma_write(struct connection *conn){
    extern int verbose;
    struct ibv_send_wr wr, *bad_wr = NULL;
    struct ibv_sge sge;


    if(verbose >= 1){
        printf("rdma write in progress\n");
    }
    //init_buffer_content(conn->rdma_local_buff);
    //sprintf(conn->rdma_local_buff, "hello world from %d\n", getpid());

    memset(&wr, 0, sizeof(wr));

    wr.wr_id = (uintptr_t)conn;
    wr.opcode = IBV_WR_RDMA_WRITE;
    wr.sg_list = &sge;
    wr.num_sge = 1;
    wr.send_flags = IBV_SEND_SIGNALED;
    wr.wr.rdma.remote_addr = (uintptr_t)conn->peer_mr.addr;
    wr.wr.rdma.rkey = conn->peer_mr.rkey;

    sge.addr = (uintptr_t)conn->rdma_local_buff + BUFFER_SIZE/2;
    sge.length = BUFFER_SIZE;
    sge.lkey = conn->rdma_local_mr->lkey;

    check_nz(ibv_post_send(conn->qp, &wr, &bad_wr), "on_connection ibv_post_send", true);
}

//TODO
//void destroy_context(context *ctx){
//    for(std::map<int, struct connection *>::iterator it = ctx->connections.begin() ;
//        it != ctx->connections.end() ; it++){

//        connection *conn = it->second;
//        ibv_destroy_cq(conn->cq);

//        ibv_dereg_mr(conn->send_mr);
//        ibv_dereg_mr(conn->recv_mr);


//        ibv_dereg_mr(conn->rdma_local_mr);
//        ibv_dereg_mr(conn->rdma_remote_mr);

//        free(conn->send_msg);
//        free(conn->recv_msg);

//        free(conn->rdma_local_buff);
//        free(conn->rdma_remote_buff);

//        free(conn);
//    }


//}

int on_disconnect(struct rdma_cm_id *id, context *ctx){
    struct connection *conn = (struct connection *)id->context;
    double time_interval;
    double total_time_interval;
    double bandwidth;
    double total_bandwidth;

    printf("peer disconnected.\n");

    time_interval = (conn->stop.tv_sec + (conn->stop.tv_nsec * 1e-9)) -
            (conn->start.tv_sec + (conn->start.tv_nsec * 1e-9));

    total_time_interval = (conn->total_stop.tv_sec + (conn->total_stop.tv_nsec * 1e-9)) -
            (conn->total_start.tv_sec + (conn->total_start.tv_nsec * 1e-9));
    printf("time interval %lf s number of packets %d\n", time_interval, conn->total_packets);
    bandwidth = conn->total_packets*(BUFFER_SIZE*8./1024/1024/1024)/time_interval;
    total_bandwidth = conn->total_packets*(BUFFER_SIZE*8./1024/1024/1024)/total_time_interval;
    //TODO do this on a proper file
    printf("average bandwidth %lfGb/s\n", bandwidth);
    printf("average bandwidth including overhead %lfGb/s\n", total_bandwidth);
    fprintf(stderr, "%lf\n", bandwidth);

    rdma_destroy_qp(id);

    ibv_destroy_cq(conn->cq);

    ibv_dereg_mr(conn->send_mr);
    ibv_dereg_mr(conn->recv_mr);


    ibv_dereg_mr(conn->rdma_local_mr);
    ibv_dereg_mr(conn->rdma_remote_mr);

    free(conn->send_msg);
    free(conn->recv_msg);

    free(conn->rdma_local_buff);
    free(conn->rdma_remote_buff);

    //free(conn);

    rdma_destroy_id(id);

    return 0;
}

void post_receives(struct connection *conn) {
    struct ibv_recv_wr wr, *bad_wr = NULL;
    struct ibv_sge sge;

    wr.wr_id = (uintptr_t)conn;
    wr.next = NULL;
    wr.sg_list = &sge;
    wr.num_sge = 1;

    sge.addr = (uintptr_t)conn->recv_msg;
    sge.length = sizeof(Message);
    sge.lkey = conn->recv_mr->lkey;

    check_nz(ibv_post_recv(conn->qp, &wr, &bad_wr), "post_receives ibv_post_recv", true);
}
