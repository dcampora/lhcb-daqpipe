//#include "simple_verbs_lib.h"
#include "../transport/verbs/Verbs_lib.h"
#include "../transport/verbs/rdmatransfer.h"
#include "../transport/verbs/message.h"

#include "../common/Logging.h"
#include "../common/Logger.h"
#include "../transport/config/ConfigurationCore.h"
#include "../transport/verbs/VerbsCore.h"

#include <pthread.h>
#include <map>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 1024

#define USAGE (fprintf(stderr, "Usage %s " "[-c <max number of clients] [-p <server port>] [-v]\n" ,argv[0]))
namespace Core { Core::ConfigurationLoader* Configuration = new Verbs::ConfigurationLoader(); }
char *send_data_buffer;
int verbose;
pthread_mutex_t poll_cq_mtx;

void *poller(void *ctx);

int main(int argc, char **argv){
    verbose = 0;
    struct sockaddr_in addr;
    struct rdma_cm_event *event = NULL;
    struct rdma_cm_id *listener = NULL;
    struct rdma_event_channel *ec = NULL;
    struct rdma_cm_event *event_copy;
    struct context *ctx = NULL;
    uint16_t port = 0;
    int num_packet = 0;
    int max_connections = 1;
    std::map <unsigned int, int> mapIPID;
    char *msg_buffer;
    char *recv_data_buffer;

    mapIPID[inet_addr("192.168.1.4")] = 12;
    //mapIPID[inet_addr("192.168.1.3")] = 13;

    //ibv_fork_init();
    Core::Configuration->logLevel = 0;


    /* get options */
    int c;
    while ((c = getopt(argc, argv, "p:vc:")) != -1){
        switch (c){
        case 'p': port = atoi(optarg); break;
        case 'v': Core::Configuration->logLevel++; break;
        case 'c': max_connections = atoi(optarg); break;
        case '?': USAGE;
            return -1;
            break;
        }
    }
    verbose = Core::Configuration->logLevel;
    /* end get option*/

    /* init addr values */

    ctx = build_context_server(max_connections, port, mapIPID);
    sleep(1);

    pthread_t poller_thread;
    check_nz(pthread_create(&(poller_thread), NULL, poller, ctx), "build_context pthread_create", true);

    send_data_buffer = (char*)malloc(BUFFER_SIZE);
    memset(send_data_buffer, 'A', BUFFER_SIZE/2);
    memset(send_data_buffer+BUFFER_SIZE/2, 'B', BUFFER_SIZE/2);

    recv_data_buffer = (char*)malloc(mapIPID.size());

    msg_buffer = (char*) malloc(sizeof(struct ibv_mr*)*mapIPID.size());
    check_alloc(msg_buffer, "msg_buffer malloc", true);
    int i = 0;
    for(std::map<int, struct connection*>::iterator it=ctx->connections_id.begin() ; it != ctx->connections_id.end(); it++){
        Message *tmp_msg = it->second->recv_msg;
        if(tmp_msg->setPayload(send_data_buffer+i)){
            tmp_msg->setSize(1);
            tmp_msg->receive_message(it->second);
           DEBUG << "post recv" << std::endl;
        }
        tmp_msg = it->second->send_msg;
        *(((struct ibv_mr**)msg_buffer) + i) = ibv_reg_mr(it->second->pd, send_data_buffer, BUFFER_SIZE, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
        DEBUG << "mr addr" << (*(((struct ibv_mr**)msg_buffer) + i))->addr << std::endl;

        check_alloc(msg_buffer + i*sizeof(struct ibv_mr*), "reg_mr ", true);
        if(tmp_msg->setPayload(*(((struct ibv_mr**)msg_buffer) + i))){
            struct ibv_mr *tmp_mr;
            tmp_mr = (struct ibv_mr*)tmp_msg->payload();
            tmp_mr->length = 12;

            DEBUG << "server addr" << ((struct ibv_mr*) tmp_msg->payload())->addr << std::endl;
            tmp_msg->setType(MSG_MR);
            tmp_msg->setSize(sizeof(struct ibv_mr));
            tmp_msg->send_message(it->second);
            DEBUG << "sending MR" << std::endl;
        }
        i++;
    }


    while(true){
        sleep(1);
    }
    printf("done\n");

    return(EXIT_SUCCESS);

}

void *poller(void *ctx){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    struct context *contx;
    connection *conn;
    Message *msg;
    RDMATransfer *rdma_msg;
    static int events_to_ack = 0;
    static int max_events_to_ack = 20;
    //pthread_mutex_t tmp;
    struct ibv_mr *tmp_peer_mr;
    RDMATransfer rmda_send;

    char *buffer = (char *)malloc(BUFFER_SIZE);

    check_alloc(buffer, "buffer malloc", true);

    contx = ((struct context *)ctx);

    if(rmda_send.setPayload(buffer)){
        rmda_send.setSize(BUFFER_SIZE);
    }

    while (1) {
        check_nz(ibv_get_cq_event(contx->comp_channel, &cq, &ctx_ret), "poll_cq ibv_get_cq_event", true);
        //tmp=poll_cq_mtx;
        //pthread_mutex_lock(&poll_cq_mtx);
        while (ibv_poll_cq(cq, 1, &wc)){
            conn = contx->connections_qp_num[wc.qp_num];
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "client on_completion status is " << ibv_wc_status_str(wc.status) << std::endl;
            }else if(wc.opcode & IBV_WC_RECV){
                msg = (Message *) wc.wr_id;
                DEBUG << "VerbsBUPolCqRU::run MSG received from " << msg->sender_id() << std::endl;
                if(msg->type() == MSG_END){
                    DEBUG << "server END  received" << std::endl;
                    INFO << "Message content" << std::endl;
                    for( int i = 0 ; i <12 ; i++){
                        std::cout << (int) (send_data_buffer[i]) << " " ;
                    }
                    INFO << std::endl;

                    usleep(10000);
                    conn->send_msg->send_message(conn);
                }else{
                    ERROR << "client unexpected message received" <<std::endl;
                }
                msg->release_mr();
                msg->release_payload();
                msg->receive_message(conn);
            }else if(wc.opcode == IBV_WC_SEND){
                    msg = (Message *) wc.wr_id;
                    DEBUG << "message sent" << std::endl;
                    msg->release_mr();
                    msg->release_payload();
            }else{
                WARNING << "Unexpected event dropping" << std::endl;
            }
        }
        //events_to_ack++;
        //if(events_to_ack == max_events_to_ack){
        ibv_ack_cq_events(cq, 1);
        //events_to_ack = 0;
        //}
        check_nz(ibv_req_notify_cq(cq, 0), "poll_cq ibv_req_notify_cq", true);
        //pthread_mutex_unlock(&poll_cq_mtx);

        //poll_cq_mtx = tmp;

    }

}
