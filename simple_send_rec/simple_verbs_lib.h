#ifndef SIMPLE_VERBS_LIB_H
#define SIMPLE_VERBS_LIB_H

#include <time.h>
#include <stdint.h>
#include <netdb.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <infiniband/verbs.h>
#include <unistd.h>
#include <rdma/rdma_cma.h>
#include <pthread.h>
#include <iostream>
#include <map>

//const int BUFFER_SIZE = (102400000UL*sizeof(int));
const unsigned long long int BUFFER_SIZE = (1073741824/16);
//const int BUFFER_SIZE = (1024);
const int TIMEOUT_IN_MS = 500; /* ms */

extern int verbose;
//extern pthread_mutex_t poll_cq_mtx;

enum message_type{
    MSG_RDMA,
    MSG_MR,
    MSG_NUM_PACK,
    MSG_END
};

enum node_type{
    CLIENT,
    SERVER
};

union message_payload{
    struct ibv_mr mr;
    int num_packets;
};


class Message {
private:
    enum message_type _type;
    message_payload _payload;

public:
    Message();
    Message(message_type type, message_payload payload);
    ~Message();
    message_type& get_type();
    message_payload& get_payload();
    void set_type(message_type type);
    void set_payload(message_payload payload);
    void send_message(message_type type, message_payload payload, struct connection *conn);
    void send_message(struct connection *conn);
    void send_mr(struct connection *conn);
};

struct context {
    struct ibv_context *ctx;
    struct ibv_pd *pd;
    struct ibv_cq *cq;
    struct ibv_srq *srq;
    struct ibv_comp_channel *comp_channel;

    //struct connection *connections;
    std::map <int, struct connection *> connections;
    int number_of_connections;

    pthread_t cq_poller_thread;
};

struct connection {
    int connection_number;
    struct rdma_cm_id *id;
    struct ibv_qp *qp;
    struct ibv_cq *cq;
    struct ibv_pd *pd;

    bool local_done;
    bool remote_done;

    // not the best but ok
    node_type my_type;

    struct ibv_mr *recv_mr;
    struct ibv_mr *send_mr;
    struct ibv_mr *rdma_local_mr;
    struct ibv_mr *rdma_remote_mr;

    struct ibv_mr peer_mr;

    Message *recv_msg;
    Message *send_msg;

    struct sockaddr *client_addr;

    int total_packets;

    // for benchmarking
    struct timespec start;
    struct timespec stop;
    bool stop_gone;
    struct timespec total_start;
    struct timespec total_stop;

    int packet_left;

    void *rdma_local_buff;
    void *rdma_remote_buff;
};


/* check memory allocation errors
 * if array is NULL call perror(message)
 * if fatal is true exit(EXIT_FAILURE)
 */

void init_buffer_content(void *buffer);

void printf_buffer_content(void *buffer);

void check_alloc(const void* array, const char *Message, bool fatal);

void check_nz(int value, const char *Message, bool fatal);

void build_context(struct ibv_context *verbs, struct context *ctx, int connection_number) ;

void build_qp_attr(struct ibv_qp_init_attr *qp_attr, struct connection *conn) ;

void on_completion(struct ibv_wc *wc);

void * poll_cq(void *ctx);

int on_connect_request(struct rdma_cm_id *id, context *ctx, std::map<unsigned int, int> &mapIPID);

void register_memory(struct connection *conn, struct context *ctx) ;

void printf_mr(const struct ibv_mr *mr);

int on_connection(void *context);

int on_addr_resolved(struct rdma_cm_id *id, context *ctx, std::map<unsigned int, int> &mapIPID);

int on_route_resolved(struct rdma_cm_id *id);

void rdma_write(struct connection *conn);

int on_disconnect(struct rdma_cm_id *id);

void post_receives(struct connection *conn) ;

#endif
