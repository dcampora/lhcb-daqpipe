//#include "simple_verbs_lib.h"
#include "../transport/verbs/Verbs_lib.h"
#include "../transport/verbs/rdmatransfer.h"
#include "../transport/verbs/message.h"

#include "../common/Logging.h"
#include "../common/Logger.h"
#include "../transport/verbs/VerbsCore.h"

#include <pthread.h>
#include <map>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#define USAGE (fprintf(stderr, "Usage %s " "-a <server addr> -p <server port> -n <number of packet> [-v]\n" ,argv[0]))

#define BUFFER_SIZE 1024
namespace Core { Core::ConfigurationLoader* Configuration = new Verbs::ConfigurationLoader(); }
int verbose;

void *poller(void *ctx);

int main(int argc, char **argv){
    verbose = 0;
    char *ip_addr = NULL;
    uint16_t port = 0;
    int num_packet = 0;
    std::map <unsigned int, int> mapIPID;
    struct context *ctx = NULL;
    char *msg_buffer;

    //ibv_fork_init();
    Core::Configuration->logLevel = 0;
    /* get options */
    int c;
    while ((c = getopt(argc, argv, "p:a:vn:")) != -1){
        switch (c){
        case 'p': port = atoi(optarg); break;
        case 'a': ip_addr = optarg; break;
        case 'v': Core::Configuration->logLevel++; break;
        case 'n': num_packet = atoi(optarg); break;
        case '?': USAGE;
            return -1;
            break;
        }
    }

    if(ip_addr == NULL || port == 0 || num_packet == 0) {
        USAGE;
        exit(EXIT_FAILURE);
    }
    /* end get option*/

    verbose = Core::Configuration->logLevel;

    printf("server addr:port %s:%u\n", ip_addr, port);
    /* init addr values */

    mapIPID[inet_addr(ip_addr)] = 12;
    //mapIPID[inet_addr("192.168.1.19")] = 13;
    std::map <int, uint16_t> map_IDPort;

    map_IDPort[12] = port;

    ctx = build_context_client(map_IDPort, mapIPID);

    pthread_t poller_thread;
    check_nz(pthread_create(&poller_thread, NULL, poller, ctx), "build_context pthread_create", true);
    msg_buffer = (char*) malloc(sizeof(struct ibv_mr)*mapIPID.size());
    check_alloc(msg_buffer, "msg_buffer malloc", true);
    int i = 0;
    for(std::map<int, struct connection*>::iterator it=ctx->connections_id.begin() ; it != ctx->connections_id.end(); it++){
        Message *tmp_msg = it->second->recv_msg;
       if(tmp_msg->setPayload(msg_buffer+i++*sizeof(struct ibv_mr))){
           tmp_msg->setSize(sizeof(struct ibv_mr));
           tmp_msg->receive_message(it->second);
       }
    }





    while(true){
        sleep(1);
    }
    return(EXIT_SUCCESS);

}

void *poller(void *ctx){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    struct context *contx;
    connection *conn;
    Message *msg;
    RDMATransfer *rdma_msg;
    static int events_to_ack = 0;
    static int max_events_to_ack = 20;
    //pthread_mutex_t tmp;
    struct ibv_mr *tmp_peer_mr;
    RDMATransfer rmda_send;

    char *buffer = (char *)malloc(BUFFER_SIZE);

    for(int i = 0; i < BUFFER_SIZE/2 ; i++){
        buffer[i] = 'a';
        buffer[i+BUFFER_SIZE/2] = 'z';
    }
    check_alloc(buffer, "buffer malloc", true);

    contx = ((struct context *)ctx);

    //if(rmda_send.setPayload(buffer)){
        //rmda_send.setSize(BUFFER_SIZE);
    //}

    ibv_mr *tmp_mr;
    for(std::map<int, struct connection*>::iterator it=contx->connections_id.begin() ; it != contx->connections_id.end(); it++){
        //rmda_send.set_mr(buffer, BUFFER_SIZE, it->second);
        tmp_mr = ibv_reg_mr(contx->pd, buffer, BUFFER_SIZE/2, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
        rmda_send.add_static_mr(tmp_mr);
        tmp_mr = ibv_reg_mr(contx->pd, buffer+BUFFER_SIZE/2, BUFFER_SIZE/2,
                            IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
        rmda_send.add_static_mr(tmp_mr);
    }


    int k = 0;
    while (1) {
        check_nz(ibv_get_cq_event(contx->comp_channel, &cq, &ctx_ret), "poll_cq ibv_get_cq_event", true);
        //tmp=poll_cq_mtx;
        //pthread_mutex_lock(&poll_cq_mtx);
        while (ibv_poll_cq(cq, 1, &wc)){
            conn = contx->connections_qp_num[wc.qp_num];
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "client on_completion status is " << ibv_wc_status_str(wc.status) << std::endl;
            }else if(wc.opcode & IBV_WC_RECV){
                msg = (Message *) wc.wr_id;
                DEBUG << "VerbsBUPolCqRU::run MSG received from " << msg->sender_id() << std::endl;
                if(msg->type() == MSG_MR){
                    DEBUG << "client MR received" << std::endl;
                    if(k == 50){
                        int ret =  rmda_send.remove_static_mr(tmp_mr);
                        if (ret == 0){
                            INFO << "static_mr correctly removed" << std::endl;
                        }else if(ret == -1){
                            INFO << "static_mr busy" << std::endl;
                        }else{
                            INFO << "static_mr not found" << std::endl;
                        }

                        getchar();
                    }
                    rmda_send.setSize(12);
                    rmda_send.setPayload(buffer + (k++ % (BUFFER_SIZE - 12)));
                    tmp_peer_mr = (struct ibv_mr*) msg->payload();
                    rmda_send.set_peer_mr(*tmp_peer_mr);
                    rmda_send.rdma_write(conn);
                }else{
                    ERROR << "client unexpected message received" <<std::endl;
                }
                msg->release_mr();
                msg->release_payload();
                msg->receive_message(conn);
            }else if(wc.opcode == IBV_WC_SEND){
                    msg = (Message *) wc.wr_id;
                    DEBUG << "message sent" << std::endl;
                    msg->release_mr();
                    msg->release_payload();
            }else if(wc.opcode == IBV_WC_RDMA_WRITE){
                rdma_msg = (RDMATransfer *)wc.wr_id;
                rdma_msg->release_mr();
                rdma_msg->release_payload();
                DEBUG << "VerbsRUPollCqBU::run RDMA write over" << std::endl;
                conn->send_msg->setType(MSG_END);
                conn->send_msg->send_message(conn);
            }else{
                WARNING << "Unexpected event dropping" << std::endl;
            }
        }
        //events_to_ack++;
        //if(events_to_ack == max_events_to_ack){
        ibv_ack_cq_events(cq, 1);
        //events_to_ack = 0;
        //}
        check_nz(ibv_req_notify_cq(cq, 0), "poll_cq ibv_req_notify_cq", true);
        //pthread_mutex_unlock(&poll_cq_mtx);

        //poll_cq_mtx = tmp;

    }

    return NULL;
}
