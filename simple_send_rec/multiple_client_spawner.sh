#!/bin/bash
#

declare -a clients=('lab09' 'lab10')
server="lab09"
server_ip=$3

client_exe="/home/fpisani/soft_git/lhcb-daqpipe/simple_send_rec/simple_server"
server_opt="-p 123"
client_exe="/home/fpisani/soft_git/lhcb-daqpipe/simple_send_rec/simple_client"
client_opt="-a $server_ip -n $2 -p 1234"
server_for="for i in `seq 1 $1`; do $server_exe $server_opt\$i & ; done"

trap 'killall -9 `basename "$client_exe"`; exit' SIGHUP SIGINT SIGTERM
# spawning clients 
for i in `seq 0 $1`; do
	echo $i
#pippo=$( { $client_exe $client_opt$i > /dev/null  ; & } 2>&1 )
	ssh ${clients[(($i % 2))]} numactl --cpunodebind 1 --membind 1 $client_exe $client_opt > /dev/null & 
done


cat
