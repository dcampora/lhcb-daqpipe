
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

int a;

// class FileStdLogger : public std::ostream {
// public:
    // typedef FileStdLogger& (*StreamManipulator)(FileStdLogger&);

    // FileStdLogger& operator<<(StreamManipulator manip){
    //     std::cout << manip;
    //     std::ofstream::<<(manip);
    //     return this;
    // }

    // template<typename T>
    // FileStdLogger& operator<<(FileStdLogger& stream, const T& t){
    //     std::cout << t;
    //     std::ofstream::<<(stream, t);
    //     return this;
    // }
    
    // template<typename T>
    // FileStdLogger& operator<<(const T& t){
    //     std::cout << t;
    //     return *this;
    // }


    // std::basic_ostream& operator<< (std::ios_base& (*pf)(std::ios_base&))
    //     { return foo(); }
// };


// FileStdLogger l;

// std::ostream& operator<<(std::ostream& stream, const std::string& s){
//     std::stringbuf buffer;
//     buffer.sputn(s.c_str(), s.length());

//     // std::cout.operator<<(buffer);
//     // l.operator<<(buffer);

//     return stream;
// }

// template<typename T>
// FileStdLogger& operator<<(FileStdLogger& stream, const T& t){
//     b++;
//     return stream;
// }



// FileStdLogger& operator<<(FileStdLogger& stream, const std::string& t){
//     a++;

//     for (int i=0; i<t.length; ++i){
//         std::cout.operator<<(t[i]);
//     }
//     return stream;
// }


// No dynamic dispatch with references
// class A {
// public:
//     void a(){
//         std::cout << "a" << std::endl;
//     }
// };
// class B : public A {
// public:
//     void a(){
//         std::cout << "b" << std::endl;
//     }
// };
// A& foo(A& a){
//     return a;
// }

//     B b;
//     (foo(b)).a();    

class FileStdLogger : public std::ostream {};
FileStdLogger l;
std::ofstream f;

class MessageLogger : public std::streambuf {
public:
    std::string _buf;
    std::streambuf* _old;

    MessageLogger() : _buf("") {
        _old = l.rdbuf(this);
    }

    ~MessageLogger(){
        l.rdbuf(_old);
    }

    int overflow(int c){
        if (c == '\n'){
            std::cout << _buf << std::endl;
            _buf = "";
            // f << _buf << std::endl;
        }
        else
            _buf += c;

        return c;
    }
};



std::ostream& some_function(){
    return l;
}

std::ostream& some_other_function(){
    return std::cout;
}

int main(int argc, char const *argv[])
{
    a = 0;

    // some_function() << std::string("hello") << " again";
    // l << std::string("hey") << " how are you";
    // some_other_function() << "hello";

    // std::cout << std::endl << a << std::endl;
    
    MessageLogger m;
    f.open("log");

    some_function() << "something" << std::endl << "a" << std::endl;

    return 0;
}