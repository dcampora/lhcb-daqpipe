
/**
 *      Shared memory communicator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      February, 2014
 *      CERN
 */

#include <cstring>
#include "../../common/Logging.h"
#include "../../common/GenStructures.h"
#include "../../common/Shmem_SharedQueue.h"
#include "../../common/pthreadHelper.h"

class ShmemComm {
private:
    NamedPipe<Transaction> * qtransactions;
    LockShmem_SharedQueue<int> * qfinishedTransactions;

    // Shmem
    std::map<int, void*> pmems;
    std::map<int, int> descriptors;

public:
    ShmemComm();
    void run();
};

class Copycat {
private:
    char* p1, *p2;
    unsigned int size;
    SharedQueue* qfinishedTransactions;

public:
    Copycat(char* p1, char* p2, unsigned int size, int id, SharedQueue* qfinished) : 
        p1(p1), p2(p2), size(size), qfinishedTransactions(qfinished) {}
    void run();
};
