
/**
 *      Configuration Core impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#include "ConfigurationCore.h"

// TODO: Load configuration from configuration file.
Core::ConfigurationLoader::ConfigurationLoader() {
    logLevel = 3;
    mapLogLevels[5] = "DEBUG";
    mapLogLevels[4] = "INFO";
    mapLogLevels[3] = "MON";
    mapLogLevels[2] = "WARNING";
    mapLogLevels[1] = "ERROR";
    mapLogLevels[0] = "SILENT";

    initialCredits = 3;
    triggerFrequency = 1;

    numnodes = 2;
    name = "machinename";
    
    trigger_sqName = "/trigger";
    maxRUFragments = MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS;
    
    BUMetaBufferName = "/bu-meta";
    BUDataBufferName = "/bu-data";
    BUDataBufferSize = 1024 * 1024 * 1024; // 1 G
    BUMetaBufferSize = 256 * 1024 * 1024; // 256 M

    // RU-related stuff
    // Note: dataBufferSize and dataFragmentSize must be aligned (multiple)
    RUDataBufferSize = BUDataBufferSize; // 1 GB for data1 and data2
    RUMetaBufferSize = BUMetaBufferSize; // 128 MB for meta1 and meta2

    dataFragmentSize = 1024;
    eventFragmentSize = 1024; // 1 KB for event fragment size

    RUgpuGeneratedQName1 = "/gpuru-gpugenerated-meta1";
    RUgpuGeneratedQName2 = "/gpuru-gpugenerated-meta2";
    RUProcessedQName = "/gpuru-ruprocessed";
    RUMeta1Name = "/meta1";
    RUMeta2Name = "/meta2";
    RUData1Name = "/data1";
    RUData2Name = "/data2";
    RUQueueElements = 10000;

    shmemCommQTransactionName = "/shmem-transaction";
    shmemCommQFinishedName = "/shmem-finished";
    shmemQueueSize = 10000;

    // IDs for each buffer
    buffer_IDName.push_back(RUMeta1Name); buffer_IDSize.push_back(RUMetaBufferSize);
    buffer_IDName.push_back(RUMeta2Name); buffer_IDSize.push_back(RUMetaBufferSize);
    buffer_IDName.push_back(RUData1Name); buffer_IDSize.push_back(RUDataBufferSize);
    buffer_IDName.push_back(RUData2Name); buffer_IDSize.push_back(RUDataBufferSize);
    buffer_IDName.push_back(BUMetaBufferName); buffer_IDSize.push_back(BUMetaBufferSize);
    buffer_IDName.push_back(BUDataBufferName); buffer_IDSize.push_back(BUDataBufferSize);

    // I want this to be 10000, however this hits me,
    // http://www.open-mpi.org/community/lists/users/2010/08/14137.php
    // It's really annoying.
    EMSharedQueueSize = 10000;

    // Overriden by main.cpp
    eventSize = 1024;
}
