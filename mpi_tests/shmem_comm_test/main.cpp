

// main.cpp

#define EXTERN_CONFIGURATION_CORE 1
#include "ConfigurationCore.h"

namespace Core { Core::ConfigurationLoader Configuration; }

#pragma pack(push)
struct bit {
    bool b : 1;
};
#pragma pop

#pragma pack(push)
struct bits {
    bit b[32];
};
#pragma pop

#include "MemcpyComm.h"

int main(int argc, char const *argv[])
{
    std::cout << "size of bit " << sizeof(bit) << ", sizeof bits " << sizeof(bits) << std::endl;

    // Start shmemcomm
    MemcpyComm mc;

    // std::cout << "Starting ShmemComm..." << std::endl;
    // startThreadWithReference<MemcpyComm>(mc);

    // std::cout << "Transaction business" << std::endl;
    // Shmem_SharedQueue<ShmemTransaction>* qtransactions = new Shmem_SharedQueue<ShmemTransaction>(Core::Configuration.shmemCommQTransactionName, Core::Configuration.shmemQueueSize);
    // Shmem_SharedQueue<int>* qfinishedTransactions = new Shmem_SharedQueue<int>(Core::Configuration.shmemCommQFinishedName, Core::Configuration.shmemQueueSize);

    // // Send some transactions
    // ShmemTransaction t;
    // t.bufferOrigID = 0;
    // t.bufferDestID = 1;
    // t.elem1 = 0;
    // t.elem2 = 0;
    // t.size = 50 * 1024 * 1024;
    
    std::cout << "Transaction business" << std::endl;
    Shmem_SharedQueue<Transaction>* qtransactions = new Shmem_SharedQueue<Transaction>(Core::Configuration.shmemCommQTransactionName, 1);
    LockShmem_SharedQueue<int>* qfinishedTransactions = new LockShmem_SharedQueue<int>(Core::Configuration.shmemCommQFinishedName, Core::Configuration.shmemQueueSize);    

    char* p1 = (char*) malloc(10000 * sizeof(char));
    char* p2 = (char*) malloc(10000 * sizeof(char));

    Transaction t;
    t.bufferOrig = p1;
    t.bufferDest = p2;
    t.size = 10000;

    int r;
    std::cout << "writing..." << std::endl;
    r = qtransactions->write( t );
    std::cout << (int) r << std::endl;
    r = qtransactions->write( t );
    std::cout << (int) r << std::endl;

    int ntransactions = 1000;
    for (int i=0; i<ntransactions; ++i){
        t.id = i;
        qtransactions->write( t );
    }

    for (int i=0; i<ntransactions; ++i){
        int tfin = 0;
        while(qfinishedTransactions->read(tfin) == -1) usleep(100);
        std::cout << "main: Finished transaction " << i << " (id " << tfin << ")" << std::endl;
    }

    // getchar();

    return 0;
}