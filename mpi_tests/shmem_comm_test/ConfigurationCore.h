
/**
 *      Configuration Core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#ifndef CONFIGURATION_CORE
#define CONFIGURATION_CORE 1

#include <string>
#include <map>
#include <pthread.h>
#include <vector>

#define MAX_RU_METADATA_FRAGMENTS 5
#define MAX_RU_DATA_FRAGMENTS 5
#define MAX_RU_FRAGMENTS (MAX_RU_METADATA_FRAGMENTS+MAX_RU_DATA_FRAGMENTS)

namespace Core {

class ConfigurationLoader {
public:
    int logLevel;
    std::map<int, std::string> mapLogLevels;

    int initialCredits;
    int triggerFrequency;
    int numnodes;
    std::string name;

    std::string trigger_sqName;

    std::string BUMetaBufferName;
    std::string BUDataBufferName;
    size_t BUMetaBufferSize;
    size_t BUDataBufferSize;

    size_t RUDataBufferSize;
    size_t RUMetaBufferSize;
    int maxRUFragments;
    int eventFragmentSize;
    int dataFragmentSize;
    int eventSize;

    std::string RUgpuGeneratedQName1;
    std::string RUgpuGeneratedQName2;
    std::string RUProcessedQName;
    std::string RUMeta1Name;
    std::string RUMeta2Name;
    std::string RUData1Name;
    std::string RUData2Name;
    int RUQueueElements;
    
    std::vector<std::string> buffer_IDName;
    std::vector<int> buffer_IDSize;
    std::vector<int> IDSize;

    std::string shmemCommQTransactionName;
    std::string shmemCommQFinishedName;
    int shmemQueueSize;

    int EMSharedQueueSize;

    ConfigurationLoader();
};

#ifndef EXTERN_CONFIGURATION_CORE
extern ConfigurationLoader Configuration;
#endif

};

#endif
