
/**
 *      shmem my way - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "Shmem.h"

int openSharedMem(std::string& sharedName, int size, int& descriptor, void*& pmem){
    int flags = O_CREAT | O_RDWR;
    mode_t perms = S_IRWXU | S_IRWXG | S_IRWXO;

    descriptor = shm_open(sharedName.c_str(), flags, perms);
    if (descriptor == -1){
        std::cout << "shm_open error"  << std::endl;
        return -1;
    }

    if (ftruncate(descriptor, size) == -1){
        std::cout << "ftruncate error"  << std::endl;
        return -1;
    }

    /* Map shared memory object */
    pmem = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, descriptor, 0);
    if (pmem == MAP_FAILED){
        // ERROR << "SharedQueue: " << strerror(errno) << std::endl;
        return -1;
    }

    return 0;
}

int closedSharedMem(int descriptor){
    return close(descriptor);
}

int deleteSharedMem(std::string& sharedName){
    return shm_unlink(sharedName.c_str());
}
