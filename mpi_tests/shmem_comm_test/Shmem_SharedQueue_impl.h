
/**
 *      SharedQueue
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include <string>

/**
 * Queue for one producer / one consumer
 */

// class Shmem_SharedQueue;

template<class T>
Shmem_SharedQueue<T>::Shmem_SharedQueue(std::string& sharedName, int numElements) : 
    sharedName(sharedName), size(numElements) {
    polls = 0;
    int error = openSharedMem(sharedName, (size + 2) * sizeof(T), descriptor, pmem);
    if (error == -1){
        // ERROR << "Shmem_SharedQueue: Couldn't openSharedMem" << std::endl;
    }

    nread = (int*) ((T*) pmem);
    nwritten = (int*) ((T*) pmem) + 1;
    ipmem = ((T*) pmem) + 2;

    // Overwriting nread and nwritten
    *nread = 0;
    *nwritten = 0;

    producerPointer = ipmem + (*nwritten % size);
    consumerPointer = ipmem + (*nread % size);
}

template<class T>
Shmem_SharedQueue<T>::~Shmem_SharedQueue(){
    closedSharedMem(descriptor);
}

/**
 * Writes a value onto the SharedQueue.
 * @param  x Value to write.
 * @return   -1 if unsuccesful, 0 if succesful.
 */
template<class T>
int Shmem_SharedQueue<T>::write(T x){
    // The substraction naturally takes care of overflows
    if (*nwritten - *nread < size){
        *producerPointer = x;

        if ((++producerPointer) >= ipmem + size)
            producerPointer = ipmem;
        ++*nwritten;
        return 0;
    }
    return -1;
}

/**
 * Reads a value from the SharedQueue.
 * @return -1 if unsuccesful, the value otherwise.
 * Note: The accepted values on the SharedQueue are positive.
 */
template<class T>
int Shmem_SharedQueue<T>::read(T& r){
    // std::cout << "polls: " << _polls << std::endl;
    ++polls;
    if (*nwritten > *nread){
        // DEBUG << "(written, read) - (" << *nwritten << ", " << *nread << ") after " << polls << " polls" << std::endl;
        // DEBUG << "mem locs " << std::hex << nwritten << ", " << nread << std::endl;
        r = *consumerPointer;
        if (++consumerPointer >= ipmem + size)
            consumerPointer = ipmem;

        ++*nread;
        return 0;
    }
    return -1;
}

template<class T>
int Shmem_SharedQueue<T>::getRead(){
    return *nread;
}

template<class T>
int Shmem_SharedQueue<T>::getWritten(){
    return *nwritten;
}

template<class T>
int* Shmem_SharedQueue<T>::getArray(){
    return ipmem;
}
