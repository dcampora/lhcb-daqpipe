
/**
 *      Shared memory communicator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "MemcpyComm.h"

MemcpyComm::MemcpyComm(){
    std::cout << "Memcpy: Constructor" << std::endl;
    qtransactions = new Shmem_SharedQueue<Transaction>(Core::Configuration.shmemCommQTransactionName, Core::Configuration.shmemQueueSize);
    qfinishedTransactions = new LockShmem_SharedQueue<int>(Core::Configuration.shmemCommQFinishedName, Core::Configuration.shmemQueueSize);
}

void MemcpyComm::run(){
    // Spawn threads if necessary
    // Listen to incoming requests, make transactions as they come
    Transaction t;
    Copycat* cc;
    int submitted = 0;
    pthread_t* p; // Mem leak, but just for test, ok...

    while(true){
        std::cout << "Memcpy: Listening..." << std::endl;
        while ((qtransactions->read(t)) == -1) usleep(10);
        
        // std::cout << "Shmem: id " << t.id << ": " << std::hex << (long long int) p1 << ", " << (long long int) p2 << std::dec << std::endl;
        std::cout << "Memcpy: Submitted " << submitted++ << std::endl;

        // Copy!
        // (char* p1, char* p2, unsigned int size, int id, SharedQueue* qfinished)
        p = (pthread_t*) malloc(sizeof(pthread_t));
        
        cc = new Copycat(t.bufferOrig, t.bufferDest, t.size, t.id, qfinishedTransactions, p);
        while (startThreadWithReference<Copycat>(*cc, p) != 0); // Attempt to start until free resources
    }
}

void Copycat::run(){
    std::cout << "Copycat: Started " << id << "..." << std::endl;

    // Copy and tell we finished.
    // Such an easy function!
    memcpy(p2, p1, size);
    while(qfinishedTransactions->write(id) == -1) usleep(100);

    std::cout << "Copycat: Finished " << id << " (" << size << " B)" << std::endl;

    // this->~Copycat();
    free(me);
}
