
/**
 *      LockShmem_SharedQueue
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      February, 2013
 *      CERN
 */

/**
 * Queue for multiple producers / multiple consumers
 */

/**
 * Writes a value onto the SharedQueue.
 * @param  x Value to write.
 * @return   -1 if unsuccesful, 0 if succesful.
 */
template<class T>
int LockShmem_SharedQueue<T>::write(T x){
    // The substraction naturally takes care of overflows
    if ( ((unsigned int) (*this->nwritten - *this->nread)) < this->size){
        pthread_mutex_lock(&mutex);
        ++this->polls;
        std::cout << "LockShmem: Write " << this->polls << std::endl;
        *this->producerPointer = x;
        if ((++this->producerPointer) >= this->ipmem + this->size)
            this->producerPointer = this->ipmem;
        ++*this->nwritten;
        pthread_mutex_unlock(&mutex);
        return 0;
    }
    return -1;
}

/**
 * Reads a value from the SharedQueue.
 * @return -1 if unsuccesful, the value otherwise.
 * Note: The accepted values on the SharedQueue are positive.
 */
template<class T>
int LockShmem_SharedQueue<T>::read(T& r){
    // std::cout << "polls: " << _polls << std::endl;
    if (*this->nwritten - *this->nread > 0){
        pthread_mutex_lock(&mutex);
        r = *this->consumerPointer;
        if (++this->consumerPointer >= this->ipmem + this->size)
            this->consumerPointer = this->ipmem;
        ++*this->nread;
        pthread_mutex_unlock(&mutex);
        return 0;
    }
    return -1;
}
