
/**
 *      pthread wrappers :)
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include <pthread.h>
// #include "Logging.h"
#include "ConfigurationCore.h"

template<class T>
void* pthreadEntrypoint(void* toBeIgnored){
    T t;
    t.run();
    pthread_exit(0);
}
template<class T>
pthread_t* startThread(){
    pthread_t* pthreadInstance = (pthread_t*) malloc(sizeof(pthread_t));
    pthread_create(pthreadInstance, 0, pthreadEntrypoint<T>, 0);
    return pthreadInstance;
}

template<class T, class Z>
void* pthreadEntrypoint(void* toBeConsidered){
    T t( (Z&) (reinterpret_cast<int*>(toBeConsidered))[0] );
    t.run();
    pthread_exit(0);
}
template<class T, class Z>
pthread_t* startThread(Z& z){
    pthread_t* pthreadInstance = (pthread_t*) malloc(sizeof(pthread_t));
    pthread_create(pthreadInstance, 0, pthreadEntrypoint<T, Z>, (void*) &z);
    return pthreadInstance;
}


template<class Z>
void* pthreadEntrypointWithReference(void* reference){
    ( (Z&) (reinterpret_cast<int*>(reference))[0] ).run();
    pthread_exit(0);
}
template<class Z>
int startThreadWithReference(Z& z, pthread_t* p){
    return pthread_create(p, 0, pthreadEntrypointWithReference<Z>, (void*) &z);
}

template<class Z>
pthread_t* startThreadWithReference(Z& z){
    pthread_t* pthreadInstance = (pthread_t*) malloc(sizeof(pthread_t));
    int errCode;
    if ((errCode = startThreadWithReference<Z>(z, pthreadInstance)) != 0){
        std::cout << "[ERROR]: " << errCode << std::endl;
        free(pthreadInstance);
        return 0;
    }
    return pthreadInstance;
}
