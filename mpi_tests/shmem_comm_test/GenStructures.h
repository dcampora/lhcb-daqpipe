
/**
 *      Generator structures
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#include "ConfigurationCore.h"

#ifndef FRAGMENT_COMPOSITION
#define FRAGMENT_COMPOSITION 1

struct Transaction {
    int id;
    char* bufferOrig;
    char* bufferDest;
    int size;
};

struct ShmemTransaction {
    int id;
    int bufferOrigID;
    int bufferDestID;
    int elem1;
    int elem2;
    int size;
};

// #pragma pack(push, 1)
struct FragmentComposition {
    int metablocks, datablocks;
    int metasizes[5];
    int datasizes[5];
};
// #pragma pop


#define MAX_EVENTIDS_METADATA 1024

// #pragma pack(push, 1)
struct MetadataEntry {
    int eid_lsb;
    int eid_pointer; // The "pointer" here refers to the element
    // short eid_lsb : 16;
    // short eid_pointer : 16;
};
// #pragma pop

// #pragma pack(push, 1)
struct Metadata { // Size is 1024 B with MAX_EVENTIDS_METADATA (if sizeof(MetadataEntry) == 32)
    short length : 16;
    short noEvents : 16;
    unsigned int firstEID;
    unsigned int lastEID;
    char* startingPointer;
    MetadataEntry entries[MAX_EVENTIDS_METADATA];
};
// #pragma pop

#endif
