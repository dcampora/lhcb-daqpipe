
/**
 *      Shared memory communicator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include <cstring>
#include <map>
#include "GenStructures.h"
#include "Shmem_SharedQueue.h"
#include "pthreadHelper.h"
#include "LockShmem_SharedQueue.h"
#include "Shmem.h"

class MemcpyComm {
private:
    Shmem_SharedQueue<Transaction> * qtransactions;
    LockShmem_SharedQueue<int> * qfinishedTransactions;

    // Shmem
    // openSharedMem(data1Name, Core::Configuration.RUDataBufferSize, descriptors[data1Name], pmems[data1Name]);
    // openSharedMem(data2Name, Core::Configuration.RUDataBufferSize, descriptors[data2Name], pmems[data2Name]);
    std::map<int, void*> pmems;
    std::map<int, int> descriptors;

public:
    MemcpyComm();
    void run();
    char* getPointer(int bufferID);
};

class Copycat {
private:
    char* p1, *p2;
    int size, id;
    LockShmem_SharedQueue<int>* qfinishedTransactions;
    pthread_t* me;

public:
    Copycat(char* p1, char* p2, int size, int id, LockShmem_SharedQueue<int>* qfinished, pthread_t* t) : 
        p1(p1), p2(p2), size(size), id(id), qfinishedTransactions(qfinished), me(t) {}
    void run();
};
