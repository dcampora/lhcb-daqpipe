#! /bin/bash
rate=1
tot_time=10
if [ $2 != 0 ] 
then
	rate=$2
fi
if [ $1 != 0 ] 
then
	tot_time=$1
fi
sar -u $rate $tot_time | awk '(NR <= 3){print "# ",$0}(NR>3 && $1 !~ /Average/){gsub(/all/,"", $0); print $0}'
