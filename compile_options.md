# Compilation options

## Select compiler, g++ or clang

    export OMPI_CXX=clang++
    export OMPI_MPICC=clang++

## cmake building options
## MPI transport

    mkdir build
    cd build
    cmake -DTRANSPORT=MPI ..
    make

## verbs transport

    mkdir build
    cd build
    cmake -DTRANSPORT=verbs ..
    make

    # debug build set this variable
    cmake -DTRANSPORT=verbs -DCMAKE_BUILD_TYPE=Debug ..

## special build: farm

    cmake -DTRANSPORT=MPI -DFARM_RUN=1 ..
