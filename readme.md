
# Event Building application #

The Event Builder is a modular application with several components.

It's subject to modification and open to suggestions
(and improvements :))!

## Skip the doc! - Quickly compile and execute

Currently there are three transports supported: MPI, OFED verbs and TCP sockets.
To compile and launch, have a look into compile\_options.md and mpirun\_options.md.

The quick and cheap is, you may specify before starting the MPI implementation
to use. Use mpi-selector, or mpi-selector-menu. You would have to reopen a
shell after.

The following would setup the application to run with the shipped lab
configuration in **config/labs\_13_14.labs** and the application **eb\_app**,

    # Run the OpenMPI version
    mpirun -npernode 4 -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -mca btl_openib_pkey 0x111 -machinefile config/labs_1314.labs `pwd`/taskset_launch.sh 0 1 `pwd`/eb_app -f 100000 -s $((100*1024)) -c 2

## Transport

The different transports are implemented in the **transport/** folder,
following the common structure defined in the headers in **transport/common/**.

In order to implement a new transport, inheriting classes have to be
implemented following the strategy defined in those header files. The _doc_
folder contains additional information.

Additionally, an overriden version of the ConfigurationLoader class,
defined in **transport/config/ConfigurationCore.h** is needed,
implementing the pure virtual methods left in that config file.

Lastly, the **main.cpp** file is MPI dependent, as it bootstraps the
OpenMPI application. That is, different bootstrappers have to be defined for
each transport implementation.

## Protocol

The protocol (PUSH, PULL or PUSH\_BARREL) is decoupled in logical design from
the transport, but uses a transport instance for performing the EB.

Currently there are four components, with their counterparts in the transport:

* Event Manager Listener - Listens for connections incoming from
    the BUs (credit requests). It then passes the requests to a shared memory
    queue, shared between the two Event Managers.

* Event Manager Consumer - The EM Consumer spawns an additional thread, the
    _trigger_, which acts an artificial event generator (triggers _frequency_
    times per second). The trigger consumes credits from the shared queue every
    tick, and passes it on to the Consumer (effectively acting as a frequency
    delimiter).

    The EM Consumer consumes these credits and generates an _event_ (currently
    _size_, _eventID_ and _assignedBU_), which then distributes to the RUs and
    BUs.

* Readout Unit - The Readout generates the requested sized events and
    distributes them to the designated BU.

* Builder Unit - The BU generates credits, and consumes the events from the RUs.


Each component is implemented in a run() entrypoint in a <component\_name>Master
file. Ie. A BU currently works by instantiating a BUMaster with a transport,

    BUTransportCore* ebtransport = new MPIBuilderUnit();
    BUTransportCore* shmem_ebtransport = new ShmemBuilderUnit();
    BUMaster* bu = new BUMasterPush(ebtransport, shmem_ebtransport, Core::Configuration->initialCredits, Core::Configuration->eventSize);
    
    bu->run();

If using a transport which supports multi-threading (ie. not-MPI), the fourth
line can be replaced by,
    
    startThreadWithReference<BUMaster>(bu);

The flow of the application is intended to be simplistic, with small overhead,
and readable. The **run** method of each component-protocol should be easy to
read and modify (it's the core of each app).

The design is classful, intended to have a meaningful naming scheme. The run method of each component-protocol defines its core functionality, and should be easy to read and modify. 
