
/**
 *      Builder Unit Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef BUMASTERPUSH
#define BUMASTERPUSH 1
 
#include <stdlib.h>
#include "BUMaster.h"

class BUMasterPush : public BUMaster {
public:
    BUMasterPush(BUTransportCore* transport, BUTransportCore* shmemTransport, int initialCredits, int maxSize);
    bool mainLoop(int slot);
    virtual void run();

    std::map<int, int> eventID_slot;
    int size, id;
    Timer ttotal, twait, tsendCredits, trecSizeID, tprepare,
        tprepareNodes, tprepareShared, tsetup, tprepareComplete;
    Timer tmon_total, tmon_period;

    // eid -> vector<char*>
    std::map<int, std::vector<char*> > eid_shmemPointers;
    char* shmem_metapointer;
    void* pmem;
    int descriptor;

    int credit;
    int num_credits;
};

#endif
