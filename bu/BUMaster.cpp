
/**
 *      Builder Unit Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "BUMaster.h"

void BUMaster::allocateBuffers(){
    // Buffers
    fragmentsBuffer.resize(initialCredits, (FragmentComposition*) malloc(
        Core::Configuration->numTotalReadouts * Core::Configuration->numBuildersPerNode * sizeof(FragmentComposition)));

    // shmem
    openSharedMem(Core::Configuration->BUMetaBufferName, Core::Configuration->BUMetaBufferSize, sh_meta_descriptor, sh_meta_pmem);
    openSharedMem(Core::Configuration->BUDataBufferName, Core::Configuration->BUDataBufferSize, sh_data_descriptor, sh_data_pmem);
    
    // Pointers for each received event meta / data
    metaBuffer.resize(initialCredits);
    dataBuffer.resize(initialCredits);

    bumeta_perbuffer_size = Core::Configuration->BUMetaBufferSize / initialCredits;
    budata_perbuffer_size = Core::Configuration->BUDataBufferSize / initialCredits;

    for (int i=0; i<initialCredits; ++i){
        metaBuffer[i] = ((char*) sh_meta_pmem) + i * bumeta_perbuffer_size;
        dataBuffer[i] = ((char*) sh_data_pmem) + i * budata_perbuffer_size;
    }
}

BUMaster::BUMaster(BUTransportCore* transport, BUTransportCore* shmemTransport, int initialCredits, int eventFragmentSize) :
    transport(transport), shmemTransport(shmemTransport), initialCredits(initialCredits){
    allocateBuffers();
}

BUMaster::~BUMaster(){
    closedSharedMem(sh_meta_descriptor);
    closedSharedMem(sh_data_descriptor);
}
