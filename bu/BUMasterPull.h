
/**
 *      Builder Unit Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#ifndef BUMASTERPULL
#define BUMASTERPULL 1
 
#include <stdlib.h>
#include "BUMaster.h"

class BUMasterPull : public BUMaster {
public:
    BUMasterPull(BUTransportCore* transport, BUTransportCore* shmemTransport, int initialCredits, int maxSize);
    bool mainLoop(int slot);
    virtual void run();

    std::map<int, int> eventID_slot;
    int size, id;
    Timer ttotal, twait, tsendCredits, trecSizeID, tprepare,
        tprepareNodes, tprepareShared, tsetup, tprepareComplete;
    Timer tmon_total, tmon_period;

    // eid -> vector<char*>
    std::map<int, std::vector<char*> > eid_shmemPointers;
    char* shmem_metapointer;
    void* pmem;
    int descriptor;

    int credit;
    int eventSkip;

    std::vector<int*> size_id_bu;
};

#endif
