
/**
 *      Builder Unit Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef BUMASTER
#define BUMASTER 1
 
#include <vector>
#include <malloc.h>
#include "../transport/config/ConfigurationCore.h"
#include "../transport/common/BUTransportCore.h"
#include "../common/Logging.h"
#include "../common/Timer.h"
#include "../common/GenStructures.h"
#include "../common/Shmem.h"

class BUMaster {
protected:
    BUTransportCore* transport;
    BUTransportCore* shmemTransport;
    int initialCredits;
    size_t bumeta_perbuffer_size, budata_perbuffer_size;

    std::vector<FragmentComposition*> fragmentsBuffer;

    // shmem
    int sh_meta_descriptor, sh_data_descriptor;
    void * sh_meta_pmem, * sh_data_pmem;
    std::vector<char *> metaBuffer;
    std::vector<char *> dataBuffer;

public:
    BUMaster(BUTransportCore* transport, BUTransportCore* shmemTransport, int initialCredits, int maxSize);
    ~BUMaster();
    void allocateBuffers();

    /**
     * Starts the whole BU cycle.
     */
    virtual void run() = 0;
};

#endif
