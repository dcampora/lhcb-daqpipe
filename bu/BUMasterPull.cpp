
/**
 *      Builder Unit Pull Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#include "BUMasterPull.h"

BUMasterPull::BUMasterPull(BUTransportCore* transport, BUTransportCore* shmemTransport, int initialCredits, int maxSize) :
    BUMaster(transport, shmemTransport, initialCredits, maxSize),
    credit(1) {

    openSharedMem(Core::Configuration->shmemCommMetaBuffer, Core::Configuration->BUMetaBufferSize, descriptor, pmem);
    shmem_metapointer = (char*) pmem;
    size_id_bu.resize(initialCredits, (int*) malloc(3 * sizeof(int)));
    eventSkip = 0;
}

bool BUMasterPull::mainLoop(int slot){

    int eventSize, eventID, readoutno;
    int activePrepares = 0;

    // Receive size and ID
    transport->receiveSizeAndID(eventSize, eventID);

    DEBUG << "BUMasterPull: Assigned eid " << eventID << ". Yahoo!" << std::endl;

    eventID_slot[eventID] = slot;

    size_id_bu[slot][0] = eventSize;
    size_id_bu[slot][1] = eventID;
    size_id_bu[slot][2] = Core::Configuration->builderNumber(Core::Configuration->nodeID());

    DEBUG << "BUMasterPull: Sending pull request for event " << eventID << " to ru " << readoutno << " from " << size_id_bu[slot][2] << std::endl;
    // Prepare all receives
    for (readoutno=0; readoutno<Core::Configuration->numTotalReadouts; ++readoutno){
        transport->pullRequest(size_id_bu[slot], readoutno);
    }

    transport->prepareReceiveEvent(fragmentsBuffer[slot]);

    activePrepares = Core::Configuration->numTotalReadouts;

    // Send data!
    int meta_elem_pointer = 0, data_elem_pointer = 0;
    bool ret = true;
    while(activePrepares > 0){
        tprepareComplete.start();
        int wait = 0;
        while( transport->prepareReceiveEventComplete(readoutno) == 0 && ret){
            usleep(1);

            //check exit
            if (transport->checkExitMessage())
            {
                DEBUG << "BUMasterPull: Get exit message for BU" << std::endl;
                ret = false;
            }

            // Error management
            // wait++;
            // if (wait > Core::Configuration->timeoutPrepareReceiveEvent){
            //     eventSkip++;
            //     goto timeout;
            // }
        }
        //has exit
        if (ret == false)
            break;
        tprepareComplete.stop();
        activePrepares--;

        DEBUG << "BUMasterPull: Received fragment " << readoutno << ", " << activePrepares << " more to go" << std::endl;

        tsetup.start();

        // Meta and Data coming from other nodes
        for (int j=0; j<fragmentsBuffer[slot][readoutno].metablocks; ++j){
            if ((meta_elem_pointer + fragmentsBuffer[slot][readoutno].metasizes[j]) > bumeta_perbuffer_size){
                ERROR << "BUMasterPull: meta_elem_pointer requested exceeds allocated size ("
                    << meta_elem_pointer + fragmentsBuffer[slot][readoutno].metasizes[j] << " > "
                    << bumeta_perbuffer_size << ")" << std::endl;
                
                meta_elem_pointer = 0;
            }

            char* dataPointer = &(metaBuffer[slot][meta_elem_pointer]);
            transport->setupReceiveEvent(dataPointer, fragmentsBuffer[slot][readoutno].metasizes[j], readoutno, eventID, Core::METADATA);
            meta_elem_pointer += fragmentsBuffer[slot][readoutno].metasizes[j];
        }

        for (int j=0; j<fragmentsBuffer[slot][readoutno].datablocks; ++j){
            if ((data_elem_pointer + fragmentsBuffer[slot][readoutno].datasizes[j]) > budata_perbuffer_size){
                ERROR << "BUMasterPull: data_elem_pointer requested exceeds allocated size ("
                    << data_elem_pointer + fragmentsBuffer[slot][readoutno].datasizes[j] << " > "
                    << budata_perbuffer_size << ")" << std::endl;
                
                data_elem_pointer = 0;
            }

            char* dataPointer = &(metaBuffer[slot][meta_elem_pointer]);
            transport->setupReceiveEvent(dataPointer, fragmentsBuffer[slot][readoutno].datasizes[j], readoutno, eventID, Core::DATA);
            data_elem_pointer += fragmentsBuffer[slot][readoutno].datasizes[j];
        }
        tsetup.stop();
    }
timeout: ;
    return ret;
}

void BUMasterPull::run(){
    int transmittedBytes;
    int finishedEventID;
    float printPeriod = 5.0f; // Print every 5.0 seconds monitoring info
    int processedEvents = 0, relProcessedEvents = 0;

    tmon_total.start();
    tmon_period.start();

    transport->initialize(initialCredits, sh_meta_pmem, sh_data_pmem);
    shmemTransport->initialize(initialCredits);

    //register
    transport->registerSegment(sh_meta_pmem,Core::Configuration->BUMetaBufferSize);
    transport->registerSegment(sh_data_pmem,Core::Configuration->BUDataBufferSize);

    transport->sendCredits(initialCredits);

    // Setup rx
    for (int i=0; i<initialCredits; ++i){
        mainLoop(i);
    }

    long long unsigned int transmittedBytesAcum = 0;
    while(1){
        ttotal.start();

        finishedEventID = -1;
        twait.start();
        while(!transport->receiveEvent(finishedEventID, transmittedBytes)) usleep(1);
        twait.stop();

        INFO << "BUMasterPull: Received event " << finishedEventID  << " (" << transmittedBytes << " B)" << std::endl;
        ++processedEvents;
        ++relProcessedEvents;
        transmittedBytesAcum += transmittedBytes;

        // TODO: Do something with the data
        if (Core::Configuration->logMetadata){
            DEBUG << "BUMasterPull: Printing metadatas of event " << finishedEventID << std::endl;
            // Logger::printMeta(&(metaBuffer[0][0]), fragmentsBuffer[0][0].metasizes[0]);

            int finished_slot = eventID_slot[finishedEventID];

            int meta_elem_pointer = 0;
            for (int node=0; node<Core::Configuration->numTotalReadouts; ++node){
                DEBUG << "BUMasterPull: From node " << node << std::endl;
                for (int j=0; j<fragmentsBuffer[finished_slot][node].metablocks; ++j){
                    if (!Core::Configuration->readoutInSameNode(node)){
                        Logger::printMeta(&(metaBuffer[finished_slot][meta_elem_pointer]), fragmentsBuffer[finished_slot][node].metasizes[j]);
                        meta_elem_pointer += fragmentsBuffer[finished_slot][node].metasizes[j];
                    }
                    else {
                        Logger::printMeta(eid_shmemPointers[finishedEventID][j], fragmentsBuffer[finished_slot][node].metasizes[j]);
                    }
                }
            }
        }
        eid_shmemPointers.erase(finishedEventID);

        // We have a credit
        tsendCredits.start();
        transport->sendCredits(credit);
        tsendCredits.stop();

        // Setup handler for event
        int slot = eventID_slot[finishedEventID];
        eventID_slot.erase(finishedEventID);

        DEBUG << "BUMasterPull: slot " << slot << " is freed" << std::endl;
        //check for exit
        if (mainLoop(slot) == false)
            break;

        ttotal.stop();
        
        // Some monitoring
        if (tmon_period.getElapsedTime() > printPeriod){
            // rate is: num_events_processed * size_of_event / time (B / s)
            // note: 1000000000 / (1024 * 1024.0f) = 953.67f
            
            // float receptionRate = relProcessedEvents * (Core::Configuration->numnodes - 1) * (size / (trec.getLong() / 953.67f));
            // float totalRate = relProcessedEvents * (Core::Configuration->numnodes - 1) * ((transmittedBytes * 8.0f) / (tmon_period.getElapsedTimeLong()));
            float totalRate = ((transmittedBytesAcum * 8.0f) / (tmon_period.getElapsedTimeLong()));

            MON << "BUMasterPull: " << Core::Configuration->name << ", t " << tmon_total.getElapsedTime() << ": "
                << "EB rate " << totalRate << " Gb/s" << std::endl;
            
            // Profiling
            if (Core::Configuration->logActivated[Logger::_PROFILE]){
                // Timer ttotal, twait, torder, tmeta, tfragment, tsetup;
                PROFILE << "BUMasterPull: " << Core::Configuration->name << " timers: " << std::endl
                    << std::setw(15) << "ttotal " << ttotal.get() << std::endl
                    << std::setw(15) << "twait " << twait.get() << " (" << 100.f * twait.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tsendCredits " << tsendCredits.get() << " (" << 100.f * tsendCredits.get() / ttotal.get() << "\%)" << std::endl
                    // << std::setw(15) << "trecSizeID " << trecSizeID.get() << " (" << 100.f * trecSizeID.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tprepareShared " << tprepareShared.get() << " (" << 100.f * tprepareShared.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tprepareNodes " << tprepareNodes.get() << " (" << 100.f * tprepareNodes.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tsetup " << tsetup.get() << " (" << 100.f * tsetup.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tprepareComplete " << tprepareComplete.get() << " (" << 100.f * tprepareComplete.get() / ttotal.get() << "\%)" << std::endl
                    // << std::setw(15) << "tsetup_2 " << tsetup_2.get() << " (" << 100.f * tsetup_2.get() / ttotal.get() << "\%)" << std::endl
                    << std::endl;
            }

            // Reset counters
            relProcessedEvents = 0;
            transmittedBytesAcum = 0;
            tmon_period.flush();
            tmon_period.start();
        }
    }
}
