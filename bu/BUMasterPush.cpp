
/**
 *      Builder Unit Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "BUMasterPush.h"

BUMasterPush::BUMasterPush(BUTransportCore* transport, BUTransportCore* shmemTransport, int initialCredits, int maxSize) :
    BUMaster(transport, shmemTransport, initialCredits, maxSize),
    credit(1) {

    openSharedMem(Core::Configuration->shmemCommMetaBuffer, Core::Configuration->BUMetaBufferSize, descriptor, pmem);
    shmem_metapointer = (char*) pmem;
}

bool BUMasterPush::mainLoop(int slot){
    // Always send FragmentComposition
    tprepare.start();
    transport->prepareReceiveEvent(fragmentsBuffer[slot]);
    tprepare.stop();

    // Only focusing on bypass now
    long long int known_eid = -1;
    int readoutid;
    int activePrepares = Core::Configuration->numTotalReadouts;
    bool ret = true;

    int meta_elem_pointer = 0, data_elem_pointer = 0;
    while(activePrepares > 0){
        tprepareComplete.start();
        while( transport->prepareReceiveEventComplete(readoutid) == 0  && ret){
            //check exit
            if (transport->checkExitMessage())
            {
                DEBUG << "Get exit message for BU" << std::endl;
                ret = false;
            }
            usleep(1);
        }
        //has exit
        if (ret == false)
            break;
        tprepareComplete.stop();
        activePrepares--;

        DEBUG << "BUMasterPush: Received fragment " << readoutid << ", " << activePrepares << " more to go" << std::endl;

        // We have a fragmentReceived!
        // First, fill the id if we didn't know it yet
        if (known_eid == -1){
            id = fragmentsBuffer[slot][readoutid].eventID;
            eventID_slot[id] = slot;
        }

        tsetup.start();
        for (int j=0; j<fragmentsBuffer[slot][readoutid].metablocks; ++j){
            if (((size_t) (meta_elem_pointer + fragmentsBuffer[slot][readoutid].metasizes[j])) > bumeta_perbuffer_size){
                ERROR << "BUMasterPush: meta_elem_pointer requested exceeds allocated size ("
                      << meta_elem_pointer + fragmentsBuffer[slot][readoutid].metasizes[j] << " > "
                      << bumeta_perbuffer_size << ")" << std::endl;

                meta_elem_pointer = 0;
            }

            char* dataPointer = &(metaBuffer[slot][meta_elem_pointer]);
            transport->setupReceiveEvent(dataPointer, fragmentsBuffer[slot][readoutid].metasizes[j], readoutid, id, Core::METADATA);
            meta_elem_pointer += fragmentsBuffer[slot][readoutid].metasizes[j];
        }

        for (int j=0; j<fragmentsBuffer[slot][readoutid].datablocks; ++j){
            if (((size_t) (data_elem_pointer + fragmentsBuffer[slot][readoutid].datasizes[j])) > budata_perbuffer_size){
                ERROR << "BUMasterPush: data_elem_pointer requested exceeds allocated size ("
                      << data_elem_pointer + fragmentsBuffer[slot][readoutid].datasizes[j] << " > "
                      << budata_perbuffer_size << ")" << std::endl;

                data_elem_pointer = 0;
            }

            char* dataPointer = &(metaBuffer[slot][meta_elem_pointer]);
            transport->setupReceiveEvent(dataPointer, fragmentsBuffer[slot][readoutid].datasizes[j], readoutid, id, Core::DATA);
            data_elem_pointer += fragmentsBuffer[slot][readoutid].datasizes[j];
        }
        tsetup.stop();
    }

    return ret;
}

void BUMasterPush::run(){
    int transmittedBytes;
    int finishedEventID;
    float printPeriod = 5.0f; // Print every 5.0 seconds monitoring info
    int processedEvents = 0, relProcessedEvents = 0;

    tmon_total.start();
    tmon_period.start();

    transport->initialize(initialCredits, sh_meta_pmem, sh_data_pmem);

    //register
    transport->registerSegment(sh_meta_pmem,Core::Configuration->BUMetaBufferSize);
    transport->registerSegment(sh_data_pmem,Core::Configuration->BUDataBufferSize);

    transport->sendCredits(initialCredits);
    num_credits = initialCredits;

    // Setup rx
    for (int i=0; i<initialCredits; ++i){
        mainLoop(i);
        num_credits--;
    }

    long long unsigned int transmittedBytesAcum = 0;
    while(1){
        ttotal.start();

        finishedEventID = -1;
        twait.start();
        while(!transport->receiveEvent(finishedEventID, transmittedBytes)){usleep(1);}
        twait.stop();

        INFO << "BUMasterPull: Received event " << finishedEventID  << " (" << transmittedBytes << " B)" << std::endl;
        ++processedEvents;
        ++relProcessedEvents;
        transmittedBytesAcum += transmittedBytes;

        // TODO: Do something with the data
        if (Core::Configuration->logMetadata){
            DEBUG << "BUMasterPush: Printing metadatas of event " << finishedEventID << std::endl;
            // Logger::printMeta(&(metaBuffer[0][0]), fragmentsBuffer[0][0].metasizes[0]);

            int finished_slot = eventID_slot[finishedEventID];

            int meta_elem_pointer = 0;
            for (int node=0; node<Core::Configuration->numTotalReadouts; ++node){
                DEBUG << "BUMasterPush: From node " << node << std::endl;
                for (int j=0; j<fragmentsBuffer[finished_slot][node].metablocks; ++j){
                    Logger::printMeta(&(metaBuffer[finished_slot][meta_elem_pointer]), fragmentsBuffer[finished_slot][node].metasizes[j]);
                    meta_elem_pointer += fragmentsBuffer[finished_slot][node].metasizes[j];
                }
            }
        }

        // We have a credit
        tsendCredits.start();
        transport->sendCredits(credit);
        num_credits += credit;
        tsendCredits.stop();

        // Setup handler for event
        int slot = eventID_slot[finishedEventID];
        eventID_slot.erase(finishedEventID);

        DEBUG << "BUMasterPush: slot " << slot << " is freed" << std::endl;
        //check for exit
        if (mainLoop(slot) == false)
            break;
        num_credits--;

        ttotal.stop();

        if(num_credits != 0){
            ERROR << "BUMaster: " << Core::Configuration->name  << " credits not processed correctly" << std::endl;
        }
        
        // Some monitoring
        if (tmon_period.getElapsedTime() > printPeriod){
            // rate is: num_events_processed * size_of_event / time (B / s)
            // note: 1000000000 / (1024 * 1024.0f) = 953.67f
            
            // float receptionRate = relProcessedEvents * (Core::Configuration->numnodes - 1) * (size / (trec.getLong() / 953.67f));
            // float totalRate = relProcessedEvents * (Core::Configuration->numnodes - 1) * ((transmittedBytes * 8.0f) / (tmon_period.getElapsedTimeLong()));
            float totalRate = ((transmittedBytesAcum * 8.0f) / (tmon_period.getElapsedTimeLong()));

            MON << "BUMaster: " << Core::Configuration->name << ", t " << tmon_total.getElapsedTime() << ": "
                << "EB rate " << totalRate << " Gb/s" << std::endl;
            DEBUG << "BUMaster: " << Core::Configuration->name << " num_credits " << num_credits << std::endl;

            // Profiling
            if (Core::Configuration->logActivated[Logger::_PROFILE]){
                // Timer ttotal, twait, torder, tmeta, tfragment, tsetup;
                PROFILE << "BUMaster: " << Core::Configuration->name << " timers: " << std::endl
                    << std::setw(15) << "ttotal " << ttotal.get() << std::endl
                    << std::setw(15) << "twait " << twait.get() << " (" << 100.f * twait.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tsendCredits " << tsendCredits.get() << " (" << 100.f * tsendCredits.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tprepareShared " << tprepareShared.get() << " (" << 100.f * tprepareShared.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tprepareNodes " << tprepareNodes.get() << " (" << 100.f * tprepareNodes.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tsetup " << tsetup.get() << " (" << 100.f * tsetup.get() / ttotal.get() << "\%)" << std::endl
                    << std::setw(15) << "tprepareComplete " << tprepareComplete.get() << " (" << 100.f * tprepareComplete.get() / ttotal.get() << "\%)" << std::endl
                    << std::endl;
            }

            // Reset counters
            relProcessedEvents = 0;
            transmittedBytesAcum = 0;
            tmon_period.flush();
            tmon_period.start();
        }
    }
}
