#!/usr/bin/python
# -*- coding: utf-8 -*-
import subprocess
import argparse
import sys
import re
import os, signal
import json
import signal
import sys
import time
import os
import inspect
import group_argparse

try:
    from subprocess import DEVNULL # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')

test = False
verbosity = 0

def filter_generic(key_value, match):
    if type(key_value) == dict:
        for key in key_value.keys():
            if key == match:
                return True
    return False
    
def signal_handler(signal, frame):
    print "SIGINT caught cleaning..."
    recovered_hosts_list = inspect.getmembers(frame, lambda key_value : filter_generic(key_value, "hosts_list"))[1][1]["hosts_list"]
    recovered_eb_app_name = inspect.getmembers(frame, lambda key_value : filter_generic(key_value, "spawner_param"))[1][1]\
                            ["spawner_param"]["eb_app"]
    clean_hosts(recovered_hosts_list, recovered_eb_app_name)
    sys.exit(0)

def param_to_executable(param_def, param):
    return ["%s %s" % (str(param_def[name][0]), \
            (param_def[name][3])=="store_true" and " " or str(value))\
            for name,value in param.items() if (value != None) ^ (((param_def[name][3])=="store_true") and (value != True))]

def absolute_file_args(param, file_args_list):
    for name,arg in param.items():
        if name in file_args_list:
            param[name] = os.path.abspath(arg)

def clean_hosts(hosts_list, eb_app_name):
    process_list = []
    # TODO implement this properly in the c++ class
    rm_command = ["ssh", "<host>", "rm", "-f", "/dev/shm/*", "/tmp/shmem*", "/tmp/gpuru*", "/tmp/trigger"]
    killal_command = ["ssh", "<host>", "killall", "-9", eb_app_name]
    if test == True : 
        rm_command.insert(0, "echo")
        killal_command.insert(0, "echo")

    rm_host_index = rm_command.index("<host>") 
    killal_host_index = killal_command.index("<host>") 
    for host in hosts_list:
        if verbosity >= 1:
            print "cleaning ", host
        rm_command[rm_host_index] = host
        killal_command[killal_host_index] = host
        process_list.append(subprocess.Popen(rm_command, stdout=DEVNULL, stderr=DEVNULL))
        process_list.append(subprocess.Popen(killal_command, stdout=DEVNULL, stderr=DEVNULL))

    for proc in process_list:
        proc.wait()


eb_apps_param_def = {"frequency"   : ("-f", "trigger frequency", int, None, 10000),
                     "credits"     : ("-c", "number fo credits", int, None, 2),
                     "size"        : ("-s", "event size", int, None, 1024),
                     "debug_level" : ("-d", "set debug level", int, None, 3),
                     "profile"     : ("-p", "activate the profiler", None, "store_true", False),
                     "logging"     : ("-l", "activate/deactivate logging to file", int, None, 0),
                     "stdout"      : ("-r", "activate/deactivate logging to stdout", int, None, 1),
                     "push"        : ("-S", "set PUSH protocol", None, "store_true", False,"protocol_group"),
                     "pull"        : ("-L", "set PULL protocol", None, "store_true", False,"protocol_group"),
                     "num_ru_bu"   : ("-n", "set the number of BU and RU per node", int, None, None),
                     "num_bu"      : ("-B", "set the number of BU per node", int, None, None),
                     "num_ru"      : ("-R", "set the number of RU per node", int, None, None),
                     "config"      : ("-a", "json config file", str, None, None),
                     "ethernet"    : ("-e", "activate Ethernet mode", None, "store_true", False,"interface_group"),
                     "infiniband"  : ("-i", "activate Infiniband mode", None, "store_true", False,"interface_group"),
                    }

sync_server_param_def = {"config"      : ("-a", "json config file", str, None, None),
                         "ethernet"    : ("-e", "activate Ethernet mode", None, "store_true", False,"interface_group"),
                         "infiniband"  : ("-i", "activate Infiniband mode", None, "store_true", False,"interface_group"),
                        }

spawner_param_def = {"eb_app"      : ("-x", "eb_app executable path", str, None, None),
                     "sync_server" : ("-y", "sync_server executable path", str, None, None),
                     "verbosity"   : ("-v", "increase verbosity", int, "count", 0),
                     "config"      : ("-a", "json config file", str, None, None),
                     "test"        : ("-t", "enable test mode", None, "store_true", False),
                     "ethernet"    : ("-e", "activate Ethernet mode", None, "store_true", False, "interface_group"),
                     "infiniband"  : ("-i", "activate Infiniband mode", None, "store_true", False, "interface_group"),
                    } 

file_args_list = ["config", "eb_app", "sync_server"]

eb_app = ["ssh", "<host>"]
sync_server = ["ssh", "<host>"]


eb_app_param = {}
sync_server_param = {}
spawner_param = {}

parser = group_argparse.GroupArgparser()

parser["sync_server"] = sync_server_param_def
parser["eb_app"] = eb_apps_param_def
parser["spawner"] = spawner_param_def

parser.parse_args()

eb_app_param = parser.get_group_args("eb_app")
absolute_file_args(eb_app_param, file_args_list)

sync_server_param = parser.get_group_args("sync_server")
absolute_file_args(sync_server_param, file_args_list)

spawner_param = parser.get_group_args("spawner")
absolute_file_args(spawner_param, file_args_list)

verbosity = spawner_param["verbosity"]
test = spawner_param["test"]

if(verbosity >= 2):
    print "eb_abb options:"
    print "\n".join([ "%s=%s" % (k,v) for k,v in eb_app_param.items()]) 

    print "\nsync_server options:"
    print "\n".join([ "%s=%s" % (k,v) for k,v in sync_server_param.items()]) 

    print "\nspawner options:"
    print "\n".join([ "%s=%s" % (k,v) for k,v in spawner_param.items()]) 

if test == True:
    eb_app.insert(0, "echo")
    sync_server.insert(0, "echo")

try:
    json_file = open(str(spawner_param["config"]), "r")
except IOError:
    print "ERROR ", spawner_param["config"],  ": IOError"
    exit (1)
    
json_tree = json.load(json_file)
hosts_list = json_tree[spawner_param["infiniband"] and \
             "infiniband capabitilies" or "ethernet capabitilies"]["hosts"]

eb_app.append(spawner_param["eb_app"])

eb_app.extend(param_to_executable(eb_apps_param_def, eb_app_param))

num_ru = 1
num_bu = 1

if eb_app_param["num_ru_bu"] and (eb_app_param["num_bu"] or eb_app_param["num_ru"]):
    print "ERROR: multiple definition of RU/BU number"
    exit(1)
elif eb_app_param["num_ru_bu"]:
    num_ru = eb_app_param["num_ru_bu"]
    num_bu = num_ru
else:
    if(eb_app_param["num_ru"]):
        num_ru = eb_app_param["num_ru"]
    if(eb_app_param["num_bu"]):
        num_bu = eb_app_param["num_bu"]
    
clean_hosts(hosts_list, spawner_param["eb_app"])

sync_server.append(spawner_param["sync_server"])

sync_server.extend(param_to_executable(sync_server_param_def, sync_server_param))

sync_server.extend(["-B", str(num_bu), "-R", str(num_ru)])

sync_server[sync_server.index("<host>")] = str(json_tree["sync server ip"])

if (verbosity >= 1):
    print "spawning sync server on:", str(json_tree["sync server ip"])

subprocess.Popen(sync_server)

process_list = []
if (verbosity >= 1):
    print "spawning EML, EMC, BUs and RUs on first node:", hosts_list[0]

eb_app_host_index = eb_app.index("<host>")
for num_proc in range(0,num_ru+num_bu+2):
    eb_app[eb_app_host_index] = hosts_list[0]
    process_list.append(subprocess.Popen(eb_app))

for host in hosts_list[1:]:
    if (verbosity >= 1):
        print "spawning BUs and RUs on ", host
    for num_proc in range(0,num_ru+num_bu):
        tmp_list = list(eb_app)
        eb_app[eb_app_host_index] = host
        process_list.append(subprocess.Popen(eb_app))

signal.signal(signal.SIGINT, signal_handler)
signal.pause()
