#!/usr/bin/python

import subprocess
import group_argparse
import sys
import signal
import paramiko
import re
import json
try:
    from subprocess import DEVNULL # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')    

  
class Config_generator(object):


    gen_config_param_def = {"machine"       :("-m", "machine file", str, None ,None),
                            "ethernet"      :("-e", "ethernet host output file", str, None, None),
                            "infiniband"    :("-i", "infiniband host output file", str, None, None),
                            "interfaces"    :("-f", "ethernet interfaces list csv", str, None, "p2p1,p2p2,eth1,eth2"),
                            "verbosity"     :("-v", "increase verbosity", int, "count", 0),
                            "log_meta"      :("-l", "enable/disable metadata log", bool, None, False),
                            "log_fc"        :("-c", "enable/disable fragment composition log", bool, None, False),
                            "port"          :("-p", "starting port", int, None, 7000),
                            "speed"         :("-s", "minimum speed", int, None, 40000),
                            "sync_addr"     :("-a", "sync server addr", str, None, ""),
                            "sync_port"     :("-y", "sync server port", str, None, "8001"),
                            "pub_port"      :("-u", "publisher port", str , None, "8002"),
                            "min_mem"       :("-M", "minimum required memory", int , None, 1024 * 1024 * 1024 ),
                            "output_file"   :("-o", "json ouput file", str , None, "autogen.cfg" ),
                            "log_path"      :("-L", "log files base directory", str, None, "logs/"),
                           }

    json_single_attr = [ ("number of nodes", "ssh_connections.__len__"),
                         ("number of infiniband nodes", "infiniband_hosts.__len__"),
                         ("number of ethernet nodes", "ethernet_hosts.__len__"),
                         ("log fragment composition", "gen_config_param.get", "log_fc"),
                         ("log fragment composition", "gen_config_param.get", "log_fc"),
                         ("log metadata" , "gen_config_param.get", "log_meta"),
                         ("starting port", "gen_config_param.get", "port"),
                         ("data buffer size", "calculate_data_buffer_size"),
                         ("meta buffer size", "calculate_meta_buffer_size"),
                         ("sync server ip", "gen_config_param.get", "sync_addr"),
                         ("sync server port", "gen_config_param.get", "sync_port"),
                         ("pub server port", "gen_config_param.get", "pub_port"),
                         ("log path", "gen_config_param.get", "log_path"),
                       ]

    max_data_buffer = 1024 * 1024 * 1024
    max_meta_buffer = 256 * 1024 * 1024

    def __init__(self, param=None):
        object.__init__(self)
        self.ssh_connections = {}
        self.json_root = {}
        self.gen_config_param = param
        self.min_memory = sys.maxint
        self.infiniband_hosts = []
        self.ethernet_hosts = []


    def print_config(self):
        try:
            output_file = open(str(self.gen_config_param["output_file"]), "w")
        except IOError:
            print "ERROR ", self.gen_config_param["output_file"],  ": IOError"
            exit (1)
        else :
            output_file.writelines(json.dumps(self.json_root, indent=4, sort_keys=True))
        finally:
            output_file.close()

    def print_ib_hosts(self):
        if self.gen_config_param["infiniband"]:
            try:
                output_file = open(str(self.gen_config_param["infiniband"]), "w")
            except IOError:
                print "ERROR ", self.gen_config_param["infiniband"],  ": IOError"
                exit (1)
            else :
                output_file.writelines("\n".join(self.infiniband_hosts))
            finally:
                output_file.close()

    def print_eth_hosts(self):
        if self.gen_config_param["ethernet"]:
            try:
                output_file = open(str(self.gen_config_param["ethernet"]), "w")
            except IOError:
                print "ERROR ", self.gen_config_param["ethernet"],  ": IOError"
                exit (1)
            else :
                output_file.writelines("\n".join(self.ethernet_hosts))
            finally:
                output_file.close()

    def gen_config(self):
        self._connect_to_hosts()
        self._get_min_memory()
        self._get_infiniband()
        self._get_ethernet()
        self.add_json_attr([(name_value[0], self.string_value_to_variable(name_value[1]), name_value[2:]) 
                for name_value in Config_generator.json_single_attr])
        
    def string_value_to_variable(self, string):
        name_list = string.split(".")
        return self.names_list_to_variable(self, name_list)

    def names_list_to_variable(self, base, name_list):
        if (len(name_list) == 1):
            name = name_list.pop(0)
            return hasattr(base, name) and getattr(base, name) or None
        elif (len(name_list) == 0):
            return None
        else:
            name = name_list.pop(0)
            return self.names_list_to_variable(hasattr(base, name) and getattr(base, name) or None, name_list)


    def add_json_attr(self, attr_value_list):
        for name_value_args in attr_value_list:
            if callable(name_value_args[1]):
                if (name_value_args[2]) :
                    self.json_root[name_value_args[0]] = name_value_args[1](name_value_args[2][0])
                else:
                    self.json_root[name_value_args[0]] = name_value_args[1]()
            else:
                self.json_root[name_value_args[0]] = name_value_args[1]

    def _load_machine_file(self):
        try:
            machine_file = open(str(self.gen_config_param["machine"]), "r")
        except IOError, e:
            print "ERROR ", self.gen_config_param["machine"],  ":", e
            exit (1)
        else :
            hosts = [k.strip() for k in machine_file]
        finally:
            machine_file.close()
        return hosts

    def _connect_to_hosts(self):
        for host in self._load_machine_file():
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            try:
                ssh.connect(host)
            except:
                print "WARNING", host, "unreachable skipping"
            else:
                self.ssh_connections[host] = ssh

    def _get_min_memory(self):
        for host, connection in self.ssh_connections.items():
            stdin, stdout, stderr = connection.exec_command("/usr/bin/free -b")
            match = self._parse_out('-/\+\\s+buffers/cache:\\s+[0-9]+\\s+([0-9]+)', stdout.read())
            if (match != None):
                mem = int(match[0])
                if (mem >= self.gen_config_param["min_mem"]):
                    if (mem < self.min_memory):
                        self.min_memory = mem
                else:
                    print "WARNING", host, ": not enough memory skipping"
                    connection.close()
                    del self.ssh_connections[host]

    def calculate_meta_buffer_size(self):
        meta_buffer = self.min_memory / 20
        if (meta_buffer > Config_generator.max_meta_buffer):
            meta_buffer = Config_generator.max_meta_buffer

        return meta_buffer

    def calculate_data_buffer_size(self):
        data_buffer = self.min_memory / 5
        if (data_buffer > Config_generator.max_data_buffer):
            data_buffer = Config_generator.max_data_buffer

        return data_buffer

    def _get_infiniband(self):
        infiniband_section = {}
        for host, connection in self.ssh_connections.items():
            stdin, stdout, stderr = connection.exec_command("/usr/sbin/ibstat")
            output = stdout.read()
            port_state_match = self._parse_out('Physical state:\\s+([A-Za-z]+)', output)
            link_layer_match = self._parse_out('Link layer:\\s+([A-Za-z]+)', output)
            port_rate_match = self._parse_out('Rate:\\s+([0-9]+)', output)
            stdin, stdout, stderr = connection.exec_command("/usr/bin/ibdev2netdev")
            output = stdout.read()
            net_dev_match = self._parse_out('>\\s+([A-Za-z0-9]+)\\s+', output)
            interface_index = 0
            ip = []
            interfaces = []
            for port_state, link_layer, port_rate in zip(port_state_match,link_layer_match, port_rate_match):
                if ((link_layer == "InfiniBand") and (port_state == "LinkUp") and 
                        (int(port_rate)*1000 >= self.gen_config_param["speed"])):
                    stdin, stdout, stderr = connection.exec_command("/sbin/ip -o -4 addr list " + net_dev_match[interface_index])
                    ip.extend(self._parse_out('inet\\s+([0-9]{1,3}(?:\.[0-9]{1,3}){3})/', stdout.read()))
                    interfaces.append(net_dev_match[interface_index])

                interface_index += 1
            
            if (len(interfaces) > 0):
                host_section = {}
                host_section["number of ports"] = len(interfaces)
                host_section["interfaces list"] = interfaces
                host_section["ip list"] = ip
                infiniband_section[host]=host_section
                self.infiniband_hosts.append(host)
        infiniband_section["hosts"] = self.infiniband_hosts
        self.json_root["infiniband capabitilies"] = infiniband_section

    def _get_ethernet(self):
        ethernet_section = {}
        for host, connection in self.ssh_connections.items():
            ip = []
            interfaces = []

            for interface in self.gen_config_param["interfaces"].split(','):
                speed_match = []
                link_match = []
                stdin, stdout, stderr = connection.exec_command("sudo ethtool " + interface)
                output = stdout.read()
                speed_match.extend(self._parse_out('Speed:\\s+([0-9]+)', output))
                link_match.extend(self._parse_out('Link detected:\\s+([A-Za-z]+)', output))
                for port_speed, link in zip(speed_match,link_match):
                    if self.gen_config_param["verbosity"] >= 1:
                        print "port speed", port_speed, "link", link
                    if ((int(port_speed) >= self.gen_config_param["speed"]) and (link == "yes")):
                        stdin, stdout, stderr = connection.exec_command("/sbin/ip -o -4 addr list " + interface)
                        tmp_it = self._parse_out('inet\\s+([0-9]{1,3}(?:\.[0-9]{1,3}){3})/', stdout.read())
                        if (tmp_it):
                            ip.extend(tmp_it)
                            interfaces.append(interface)
            if (ip):
                host_section = {}
                host_section["number fo ports"] = len(ip)
                host_section["interfaces list"] = interfaces
                host_section["ip list"] = ip
                ethernet_section[host]=host_section
                self.ethernet_hosts.append(host)

        ethernet_section["hosts"] = self.ethernet_hosts
        self.json_root["ethernet capabitilies"] = ethernet_section

    def _parse_out(self, regexpr, lines):
        pattern = re.compile(regexpr)
        match_list = pattern.findall(lines)
        return match_list


    def print_param(self):
        print "gen_config options:"
        print "\n".join([ "%s=%s" % (k,v) for k,v in self.gen_config_param.items()]) 

if __name__ == '__main__':
    config_gen = Config_generator()
    parser = group_argparse.GroupArgparser()

    parser["gen_config"] = Config_generator.gen_config_param_def

    parser.parse_args()


    config_gen.gen_config_param = parser.get_group_args("gen_config")

    if config_gen.gen_config_param["verbosity"] >= 2:
        config_gen.print_param()

    config_gen.gen_config()
    config_gen.print_config()
    config_gen.print_ib_hosts()
    config_gen.print_eth_hosts()

