
# Run options

DAQPIPE is made to be as simple as possible, yet providing a full Event Building functionality.

The following are some run configurations to achieve different goals, namely check things work and get the best performance. Since DAQPIPE is crafted flexibly to run across very various setups, several examples may be provided for each transport.

## MPI examples

In the case of MPI, a machinefile with the names of the machines where the software is going to run, in a \n separated manner, is required as with any MPI program.

A configuration with a variable number of Readout Units and Builder Units per node is possible. In order to achieve best performance, several BUs / RUs are suggested per node. Depending on the setup, one may want to specify up to 10 BUs and up to 4 RUs to ensure the highest throughput is achieved. The recommended message size is around 10 MB [-s $((10*1024))], and the number of credits per instance (-c) works best with 2-5. At the moment, at least 3 nodes are required (one is dedicated for the Event Managers).

For more info, check the website https://lbdokuwiki.cern.ch/doku.php?id=upgrade:daqpipe

Note: All examples assume the app is built on the build folder

### The simplest MPI run example

    mpirun --map-by ppr:2:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/build/eb_app  -f 100000 -s $((10*1024)) -c 2

### A simple MPI run cleaning leftover processes beforehand

    ./config/clean.sh <machine_file>; mpirun --map-by ppr:2:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/build/eb_app  -f 100000 -s $((10*1024)) -c 2

### MPI with TCP transport

    mpirun --map-by ppr:2:node -mca btl tcp,self --mca btl_tcp_if_include <subnet/submask> (eg. 192.168.0.0/16) -machinefile <machine_file> `pwd`/mpilaunch_debug.sh xterm -sl 10000 -e gdb -x `pwd`/gdb_run.command --args `pwd`/build/eb_app -f 100000 -s $((10*1024)) -c 2

### Debug mode

    mpirun --map-by ppr:2:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/mpilaunch_debug.sh xterm -sl 10000 -e gdb -x `pwd`/gdb_run.command --args `pwd`/build/eb_app -s $((10*1024)) -c 1 -f 1

### Good performance on EDR setup

    mpirun --map-by ppr:20:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/build/eb_app  -f 100000 -s $((10*1024)) -c 5 -B 10 -R 10

## Verbs examples

### json config file generator script you need sudo rights on the hosts

    ./config/gen_config.py -m <machine_file> [-v -p <starting port> -l <log metadata (0,1)> -c <log FC (0,1)> -e <ethernet_hosts_out_file> -i <ib_hosts_out_file> -f <ethernet interfaces list csv> -s <minimum speed Mb/s>]

### example using the zmq sync server and spawner script (-e for ethernet -i for infiniband)

    ./run/gen_config.py -s 40000 -m config/labs_09_10.labs -f eth1,eth2,eth3 -o config/autogen_config.cfg
./run/spawner.py -e -y build_tcp/zmq_sync_server -S -s $((1024*10)) -f 100000 -d 3 -a config/autogen_config.cfg -x build_tcp/eb_app

    # press ctrl+c to exit the script automatically cleans the nodes before and after the execution
