/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef LIBFABRIC_CLIENT_ENDPOINT_HPP
#define LIBFABRIC_CLIENT_ENDPOINT_HPP

/********************  HEADERS  *********************/
//std
#include <map>
#include <list>
//files from libfabric
#include <rdma/fabric.h>
#include <rdma/fi_errno.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_rma.h>
//to implement
#include "HydraLauncher.hpp"

namespace LibFabric
{

/********************  STRUCT  **********************/
struct RDMAContext
{
	void * ptr;
	size_t size;
	fi_context * request;
	uint64_t tag;
};

/*********************  TYPES  **********************/
typedef std::map<uint64_t,RDMAContext> RDMAContextMap;
typedef std::list<RDMAContext> RDMAPendingList;

/*********************  CLASS  **********************/
/**
 * Provide an abstraction to hide the low level function from libfabric to manipulate endpoints.
**/
class  Endpoint
{
	public:
		Endpoint(int queueSize = -1);
		~Endpoint(void);
		void setQueueSize(int size);
		int connect(int localRank,fid_fabric * fabric,fid_domain * domain,fi_info * fi);
		int accept(int localRank,fid_fabric * fabric,fid_domain * domain,fi_info * fi);
		void sendMsg( void* ptr, size_t size, fid_mr* mr, int targetRank, fi_context * context, bool wait );
		void recvMsg( void* ptr, size_t size, fid_mr* mr, int targetRank, fi_context * context, bool wait );
		void recvTag( void* ptr, size_t size, fid_mr* mr, int targetRank, int tag, fi_context * context, bool wait );
		void sendTag( void* ptr, size_t size, fid_mr* mr, int targetRank, int tag, fi_context * context, bool wait );
		void recvRDMA( int targetRank, int tag, void* ptr, size_t size, fi_context * request );
		void writeRDMA( int targetRank, int tag, fid_mr * mr, void* sourcePtr, size_t size, void* remotePtr, uint64_t remoteKey, fi_context* request);
		bool active(void);
		void import(Endpoint & endpoint);
		int setupAddressVector( fid_fabric* fabric, fid_domain* domain, fi_info* fi, HydraLauncher& launcher );
		int setupRDM( fid_fabric* fabric, fid_domain* domain, fi_info* fi ,HydraLauncher & launcher);
		fi_context * testQueue(bool recv,bool send);
		void checkRDMAPending( int targetRank, int tag, void* ptr, size_t size, fi_context* request );
	private:
		fi_context * getContext(fi_cq_data_entry & comp);
		Endpoint(const Endpoint & endpoint);//copy if forbidden
		int allocRessources(fid_fabric * fabric,fid_domain * domain, fi_info * fi);
		int allocEventQueue(fid_fabric * fabric);
		int bindRessources(bool useEventQueue);
		void freeRessources(void);
		int fillAddressVector( HydraLauncher& launcher );
		bool recvRDMACheckPending( LibFabric::RDMAContext& context, fi_context* request );
		fi_context * testPending(void);
	private:
		/** Receive queue attached the the endpoint. **/
		fid_cq * recvQueue;
		/** Send queue attached the the endpoint. **/
		fid_cq * sendQueue;
		/** Endpoint representation from libfabric. **/
		fid_ep * endpoint;
		/** Event queue used to track connect/accept events. **/ 
		fid_eq * eventQueue;
		/** Address vector attached to the endpoint (only for RDM mode). **/
		fid_av * addressVector;
		/** Array of remote addresses to fill the address vector. **/
		fi_addr_t * remoteFiAddr;
		/** Configure the size of the queues. **/
		int queueSize;
		/** Counter to track current onfly requests in send queue. **/
		int cntOnSend;
		/** Counter to track current onfly requests in recv queue. **/
		int cntOnRecv;
		/** Context map to match RDMA ack to requests. **/
		RDMAContextMap rdmaContextMap;
		/** List of pending rdma ACK recieved before getting the recvRDMA posted (might not apppend but make code safer) **/
		RDMAPendingList rdmaPending;
		/** Keep trak of the local rank ID to compute RDMA tags. **/
		int localRank;
};

}

#endif //LIBFABRIC_CLIENT_ENDPOINT_HPP
