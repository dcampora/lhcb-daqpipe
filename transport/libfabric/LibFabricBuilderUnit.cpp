/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "../../common/Debug.hpp"
#include "LibFabricBuilderUnit.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric 
{

/*******************  FUNCTION  *********************/
void BuilderUnit::initialize ( int initialCredits )
{
	//setup locals
	this->initialCredits = initialCredits;
	this->driver = &gblConfiguration->driver;
	
	//check
	assumeArg(initialCredits < LIBFABRIC_MAX_CREDITS,"You request too much credits (%1), max alowed is %2")
		.arg(initialCredits)
		.arg(LIBFABRIC_MAX_CREDITS)
		.end();
	
	//fetch localy some values
	int cntRU = LibFabric::gblConfiguration->numTotalReadouts;
	
	//setup requests for data exchanges & frgament descriptor requests
	for (int i=0; i<initialCredits; ++i)
	{
		//allocate
		fragmentDescriptorRequests[i] = new Request[cntRU];
		dataRequests[i] = new Request[cntRU * (MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS)];
		dataPointerRequests[i] = new Request[cntRU * (MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS)];
		dataRemoteIOV[i] = new RemoteIOV[cntRU * (MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS)];
		requestReadoutIndex[i] = new int[gblConfiguration->numTotalReadouts];
		memset(requestReadoutIndex[i],0,sizeof(int)*gblConfiguration->numTotalReadouts);
		activeRequests[i] = -1;
		currentFragmentRequest[i] = 0;
		activeFragments[i] = NULL;
		requestIndex[i] = 0;
		transmissionSize[i] = 0;
	}
	
	//post recv of exit request
	static char buffer[] = "exit";
	driver->registerSegment(buffer,sizeof(buffer),FI_REMOTE_WRITE);
	driver->recvTag(RANK_EM_CONSUMMER,TAG_EXIT_SIGNAL,buffer,sizeof(buffer),&exitRequest,false);
}

/*******************  FUNCTION  *********************/
BuilderUnit::~BuilderUnit ( void )
{
	for (int i = 0 ; i < initialCredits ; i++)
	{
		delete [] fragmentDescriptorRequests[i];
		delete [] dataRequests[i];
		delete [] dataPointerRequests[i];
		delete [] requestReadoutIndex[i];
	}
}

/*******************  FUNCTION  *********************/
bool BuilderUnit::checkExitMessage ( void )
{
	return driver->testAny(&exitRequest,1) == 0;
}

/*******************  FUNCTION  *********************/
bool BuilderUnit::prepareReceiveEventComplete ( int& fragmentReceived )
{
	// Wait for any fragment to arrive
	// DEBUG << "MPI_transport::BuilderUnit: prepareReceiveEvent waiting on all..." << std::endl;
	fragmentReceived = driver->testAny(fragmentDescriptorRequests[currentSlot],gblConfiguration->numTotalReadouts);

	if (fragmentReceived != -1) {
		// It must be coming from the same event,
		// or it must be the first we take
		int currentEvent = (*(activeFragments[currentSlot] + fragmentReceived)).eventID;

		if (activeRequests[currentSlot] == -1 ||
			activeRequests[currentSlot] == currentEvent){
			
			DAQ_DEBUG_ARG("libfabric","BuilderUnit: Received fragment %1").arg(fragmentReceived).end();
			activeRequests[currentSlot] = currentEvent;
			return true;
		}
	}

	return false;
}

/*******************  FUNCTION  *********************/
void BuilderUnit::registerSegment ( void* ptr, size_t size )
{
	assert(driver != NULL);
	assert(ptr != NULL);
	assert(size > 0);
    driver->registerSegment(ptr,size,FI_REMOTE_WRITE);
}

/*******************  FUNCTION  *********************/
void BuilderUnit::prepareReceiveEvent ( FragmentComposition* fc )
{
	// Find a slot
	int slot;
	for (slot=0 ; slot<initialCredits ; ++slot)
		if (activeRequests[slot] == -1) break;

	//check
    if (slot == initialCredits)
	{
		DAQ_WARNING("LibFabricBuilderUnit: prepareReceiveEvent can't listen because there are no free slots. ");
		return;
	}
	DAQ_DEBUG_ARG("libfabric","BuilderUnit: prepareReceiveEvent listening on slot %1").arg(slot).end();

	//push to list
	activeRequestsList.push_back(slot);
	currentFragmentRequest[slot] = 0;
	activeFragments[slot] = fc;

	//push all isend
	for (int i=0; i < gblConfiguration->numTotalReadouts ; ++i)
	{
		DAQ_DEBUG_ARG("libfabric","BuilderUnit: prepareReceiveEvent RU=%1, Rank=%2, ID=%3")
			.arg(gblConfiguration->readoutID(i))
			.arg(gblConfiguration->rankID)
			.arg(0)
			.end();
       
		//post send
		driver->recvTag(gblConfiguration->readoutID(i),TAG_EVENT_PREPARE,fc+i,sizeof(FragmentComposition),&fragmentDescriptorRequests[slot][i],false);
	}

	// inactiveSlots.push_back(slot);
	currentSlot = slot;
}

/*******************  FUNCTION  *********************/
void BuilderUnit::discardEvent()
{
	int slot;
	std::list<int>::iterator it = activeRequestsList.begin();

	DAQ_DEBUG_ARG("libfavric","BuilderUnit: Event %1 discarded").arg(*it).end();
	
	// Cleanup
	slot = *it;
	activeRequestsList.erase(it);
	activeRequests[slot] = -1;
	requestIndex[slot] = 0;
	transmissionSize[slot] = 0;
	for (int i=0; i<initialCredits; ++i)
		requestReadoutIndex[slot][i] = 0;
}

/*******************  FUNCTION  *********************/
bool BuilderUnit::receiveEvent ( int& finishedEventID, int& tSize )
{
	std::list<int>::iterator it;
	bool eventReceived;
	int slot;

	//check
	for (it = activeRequestsList.begin(); it != activeRequestsList.end(); it++)
	{
		eventReceived = driver->testAll(dataRequests[*it],requestIndex[*it]);
		if (eventReceived)
			break;
	}

	//if get some make progress
	if (eventReceived)
	{
		//cleanup pointer requests
		while(driver->testAll(dataPointerRequests[*it],requestIndex[*it])){};
		
		//some debug
		DAQ_DEBUG("libfabric","BuilderUnit: Event received!");
		
		//setup answer
		slot = *it;
		finishedEventID = activeRequests[slot];
		tSize = transmissionSize[slot];

		//reset state
		activeRequestsList.erase(it);
		activeRequests[slot] = -1;
		requestIndex[slot] = 0;
		transmissionSize[slot] = 0;
		
		//reset states
		for (int i=0; i<gblConfiguration->numTotalReadouts; ++i)
			requestReadoutIndex[slot][i] = 0;
		return true;
	}

	return false;
}

/*******************  FUNCTION  *********************/
void BuilderUnit::receiveSizeAndID ( int& eventSize, int& eventID )
{
	DAQ_DEBUG_ARG("libfabric","BuilderUnit: receiveSizeAndID, source=%1, rank=%2, id=%3")
		.arg(gblConfiguration->rankID)
		.arg(RANK_EM_CONSUMMER)
		.arg(TAG_EVENT_ASSIGN)
		.end();

	//post recv and wait it
	driver->recv(RANK_EM_CONSUMMER,size_id,sizeof(size_id),NULL,true);
    
	eventSize = size_id[0];
	eventID = size_id[1];
}

/*******************  FUNCTION  *********************/
void BuilderUnit::sendCredits ( int& numberOfCredits )
{
	DAQ_DEBUG_ARG("libfabric","BuilderUnit: sendCredits %1 credits, source=%2, rank=%3, id=%4")
		.arg(numberOfCredits)
		.arg(gblConfiguration->rankID)
		.arg(RANK_EM_LISTENER)
		.arg(TAG_CREDIT_ANNOUNCE)
		.end();

	ListenerMsg msg;
	msg.credits = numberOfCredits;
	msg.sourceRank = gblConfiguration->rankID;
	driver->sendTag(RANK_EM_LISTENER,TAG_EVENT_CREDIT,&msg,sizeof(msg),NULL,true);
}

/*******************  FUNCTION  *********************/
void BuilderUnit::setupReceiveEvent ( char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype )
{
	// Find assigned slot
	int slot;
	for (slot=0; slot<initialCredits; ++slot)
		if (activeRequests[slot] == eventID) break; // Slot found! :D
		
	//check errors
	if (slot == initialCredits)
	{
		DAQ_ERROR("LibFabricBuilderUnit: setupReceiveEvent: No slot was found");
		return;
    }

	// Event fragment packet description
	// 
	// 01x-xyyyy
	// ^        reserved
	//  ^       packet type (1 - event fragment)
	//   ^      event id (18 bits)
	//      ^   fragment id (4 bits)

	int packet_type = 1;
	int event_id = eventID & 0x3FFFF;
	int fragment_id = requestReadoutIndex[slot][readoutIndex] & 0xF;
	int id = (packet_type << 30) | (event_id << 4) | fragment_id;

    DAQ_DEBUG_ARG("libfabric","BuilderUnit: Irecv: rank=%1, source=%2, id=%3, size=%4")
		.arg(gblConfiguration->rankID)
		.arg(gblConfiguration->readoutID(readoutIndex))
		.arg(id)
		.arg(size)
		.end();
	
	//ensure they are finished so unlikely to reuse one
	while(driver->testAll(dataPointerRequests[slot],requestIndex[slot])){};
	
	//getIOV
	dataRemoteIOV[slot][requestIndex[slot]] = driver->getIOV(dataPointer,size);
	driver->sendTag(gblConfiguration->readoutID(readoutIndex),id,&dataRemoteIOV[slot][requestIndex[slot]],sizeof(dataRemoteIOV[slot][requestIndex[slot]]),&dataPointerRequests[slot][ requestIndex[slot] ],false);
	DAQ_DEBUG_ARG("libfabric","Send remote IOV : addr=%1, key=%2").arg(dataRemoteIOV[slot][requestIndex[slot]].addr).arg(dataRemoteIOV[slot][requestIndex[slot]].key).end();
	
	//wait RDMA write to be finished
	driver->recvRDMA(gblConfiguration->readoutID(readoutIndex),id,dataPointer,size,&dataRequests[slot][ requestIndex[slot] ],false);

	requestIndex[slot]++;
	requestReadoutIndex[slot][readoutIndex]++;

	if (datatype == Core::DATA)
		transmissionSize[slot] += size;
}

/*******************  FUNCTION  *********************/
void BuilderUnit::pullRequest ( int* size_id_bu, int& readoutno )
{
	DAQ_DEBUG_ARG("libfabric","BuilderUnit: pullRequest source=%1, Rank=%2, ID=%3")
			.arg(gblConfiguration->rankID)
			.arg(gblConfiguration->readoutID(readoutno))
			.arg(TAG_EVENT_PULL_REQUEST)
			.end();

	driver->sendTag(gblConfiguration->readoutID(readoutno),TAG_EVENT_PULL_REQUEST,size_id_bu,3,NULL,true);
}

}
