/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include "../LibFabricHelper.hpp"
#include "../LibFabricCore.h"
#include "../../config/ConfigurationCore.h"

/*******************  NAMESPACE  ********************/
using namespace LibFabric;
using namespace std;

/********************  GLOBALS  *********************/
namespace Core { Core::ConfigurationLoader* Configuration = new LibFabric::ConfigurationLoader(); }

/********************  GLOBALS  *********************/
const char CST_STRING_1[] = "100AABACADAEAFAGAHAIAJAKALAMANAOAPAABBBCBDBEBFBGBHBIBJBKBLBMBNBOBPBACBCCCDCECFCGCHCICJCKCLCMCNCOCPCADBDCDDDEDFDGDHDIDJDKDLDMDNDODPDAEBECEDEEEFEGEHEIEJEKELEMENEOEPEAFBFCFDFEFFFGFHFIFJFKFLFMFNFOFPFAGBGCGDGEGFGGGHGIGJGKGLGMGNGOGPGAHBHCHDHEHFHGHHHIHJHKHLHMHNHOHPHAIBICIDIEIFIGIHIIIJIKILIMINIOIPIAJBJCJDJEJFJGJHJIJJJKJLJMJNJOJPJAKBKCKDKEKFKGKHKIKJKKKLKMKNKOKPKALBLCLDLELFLGLHLILJLKLLLMLNLOLPLAMBMCMDMEMFMGMHMIMJMKMLMMMNMOMPMANBNCNDNENFNGNHNINJNKNLNMNNNONPNAOBOCODOEOFOGOHOIOJOKOLOMONOOOPOAPBPCPDPEPFPGPHPIPJPKPLPMPNPOPPP";
const unsigned char CST_HW_ADDR_1[] = { 0x2,0x0,0x82,0x16,0x89,0x8a,0xd4,0xbd,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0 };

/*******************  FUNCTION  *********************/
TEST(LibFabricHelper,convertAddrToHex)
{
	unsigned char data[256];
	for (int i = 0 ; i < 256 ; i++)
		data[i] = i;

	string res = Helper::convertAddrToHex(data,sizeof(data));

	EXPECT_EQ(CST_STRING_1,res);
}

/*******************  FUNCTION  *********************/
TEST(LibFabricHelper,convertAddrFromHex)
{
	unsigned char data[256];

	Helper::convertAddrFromHex(data,sizeof(data), CST_STRING_1);

	for (int i = 0 ; i < 256 ; i++)
		EXPECT_EQ((int)i,(int)data[i]);
}

/*******************  FUNCTION  *********************/
TEST(LibFabricHelper, convertAndConvertBack)
{
	unsigned char data1[] = {1,5,10,55,22,33,35,37,14,155,201};
	unsigned char data2[] = {0,0, 0, 0, 0, 0, 0, 0, 0,  0,  0};
	string res = Helper::convertAddrToHex(data1,sizeof(data1));	
	Helper::convertAddrFromHex(data2,sizeof(data2), res);
	for (int i = 0 ; i < sizeof(data1) ; i++)
		EXPECT_EQ((int)data1[i],(int)data2[i]);
}

/*******************  FUNCTION  *********************/
TEST(LibFabricHelper, buggy_case_1)
{
	unsigned char data2[sizeof(CST_HW_ADDR_1)];
	string res = Helper::convertAddrToHex(CST_HW_ADDR_1,sizeof(CST_HW_ADDR_1));	
	Helper::convertAddrFromHex(data2,sizeof(CST_HW_ADDR_1), res);
	for (int i = 0 ; i < sizeof(CST_HW_ADDR_1) ; i++)
		EXPECT_EQ((int)CST_HW_ADDR_1[i],(int)data2[i]);
}
