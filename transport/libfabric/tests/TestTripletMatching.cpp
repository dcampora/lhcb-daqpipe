/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include "../LibFabricCore.h"
#include "../LibFabricDriver.h"
#include "../HydraLauncher.hpp"
#include "../../../common/Debug.hpp"
#include "../../config/ConfigurationCore.h"

/********************  MACROS  **********************/
#define SIZE 128
#define REPEAT 1000

/*******************  NAMESPACE  ********************/
using namespace LibFabric;

/********************  GLOBALS  *********************/
namespace Core { Core::ConfigurationLoader* Configuration = new LibFabric::ConfigurationLoader(); }

/********************  STRUCT  **********************/
struct Message
{
	int sourceRank;
	int targetRank;
};

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//setup launcher
	HydraLauncher launcher;
	launcher.initilize();
	
	//setup config
	Json::Value config;
	//load from file
	if (argc == 2)
	{
		char * file_name = argv[1];
		Json::Reader file_reader;
		std::ifstream config_file;
		config_file.open(file_name);
		bool ret_val;
		if(!config_file.is_open()){
			ret_val = false;
			ERROR << "Core::ConfigurationLoader::loadConfigFile: " << file_name << ": Error opening config file" << std::endl;
		}else{
			ret_val = file_reader.parse(config_file, config);
			if (!ret_val)
				ERROR << "Fail to load config" << std::endl;
		}
	}
	
	//setup driver
	Driver driver;
	driver.initialize(config,launcher);
	
	//build buffer
	char buffer[SIZE];
	
	//get rank and size
	int rank = launcher.getRank();
	assumeArg(launcher.getWorldSize() >= 3 && launcher.getWorldSize() < 1024,"This test only work with at least 3 processes, get %1.").arg(launcher.getWorldSize()).end();
	
	//setup connection
	printf("[%d] Setup connection\n",rank);
	if (!driver.useRDMMode())
	{
		if (rank == 0)
			driver.waitConnection();
		else 
			driver.setupConnexion(0,launcher);
	}
	printf("[%d] Connection established\n", rank);
	
	//datas
	Request request[1024];
	Message message[1024];
	Message m;
	
	//initial setup, 2 post recv
	if (rank == launcher.getWorldSize() - 1)
		for (int i = 0 ; i < launcher.getWorldSize() - 1 ; i++)
			driver.recv(i,&message[i],sizeof(message[i]),&request[i],false);
	
	//run each
	if (rank == launcher.getWorldSize() - 1)
	{
		for (int i = 0 ; i < REPEAT ; i++)
		{
			//wait any
			int id = driver.waitAny(request,launcher.getWorldSize() - 1);

			//check
			assumeArg(id >= 0 && id < 2,"Get invalid ID : %1").arg(id).end();
			assumeArg(launcher.getWorldSize() - 1 == message[id].targetRank,
				"Get matching error, get %1 instead of %2")
				.arg(message[id].targetRank)
				.arg(launcher.getWorldSize() - 1)
				.end();
			assumeArg(id == message[id].sourceRank,
				"Get matching error, get %1 instead of %2")
				.arg(message[id].sourceRank)
				.arg(id)
				.end();

			//republish
			driver.recv(id,&message[id],sizeof(message[id]),&request[id],false);
		}
	} else {
		//setup
		m.sourceRank = launcher.getRank();
		m.targetRank = launcher.getWorldSize() - 1;
		
		//send to last node
		for (int i = 0 ; i < REPEAT ; i++)
			driver.send(launcher.getWorldSize() - 1,&m,sizeof(m),NULL,true);
	}
	
	//finish
	driver.finalize();
	launcher.finalize();
	
	//exit
	return EXIT_SUCCESS;
}
