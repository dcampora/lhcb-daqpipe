/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include "../LibFabricCore.h"
#include "../LibFabricHelper.hpp"
#include "../LibFabricDriver.h"
#include "../HydraLauncher.hpp"
#include "../../config/ConfigurationCore.h"

/********************  MACROS  **********************/
#define SIZE 128
#define REPEAT 1000

/*******************  NAMESPACE  ********************/
using namespace LibFabric;

/********************  GLOBALS  *********************/
namespace Core { Core::ConfigurationLoader* Configuration = new LibFabric::ConfigurationLoader(); }

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//setup launcher
	HydraLauncher launcher;
	launcher.initilize();

	//setup config
	Json::Value config;

	//load from file
	if (argc == 2)
	{
		char * file_name = argv[1];
		Json::Reader file_reader;
		std::ifstream config_file;
		config_file.open(file_name);
		bool ret_val;
		if(!config_file.is_open()){
			ret_val = false;
			ERROR << "Core::ConfigurationLoader::loadConfigFile: " << file_name << ": Error opening config file" << std::endl;
		}else{
			ret_val = file_reader.parse(config_file, config);
			if (!ret_val)
				ERROR << "Fail to load config" << std::endl;
		}
	}
	
	//names
	std::string hostname;
	std::string service;
	
	//from options
	if (argc == 3)
		hostname = argv[2];
	else if (argc >= 2 && config.get("iface","").asString() != "")
		hostname = Helper::getLocalIP(config.get("iface","").asString());
	
	//service from options
	if (argc == 4)
		service = argv[3];
	
	//infos
	printf("Use : hostname = '%s' and service = '%s'\n",hostname.c_str(),service.c_str());

	//setup driver
	if (launcher.getRank() == 0)
	{
		Driver driver;
		driver.printDomainsInfos(config,hostname,service);
		driver.finalize();
	}

	//finish
	launcher.finalize();

	//exit
	return EXIT_SUCCESS;
}

