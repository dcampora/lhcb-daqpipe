/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include "../LibFabricCore.h"
#include "../LibFabricDriver.h"
#include "../HydraLauncher.hpp"
#include "../../config/ConfigurationCore.h"

/********************  MACROS  **********************/
#define SIZE (128)
#define REPEAT 1000

/*******************  NAMESPACE  ********************/
using namespace LibFabric;

/********************  GLOBALS  *********************/
namespace Core { Core::ConfigurationLoader* Configuration = new LibFabric::ConfigurationLoader(); }

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//setup launcher
	HydraLauncher launcher;
	launcher.initilize();
	
	//setup config
	Json::Value config;
	if (argc >= 2)
		config["ep type"] = argv[1];
	
	//setup driver
	Driver driver;
	driver.initialize(config,launcher);
	
	//build buffer
	char * buffer = new char[SIZE];
	
	//get rank and size
	int rank = launcher.getRank();
	assumeArg(launcher.getWorldSize() == 2,"This test only work with exactly 2 processes, get %1.").arg(launcher.getWorldSize()).end();
	
	//setup connection
	printf("[%d] Setup connection\n",rank);
	if (!driver.useRDMMode())
	{
		if (rank == 0)
			driver.waitConnection();
		else 
			driver.setupConnexion(0,launcher);
	}
	printf("[%d] Connection established\n", rank);
	
	//register segment
	driver.registerSegment(buffer,SIZE,FI_WRITE|FI_REMOTE_WRITE);
	
	//exchange remote keys
	RemoteIOV localIov = driver.getIOV(buffer,SIZE);
	
	//exchange
	printf("[%d] Exchange IOV\n",rank);
	RemoteIOV remoteIov;
	Request req[2];
	driver.recv(rank==0?1:0,&remoteIov,sizeof(remoteIov),&req[0],false);
	driver.send(rank==0?1:0,&localIov,sizeof(localIov),&req[1],false);
	driver.waitAll(req,2);
	printf("[%d] Finish exchange IOV\n",rank);
	
	//do ping pong
	for (int i = 0 ; i < REPEAT ; i++)
	{
		if (rank == 0)
			printf("[%d] Loop %d\n",rank,i);
		if (rank == 0)
		{
			driver.rdmaWrite(1,i,buffer,SIZE,remoteIov.addr,remoteIov.key,NULL,true);
			driver.recvRDMA(1,i,buffer,SIZE,NULL,true);
		} else {
			driver.recvRDMA(0,i,buffer,SIZE,NULL,true);
			driver.rdmaWrite(0,i,buffer,SIZE,remoteIov.addr,remoteIov.key,NULL,true);
		}
	}
	
	//finish
	driver.finalize();
	launcher.finalize();
	
	delete [] buffer;
	
	//exit
	return EXIT_SUCCESS;
}
