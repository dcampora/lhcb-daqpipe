/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "../../common/Debug.hpp"
#include "LibFabricReadoutUnit.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*******************  FUNCTION  *********************/
void ReadoutUnit::initialize ( int initialCredits )
{
	this->initialCredits = initialCredits;
	
	DAQ_DEBUG_ARG("libfabric","ReadoutUnit init with credits = %1").arg(initialCredits).end();
	
	//check
	assumeArg(initialCredits < LIBFABRIC_MAX_CREDITS,"You request too much credits (%1), max alowed is %2")
		.arg(initialCredits)
		.arg(LIBFABRIC_MAX_CREDITS)
		.end();
	
	//init
	for (int i = 0 ; i < LIBFABRIC_MAX_CREDITS ; i++)
	{
		this->activeRequests[i] = -1;
		this->requestIndex[i] = 0;
	}
	
	//setup mem
	for (int i=0; i<initialCredits; ++i)
		requests[i] = new Request[MAX_RU_FRAGMENTS];
	
	//get driver
	assert(gblConfiguration != NULL);
	this->driver = &gblConfiguration->driver;
	
	//post bu requests
	buRequests = new Request[gblConfiguration->numTotalBuilders];
	pull_size_id_bu = new int[3*gblConfiguration->numTotalBuilders];
	for (int i = 0 ; i < gblConfiguration->numTotalBuilders ; i++)
	{
		DAQ_DEBUG("libfabric","ok");
		driver->recvTag(gblConfiguration->builderID(i),TAG_EVENT_PULL_REQUEST,&pull_size_id_bu[3*i],sizeof(int)*3,&buRequests[i],false);
	}

	//push exit wait message
	static char exitBUffer[5];
	driver->recvTag(RANK_EM_CONSUMMER,TAG_EXIT_SIGNAL,exitBUffer,sizeof(exitBUffer),&waitOrExitSignal[0],false);
}

/*******************  FUNCTION  *********************/
void ReadoutUnit::registerSegment ( void* ptr, size_t size )
{
    assert(driver != NULL);
	assert(ptr != NULL);
	assert(size > 0);
	driver->registerSegment(ptr,size,FI_WRITE);
}

/*******************  FUNCTION  *********************/
bool ReadoutUnit::EventListen ( int& size, int& eventID, int& destinationBU )
{
	//errors
	assert(driver != NULL);
	
	DAQ_DEBUG("libfabric","ReadoutUnit: Listening...");
	DAQ_DEBUG_ARG("libfabric","ReadoutUnit: source=%1, rank=%2, id=%3")
		.arg(RANK_EM_CONSUMMER)
		.arg(gblConfiguration->rankID)
		.arg(TAG_EVENT_REQUEST)
		.end();
    
	//post
	driver->recvTag(RANK_EM_CONSUMMER,TAG_EVENT_REQUEST,size_id_bu,sizeof(size_id_bu),&waitOrExitSignal[1],false);
	
	//wait
	int index = driver->waitAny(waitOrExitSignal,2);
	assert(index == 0 || index ==1);
    
	//if exit
	if (index == 0)
		return false;

	//setup return vars
	size = size_id_bu[0];
	eventID = size_id_bu[1];
	destinationBU = size_id_bu[2];

	//some debug
	DAQ_DEBUG("libfabric","ReadoutUnit: Received!");

	//return ok to continue
	return true;
}

/*******************  FUNCTION  *********************/
void ReadoutUnit::prepareSendEvent ( FragmentComposition*& fc, int destinationBU )
{
	//debug
	DAQ_DEBUG_ARG("libfabric","ReadoutUnit: destinationBU: %1")
		.arg(gblConfiguration->builderID(destinationBU))
		.end();

	//Debug
	DAQ_DEBUG_ARG("libfabric","ReadoutUnit: source=%1, rank=%2, id=%3")
		.arg(gblConfiguration->builderID(destinationBU))
		.arg(gblConfiguration->rankID)
		.arg(0)
		.end();

	// TODO: This is a memory leak.
	//       It's still not understood why.
	//       
	// operator= segfaults (?)
	// 
	// int requestno = MPI::Request::Waitany(initCredits, fragmentRequests);
	// fragmentRequests[requestno] = MPI::COMM_WORLD.Isend(&(fc[0]), sizeof(FragmentComposition), MPI::CHAR,
	//     MPICore::MPIConfig->builderID(destinationBU), shift + MPICore::EVENT_MESSAGES_SIZE + MPICore::EVENT_FRAGMENT_COMPOSITION);
	// 
	// 
	// Therefore, TODO: Make design with Isend and Irecv work.
	driver->sendTag(gblConfiguration->builderID(destinationBU),TAG_EVENT_PREPARE,fc,sizeof(FragmentComposition),NULL,true);
}

/*******************  FUNCTION  *********************/
void ReadoutUnit::pullRequestReceive ( int& eventID, int& size, int& destinationBU )
{
    DAQ_DEBUG_ARG("libfabric","ReadoutUnit: pullRequestReceive: source=any, rank=%2, id=%3")
		.arg(gblConfiguration->rankID)
		.arg(TAG_EVENT_REQUEST)
		.end();

	int id = driver->waitAny(buRequests,gblConfiguration->numTotalBuilders);
	assert(id >= 0 && id < gblConfiguration->numTotalBuilders);
	
	//republish
	driver->recvTag(gblConfiguration->builderID(id),TAG_EVENT_PULL_REQUEST,&pull_size_id_bu[3*id],sizeof(int)*3,&buRequests[id],false);
   
	size = pull_size_id_bu[3*id+0];
	eventID = pull_size_id_bu[3*id+1];;
	destinationBU = pull_size_id_bu[3*id+2];
}

/*******************  FUNCTION  *********************/
bool ReadoutUnit::sendAnyEvent ( int& eventID )
{
	 bool eventSent = false;
	std::list<int>::iterator it;

	// Check against the activeRequests.
	// Test against the oldest first, so as to [try to] keep a finish order
	// (this is useful for the RUMaster -> Generator communication)
	for (it = activeRequestsList.begin(); it != activeRequestsList.end(); it++){
		// We test an event receival
		int eid = activeRequests[*it];
		eventSent = sendEvent(eid);
		if (eventSent) {
			eventID = eid;
			break;
		}
	}

	return eventSent;
}

/*******************  FUNCTION  *********************/
bool ReadoutUnit::sendEvent ( int eventID )
{
	// Check that eventID is in our list,
	// and test if it was sent. Return true and do
	// some cleanup in that case.
	bool found = false;
	bool eventSent = false;
	std::list<int>::iterator it;
	int eventSlot;
	
	for (it = activeRequestsList.begin(); it != activeRequestsList.end(); it++){
		found = true;
		if (activeRequests[*it] == eventID){
			eventSent = driver->testAll(requests[*it],requestIndex[*it]);
			if (eventSent)
				break;
		}
	}

	if (eventSent){
		// Do some cleanup
		eventSlot = *it;
		activeRequestsList.erase(it);
		activeRequests[eventSlot] = -1;
		requestIndex[eventSlot] = 0;
	}

	return eventSent;
}

/*******************  FUNCTION  *********************/
void ReadoutUnit::discardEvent()
{
	 std::list<int>::iterator it = activeRequestsList.begin();
	int eventSlot;

	DAQ_DEBUG_ARG("libfabric","Event %1 discarded").arg(*it).end();

	// Do some cleanup
	eventSlot = *it;
	activeRequestsList.erase(it);
	activeRequests[eventSlot] = -1;
	requestIndex[eventSlot] = 0;
}

/*******************  FUNCTION  *********************/
void ReadoutUnit::setupSendEvent ( char* dataPointer, int size, int eventID, int destinationBU )
{
	// Find a free request.
	int slot;
	for (slot=0; slot<initialCredits; ++slot)
		if (activeRequests[slot] == eventID) break; // Slot found! :D
	if (slot == initialCredits) {
		// We don't have a slot already assigned to us
		for (slot=0; slot<initialCredits; ++slot)
			if (activeRequests[slot] == -1) break;
		if (slot == initialCredits){
			ERROR << "MPI_transport::MPIReadoutUnit: setupSendEvent can't send because there are no free slots." << std::endl;
			return;
		}
		activeRequests[slot] = eventID;
		activeRequestsList.push_back(slot);
	}

    // Event fragment packet description
    // 
    // 01x-xyyyy
    // ^        reserved
    //  ^       packet type (1 - event fragment)
    //   ^      event id (18 bits)
    //      ^   fragment id (4 bits)

	int packet_type = 1;
	int event_id = eventID & 0x3FFFF;
	int fragment_id = requestIndex[slot] & 0xF;
	int id = (packet_type << 30) | (event_id << 4) | fragment_id;

	DAQ_DEBUG_ARG("libfabric","MPI_transport::MPIReadoutUnit: setupSendEvent source=%1, rank=%2, id=%3, size=%4")
		.arg(gblConfiguration->rankID)
		.arg(gblConfiguration->builderID(destinationBU))
		.arg(id)
		.arg(size)
		.end();
	
	//wait to get
	RemoteIOV remote;
	driver->recvTag(gblConfiguration->builderID(destinationBU),id,&remote,sizeof(remote),NULL,true);
	DAQ_DEBUG_ARG("libfabric","Get remote IOV : addr=%1, key=%2").arg(remote.addr).arg(remote.key).end();

	//now run the RDMA write
	driver->rdmaWrite(gblConfiguration->builderID(destinationBU),id,dataPointer,size,remote.addr,remote.key,&requests[slot][ requestIndex[slot] ], false);
	//TODO here I need to know on which addresses to write with RDMA
// 	requests[slot][ requestIndex[slot] ] = MPI::COMM_WORLD.Isend(dataPointer, size, MPI::CHAR, MPI_transport::Configuration->builderID(destinationBU), id);
    
	requestIndex[slot]++;
}

}
