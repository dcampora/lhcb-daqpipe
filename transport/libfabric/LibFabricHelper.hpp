/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef DAQPIPE_LIBFABRIC_HELPER_HPP
#define DAQPIPE_LIBFABRIC_HELPER_HPP

/********************  HEADERS  *********************/
#include "../../common/Debug.hpp"

/********************  MACROS  **********************/
#define LIBFABRIC_VERSION FI_VERSION(1,1)
// #define LIBFABRIC_VERSION FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION)
#define LIBFABRIC_CHECK_STATUS(call, status) assumeArg(status == 0,"Libfabric get failure on command %1 with status %2 : %3").arg(call).arg(status).arg(fi_strerror(-status)).end();
#define LIBFABRIC_EQ_FATAL_ERROR(call, eq) DAQ_FATAL_ARG("Libfabric get failure on command %1: \n%2").arg(call).arg(Helper::getEqErrString(eq,call)).end();
#define LIBFABRIC_CQ_FATAL_ERROR(call, cq) DAQ_FATAL_ARG("Libfabric get failure on command %1: \n%2").arg(call).arg(Helper::getCqErrString(cq,call)).end();
#define LIBFABRIC_CHECK_AND_GOTO(call, status,label) do {if (status != 0) { DAQ_ERROR_ARG("Libfabric get failure on command %1 with status %2 : %3").arg(call).arg(status).arg(fi_strerror(-status)).end(); goto label;} } while(0)

/*********************  TYPES  **********************/
struct fid_eq;
struct fid_cq;

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*********************  CLASS  **********************/
struct Helper
{
	static std::string convertAddrToHex( const void* addr, int size );
	static void convertAddrFromHex( void* addr, int size, const std::string& value );
	static std::string getEqErrString(fid_eq *eq, const char *eq_str);
	static std::string getCqErrString( fid_cq* eq, const char* eq_str );
	static int waitForCompletion(fid_cq *cq, int num_completions);
	static std::string getHostIp(const std::string & hostname);
	static std::string getLocalIP(const std::string & iface);
};

}

#endif //DAQPIPE_LIBFABRIC_HELPER_HPP
