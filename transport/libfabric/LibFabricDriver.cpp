/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <cstring>
#include <cassert>
//for gethostname
#include <unistd.h>
//what we implement
#include "LibFabricHelper.hpp"
#include "LibFabricDriver.h"

/********************  MACROS  **********************/
#define DAQ_ENPOINT_QUEUE_SIZE 64

/*******************  NAMESPACE  ********************/
using namespace std;

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*******************  FUNCTION  *********************/
void DriverConfig::setup ( Json::Value& config )
{
	this->firstPort = config.get("first port",4444).asInt();
	this->portRange = config.get("port range",1024).asInt();
	this->waitSleep = config.get("wait sleep",256).asLargestUInt();
	this->epType    = config.get("ep type","").asString();
	this->iface     = config.get("iface","").asString();
}

/*******************  FUNCTION  *********************/
Driver::Driver ( void )
{
	this->fabric = NULL;
	this->listenEndpoint = NULL;
	this->listenQueue = NULL;
	this->domain = NULL;
	this->endpoints = NULL;
	this->worldSize = 0;
	this->launcher = NULL;
}

/*******************  FUNCTION  *********************/
void Driver::printDomainsInfos(Json::Value& config,const std::string name,const std::string service)
{
	//setup config
	this->config.setup(config);
	
	//create hint and set
	hints = fi_allocinfo();
	setupHint(config,hints);
	
	//get 
	const char * hintName = (name.empty())?NULL:name.c_str();
	const char * hintService = (service.empty())?NULL:service.c_str();;
	
	//check available domains
	fi_info * fiTmp;
	int ret = fi_getinfo(LIBFABRIC_VERSION, hintName, hintService, 0, hints, &fiTmp);
	LIBFABRIC_CHECK_STATUS("fi_getinfo",ret);
	
	//loop on all
	fi_info * cur = fiTmp;
	while (cur != NULL)
	{
		printf("---------------------- %s : %s ----------------------\n",cur->fabric_attr->prov_name,cur->domain_attr->name);
		printf("type: %s\n", fi_tostr(&cur->ep_attr->type, FI_TYPE_EP_TYPE));
		printf("protocol: %s\n", fi_tostr(&cur->ep_attr->protocol, FI_TYPE_PROTOCOL));
		printf("%s", fi_tostr(cur, FI_TYPE_INFO));
		cur = cur->next;
	}
	printf("------------------------------------------------\n");
}

/*******************  FUNCTION  *********************/
void Driver::initialize ( Json::Value& config, HydraLauncher & launcher )
{
	//setup launcher
	this->launcher = &launcher;
	
	//setup config
	this->config.setup(config);
	
	//Setup some infos
	this->worldSize = launcher.getWorldSize();
	
	//create hint and set
	hints = fi_allocinfo();
	setupHint(config,hints);
	
	//check available domains
	fi_info * fiTmp;
	int ret = fi_getinfo(LIBFABRIC_VERSION, NULL, NULL, 0, hints, &fiTmp);
	LIBFABRIC_CHECK_STATUS("fi_getinfo",ret);
	
	//print some info about the fabric
	DAQ_INFO_ARG("On rank %1, use fabric provider '%2' and domain '%3'")
		.arg(launcher.getRank())
		.arg(fiTmp->fabric_attr->prov_name)
		.arg(fiTmp->domain_attr->name)
		.end();
	
	//setup mode
	providerName = fiTmp->fabric_attr->prov_name;
	useRDM = (fiTmp->ep_attr->type == FI_EP_RDM);
	if (fiTmp->ep_attr->type == FI_EP_RDM)
		useRDM = true;
	else if (fiTmp->ep_attr->type == FI_EP_MSG)
		useRDM = false;
	else if (fiTmp->ep_attr->type == FI_EP_UNSPEC)
		useRDM = getEpType() == FI_EP_RDM;
	else
		DAQ_FATAL("Don't known which protocole to use !");

	//allocate endpoints
	if (useRDM == false)
	{
		this->endpoints = new Endpoint[launcher.getWorldSize()];
		for (int i = 0 ; i < launcher.getWorldSize() ; i++)
			this->endpoints[i].setQueueSize(DAQ_ENPOINT_QUEUE_SIZE);
	} else {
		rdmEndpoint.setQueueSize(DAQ_ENPOINT_QUEUE_SIZE);
	}

	//setup
	if (useRDM)
	{
		DAQ_DEBUG("libfabric","Using RDM mode to setup connections");
		setupRDMConnections(launcher);
	} else {
		DAQ_DEBUG("libfabric","Using MSG mode to setup connections");
		setupMSGConnections(launcher);
	}
}

/*******************  FUNCTION  *********************/
int Driver::getServicePort ( int rank )
{
	return config.firstPort + rank % config.portRange;
}

/*******************  FUNCTION  *********************/
//for MSG mode we mostly just setup the passive endpoint which listen approach
void Driver::setupMSGConnections ( HydraLauncher & launcher )
{
	int ret;
	fi_info * fi = NULL;
	
	//get IP
	std::string ip = Helper::getLocalIP(config.iface); 
	
	//compute port
	//TODO import port and range from config
	int service = getServicePort(launcher.getRank());
	char serviceStr[32];
	sprintf(serviceStr,"%d",service);
	
	//register to launcher and ensure to get all shared
	launcher.set("hostname",ip);
	launcher.commit();
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Start to listen on %1:%2\n").arg(ip).arg(service).end();

	//get info
	ret = fi_getinfo(LIBFABRIC_VERSION, ip.c_str(), serviceStr, FI_SOURCE, hints, &fi);
	assumeArg(fi != NULL,"Get not available fi_info structure from the given hint and %1:%2").arg(ip).arg(service).end();
	LIBFABRIC_CHECK_STATUS("fi_getinfo",ret);
	
	//setup fabric
	ret = fi_fabric(fi->fabric_attr, &fabric, NULL);
	LIBFABRIC_CHECK_AND_GOTO("fi_fabric",ret,err0);
	assert((fi->mode & FI_MSG_PREFIX) == 0);
	
	//create passive endpoint
	ret = fi_passive_ep(fabric, fi, &listenEndpoint, NULL);
	LIBFABRIC_CHECK_AND_GOTO("fi_passive_ep",ret,err0);
	
	//setup connection management queue
	struct fi_eq_attr cm_attr;
	memset(&cm_attr, 0, sizeof cm_attr);
	cm_attr.wait_obj = FI_WAIT_FD;
	ret = fi_eq_open(fabric, &cm_attr, &listenQueue, NULL);
	LIBFABRIC_CHECK_AND_GOTO("fi_eq_open",ret,err1);
	
	//bind it to endpoint
	ret = fi_pep_bind(listenEndpoint, &listenQueue->fid, 0);
	LIBFABRIC_CHECK_AND_GOTO("fi_pep_bind",ret,err2);

	//start listen
	ret = fi_listen(listenEndpoint);
	LIBFABRIC_CHECK_AND_GOTO("fi_listen",ret,err3);
	
	//cleanup tmp
	fi_freeinfo(fi);
	
	//wait for all that listen be ok
	launcher.barrier();
	
	//ok exit
	return;

	//errors
	err3:
		fi_close(&listenEndpoint->fid);
	err2:
		fi_close(&listenQueue->fid);
	err1:
		fi_close(&fabric->fid);
	err0:
		fi_freeinfo(fi);
		listenQueue = NULL;
		listenEndpoint = NULL;
		fabric = NULL;
		abort();
}

/*******************  FUNCTION  *********************/
void Driver::setupConnexion ( int rank , HydraLauncher & launcher)
{
	//check
	assert(rank >= 0 && rank < worldSize);
	assert(endpoints[rank].active() == false);
	
	//setup the connexion
	struct fi_eq_cm_entry entry;
	uint32_t event;
	ssize_t rd;
	int ret;
	fi_info * fi;
	fid_mr * mr;
	
	//errors
	if (endpoints[rank].active())
	{
		DAQ_WARNING_ARG("Enpoint %1 already active on %1, do nothing.").arg(rank).arg(launcher.getRank()).end();
		return;
	}
	
	//get hostname
	//TODO replace which more lowlevel addr in hints
	std::string hostname = launcher.get("hostname",rank);
	char serviceStr[32];
	sprintf(serviceStr,"%d",getServicePort(rank));

	//get info
	ret = fi_getinfo(LIBFABRIC_VERSION, hostname.c_str(), serviceStr, 0, hints, &fi);
	LIBFABRIC_CHECK_AND_GOTO("fi_getinfo",ret,err0);

	//setup fabric
	if (fabric == NULL)
	{
		ret = fi_fabric(fi->fabric_attr, &fabric, NULL);
		LIBFABRIC_CHECK_AND_GOTO("fi_fabric",ret,err1);
	}
	
	//check mr mode
	mr_mode = fi->domain_attr->mr_mode;

	//setup domain
	if (domain == NULL)
	{
		ret = fi_domain(fabric, fi, &domain, NULL);
		LIBFABRIC_CHECK_AND_GOTO("fi_domain",ret,err2);
	}
	
	//register segments use for connexion setup
	this->registerSegment(&ackConnect,sizeof(ackConnect),FI_SEND|FI_RECV);
	
	ret = endpoints[rank].connect(launcher.getRank(),fabric,domain,fi);
	LIBFABRIC_CHECK_AND_GOTO("endpoint.connect",ret,err4);
	
	//send rank
	ackConnect.rank = launcher.getRank();
	mr = getFidMR(&ackConnect,sizeof(ackConnect));
	assume(mr != NULL,"Fail to find segment in list, might be not registered, please use registerSegment() before using addresses to send/recv");
	endpoints[rank].sendMsg(&ackConnect,sizeof(ackConnect),mr,rank,NULL,true);

	//free tmp info
	fi_freeinfo(fi);
	return;

err4:
// 	free_ep_res();
err3:
	fi_close(&domain->fid);
err2:
	fi_close(&fabric->fid);
err1:
	fi_freeinfo(fi);
err0:
	domain =NULL;
	fabric = NULL;
	abort();
}

/*******************  FUNCTION  *********************/
RemoteIOV Driver::getIOV ( void* ptr, size_t size )
{
	//erros
	assert(ptr != NULL);
	assert(size > 0);
	
	//vars
	RemoteIOV iov;
	
	//get mr
	MemoryRegion * mr = getMR(ptr,size);
	assert(mr != NULL);

	//setup
	iov.addr = (mr_mode == FI_MR_SCALABLE) ? (void*)((char*)ptr - (char*)mr->ptr) : ptr;
	iov.key = fi_mr_key(mr->mr);
	
	if (mr_mode == FI_MR_SCALABLE && ptr != mr->ptr)
		DAQ_DEBUG_ARG("libfabric","Use offset inside segment for %1 (key=%2)").arg(iov.addr).arg(iov.key).end();
	
	return iov;
}

/*******************  FUNCTION  *********************/
Endpoint& Driver::getEndpoint ( int targetRank )
{
	//errors
	assert(targetRank >= 0 && targetRank < launcher->getWorldSize());
	
	//select
	if (useRDM)
		return rdmEndpoint;
	else
		return endpoints[targetRank];
}

/*******************  FUNCTION  *********************/
void Driver::rdmaWrite ( int targetRank, int tag, void* localPtr, size_t size, void* remotePtr,uint64_t remoteKey, Request* request, bool wait )
{
	//check
	assert(targetRank >= 0 && targetRank < launcher->getWorldSize());
	assert(localPtr != NULL);
	assert(size >= 0);
	
	//local wait request
	Request req;
	if (wait == true && request == NULL)
		request = &req;

	//Get ep and check
	Endpoint & ep = getEndpoint(targetRank);
	assert(ep.active());
	
	//get memory region
	fid_mr * mr = getFidMR(localPtr,size);
	assert(mr != NULL);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Post RDMA write to %1, tag=%2").arg(targetRank).arg(tag).end();
	
	//post send
	ep.writeRDMA(targetRank,tag,mr,localPtr,size,remotePtr,remoteKey,request);
	
	//wait
	if (wait)
	{
		int id = this->waitAny(request,1);
		assert(id == 0);
	}
}

/*******************  FUNCTION  *********************/
void Driver::recvRDMA ( int targetRank, int tag, void* ptr, size_t size, Request* request, bool wait )
{
	//check
	assert(targetRank >= 0 && targetRank < launcher->getWorldSize());
	assert(ptr != NULL);
	assert(size >= 0);
	
	//local wait request
	Request req;
	if (wait == true && request == NULL)
		request = &req;
	
	//Get ep and check
	Endpoint & ep = getEndpoint(targetRank);
	assert(ep.active());
	
	//first check in pending
	ep.checkRDMAPending(targetRank,tag,ptr,size,request);
	
	//get memory region
	fid_mr * mr = getFidMR(ptr,size);
	assert(mr != NULL);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Post RDMA wait write to %1, tag=%2").arg(targetRank).arg(tag).end();
	
	//post send
	ep.recvRDMA(targetRank,tag,ptr,size,request);
	
	//wait
	if (wait)
	{
		int id = this->waitAny(request,1);
		assert(id == 0);
	}
}

/*******************  FUNCTION  *********************/
void Driver::send ( int targetRank, void* ptr, size_t size, Request* requests, bool wait )
{
	//check
	assert(targetRank >= 0 && targetRank < launcher->getWorldSize());
	assert(ptr != NULL);
	assert(size >= 0);
	
	//local wait request
	Request req;
	if (wait == true && requests == NULL)
		requests = &req;
	
	//Get ep and check
	Endpoint & ep = getEndpoint(targetRank);
	assert(ep.active());
	
	//get memory region
	//fid_mr * mr = getMR(ptr,size);
	//assert(mr != NULL);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Post send message to %1").arg(targetRank).end();
	
	//post send
	ep.sendMsg(ptr,size,NULL,targetRank,requests,false);
	
	//wait
	if (wait)
	{
		int id = this->waitAny(requests,1);
		assert(id == 0);
	}
}

/*******************  FUNCTION  *********************/
void Driver::recv ( int targetRank, void* ptr, size_t size, Request* requests, bool wait )
{
	//check
	assert(targetRank >= 0 && targetRank < launcher->getWorldSize());
	assert(ptr != NULL);
	assert(size >= 0);
	
	//local wait request
	Request req;
	if (wait == true && requests == NULL)
		requests = &req;
	
	//Get ep and check
	Endpoint & ep = getEndpoint(targetRank);
	assert(ep.active());
	
	//get memory region
	//fid_mr * mr = getMR(ptr,size);
	//assert(mr != NULL);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Post recv message to %1").arg(targetRank).end();
	
	//post send
	ep.recvMsg(ptr,size,NULL,targetRank,requests,false);
	
	//wait
	if (wait)
	{
		int id = this->waitAny(requests,1);
		assert(id == 0);
	}
}

/*******************  FUNCTION  *********************/
void Driver::recvTag ( int targetRank, int tag, void* ptr, size_t size, Request* requests, bool wait )
{
	//check
	assert(targetRank >= 0 && targetRank < launcher->getWorldSize());
	assert(ptr != NULL);
	assert(size >= 0);
	
	//local wait request
	Request req;
	if (wait == true && requests == NULL)
		requests = &req;
	
	//Get ep and check
	Endpoint & ep = getEndpoint(targetRank);
	assert(ep.active());
	
	//get memory region
	fid_mr * mr = getFidMR(ptr,size);
	//here we don't care for non registred segment
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Post recv tagged message to %1, tag=%2").arg(targetRank).arg(tag).end();
	
	//post send
	ep.recvTag(ptr,size,mr,targetRank,tag,requests,false);
	
	//wait
	if (wait)
	{
		int id = this->waitAny(requests,1);
		assert(id == 0);
	}
}

/*******************  FUNCTION  *********************/
void Driver::sendTag ( int targetRank, int tag, void* ptr, size_t size, Request* requests, bool wait )
{
	//check
	assert(targetRank >= 0 && targetRank < launcher->getWorldSize());
	assert(ptr != NULL);
	assert(size >= 0);
	
	//local wait request
	Request req;
	if (wait == true && requests == NULL)
		requests = &req;
	
	//Get ep and check
	Endpoint & ep = getEndpoint(targetRank);
	assert(ep.active());
	
	//get memory region
	fid_mr * mr = getFidMR(ptr,size);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Post send tagged message to %1, tag=%2").arg(targetRank).arg(tag).end();
	
	//post send
	ep.sendTag(ptr,size,mr,targetRank,tag,requests,false);
	
	//wait
	if (wait)
	{
		int id = this->waitAny(requests,1);
		assert(id == 0);
	}
}

/*******************  FUNCTION  *********************/
void Driver::registerSegment ( void* ptr, size_t size, int mode )
{
	//checks
	assert(domain != NULL);
	assert(ptr != NULL);
	assert(size > 0);
	
	//setup
	MemoryRegion region;
	region.ptr = ptr;
	region.size = size;
	region.mr = NULL;
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Register new segment into driver : %1 of size %2 and id %3")
		.arg(ptr)
		.arg(size)
		.arg(segments.size())
		.end();
	
	//check if already registred
	if (getFidMR(ptr,size) != NULL)
		return;

	//reg into OFI
	int ret = fi_mr_reg(domain, ptr, size, mode, 0, segments.size(), 0, &(region.mr), NULL);
	LIBFABRIC_CHECK_STATUS("fi_mr_reg",ret);
	
	//reg
	segments.push_back(region);
}

/*******************  FUNCTION  *********************/
int Driver::waitConnection ( void )
{
	//check mode
	assume(useRDM == false,"You try to use waitConnection() while libfabric is started in RDM mode !");
	
	//some vars
	struct fi_eq_cm_entry entry;
	uint32_t event;
	struct fi_info *info = NULL;
	ssize_t rd;
	int ret;
	Endpoint endpoint(DAQ_ENPOINT_QUEUE_SIZE);
	fid_mr * mr;

	//wait on queue
	rd = fi_eq_sread(listenQueue, &event, &entry, sizeof entry, -1, 0);
	if (rd != sizeof entry)
		LIBFABRIC_EQ_FATAL_ERROR("fi_eq_sread",listenQueue);

	//check
	info = entry.info;
	if (event != FI_CONNREQ) {
		DAQ_ERROR_ARG("Unexpected CM event %1").arg(event).end();
		ret = -FI_EOTHER;
		goto err1;
	}
	
	//check mr mode to use
	mr_mode = info->domain_attr->mr_mode;

	//build domain
	if (domain == NULL)
	{
		ret = fi_domain(fabric, info, &domain, NULL);
		LIBFABRIC_CHECK_AND_GOTO("fi_domain",ret,err1);
	}
	
	//register segment
	this->registerSegment(&ackListen,sizeof(ackListen),FI_SEND|FI_RECV);
	
	//accept
	endpoint.accept(launcher->getRank(),fabric,domain,info);
	
	//wait to get first message with remote rank ID
	mr = getFidMR(&ackListen,sizeof(ackListen));
	assume(mr != NULL,"Fail to find segment in list, might be not registered, please use registerSegment() before using addresses to send/recv");
	endpoint.recvMsg(&ackListen,sizeof(ackListen),mr,-1,NULL,true);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Ack rank is : %1\n").arg(ackListen.rank).end();
	
	//setup local rank endpoint
	assert(endpoints[ackListen.rank].active() == false);
	endpoints[ackListen.rank].import(endpoint);

	//free tmp
	fi_freeinfo(info);
	return 0;

err3:
//	free_ep_res();
err1:
	fi_reject(listenEndpoint, info->handle, NULL, 0);
	fi_freeinfo(info);
	return ret;
}

/*******************  FUNCTION  *********************/
void Driver::pushToRequestCache ( Request* request )
{
	assert(request != NULL);
	requestCache[request] = true;
}

/*******************  FUNCTION  *********************/
Request* Driver::checkRequestCache ( Request* requests, size_t size, bool remove )
{
	//search for all
	for (int i = 0 ; i < size ; i++)
	{
		LibFabricRequestMap::iterator it = requestCache.find(&requests[i]);
		if (it != requestCache.end())
		{
			//remove from cache
			if (remove)
				requestCache.erase(it);
			return &requests[i];
		}
	}
	
	//not found
	return NULL;
}

/*******************  FUNCTION  *********************/
//TODO can be optimized by attaching state into requests like MPI implemention does
bool Driver::checkAllRequestCache ( Request* requests, size_t size, bool remove )
{
	//check if get all in cache
	int cnt = 0;
	for (int i = 0 ; i < size ; i++)
	{
		if (checkRequestCache(&requests[i],1,remove))
			cnt++;
		else
			break;
	}
	
	//return
	return cnt == size;
}

/*******************  FUNCTION  *********************/
bool Driver::testAll ( Request* requests, int size )
{
	//errors
	assert(requests != NULL || size == 0);
	
	//if none
	if (size == 0)
		return false;
	
	//check if already get them
	bool all = checkAllRequestCache(requests,size,false);
	
	//if not, try to wait some connections
	if (!all)
	{
		int id = testAny(requests,size,true);
		//if get one, recheck
		if (id != -1)
		{
			pushToRequestCache(&requests[id]);
			all = checkAllRequestCache(requests,size,false);
		}
	}

	//if get all, remove them from cache
	if (all)
	{
		all = checkAllRequestCache(requests,size,true);
		assert(all = true);
		return true;
	} else {
		return false;
	}
}

/*******************  FUNCTION  *********************/
int Driver::testAny ( Request* requests, int size, bool ignoreCache )
{
	//errors
	assert(requests != NULL || size == 0);
	
	//if none
	if (size == 0)
		return -1;
	
	//search for a request to be finished
	Request * cur = NULL;
	
	//first check in cache
	if (!ignoreCache)
		cur = checkRequestCache(requests,size);
	
	//loop on all connections if none
	if (cur == NULL && endpoints != NULL)
	{
		for (int i = 0 ; i < worldSize ; i++)
		{
			cur = endpoints[i].testQueue(true,true);
			if (cur != NULL)
				break;
		}
	} else if (cur == NULL && endpoints == NULL) {
		cur = rdmEndpoint.testQueue(true,true);
	}
	
	//if get nont return -1
	if (cur == NULL)
		return -1;
	
	//search ID
	int id = -1;
	for (int i = 0 ; i < size ; i++)
	{
		if (&requests[i] == cur)
		{
			id = i;
			break;
		}
	}
	
	//if ID not found, put it in cache
	if (id == -1)
		pushToRequestCache(cur);
	
	//return
	return id;
}

/*******************  FUNCTION  *********************/
int Driver::waitAny ( Request* requests, int size )
{
	//errors
	assert(requests != NULL || size == 0);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Start to wait any event").end();
	
	//if none
	if (size == 0)
		return -1;
	
	//loop on all connections to wait something
	int id = -1;
	while (id == -1)
	{
		//test
		id = testAny(requests,size);
		//sleep to not do too much cpu usage while polling
		if (id == -1)
			usleep(config.waitSleep);
	}
	
	//return
	assert(id != -1);
	return id;
}

/*******************  FUNCTION  *********************/
void Driver::setupRDMConnections ( HydraLauncher & launcher )
{
	//get IP
	std::string ip = Helper::getLocalIP(config.iface);
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Start to listen on %1\n").arg(ip).end();

	//get info
	fi_info * fi;
	int ret = fi_getinfo(LIBFABRIC_VERSION, ip.c_str(), NULL, FI_SOURCE, hints, &fi);
	LIBFABRIC_CHECK_STATUS("fi_getinfo",ret);
	assumeArg(fi != NULL,"Get not available fi_info structure from the given hint and %1").arg(ip).end();
	
	//setup fabric
	ret = fi_fabric(fi->fabric_attr, &fabric, NULL);
	LIBFABRIC_CHECK_STATUS("fi_fabric",ret);
	
	//check mr mode to use
	mr_mode = fi->domain_attr->mr_mode;
	
	//setup domain
	ret = fi_domain(fabric, fi, &domain, NULL);
	LIBFABRIC_CHECK_STATUS("fi_domain",ret);
	
	//setup rdm endoint
	rdmEndpoint.setupRDM(fabric,domain,fi,launcher);
	
	//free tmp info
	fi_freeinfo(fi);
	return;
	
	//errors
	err1:
		fi_close(&fabric->fid);
	err0:
		fi_freeinfo(fi);
		fabric = NULL;
		domain = NULL;
}

/*******************  FUNCTION  *********************/
void Driver::finalize ( void )
{
	if (endpoints != NULL)
		delete [] endpoints;
	if (hints != NULL)
		fi_freeinfo(hints);
	if (listenEndpoint != NULL)
		fi_close(&listenEndpoint->fid);
	if (listenQueue != NULL)
		fi_close(&listenQueue->fid);
	if (domain != NULL)
		fi_close(&domain->fid);
}

/*******************  FUNCTION  *********************/
bool Driver::useRDMMode ( void )
{
	return useRDM;
}

/*******************  FUNCTION  *********************/
fi_ep_type Driver::getEpType ( void )
{
	//force RDM mode if setup in config
	if (this->config.epType == "RDM")
	{
		return FI_EP_RDM;
	} else if (this->config.epType == "MSG") {
		return FI_EP_MSG;
	} else {
		assumeArg (this->config.epType.empty(),"Get invalid EP type from config file : %1").arg(this->config.epType).end();
		return FI_EP_UNSPEC;
	}
}

/*******************  FUNCTION  *********************/
void Driver::setupHint ( Json::Value& config, fi_info* hints )
{
	hints->ep_attr->type = getEpType();
	//setup default, we need MSG and RMA
	hints->caps = FI_MSG | FI_RMA | FI_TAGGED;
	//Need contxt, memory registration and RDMA write aknowledgment
	hints->mode = FI_CONTEXT | FI_RX_CQ_DATA;
	
	//load some hint from config file
	Json::Value subconfig = config["libfabric"];
	if (!subconfig.isNull())
	{
		//load domain name
		if (subconfig["provider name"].isString())
			hints->fabric_attr->prov_name = strdup(subconfig["provider name"].asString().c_str());
	}
}

/*******************  FUNCTION  *********************/
MemoryRegion* Driver::getMR ( void* ptr, size_t size )
{
	//search
	MemoryRegion * mr = NULL;
	for (LibfabricMemoryRegionVector::iterator it = segments.begin() ; it != segments.end() ; ++it)
	{
		if (it->ptr <= ptr && (char*)it->ptr + it->size > ptr)
		{
			if ((char*)ptr+size > (char*)it->ptr + it->size)
				DAQ_WARNING("Caution, a segment from libfabric not completetly fit with the request which is larger.");
			mr = &(*it);
			break;
		}
	}
	return mr;
}

/*******************  FUNCTION  *********************/
fid_mr* Driver::getFidMR ( void* ptr, size_t size )
{
	MemoryRegion * region = getMR(ptr,size);
	if (region == NULL)
		return NULL;
	else
		return region->mr;
}

/*******************  FUNCTION  *********************/
void Driver::waitAll ( Request* requests, int size )
{
	//checks
	assert(requests != NULL || size == 0);
	
	//trivial
	if (size == 0)
		return;
	
	//debug
	DAQ_DEBUG_ARG("libfabric","Start to wait all events").end();

	//TODO optimize with states stored inside request struct
	while(!testAll(requests,size))
		usleep(config.waitSleep);
}

}
