/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef DAQPIPE_LIBFABRIC_CORE_HPP
#define DAQPIPE_LIBFABRIC_CORE_HPP

/********************  HEADERS  *********************/
#include "LibFabricDriver.h"
#include "HydraLauncher.hpp"
#include "../config/ConfigurationCore.h"

/********************  MACROS  **********************/
#define LIBFABRIC_MAX_CREDITS 16

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/********************  ENUM  ************************/
enum ids {
	EVENT_MANAGER_LISTENER = 0,
	EVENT_MANAGER_CONSUMER = 1,
	READOUT_UNIT = 0,
	BUILDER_UNIT = 1
};

/********************  ENUM  ************************/
enum Ranks
{
	RANK_EM_CONSUMMER = 0,
	RANK_EM_LISTENER = 1,
};

/********************  ENUM  ************************/
enum NodeType
{
	NODE_READOUT_UNIT,
	NODE_BUILDER_UNIT,
	NODE_EVENT_CONSUMMER,
	NODE_EVENT_LISTENER,
	NODE_INVALID,
};

/********************  ENUM  ************************/
enum MessageTags
{
	TAG_EXIT_SIGNAL,
	TAG_EVENT_ASSIGN,
	TAG_EVENT_REQUEST,
	TAG_EVENT_PULL_REQUEST,
	TAG_CREDIT_ANNOUNCE,
	TAG_EVENT_PREPARE,
	TAG_EVENT_CREDIT,
};

/********************  STRUCT  **********************/
struct ListenerMsg
{
	//define the source rank of the message
	int sourceRank;
	//number of credit to assign (can use -1 here for STOP message)
	int credits;
};

/*********************  CLASS  **********************/
class ConfigurationLoader : public Core::ConfigurationLoader
{
	public:
		ConfigurationLoader(void);
		virtual ~ConfigurationLoader(void);
		void initialize(int argc, char ** argv);
		void finalize(void);
		void parseConfiguration(void);
		virtual int builderID (int builderNumber);
		virtual bool builderInSameNode (int builderNumber);
		virtual int builderNumber (int builderID);
		virtual std::string getLogicalName ();
		virtual bool is (int rankID);
		virtual int nodeID ();
		virtual int nodeNumber (int rankID);
		virtual bool readoutInSameNode (int rankID);
		virtual int readoutNumber (int readoutID);
		virtual int readoutID (int readoutNumber);
		virtual bool isEMListener(void);
		virtual bool isEMConsumer(void);
		int getWorldSize(void);
		int rankID;
		LibFabric::Driver driver;
	private:
		bool isBU(void);
		bool isRU(void);
		NodeType getNodeType(void);
		void buildNodeTypeMap(void);
		void establishMSGConnexions(void);
		std::string builderSuffix();
		std::string readoutSuffix();
	private:
		int worldSize;
		int procID;
		NodeType * nodeTypeMap;
		HydraLauncher launcher;
		std::map<int, std::string> logicalNames;
};

/********************  GLOBALS  *********************/
extern ConfigurationLoader * gblConfiguration;

}

#endif //DAQPIPE_LIBFABRIC_CORE_HPP
