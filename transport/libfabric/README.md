About libfabric support
=======================

Here you might find some informations about the libfabric support for lhcb-daqpipe to better
understand the approach before digging into the code itself.

Some links
----------

You can use this links to get more informations about libfabric :

 * http://ofiwg.github.io/libfabric/
 * Examples of usage : https://github.com/ofiwg/fabtests

Code splitting
--------------

Currentl lhcb-daqpipe require to implement RU/BU/EM/EL support in each transport layers so you find the
related files here :

 - LibFabricBuilderUnit.(h|cpp)
 - LibFabricReadoutUnit.(h|cpp)
 - LibFabricEMConsummer.(h|cpp)
 - LibFabricEMListener.(h|cpp)

You will also find the main state instantiation with configuration managed inside : LibFabricCore.(h|cpp).

Then I put all the low level code to call libfabric functions inside :

 - LibFabricEndpoint to keep track of connexions between node. On endpoint connect to one node in MSG mode but to all in RDM.
 - LibFabricDriver to manage the state of the library, creation of endpoints, waiting on multiple endpoints...
 - LibFabricHelper for some helper functions, mostly error checking.

Approach
--------

I started from the MPI implementation from Daniel so mostly took is code to implement the unit and provide an
interface closer to MPI in the driver layer so it mostly match one to one with the MPI code.

To get better performance the driver might run multiple threads inside to poll the internal queues on endpoints.

Configuration
-------------

The Driver class support the given options from json file :

```json
{
	"first port": 4444, /* Define the starting port range to use in TCP */
	"port range": 1024, /* Define the number of port to use from first port. */
	"wait sleep": 256,  /* Waiting time int waitAny, waitAll between two testAny, testAll. In micro-seconds. */
	"ep type": "",      /* Used to force endpoint type : RDM or MSG. */
	"iface": "",        /* Used to face network interface to use : eth0, ib0... */
	"provider name":"", /* Force profived to use (sockets, psm, infiniband...) */
}
```

Launcher
--------

To be initialized on cluster the libfabric transport use the hydra launcher from mpich to get rank, world size and exchange
addresses to interconnect tasks. See class HydraLauncher which quicly wrap the MPI functions from hydra (or slurm if available).

The launcher use hydra approach by registering some key values to the global distributed DB. The data is seen by onthers
only after commit. Here an example :

```cpp
	//setup launcher
	HydraLauncher launcher;
	launcher.initilize();
	
	//register some local data (eg local host name)
	char hostanme[1024];
	gethostname(hostanme,sizeof(hostanme));
	
	//register key
	launcher.set("hostname",hostname);
	
	//commit and wait all to have register
	launcher.commit();
	launcher.barrier();
	
	//master print all the hostnames
	if (launcher.getRank() == 0)
	{
		for (int i = 0 ; i < launcher.getWordSize() ; i++)
			cout << launcher.get("hostname",i);
	}
```

About usage of Driver
---------------------

The Driver class mostly implement a MPI like interface providing :

 * simple send messages (`send()` and `recv()`).
 * Tagged messages (`sendTag()` and `recvTag()`).
 * RDMA (`rdmaWrite()` and `recvRDMA()`).
 * Wait asynchonous operations (`waitAny()`, `waitAll()`, `testAny()` and `testAll()`).

It aims to be used directly by the RU,BU,EM, EL units transport implementation just like MPI.
You can find some full usage example in `tests` subdirectory mostly with ping pong. But here some short example.

First you need to initialize :

```cpp
	//get json config file
	Json::Value config;
	
	//setup launcher
	HydraLauncher launcher;
	launcher.initialize();

	//setup driver
	Driver driver;
	driver.initialize(config,launcher);
	
	//Do your stuff
	
	//close cleanly
	driver.finalize();
	launcher.finalize();
```

Now you can for example exchange some messages in synchronous way :

```cpp
	char buffer[1024];

	//exchange
	if (launcher.getRank() == 0)
		driver.send(1,buffer,sizeof(buffer),NULL,true);
	else
		driver.recv(0,buffer,sizeof(buffer),NULL,true);
```

Or asynchonous :

```cpp
	char buffer[1024];
	Request request;

	//post exchange
	if (launcher.getRank() == 0)
		driver.send(1,buffer,sizeof(buffer),&request,false);
	else
		driver.recv(0,buffer,sizeof(buffer),&request,false);
		
	//wait
	driver.waitAll(&request,1);
```

You can do the same for tag messages :

```cpp
	const int TAG = 1;
	char buffer[1024];

	//exchange
	if (launcher.getRank() == 0)
		driver.sendTag(1,TAG,buffer,sizeof(buffer),NULL,true);
	else
		driver.recvTag(0,TAG,buffer,sizeof(buffer),NULL,true);
```

Or with RDMA :

```cpp
	const int TAG = 1;
	char buffer[1024];
	
	//need to register the buffer first
	driver.registerSegment(buffer,sizeof(buffer),FI_WRITE | FI_REMOTE_WRITE);
	
	//need to exchange IOV to get remote key
	//ideally 0 don't need to wait sendTag to continue, but it make the example simpler
	RemoteIOV iov = getIOV(buffer,sizeof(buffer));
	if (launcher.getRank() == 0)
		driver.sendTag(1,TAG,iov,sizeof(iov),NULL,true);
	else
		driver.recvTag(0,TAG,iov,sizeof(iov),NULL,true);
		
	//now can post write and do remote write
	if (launcher.getRank() == 0)
		driver.recvRDMA(1,TAG,buffer,sizeof(buffer),NULL,true);
	else
		driver.rdmaWrite(0,TAG,buffer,sizeof(buffer),iov.addr,iov.key,NULL,true);
```

About endpoints
---------------

Endpoint class is a wrapper to libfabric endpoints to ease usage into Driver. This class support three kind of usage :
 
 * Usage as listening endpoint (for MSG mode) to wait incomming connexions.
 * Usage as active enpoind in connected mode (for MSG mode) when connection is receved by listening endpoint. In that case,
 the endpoint refer to a uniq remote node.
 * Usage as unconnected endpoint (for RDM mode). In that case, the endpoint is connected to all the remote endpoints
 and use an address vector to get knowledge of the targets.
 
Due to the last case the endpoint message functions require to get the target rank as parameter. It is ignored in
MSG mode but used for RDM one.

The endpoint functions match mostly directly the `send`/`recv`/`write` functions from Driver and be called mostly
directly by them with the rank switches needed in MSG mode.

About driver wait/test functions
--------------------------------

The waiting and testing functions in Driver class as to handle the case they receive operation ending which
are not in the list of requests to wait/test. In that case, the request is registered in some lists for future 
testing (`requestCache`).

About waiting RDMA ops
----------------------

The RDMA operation are not matched with a receive side with a request into the libfabric level, 
it is matched by our own code thanks to the `data` (tag) attached to the operation acknolegment emmited
by `fi_writedata()`. Due to this the matching is done into the Endpoint class directly and note in the
Driver one which only manipulate requests.

