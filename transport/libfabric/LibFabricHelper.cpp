/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include <cstring>
#include <cstdio>
#include <cstring>
//headers from libfabric
#include <rdma/fabric.h>
#include <rdma/fi_errno.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_rma.h>
//to get ip
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
//local implement
#include "LibFabricHelper.hpp"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*******************  FUNCTION  *********************/
std::string Helper::convertAddrToHex ( const void* addr, int size )
{
	unsigned char * taddr = (unsigned char*)addr;
	std::string ret;
	
	//add size in hexa
	char tmp[4];
	sprintf(tmp,"%03x",size);
	ret+=tmp;

	//content
	for (int i = 0 ; i < size ; i++)
	{
		ret += ('A' + ((taddr[i]&0x0F)));
		ret += ('A' + ((taddr[i]&0xF0)>>4));
	}

	return ret;
}

/*******************  FUNCTION  *********************/
void Helper::convertAddrFromHex ( void* addr, int size, const std::string& value )
{
	int sizeCheck;

	//extract size
	sscanf(value.c_str(),"%03x",&sizeCheck);
	assert(sizeCheck == size);
	
	//extract data
	for (int i = 0 ; i < size ; i++)
		((unsigned char*)addr)[i] = (value[3+2*i] - 'A') + ((value[3+2*i+1] - 'A')<<4);
}

/*******************  FUNCTION  *********************/
std::string Helper::getEqErrString(struct fid_eq *eq, const char *eq_str)
{
	fi_eq_err_entry eq_err;
	const char *err_str;
	int rd;
	std::string res;

	rd = fi_eq_readerr(eq, &eq_err, 0);
	if (rd != sizeof(eq_err)) {
		LIBFABRIC_CHECK_STATUS("fi_eq_readerr",rd);
	} else {
		err_str = fi_eq_strerror(eq, eq_err.prov_errno, eq_err.err_data, NULL, 0);
		res = eq_str;
		res += ": ";
		res += fi_strerror(eq_err.err);
		res += "\n";
		res += eq_str;
		res += ": prov_err: ";
		res += err_str;
	}

	return res;
}

/*******************  FUNCTION  *********************/
std::string Helper::getCqErrString(struct fid_cq *cq, const char *eq_str)
{
	fi_cq_err_entry eq_err;
	const char *err_str;
	int rd;
	std::string res;

	rd = fi_cq_readerr(cq, &eq_err, 0);
	if (rd < 0) {
		LIBFABRIC_CHECK_STATUS("fi_cq_readerr",rd);
	} else {
		err_str = fi_cq_strerror(cq, eq_err.prov_errno, eq_err.err_data, NULL, 0);
		res = eq_str;
		res += ": ";
		res += fi_strerror(eq_err.err);
		res += "\n";
		res += eq_str;
		res += ": prov_err: ";
		res += err_str;
	}

	return res;
}

/*******************  FUNCTION  *********************/
int Helper::waitForCompletion ( fid_cq* cq, int num_completions )
{
	int ret;
	struct fi_cq_data_entry comp;
	while (num_completions > 0) {
		ret = fi_cq_read(cq, &comp, 1);
		if (ret > 0) {
			num_completions--;
		} else if (ret < 0 && ret != -FI_EAGAIN) {
			if (ret == -FI_EAVAIL) {
				LIBFABRIC_CQ_FATAL_ERROR("fi_cq_read",cq);
			} else {
				LIBFABRIC_CHECK_STATUS("fi_cq_read", ret);
			}
			return ret;
		}
	}
	return 0;
}

/*******************  FUNCTION  *********************/
std::string Helper::getHostIp(const std::string & hostname)
{
	struct hostent *he;
	struct in_addr **addr_list;
	int i;
	
	//get entry
	he = gethostbyname( hostname.c_str() );
	assumeArg(he != NULL,"Get error on gethostbyname : %1")
		.arg(hstrerror(h_errno))
		.end();

	//extract addr
	addr_list = (struct in_addr **) he->h_addr_list;
	
	//convert first one
	char buffer[256];
	strcpy(buffer , inet_ntoa(*addr_list[0]) );

	return buffer;
}

/*******************  FUNCTION  *********************/
std::string Helper::getLocalIP(const std::string & iface)
{
	//locals
	struct ifaddrs * ifAddrStruct=NULL;
	struct ifaddrs * ifa=NULL;
	void * tmpAddrPtr=NULL;
	
	//if iface is empty, search with hostname
	if (iface.empty())
	{
		char hostname[1024];
		gethostname(hostname,sizeof(hostname));
		std::string ip = getHostIp(hostname);
		DAQ_DEBUG_ARG("libfabric","Get ip for %1 = %2").arg(hostname).arg(ip).end();
		return ip;
	}

	//get addresses
	getifaddrs(&ifAddrStruct);

	//loop to seatch
	for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) 
	{
		//if no address
		if (!ifa->ifa_addr) {
			continue;
		}
		
		//check if requested one
		if (iface == ifa->ifa_name)
		{
			//check type
			if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
				// is a valid IP4 Address
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
				char addressBuffer[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
				DAQ_DEBUG_ARG("libfabric","Get ip for %1 = %2").arg(iface).arg(addressBuffer).end();
				return addressBuffer; 
			} else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
				// is a valid IP6 Address
				tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
				char addressBuffer[INET6_ADDRSTRLEN];
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
				DAQ_DEBUG_ARG("libfabric","Get ip for %1 = %2").arg(iface).arg(addressBuffer).end();
				return addressBuffer;
			}
		}
	}
	
	//error not found
	DAQ_FATAL_ARG("Fail to find address of interface %s").arg(iface).end();
}

}
