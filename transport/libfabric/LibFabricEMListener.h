/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef DAQPIPE_LIBFABRIC_EM_LISTENER_HPP
#define DAQPIPE_LIBFABRIC_EM_LISTENER_HPP

/********************  HEADERS  *********************/
#include "LibFabricCore.h"
#include "../common/EMTransportCoreListener.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*********************  CLASS  **********************/
class EMListener : public EMTransportCoreListener
{
	public:
		EMListener(void);
		virtual void listenForCreditRequest (int &node,int &numberOfCreditsRequested);
	private:
		Request * requests;
		ListenerMsg * buffer;
		Driver * driver;
		int countBU;
};

}

#endif //DAQPIPE_LIBFABRIC_EM_LISTENER_HPP
