/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "../../common/Debug.hpp"
#include "LibFabricCore.h"
#include "LibFabricEMListener.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*******************  FUNCTION  *********************/
EMListener::EMListener ( void )
{
	//get driver
	driver = &LibFabric::gblConfiguration->driver;
	
	//allocate & register segments
	countBU = LibFabric::gblConfiguration->numnodes * Core::Configuration->numBuildersPerNode;
	buffer = new ListenerMsg[countBU];
	driver->registerSegment(buffer,countBU * sizeof(buffer[0]),FI_RECV);
	
	//setup requests
	requests = new Request[countBU+1];
	
	//setup default listen
	for (int i = 0 ; i < countBU ; i++)
	{
		int buRank = LibFabric::gblConfiguration->builderID(i);
		driver->recvTag(buRank,TAG_EVENT_CREDIT,&buffer[i],sizeof(buffer[i]),&requests[i],false);
	}
	
	//setup wait for exit signal
	static char buffer[] = "exit";
	driver->registerSegment(buffer,sizeof(buffer),FI_RECV);
	driver->recvTag(LibFabric::RANK_EM_CONSUMMER,TAG_EXIT_SIGNAL,buffer,sizeof(buffer),&requests[countBU],false);
}

/*******************  FUNCTION  *********************/
void EMListener::listenForCreditRequest ( int& node, int& numberOfCreditsRequested )
{
	//Wait any
	int ret = driver->waitAny(requests,countBU+1);
	assert(ret >= 0 && ret < countBU + 1);

	//check for exit
	if (ret == countBU)
	{
		node = -1;
		return;
	}
	
	// Get the node and number of credits requested
	numberOfCreditsRequested = buffer[ret].credits;
	node = ret;
	DAQ_DEBUG_ARG("libfabric","Get %1 credit from BU %2").arg(numberOfCreditsRequested).arg(node).end();
	
	//republish the request
	if (ret < countBU)
	{
		int buRank = LibFabric::gblConfiguration->builderID(ret);
		assert(buRank == buffer[ret].sourceRank);
		driver->recvTag(buRank,TAG_EVENT_CREDIT,&buffer[ret],sizeof(buffer[ret]),&requests[ret],false);
	}
}

}
