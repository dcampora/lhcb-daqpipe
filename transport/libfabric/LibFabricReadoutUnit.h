/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef DAQPIPE_LIBFABRIC_READOUT_UNIT_HPP
#define DAQPIPE_LIBFABRIC_READOUT_UNIT_HPP

/********************  HEADERS  *********************/
#include "LibFabricCore.h"
#include "../common/RUTransportCore.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*********************  CLASS  **********************/
class ReadoutUnit : public RUTransportCore
{
	public:
		virtual void initialize (int initialCredits);
		virtual bool EventListen (int &size, int &eventID, int &destinationBU);
		virtual void prepareSendEvent (FragmentComposition * &fc,int destinationBU);
		virtual bool sendAnyEvent (int &eventID);
		virtual void setupSendEvent (char *dataPointer, int size, int eventID,int destinationBU);
		virtual bool sendEvent (int eventID);
		virtual void pullRequestReceive (int &eventID, int &size,int &destinationBU);
		virtual void discardEvent ();
		virtual void registerSegment (void *ptr, size_t size);
	private:
		int initialCredits;
		int activeRequests[LIBFABRIC_MAX_CREDITS];
		int requestIndex[LIBFABRIC_MAX_CREDITS];
		std::list<int> activeRequestsList;
		Request * buRequests;
		Request * requests[LIBFABRIC_MAX_CREDITS];
		Request fragmentRequests[LIBFABRIC_MAX_CREDITS];
		Request waitOrExitSignal[2];
		int size_id_bu[3];
		int * pull_size_id_bu;
		Driver * driver;
};

}

#endif //DAQPIPE_LIBFABRIC_READOUT_UNIT_HPP
