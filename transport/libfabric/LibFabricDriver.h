/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef DAQPIPE_LIBFABRIC_DRIVER_HPP
#define DAQPIPE_LIBFABRIC_DRIVER_HPP

/********************  HEADERS  *********************/
#include "../../common/Debug.hpp"
#include "LibFabricEndpoint.hpp"
#include "HydraLauncher.hpp"
//headers from libfabric
#include <rdma/fabric.h>
#include <rdma/fi_errno.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_rma.h>
//json
#include "../../extern-deps/jsoncpp/dist/json/json.h"

/********************  MACROS  **********************/
#define LIBFABRIC_CHECK_STATUS(call, status) assumeArg(status == 0,"Libfabric get failure on command %1 with status %2 : %3").arg(call).arg(status).arg(fi_strerror(-status)).end();
#define LIBFABRIC_CHECK_AND_GOTO(call, status,label) do {if (status != 0) { DAQ_ERROR_ARG("Libfabric get failure on command %1 with status %2 : %3").arg(call).arg(status).arg(fi_strerror(-status)).end(); goto label;} } while(0)

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*********************  STRUCT  *********************/
struct MemoryRegion
{
	void * ptr;
	size_t size;
	fid_mr * mr;
};

/********************  STRUCT  **********************/
struct DriverConfig
{
	void setup(Json::Value & config);
	int firstPort;
	int portRange;
	std::string epType;
	size_t waitSleep;
	std::string iface;
};

/*********************  STRUCT  *********************/
struct LibfabricAcceptAck
{
	int rank;
};

/*********************  TYPES  **********************/
typedef std::vector<MemoryRegion> LibfabricMemoryRegionVector;
typedef fi_context Request;
typedef std::map<Request*,bool> LibFabricRequestMap;

/********************  STRUCT  **********************/
struct RemoteIOV
{
	void * addr;
	uint64_t key;
};

/*********************  CLASS  **********************/
class Driver
{
	public:
		Driver(void);
		void initialize(Json::Value & config, HydraLauncher & launcher);
		void finalize(void);
		int waitConnection(void);
		void registerSegment(void * ptr,size_t size,int mode=FI_SEND|FI_RECV);
		bool useRDMMode(void);
		void setupConnexion(int rank,HydraLauncher & launcher);
		void recv(int targetRank, void *ptr, size_t size,LibFabric::Request * requests, bool wait);
		void recvTag(int targetRank, int tag, void *ptr, size_t size,LibFabric::Request * requests, bool wait);
		void send(int targetRank, void *ptr, size_t size,LibFabric::Request * requests, bool wait);
		void sendTag(int targetRank, int tag, void *ptr, size_t size,LibFabric::Request * requests, bool wait);
		void recvRDMA(int targetRank, int tag, void * ptr,size_t size, Request * request, bool wait);
		void rdmaWrite(int targetRank, int tag, void *localPtr, size_t size,void *remotePtr, uint64_t remoteKey,Request * request, bool wait);
		int waitAny(Request * requests,int size);
		void waitAll(Request * requests,int size);
		int testAny(Request * requests,int size,bool ignoreCache = false);
		bool testAll(Request * requests,int size);
		RemoteIOV getIOV(void * ptr, size_t size);
		void printDomainsInfos(Json::Value& config, const std::string name,const std::string service);
	private:
		void setupHint(Json::Value & config, fi_info * hints);
		void setupMSGConnections(HydraLauncher & launcher);
		void setupRDMConnections(HydraLauncher & launcher);
		int getServicePort(int rank);
		fid_mr * getFidMR(void * ptr,size_t size);
		MemoryRegion * getMR(void * ptr,size_t size);
		void pushToRequestCache(Request * request);
		Request * checkRequestCache(LibFabric::Request * requests, size_t size,bool remove = true);
		bool checkAllRequestCache(LibFabric::Request * requests, size_t size,bool remove = true);
		fi_ep_type getEpType(void);
		Endpoint & getEndpoint(int targetRank);
	private:
		fi_info * hints;
		fid_fabric * fabric;
		fid_domain * domain;
		fid_pep * listenEndpoint;
		fid_eq * listenQueue;
		enum fi_mr_mode mr_mode;
		bool useRDM;
		std::string providerName;
		DriverConfig config;
		LibfabricAcceptAck ackListen;
		LibfabricAcceptAck ackConnect;
		LibfabricMemoryRegionVector segments;
		Endpoint * endpoints;
		Endpoint rdmEndpoint;
		HydraLauncher * launcher;
		int worldSize;
		LibFabricRequestMap requestCache;
};

}

#endif //DAQPIPE_LIBFABRIC_DRIVER_HPP
