/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include "LibFabricEndpoint.hpp"
#include "LibFabricHelper.hpp"
#include <cstring>
#include <cstdio>
#include <cassert>
#include <rdma/fi_tagged.h>

namespace LibFabric
{

/*******************  FUNCTION  *********************/
/**
 * Endpoint constructor, it mostly init the pointers to NULL to ensure they will be init
 * before usage.
 * @param queueSize Define the queue size, can also be done later with setQueueSize() function.
**/
Endpoint::Endpoint ( int queueSize )
{
	this->queueSize = queueSize;
	recvQueue = NULL;
	sendQueue = NULL;
	endpoint = NULL;
	eventQueue = NULL;
	addressVector = NULL;
	remoteFiAddr = NULL;
	cntOnRecv = 0;
	cntOnSend = 0;
	localRank = -1;
}

/*******************  FUNCTION  *********************/
/**
 * Cleanup the ressources used by the endpoint if initialized.
**/
Endpoint::~Endpoint ( void )
{
	if (endpoint != NULL)
		fi_close(&endpoint->fid);
	if (recvQueue != NULL)
		fi_close(&recvQueue->fid);
	if (sendQueue != NULL)
		fi_close(&sendQueue->fid);
	if (addressVector != NULL)
		fi_close(&addressVector->fid);
	if (remoteFiAddr != NULL)
		delete [] remoteFiAddr;
}

/*******************  FUNCTION  *********************/
/**
 * Check if the current endpoint if configured or not.
**/
bool Endpoint::active ( void )
{
	return endpoint != NULL;
}

/*******************  FUNCTION  *********************/
fi_context* Endpoint::getContext ( fi_cq_data_entry& comp )
{
	if (comp.op_context != NULL)
	{
		return (fi_context*)comp.op_context;
	} else if (comp.flags & FI_REMOTE_CQ_DATA) {
		RDMAContextMap::iterator it = rdmaContextMap.find(comp.data);
		
		//checks
		if (it == rdmaContextMap.end())
		{
			//warn
			DAQ_WARNING_ARG("Fail to get rdmaContextMap entry for tag %1 (%2:%3), use pending storage for future matching")
				.arg(comp.data)
				.arg(comp.data & ~((uint64_t)(1UL<<32)-1))
				.arg(comp.data & ((uint64_t)(1UL<<32)-1))
				.end();
			
			//setup pending
			RDMAContext context;
			context.ptr = comp.buf;
			context.size = comp.len;
			context.tag = comp.data;
			context.request = NULL;
			
			//push to pending list
			rdmaPending.push_back(context);

			//get nothing
			return NULL;
		} else {
			DAQ_DEBUG_ARG("libfabric","Get RDMA write finished on recv side, tag = %1").arg(comp.data).end();
		}

		/*assumeArg(comp.buf == it->second.ptr,"Get mismatch on completion event addresses, expect %1, get %2")
			.arg(it->second.ptr)
			.arg(comp.buf)
			.end();*/
		assumeArg(comp.len == it->second.size,"Get mismatch on completion event size expect %1, get %2")
			.arg(it->second.size)
			.arg(comp.len)
			.end();

		//Extract request
		fi_context * ret = it->second.request;
		
		//remove
		rdmaContextMap.erase(it);
		
		//ok return
		return ret;
	} else {
		DAQ_FATAL("Get invalid completion event in endpoint queue!");
		return NULL;
	}
}

/*******************  FUNCTION  *********************/
void Endpoint::checkRDMAPending (int targetRank, int tag, void* ptr, size_t size, fi_context * request )
{
	//compute full tag
	uint64_t fulltag = (uint64_t)((uint32_t)tag) + (((uint64_t)targetRank) << 32);

	//check in pending
	for (RDMAPendingList::iterator it = rdmaPending.begin() ; it != rdmaPending.end() ; ++it)
	{
		if (it->ptr == ptr && it->size == size && it->tag == fulltag)
			it->request = request;
	}
}

/*******************  FUNCTION  *********************/
fi_context* Endpoint::testPending ( void )
{
	//check in pending
	for (RDMAPendingList::iterator it = rdmaPending.begin() ; it != rdmaPending.end() ; ++it)
	{
		if (it->request != NULL)
		{
			fi_context * request;
			request = it->request;
			rdmaPending.erase(it);
			return request;
		}
	}
	
	//nothing
	return NULL;
}

/*******************  FUNCTION  *********************/
/**
 * Apply a test on the queues to check if there is finished actions.
 * @param recv Enable or disable check of receive queue.
 * @param send Enable or disable check of send queue.
 * @return Return the related context of NULL if no actions can be finished.
**/
fi_context* Endpoint::testQueue ( bool recv, bool send )
{
	//vars
	int ret;
	struct fi_cq_data_entry comp;
	fi_context * context = NULL;
	
	//if not active
	if (active() == false)
		return NULL;
	
	//check pending
	context = testPending();
	if (context != NULL)
		return context;
	
	//if inactive nothing to do
	if (active() == false)
		return NULL;

	//setup queues to check
	fid_cq * queues[2] = {
		recv && cntOnRecv > 0? recvQueue : NULL,
		send && cntOnSend > 0? sendQueue : NULL,
	};
	
	//loop on all to check
	for (int i = 0 ; i < 2 ; i++)
	{
		if (queues[i] != NULL)
		{
			//get cq entry
			ret = fi_cq_read(queues[i], &comp, 1);
			
			//if get one
			if (ret > 0) {
				context = getContext(comp);
				if (i == 0)
					cntOnRecv--;
				else if (i == 1)
					cntOnSend--;
				assert(cntOnRecv >=0 );
				assert(cntOnSend >=0 );
				break;
			} else if (ret < 0 && ret != -FI_EAGAIN) {
				if (ret == -FI_EAVAIL) {
					LIBFABRIC_CQ_FATAL_ERROR("fi_cq_read",queues[i]);
				} else {
					LIBFABRIC_CHECK_STATUS("fi_cq_read", ret);
				}
			}
		}
	}

	//return
	return context;
}

/*******************  FUNCTION  *********************/
/**
 * Setup an event queue from the given fabric.
 * @param fabric Fabric from which to build the event queue.
**/
int Endpoint::allocEventQueue (  fid_fabric * fabric )
{
	//check
	assert(fabric != NULL);
	assert(eventQueue == NULL);
	
	//setup
	struct fi_eq_attr cm_attr;
	int ret;

	//setup
	memset(&cm_attr, 0, sizeof cm_attr);
	cm_attr.wait_obj = FI_WAIT_FD;
	ret = fi_eq_open(fabric, &cm_attr, &eventQueue, NULL);
	LIBFABRIC_CHECK_STATUS("fi_eq_open", ret);

	//status ok
	return ret;
}

/*******************  FUNCTION  *********************/
/**
 * Configure the queue size, can only be done before usage of connect()/accept().
 * @param size Define the new size of the queues.
**/
void Endpoint::setQueueSize ( int size )
{
	//must never append after initialization of the queue
	assert(endpoint == NULL);
	this->queueSize = size;
}

/*******************  FUNCTION  *********************/
/**
 * Allocate endpoint ressources (queues & endpoint).
 * @param fabric Fabric to use to create the ressources.
 * @param domain Domain to which attached the ressources.
 * @param fi Info to build the endpoint.
 * @return 0 for success of an FI error ID in cas of error.
**/
int Endpoint::allocRessources ( fid_fabric * fabric, fid_domain * domain, fi_info * fi )
{
	//some checks
	assert(fabric != NULL);
	assert(domain != NULL);
	assert(fi != NULL);
	assert(endpoint == NULL);
	assert(sendQueue == NULL);
	assert(recvQueue == NULL);
	
	//errors
	assert(queueSize != -1);
	
	struct fi_cq_attr cq_attr;
	int ret;

	//setup some attributes
	memset(&cq_attr, 0, sizeof cq_attr);
	cq_attr.format = FI_CQ_FORMAT_DATA;
	cq_attr.wait_obj = FI_WAIT_NONE;
	cq_attr.size = queueSize;
	
	//open send queue & recv queue
	ret = fi_cq_open(domain, &cq_attr, &sendQueue, NULL);
	LIBFABRIC_CHECK_AND_GOTO("fi_cq_open", ret,err1);
	ret = fi_cq_open(domain, &cq_attr, &recvQueue, NULL);
	LIBFABRIC_CHECK_AND_GOTO("fi_cq_open", ret,err2);

	//setup event queue
	if (!eventQueue) {
		ret = allocEventQueue(fabric);
		if (ret)
			goto err4;
	}

	//setup endpoint
	ret = fi_endpoint(domain, fi, &endpoint, NULL);
	LIBFABRIC_CHECK_AND_GOTO("fi_endpoint", ret,err4);

	//ok
	return 0;

	//manage errors
	err4:
	err3:
		fi_close(&recvQueue->fid);
	err2:
		fi_close(&sendQueue->fid);
	err1:
		return ret;
}

/*******************  FUNCTION  *********************/
/**
 * Bind ressources to the endpoint.
**/
int Endpoint::bindRessources ( bool useEventQueue )
{
	//checks
	assert(endpoint != NULL);
	assert(eventQueue != NULL);
	assert(sendQueue != NULL);
	assert(recvQueue != NULL);
	
	int ret;

	if (useEventQueue)
	{
		ret = fi_ep_bind(endpoint, &eventQueue->fid, 0);
		LIBFABRIC_CHECK_STATUS("fi_ep_bind", ret);
	}

	ret = fi_ep_bind(endpoint, &sendQueue->fid, FI_SEND|FI_WRITE);
	LIBFABRIC_CHECK_STATUS("fi_ep_bind", ret);

	ret = fi_ep_bind(endpoint, &recvQueue->fid, FI_RECV);
	LIBFABRIC_CHECK_STATUS("fi_ep_bind", ret);

	ret = fi_enable(endpoint);
	LIBFABRIC_CHECK_STATUS("fi_enable", ret);

	return ret;
}

/*******************  FUNCTION  *********************/
/**
 * Free the endpoint ressources
**/
void Endpoint::freeRessources ( void )
{
	//check
	assert(endpoint != NULL);
	assert(recvQueue != NULL);
	assert(sendQueue != NULL);
	
	//close all
	if (endpoint != NULL)
		fi_close(&endpoint->fid);
	if (recvQueue != NULL)
		fi_close(&recvQueue->fid);
	if (sendQueue != NULL)
		fi_close(&sendQueue->fid);
	if (eventQueue!= NULL)
		fi_close(&eventQueue->fid);
	
	//setup to NULL
	endpoint = NULL;
	recvQueue = NULL;
	sendQueue = NULL;
}

/*******************  FUNCTION  *********************/
/**
 * Establish a connection using the given fabric and domain and using the target informations.
 * @param localRank Define the local rank used to compute some tags on RDMA
 * @param fabric Fabric to use to establish the connection.
 * @param domain Domain in which to establish the connection.
 * @param fi Informations about the target.
**/
int Endpoint::connect(int localRank,fid_fabric * fabric,fid_domain * domain,fi_info * fi)
{
	//some checks
	assert(fabric != NULL);
	assert(domain != NULL);
	assert(fi != NULL);
	assert(endpoint == NULL);
	assert(sendQueue == NULL);
	assert(recvQueue == NULL);
	
	//vars
	struct fi_eq_cm_entry entry;
	uint32_t event;
	ssize_t rd;
	int ret;
	
	//locals
	this->localRank = localRank;

	//alloc ressources
	ret = allocRessources(fabric,domain,fi);
	if (ret)
		goto err3;

	//bind ressources
	ret = bindRessources(true);
	if (ret)
		goto err4;

	//etablish connection
	ret = fi_connect(endpoint, fi->dest_addr, NULL, 0);
	LIBFABRIC_CHECK_AND_GOTO("fi_connect", ret,err4);

	//check event in event queue
	rd = fi_eq_sread(eventQueue, &event, &entry, sizeof entry, -1, 0);
	if (rd != sizeof entry) {
		DAQ_ERROR_ARG("%1").arg(Helper::getEqErrString(eventQueue,"fi_eq_sread")).end();
		ret = (int) rd;
		goto err4;
	}

	//check event type
	if (event != FI_CONNECTED || entry.fid != &endpoint->fid) {
		DAQ_ERROR_ARG("Unexpected CM event %1 fid %2 (ep %3)").arg(event).arg(entry.fid).arg(endpoint).end();
		ret = -FI_EOTHER;
		goto err4;
	}

	//ok
	return 0;
	
	//manage errors
	err3:
	err4:
		freeRessources();
	err0:
		return ret;
}

/*******************  FUNCTION  *********************/
/**
 * Setup and RDM endpoint connected by default to all available tasks.
 * @param fabric Fabric to use to create the endpoint
 * @param domain Domain to use to create the endpoint
 * @param fi Information on the endpoint
 * @param launcher Used to get remote addresses from other tasks.
**/
int Endpoint::setupRDM ( fid_fabric * fabric, fid_domain * domain, fi_info * fi ,HydraLauncher & launcher)
{
	//some checks
	assert(fabric != NULL);
	assert(domain != NULL);
	assert(fi != NULL);
	assert(endpoint == NULL);
	assert(sendQueue == NULL);
	assert(recvQueue == NULL);
	
	//some vars
	struct fi_eq_cm_entry entry;
	uint32_t event;
	ssize_t rd;
	int ret;
	size_t addrLen = 0;
	void * addr = NULL;
	std::string remoteAddr;

	ret = allocRessources(fabric,domain,fi);
	if (ret)
		goto err3;

	ret = bindRessources(false);
	if (ret)
		goto err4;

	//setup address vector
	this->setupAddressVector(fabric,domain,fi,launcher);
	
	//enable it
	ret = fi_enable(endpoint);
	LIBFABRIC_CHECK_STATUS("fi_enable", ret);
	
	return 0;

err3:
err4:
	freeRessources();
err0:
	return ret;
}

/*******************  FUNCTION  *********************/
/**
 * Accept an incomming connection and build the related endpoint.
 * @param localRank Define the local rank used to compute some tags for RDMA
 * @param fabric Fabric to use.
 * @param domain Domain to use.
 * @param fi Information about the remote target.
**/
int Endpoint::accept (int localRank, fid_fabric* fabric, fid_domain* domain, fi_info* fi )
{
	//some checks
	assert(fabric != NULL);
	assert(domain != NULL);
	assert(fi != NULL);
	assert(endpoint == NULL);
	assert(sendQueue == NULL);
	assert(recvQueue == NULL);
	
	//vars
	int ret;
	ssize_t rd;
	uint32_t event;
	struct fi_eq_cm_entry entry;
	
	//locals
	this->localRank = localRank;
	
	//setup ressources
	ret = allocRessources(fabric,domain,fi);
	if (ret)
		 return ret;

	//bind them
	ret = bindRessources(true);
	if (ret)
		return ret;

	//accept the connection
	ret = fi_accept(endpoint, NULL, 0);
	LIBFABRIC_CHECK_STATUS("fi_accept", ret);

	//check in event queue
	rd = fi_eq_sread(eventQueue, &event, &entry, sizeof entry, -1, 0);
	if (rd != sizeof entry) {
		LIBFABRIC_EQ_FATAL_ERROR("fi_eq_sread",eventQueue);
		ret = (int) rd;
		return ret;
	}

	//check event type to be correct
	if (event != FI_CONNECTED || entry.fid != &endpoint->fid) {
		DAQ_ERROR_ARG("Unexpected CM event %1 fid %2 (ep %3)").arg(event).arg(entry.fid).arg(endpoint).end();
		ret = -FI_EOTHER;
		return ret;
	}

	//ok
	return 0;
}

/*******************  FUNCTION  *********************/
bool Endpoint::recvRDMACheckPending ( RDMAContext& context, fi_context * request )
{
	//check if match with pending
	for (RDMAPendingList::iterator it = rdmaPending.begin() ; it != rdmaPending.end() ; ++it)
	{
		if (it->ptr == context.ptr && it->size == context.size && it->tag == context.tag)
		{
			it->request = request;
			return true;
		}
	}
	return false;
}

/*******************  FUNCTION  *********************/
void Endpoint::writeRDMA ( int targetRank, int tag, fid_mr * mr, void* sourcePtr, size_t size, void* remotePtr, uint64_t remoteKey, fi_context* request )
{
	assert(localRank != -1);
	
	//get addr
	fi_addr_t addr = 0;
	if (targetRank >= 0 && remoteFiAddr != NULL)
		addr = remoteFiAddr[targetRank];
	
	//get local mr
	void * mrDesc = NULL;
	if (mr != NULL)
		mrDesc = fi_mr_desc(mr);
	
	//compute tag
	uint64_t fulltag = (uint64_t)((uint32_t)tag) + (((uint64_t)localRank) << 32);
	
	//do write with remote cq ack
	int ret = fi_writedata(endpoint, sourcePtr, size, fi_mr_desc(mr), fulltag,addr, (uint64_t)remotePtr, remoteKey,request);
	LIBFABRIC_CHECK_STATUS("fi_writedata", ret);
	
	//update counter
	cntOnSend++;
	assert(cntOnSend < queueSize);
}

/*******************  FUNCTION  *********************/
void Endpoint::recvRDMA ( int targetRank, int tag, void* ptr, size_t size, fi_context* request )
{
	//setup context
	RDMAContext context;
	context.request = request;
	context.ptr = ptr;
	context.size = size;
	context.tag = tag;
	uint64_t fulltag = (uint64_t)((uint32_t)tag) + (((uint64_t)targetRank) << 32);
	
	//register to wait list if not in pending
	if (recvRDMACheckPending(context,request) == false)
	{
		assert(this->rdmaContextMap.find(fulltag) == this->rdmaContextMap.end());
		this->rdmaContextMap[fulltag] = context;
		cntOnRecv++;
	}
}

/*******************  FUNCTION  *********************/
void Endpoint::sendMsg ( void* ptr, size_t size , fid_mr * mr , int targetRank, fi_context * context , bool wait)
{
	fi_addr_t addr = 0;
	if (targetRank >= 0 && remoteFiAddr != NULL)
		addr = remoteFiAddr[targetRank];
	
	void * mrDesc = NULL;
	if (mr != NULL)
		mrDesc = fi_mr_desc(mr);
	
	int ret = fi_send(endpoint, ptr, (size_t) size, mrDesc, addr, context);
	LIBFABRIC_CHECK_STATUS("fi_send", ret);
	
	if (wait)
	{
		DAQ_DEBUG("libfabric","Wait for send completion");
		Helper::waitForCompletion(sendQueue,1);
	} else {
		cntOnSend++;
		assert(cntOnSend < queueSize);
	}
}

/*******************  FUNCTION  *********************/
void Endpoint::recvMsg ( void* ptr, size_t size, fid_mr * mr, int targetRank,fi_context * context,bool wait)
{
	fi_addr_t  addr = 0;
	if (targetRank >= 0 && remoteFiAddr != NULL)
		addr = remoteFiAddr[targetRank];
	
	void * mrDesc = NULL;
	if (mr != NULL)
		mrDesc = fi_mr_desc(mr);
	
	int ret = fi_recv(endpoint, ptr, (size_t) size, mrDesc, addr, context);
	LIBFABRIC_CHECK_STATUS("fi_recv", ret);

	if (wait)
	{
		DAQ_DEBUG("libfabric","Wait for recv completion");
		Helper::waitForCompletion(recvQueue,1);
	} else {
		DAQ_DEBUG("libfabric","Has post msg recv");
		cntOnRecv++;
		assert(cntOnRecv < queueSize);
	}
}

/*******************  FUNCTION  *********************/
void Endpoint::recvTag ( void* ptr, size_t size, fid_mr* mr, int targetRank, int tag, fi_context* context, bool wait )
{
	fi_addr_t  addr = 0;
	if (targetRank >= 0 && remoteFiAddr != NULL)
		addr = remoteFiAddr[targetRank];
	
	void * mrDesc = NULL;
	if (mr != NULL)
		mrDesc = fi_mr_desc(mr);
	
	uint64_t fulltag = (uint64_t)((uint32_t)tag) + (((uint64_t)targetRank) << 32);
	
	int ret = fi_trecv(endpoint, ptr, (size_t) size, mrDesc, addr,fulltag,0, context);
	LIBFABRIC_CHECK_STATUS("fi_recv", ret);

	if (wait)
	{
		DAQ_DEBUG("libfabric","Wait for recv completion");
		Helper::waitForCompletion(recvQueue,1);
	} else {
		DAQ_DEBUG("libfabric","Has post msg recv");
		cntOnRecv++;
		assert(cntOnRecv < queueSize);
	}
}

/*******************  FUNCTION  *********************/
void Endpoint::sendTag ( void* ptr, size_t size, fid_mr* mr, int targetRank, int tag, fi_context* context, bool wait )
{
	fi_addr_t  addr = 0;
	if (targetRank >= 0 && remoteFiAddr != NULL)
		addr = remoteFiAddr[targetRank];
	
	void * mrDesc = NULL;
	if (mr != NULL)
		mrDesc = fi_mr_desc(mr);
	
	//compute tag
	uint64_t fulltag = (uint64_t)((uint32_t)tag) + (((uint64_t)localRank) << 32);
	
	int ret = fi_tsend(endpoint, ptr, (size_t) size, mrDesc, addr,fulltag,context);
	LIBFABRIC_CHECK_STATUS("fi_recv", ret);

	if (wait)
	{
		DAQ_DEBUG("libfabric","Wait for recv completion");
		Helper::waitForCompletion(recvQueue,1);
	} else {
		DAQ_DEBUG("libfabric","Has post msg recv");
		cntOnSend++;
		assert(cntOnSend < queueSize);
	}
}

/*******************  FUNCTION  *********************/
void Endpoint::import ( Endpoint& endpoint )
{
	//check
	assert(this->recvQueue == NULL);
	assert(this->sendQueue == NULL);
	assert(this->endpoint == NULL);
	assert(this->eventQueue == NULL);
	assert(this->localRank == -1);
	
	//copy
	this->queueSize = endpoint.queueSize;
	this->recvQueue = endpoint.recvQueue;
	this->sendQueue = endpoint.sendQueue;
	this->endpoint = endpoint.endpoint;
	this->eventQueue = endpoint.eventQueue;
	this->localRank = endpoint.localRank;
	
	//reset source
	endpoint.recvQueue = NULL;
	endpoint.sendQueue = NULL;
	endpoint.eventQueue = NULL;
	endpoint.endpoint = NULL;
	endpoint.localRank = -1;
}

/*******************  FUNCTION  *********************/
int Endpoint::setupAddressVector ( fid_fabric* fabric, fid_domain* domain, fi_info* fi, HydraLauncher& launcher )
{
	fi_av_attr av_attr;
	
	//setup vector
	memset(&av_attr, 0, sizeof av_attr);
	av_attr.type = fi->domain_attr->av_type ?fi->domain_attr->av_type : FI_AV_MAP;
	av_attr.count = launcher.getWorldSize();
	av_attr.name = NULL;
	
	//local rank
	this->localRank = launcher.getRank();
	
	//open vector
	int ret = fi_av_open(domain, &av_attr, &addressVector, NULL);
	LIBFABRIC_CHECK_AND_GOTO("fi_av_open", ret,err4);
	
	//bind
	ret = fi_ep_bind(endpoint, &addressVector->fid, 0);
	LIBFABRIC_CHECK_STATUS("fi_ep_bind", ret);
	
	return fillAddressVector(launcher);
err4:
	return ret;
}

/*******************  FUNCTION  *********************/
int Endpoint::fillAddressVector ( HydraLauncher& launcher )
{
	//get local address
	size_t addrlen = 0;
	void * hwAddr = NULL;
	
	//allocate some data
	remoteFiAddr = new fi_addr_t[launcher.getWorldSize()];
	
	//get size (addrlen 0 say to not write anything into localAddr
	int ret = fi_getname(&endpoint->fid, hwAddr, &addrlen);
	if (ret != -FI_ETOOSMALL)
		LIBFABRIC_CHECK_STATUS("fi_getname", ret);
	
	//alloc and get the name
	hwAddr = new char[addrlen];
	ret = fi_getname(&endpoint->fid, hwAddr, &addrlen);
	LIBFABRIC_CHECK_STATUS("fi_getname", ret);
	
	//convert to hex
	std::string hexHwAddr = Helper::convertAddrToHex(hwAddr,addrlen);

	//register to world and wait all
	launcher.set("hwaddr",hexHwAddr);
	launcher.commit();
	launcher.barrier();
	
	//loop on all to setup
	for (int i = 0 ; i < launcher.getWorldSize() ; i++)
	{
		//get from db for rank i & convert back
		hexHwAddr = launcher.get("hwaddr",i);
		Helper::convertAddrFromHex(hwAddr,addrlen,hexHwAddr);
		
		//insert into address vector
		ret = fi_av_insert(addressVector, hwAddr, 1, &remoteFiAddr[i], 0,NULL);//&fi_ctx_av);
		if (ret != 1)
			LIBFABRIC_CHECK_STATUS("fi_av_insert", ret);
	}
	
	//wait for all to have finish
	launcher.barrier();
	
	//free memory
	free(hwAddr);
	
	return 0;
}

}
