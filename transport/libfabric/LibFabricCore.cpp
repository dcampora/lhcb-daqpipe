/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include <pmi.h>
#include <cassert>
#include "../../common/Debug.hpp"
#include "LibFabricCore.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric 
{

/********************  GLOBALS  *********************/
ConfigurationLoader * gblConfiguration = NULL;

/*******************  FUNCTION  *********************/
ConfigurationLoader::ConfigurationLoader ( void )
{
	assert(gblConfiguration == NULL);
	gblConfiguration = this;
	this->nodeTypeMap = NULL;
}

/*******************  FUNCTION  *********************/
ConfigurationLoader::~ConfigurationLoader ( void )
{
	if (this->nodeTypeMap != NULL)
		delete [] this->nodeTypeMap;
}

/*******************  FUNCTION  *********************/
void ConfigurationLoader::initialize ( int argc, char** argv )
{
	//enable debug message for libfabric
	DAQ::Debug::enableCat("libfabric");
	
	//init launcher
	launcher.initilize();
	this->worldSize = launcher.getWorldSize();
	this->rankID = launcher.getRank();
	
	//check
	assume(this->worldSize >= 4,"This program require at least 4 task to run");

	//debug info
	char hostname[256];
	gethostname(hostname,sizeof(hostname));
	DAQ_DEBUG_ARG("libfabric","Start %1/%2 on %3").arg(rankID).arg(worldSize).arg(hostname).end();
	
	//setup locals
	procID = rankID % numProcsPerNode;
	
	//setup ids
	ru_startid = LibFabric::READOUT_UNIT;
	ru_endid = LibFabric::READOUT_UNIT + numReadoutsPerNode - 1;
	bu_startid = ru_endid + 1;
	bu_endid = bu_startid + numBuildersPerNode - 1;
	int numprocs = launcher.getWorldSize();
	numnodes = (int) (numprocs / (float) numProcsPerNode) - 1; // Ruling out the EM
	numTotalReadouts = numnodes * numReadoutsPerNode;
	
	//setup names
	logicalNames[EVENT_MANAGER_LISTENER] = "eml";
	logicalNames[EVENT_MANAGER_CONSUMER] = "emc";
	logicalNames[BUILDER_UNIT] = "bu";
	logicalNames[READOUT_UNIT] = "ru";
	
	//build node map
	this->buildNodeTypeMap();
	
	//setup libfabric driver
	driver.initialize(config_root,launcher);
	
	//establish connections
	if (driver.useRDMMode() == false)
	{
		establishMSGConnexions();
		DAQ_DEBUG_ARG("libfabric","Node %1 was connected to others, ready to run").arg(launcher.getRank()).end();
	}
}

/*******************  FUNCTION  *********************/
void ConfigurationLoader::establishMSGConnexions ( void )
{
	//number of nodes to wait for connexion
	int toWait = 0;

	switch(getNodeType())
	{
		case NODE_EVENT_CONSUMMER:
			//event consummer have wait listen from all other nodes except himself
			toWait = launcher.getWorldSize() - 1;
			break;
		case NODE_EVENT_LISTENER:
			//connect to consummer
			DAQ_DEBUG_ARG("libfabric","Listener %1 try to setup connexion to consummer").arg(launcher.getRank()).end();
			driver.setupConnexion(RANK_EM_CONSUMMER,launcher);
			DAQ_DEBUG_ARG("libfabric","Listener %1 has setup connexion to consummer").arg(launcher.getRank()).end();
			//have to wait all except himself and consummer
			toWait = launcher.getWorldSize() - 2;
			break;
		case NODE_READOUT_UNIT:
			//connect to listener and consummer
			DAQ_DEBUG_ARG("libfabric","RU %1 try to setup connexion to consummer/listener").arg(launcher.getRank()).end();
			driver.setupConnexion(RANK_EM_LISTENER,launcher);
			driver.setupConnexion(RANK_EM_CONSUMMER,launcher);
			//connect to all BUs
			DAQ_DEBUG_ARG("libfabric","RU %1 try to setup connexion to BU").arg(launcher.getRank()).end();
			for (int i = 0 ; i < (launcher.getWorldSize() - 2)/2 ; i++)
				driver.setupConnexion(builderID(i),launcher);
			//wait none
			toWait = 0;
			break;
		case NODE_BUILDER_UNIT:
			//connect to listener and consummer
			DAQ_DEBUG_ARG("libfabric","BU %1 try to setup connexion to consummer/listener").arg(launcher.getRank()).end();
			driver.setupConnexion(RANK_EM_LISTENER,launcher);
			driver.setupConnexion(RANK_EM_CONSUMMER,launcher);
			//wait connexion from RUs
			toWait = (launcher.getWorldSize() -2)/2;
			break;
		default:
			DAQ_FATAL("Invalid node type");
			break;
	}
	
	//Wait
	DAQ_DEBUG_ARG("libfabric","%1 wait for %2 connexions").arg(launcher.getRank()).arg(toWait).end();
	for (int i = 0 ; i < toWait ; i++)
		driver.waitConnection();
}

/*******************  FUNCTION  *********************/
void ConfigurationLoader::finalize ( void )
{
	//finish driver for clean exit
	driver.finalize();
	
	//finish launcher
	launcher.finalize();
}

/*******************  FUNCTION  *********************/
void ConfigurationLoader::parseConfiguration ( void )
{
}

/*******************  FUNCTION  *********************/
int ConfigurationLoader::builderID ( int builderNumber )
{
	return bu_startid + (builderNumber % numBuildersPerNode) +
	       // return numReadoutsPerNode + (builderNumber % numBuildersPerNode) +
	       (builderNumber / numBuildersPerNode) * numProcsPerNode + numProcsPerNode;
}

/*******************  FUNCTION  *********************/
bool ConfigurationLoader::builderInSameNode ( int builderNumber )
{
	// I'm a RU, I want to know if the builder builderNumber is on the same node as me.
    // Simple. Simplest!
    return nodeNumber(rankID) == nodeNumber(builderID(builderNumber));
}

/*******************  FUNCTION  *********************/
int ConfigurationLoader::builderNumber ( int builderID )
{
	return ((builderID - numProcsPerNode) / numProcsPerNode) * numBuildersPerNode +
	       ((builderID - numProcsPerNode - bu_startid) % numProcsPerNode) + numProcsPerNode;
}

/*******************  FUNCTION  *********************/
std::string ConfigurationLoader::getLogicalName()
{
	std::string post = std::string("_") + name;

    if (isEMListener()) return logicalNames[EVENT_MANAGER_LISTENER] + post;
    else if (isEMConsumer()) return logicalNames[EVENT_MANAGER_CONSUMER] + post;
    else if (is(READOUT_UNIT)) return logicalNames[READOUT_UNIT] + readoutSuffix() + post;
    else if (is(BUILDER_UNIT)) return logicalNames[BUILDER_UNIT] + builderSuffix() + post;
    else return std::string("p") + Tools::toString<int>(rankID);
}

/*******************  FUNCTION  *********************/
bool ConfigurationLoader::is ( int id )
{
	//from MPICore
	DEBUG << "ConfigurationLoader::is: " << id << std::endl;
    
	if (id == BUILDER_UNIT) return isBU();
	else if (id == READOUT_UNIT) return isRU();
    
	ERROR << "Unexpected ConfigurationLoader::is with id " << id << std::endl;
	return false;
}

/*******************  FUNCTION  *********************/
int ConfigurationLoader::nodeID()
{
	return rankID;
}

/*******************  FUNCTION  *********************/
int ConfigurationLoader::nodeNumber ( int rankID )
{
	//from MPICore
	return rankID / numProcsPerNode;
}

/*******************  FUNCTION  *********************/
int ConfigurationLoader::readoutID ( int readoutNumber )
{
	//from MPICore
	return ru_startid + (readoutNumber % numReadoutsPerNode) +
    // return  + (readoutNumber % numReadoutsPerNode) +
        (readoutNumber / numReadoutsPerNode) * numProcsPerNode + numProcsPerNode;
}

/*******************  FUNCTION  *********************/
bool ConfigurationLoader::readoutInSameNode ( int id )
{
	//from MPICore
	return nodeNumber(rankID) == nodeNumber(readoutID(id));
}

/*******************  FUNCTION  *********************/
int ConfigurationLoader::readoutNumber ( int readoutID )
{
	//from MPICore
	return ((readoutID - numProcsPerNode) / numProcsPerNode) * numReadoutsPerNode +
	       ((readoutID - numProcsPerNode - ru_startid) % numProcsPerNode);
}

/*******************  FUNCTION  *********************/
bool ConfigurationLoader::isEMConsumer ( void )
{
	return (rankID == RANK_EM_CONSUMMER);
}

/*******************  FUNCTION  *********************/
bool ConfigurationLoader::isEMListener ( void )
{
	return (rankID == RANK_EM_LISTENER);
}

/*******************  FUNCTION  *********************/
bool ConfigurationLoader::isRU(){
	//From MPICore
	return !isEMListener() && !isEMConsumer() &&
	       rankID >= numProcsPerNode &&
	       procID >= ru_startid && procID <= ru_endid;
}

/*******************  FUNCTION  *********************/
bool ConfigurationLoader::isBU(){
	//From MPICore
	return !isEMListener() && !isEMConsumer() &&
	       rankID >= numProcsPerNode &&
	       procID >= bu_startid && procID <= bu_endid;
}

/*******************  FUNCTION  *********************/
//This is to know how to interconnect the nodes
//RU as to be connected to BU and EC and EL
//BU as to be connected to RU and EC and EL
void ConfigurationLoader::buildNodeTypeMap ( void )
{
	//get node type and go to str
	char nodeTypeStr[32];
	sprintf(nodeTypeStr,"%d",(int)getNodeType());
	
	//reg into launcher DB
	launcher.set("node-type",nodeTypeStr);
	
	//ensure exhange
	launcher.commit();
	launcher.barrier();
	
	//now build local complete map
	nodeTypeMap = new NodeType[worldSize];
	
	//fill map
	for (int i = 0 ; i < worldSize ; i++)
	{
		std::string tmp = launcher.get("node-type",i);
		assert(tmp.empty() == false);
		nodeTypeMap[i] = (NodeType)atoi(tmp.c_str());
	}
}

/*******************  FUNCTION  *********************/
NodeType ConfigurationLoader::getNodeType ( void )
{
	if (isEMConsumer())
	{
		return NODE_EVENT_CONSUMMER;
	} else if (isEMListener()) {
		return NODE_EVENT_LISTENER;
	} else if (isBU()) {
		return NODE_BUILDER_UNIT;
	} else if (isRU()) {
		return NODE_READOUT_UNIT;
	} else {
		DAQ_FATAL("Cannot find current node type");
		return NODE_INVALID;
	}
}

/*******************  FUNCTION  *********************/
int ConfigurationLoader::getWorldSize ( void )
{
	return worldSize;
}

/*******************  FUNCTION  *********************/
std::string ConfigurationLoader::builderSuffix(){
    if (numBuildersPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(builderNumber(rankID));
    }
}

/*******************  FUNCTION  *********************/
std::string ConfigurationLoader::readoutSuffix(){
    if (numReadoutsPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(readoutNumber(rankID));
    }
}

}
