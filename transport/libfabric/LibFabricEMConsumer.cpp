/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

/********************  HEADERS  *********************/
#include "../../common/Debug.hpp"
#include "LibFabricEMConsumer.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*******************  FUNCTION  *********************/
EMConsumer::EMConsumer ( void )
{
	requests = new Request[gblConfiguration->numTotalReadouts];
	driver = &gblConfiguration->driver;
}

/*******************  FUNCTION  *********************/
EMConsumer::~EMConsumer ( void )
{
	delete [] requests;
}

/*******************  FUNCTION  *********************/
void EMConsumer::sendExit ( void )
{
	//debug
	DAQ_DEBUG("libfabric","EMConsumer: Send exit to all");
	
	//loop on all
	int ranks = gblConfiguration->getWorldSize();
	for (int i=0; i< ranks; ++i)
	{
		if (i != RANK_EM_CONSUMMER)
		{
			DAQ_DEBUG_ARG("libfabric","EMConsumer: Send exit event to %1").arg(i).end();
			char buffer[5];
			driver->sendTag(i,TAG_EXIT_SIGNAL,buffer,sizeof(buffer),NULL,true);
		}
	}    
}

/*******************  FUNCTION  *********************/
void EMConsumer::sendToBU ( int*& size_id, int destinationBU )
{
	DAQ_DEBUG_ARG("libfabric","EMConsumer: sendToBU: source=%1, rank=%2, id=%3")
			.arg(gblConfiguration->rankID)
			.arg(gblConfiguration->builderID(destinationBU))
			.arg(TAG_EVENT_ASSIGN)
			.end();

	driver->sendTag(gblConfiguration->builderID(destinationBU),TAG_EVENT_ASSIGN,size_id,2*sizeof(size_id[0]),NULL,true);
}

/*******************  FUNCTION  *********************/
void EMConsumer::sendToRUs ( int*& size_id_bu )
{
	DAQ_DEBUG("libfabric","EMConsumer: sending to RUs! :D");

	// Iterate in RUs and send packets of {size, id, bu}
	for (int i=0; i<gblConfiguration->numTotalReadouts; ++i)
	{
		DAQ_DEBUG_ARG("libfabric","EMConsumer: source=%1, rank=%2, id=%3")
			.arg(gblConfiguration->rankID)
			.arg(gblConfiguration->readoutID(i))
			.arg(TAG_EVENT_REQUEST)
			.end();
    
		//post send
		driver->sendTag(gblConfiguration->readoutID(i),TAG_EVENT_REQUEST,size_id_bu,3*sizeof(size_id_bu[0]),&requests[i],false);
	}
    
	//wait
	driver->waitAll(requests,gblConfiguration->numTotalReadouts);

	DAQ_DEBUG("libfabric","EMConsumer: All sent!");
}

}
