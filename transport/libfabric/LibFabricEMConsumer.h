/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef DAQPIPE_LIBFABRIC_EM_CONSUMER_HPP
#define DAQPIPE_LIBFABRIC_EM_CONSUMER_HPP

/********************  HEADERS  *********************/
#include "LibFabricCore.h"
#include "../common/EMTransportCoreConsumer.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*********************  CLASS  **********************/
class EMConsumer : public EMTransportCoreConsumer
{
	public:
		EMConsumer(void);
		virtual ~EMConsumer(void);
		virtual void sendExit (void);
		virtual void sendToBU (int *&size_id, int destinationBU);
		virtual void sendToRUs (int *&size_id_bu);
	private:
		Request * requests;
		Driver * driver;
};

}

#endif //DAQPIPE_LIBFABRIC_EM_CONSUMER_HPP
