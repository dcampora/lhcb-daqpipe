/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 07/2015
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef DAQPIPE_LIBFABRIC_BUILDER_UNIT_HPP
#define DAQPIPE_LIBFABRIC_BUILDER_UNIT_HPP

/********************  HEADERS  *********************/
#include "LibFabricCore.h"
#include "../common/BUTransportCore.h"

/*******************  NAMESPACE  ********************/
namespace LibFabric
{

/*********************  CLASS  **********************/
class BuilderUnit : public BUTransportCore
{
	public:
		virtual ~BuilderUnit(void);
		virtual void initialize (int initialCredits);
		virtual bool checkExitMessage (void);
		virtual void prepareReceiveEvent (FragmentComposition * fc);
		virtual bool prepareReceiveEventComplete (int &fragmentReceived);
		virtual bool receiveEvent (int &finishedEventID, int &transmissionSize);
		virtual void receiveSizeAndID (int &eventSize, int &eventID);
		virtual void sendCredits (int &numberOfCredits);
		virtual void setupReceiveEvent (char *dataPointer, int size,int readoutIndex, int eventID,Core::fragmentType datatype);
		virtual void pullRequest (int *size_id_bu, int &readoutno);
		virtual void discardEvent ();
		virtual void registerSegment (void *ptr, size_t size);
	private:
		int initialCredits;
		Request * fragmentDescriptorRequests[LIBFABRIC_MAX_CREDITS];
		Request * dataRequests[LIBFABRIC_MAX_CREDITS];
		Request * dataPointerRequests[LIBFABRIC_MAX_CREDITS];
		RemoteIOV * dataRemoteIOV[LIBFABRIC_MAX_CREDITS];
		int activeRequests[LIBFABRIC_MAX_CREDITS];
		int requestIndex[LIBFABRIC_MAX_CREDITS];
		int currentFragmentRequest[LIBFABRIC_MAX_CREDITS];
		int transmissionSize[LIBFABRIC_MAX_CREDITS];
		int * requestReadoutIndex[LIBFABRIC_MAX_CREDITS];
		int size_id[2];
		int currentSlot;
		FragmentComposition * activeFragments[LIBFABRIC_MAX_CREDITS];
		std::list<int> activeRequestsList;
		Request exitRequest;
		Driver * driver;
};

}

#endif //DAQPIPE_LIBFABRIC_BUILDER_UNIT_HPP
