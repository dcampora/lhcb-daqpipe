
/**
 *      MPICore
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef MPICORE
#define MPICORE 1

#include "mpi.h"
#include "MPIBuilderUnit.h"
#include "../../common/Logging.h"
#include "../config/ConfigurationCore.h"
#include "../../common/Tools.h"

namespace MPI_transport {

enum messages {
    CREDIT_ANNOUNCE = 10,
    EVENT_REQUEST,
    EVENT_ASSIGN,
    PULL_REQUEST,
	EXIT_SIGNAL,
    EVENT_SEND = 100, // From 100 onwards reserved for event IDs

    EVENT_FRAGMENT_COMPOSITION = 101,
    EVENT_SEND_FRAGMENT = 102,
};

enum ids {
    EVENT_MANAGER_LISTENER = 0,
    EVENT_MANAGER_CONSUMER = 1,
    READOUT_UNIT = 0,
    BUILDER_UNIT = 1
};

enum config {
    EMLISTENER_ID = 0,
    EMCONSUMER_ID = 1,
    EVENT_MESSAGES_SIZE=15,
};

class ConfigurationLoader : public Core::ConfigurationLoader
{
private:
    int _len;

public:
    std::map<int, std::string> mapIDIP;
    std::map<int, std::string> mapIDPort;
    int emlistenerID, emconsumerID, rankID, procID, numprocs, threadSupport,
        concurrentRUtoBUSends, numnodes;
    char name[MPI_MAX_PROCESSOR_NAME];
    MPI_Status status;
    // std::string sqName;
    std::map<int, std::string> logicalNames;

    ConfigurationLoader() : Core::ConfigurationLoader() {}
    // ~MPIConfigurationLoader(){}

    void initialize(int argc, char *argv[]);
    void parseConfiguration();
    bool is(int id);
    bool isEMListener();
    bool isEMConsumer();
    bool isRU();
    bool isBU();
    int builderNumber(int builderID);
    int readoutNumber(int readoutID);
    int builderID(int builderNumber);
    int readoutID(int readoutNumber);

    bool readoutInSameNode(int readoutNumber);
    bool builderInSameNode(int builderNumber);
    int nodeID();
    int nodeNumber(int _rankID);
    void finalize();
    std::string getLogicalName();
    std::string builderSuffix();
    std::string readoutSuffix();

    int nodePort(int nodeID);
    void initializeIPs(){}
    void differentiateShmemNames();
    bool checkExitMessage();
};

#ifndef EXTERN_MPI
extern ConfigurationLoader* Configuration;
#endif

}

#endif
