
/**
 *      MPI RU
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef MPIREADOUTUNIT 
#define MPIREADOUTUNIT 1

#include <list>
#include "../common/RUTransportCore.h"
#include "MPICore.h"

namespace MPI_transport {

class ReadoutUnit : public RUTransportCore {
private:
    MPI::Request** requests;
    MPI::Request* fragmentRequests;
    MPI::Request waitOrExitSignal[2];
    std::vector<int> requestIndex;
    std::vector<int> activeRequests;
    std::list<int> activeRequestsList;
    int* size_id_bu;
    int initCredits;
    
public:
    ReadoutUnit() : RUTransportCore() {}
    ~ReadoutUnit(){
        free(fragmentRequests);
        for (int i=0; i<initCredits; ++i){
            free(requests[i]);
        }
        free(requests);
    }

    virtual void initialize(int initialCredits, std::map<std::string, void*> &pmems);
    virtual bool EventListen(int& size, int& eventID, int& destinationBU);
    virtual void pullRequestReceive(int& eventID, int& size, int& destinationBU);
    virtual void prepareSendEvent(FragmentComposition*& fragmentCompositionPointer, int destinationBU);
    virtual void setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU);
    virtual bool sendAnyEvent(int& eventID);
    virtual bool sendEvent(int eventID);
    virtual void discardEvent();
};

}

#endif
