
/**
 *      MPI EM Listener
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef MPIEMLISTENER
#define MPIEMLISTENER 1

#include <vector>
#include <mpi.h>
#include <stdlib.h>

#include "../common/EMTransportCoreListener.h"
#include "../../common/Logging.h"
#include "MPICore.h"

#include "../../em/EMCoreListener.h"

namespace MPI_transport {

class EMListener : public EMTransportCoreListener
{
private:
    int* numberOfCredits;
    MPI::Request* requests;

public:
    EMListener(){}
    ~EMListener(){ free(requests); free(numberOfCredits); }

    virtual void setupListen();
    virtual void listenForCreditRequest(int& node, int& numberOfCreditsRequested);
};

}

#endif
