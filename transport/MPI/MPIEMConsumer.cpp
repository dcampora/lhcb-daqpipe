
/**
 *      MPI EM Consumer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "MPIEMConsumer.h"

MPI_transport::EMConsumer::EMConsumer(){
    RURequests = (MPI::Request*) malloc(MPI_transport::Configuration->numTotalReadouts * sizeof(MPI::Request));
}

MPI_transport::EMConsumer::~EMConsumer(){
    free(RURequests);
}

void MPI_transport::EMConsumer::sendToRUs(int*& size_id_bu){
    // INFO << "MPI_transport::MPIEMConsumer: sending to RUs! :D" << std::endl;
    DEBUG << "MPI_transport::MPIEMConsumer: sending to RUs! :D" << std::endl;

    // Iterate in RUs and send packets of {size, id, bu}
    for (int i=0; i<MPI_transport::Configuration->numTotalReadouts; ++i){
        DEBUG << "MPI_transport::MPIEMConsumer: s " << MPI_transport::Configuration->rankID << ", r " << MPI_transport::Configuration->readoutID(i)
            << ", id " << MPI_transport::EVENT_REQUEST << std::endl;
        
        RURequests[i] = MPI::COMM_WORLD.Isend(size_id_bu, 3, MPI::INT, MPI_transport::Configuration->readoutID(i), MPI_transport::EVENT_REQUEST);
    }
    
    MPI::Request::Waitall(MPI_transport::Configuration->numTotalReadouts, RURequests);

    DEBUG << "MPI_transport::MPIEMConsumer: All sent!" << std::endl;
}

void MPI_transport::EMConsumer::sendToBU(int*& size_id, int destinationBU){
    // DEBUG << "MPI_transport::MPIEMConsumer: Sending to BUs!" << std::endl;
    DEBUG << "MPI_transport::MPIEMConsumer: sendToBU: s " << MPI_transport::Configuration->rankID
        << ", r " << MPI_transport::Configuration->builderID(destinationBU)
        << ", id " << MPI_transport::EVENT_ASSIGN << std::endl;

    MPI::COMM_WORLD.Send(size_id, 2, MPI::INT, MPI_transport::Configuration->builderID(destinationBU), MPI_transport::EVENT_ASSIGN);
}

void MPI_transport::EMConsumer::sendExit(void)
{
    DEBUG << "MPI_transport::EMConsumer: Send exit to all" << std::endl;
    int ranks = MPI::COMM_WORLD.Get_size();
    for (int i=0; i< ranks; ++i)
    {
        if (i != Configuration->emconsumerID)
        {
            DEBUG << "MPI_transport::EMConsumer: Send exit event to " << i << std::endl;
            char buffer[5];
            MPI::COMM_WORLD.Send(buffer,sizeof(buffer),MPI_CHAR,i,MPI_transport::EXIT_SIGNAL);
        }
    }    
}
