
/**
 *      MPI EM - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "MPIEMListener.h"

/**
 * TODO: Modify the listen to make it controlled from the Master.
 */

void MPI_transport::EMListener::setupListen(){
    DEBUG << "MPI_transport::MPIEMListener: setupListen" << std::endl;

    requests = (MPI::Request*) malloc((1+MPI_transport::Configuration->numnodes * Core::Configuration->numBuildersPerNode )* sizeof(MPI::Request));
    numberOfCredits = (int*) malloc(MPI_transport::Configuration->numnodes * Core::Configuration->numBuildersPerNode * sizeof(int));

    //post recv of exit request
    static char buffer[] = "exit";
    requests[MPI_transport::Configuration->numnodes * Core::Configuration->numBuildersPerNode] = MPI::COMM_WORLD.Irecv(buffer,sizeof(buffer),MPI_CHAR,MPI_transport::Configuration->emconsumerID,MPI_transport::EXIT_SIGNAL);

    for (int i=0; i<MPI_transport::Configuration->numnodes; ++i){
        for (int j=0; j<Core::Configuration->numBuildersPerNode; ++j){
            DEBUG << "MPI_transport::MPIEMListener: startListen s " << MPI_transport::Configuration->builderID(i*Core::Configuration->numBuildersPerNode + j)
                << ", r " << MPI_transport::Configuration->rankID << ", id " << MPI_transport::CREDIT_ANNOUNCE << std::endl;
            
            requests[i*Core::Configuration->numBuildersPerNode + j] =
                MPI::COMM_WORLD.Irecv(&numberOfCredits[i*Core::Configuration->numBuildersPerNode + j], 1, MPI::INT,
                MPI_transport::Configuration->builderID(i*Core::Configuration->numBuildersPerNode + j), MPI_transport::CREDIT_ANNOUNCE);
        }
    }
}

void MPI_transport::EMListener::listenForCreditRequest(int& node, int& numberOfCreditsRequested){
    // Since my listeners are ordered, and the builderID can be gotten by
    // MPICore::MPIConfig->builderID(i), there's no need to listen for the status.
    int requestno = MPI::Request::Waitany(MPI_transport::Configuration->numnodes * Core::Configuration->numBuildersPerNode+1, requests);

    //check for exit
    if (requestno == MPI_transport::Configuration->numnodes * Core::Configuration->numBuildersPerNode)
    {
        node = -1;
        return;
    }

    // Get the node and number of credits requested
    numberOfCreditsRequested = numberOfCredits[requestno];
    node = requestno;

    DEBUG << "MPI_transport::MPIEMListener: Received a " << numberOfCreditsRequested << "-credit request from node "
          << node << std::endl;

    // Restore the listener
    DEBUG << "MPI_transport::MPIEMListener: restored listener s " << MPI_transport::Configuration->builderID(requestno)
        << ", r " << MPI_transport::Configuration->rankID << ", id " << MPI_transport::CREDIT_ANNOUNCE << std::endl;

    requests[requestno] = MPI::COMM_WORLD.Irecv(&numberOfCredits[requestno], 1, MPI::INT, MPI_transport::Configuration->builderID(requestno), MPI_transport::CREDIT_ANNOUNCE);
}
