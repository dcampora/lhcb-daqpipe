
/**
 *      MPI RU
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "MPIReadoutUnit.h"

void MPI_transport::ReadoutUnit::initialize(int initialCredits, std::map<std::string, void*> &pmems){
    initCredits = initialCredits;

    activeRequests.resize(initialCredits, -1);
    requestIndex.resize(initialCredits, 0);

    // requests.resize(initialCredits);
    // for (int i=0; i<initialCredits; ++i)
    //     requests[i].resize(MAX_RU_FRAGMENTS, (MPI::Request*) malloc(MPICore::MPIConfig->concurrentRUtoBUSends * sizeof(MPI::Request)));
    
    // requests.resize(initialCredits, (MPI::Request*) malloc(MAX_RU_FRAGMENTS * sizeof(MPI::Request)));
    requests = (MPI::Request**) malloc(initialCredits * sizeof(MPI::Request*));
    fragmentRequests = (MPI::Request*) malloc(initialCredits * sizeof(MPI::Request));
    for (int i=0; i<initialCredits; ++i){
        requests[i] = (MPI::Request*) malloc(MAX_RU_FRAGMENTS * sizeof(MPI::Request));
        fragmentRequests[i] = MPI::REQUEST_NULL;
        for (int j=0; j<MAX_RU_FRAGMENTS; ++j)
            requests[i][j] = MPI::REQUEST_NULL;
    }
    
    size_id_bu = (int*) malloc(3 * sizeof(int));

    static char exitBUffer[5];
    waitOrExitSignal[0] = MPI::COMM_WORLD.Irecv(exitBUffer, sizeof(exitBUffer), MPI::CHAR, MPI_transport::EMCONSUMER_ID, MPI_transport::EXIT_SIGNAL);
}

/**
 * Listens for a message from the EventManager.
 */
bool MPI_transport::ReadoutUnit::EventListen(int& size, int& eventID, int& destinationBU){
    DEBUG << "MPI_transport::MPIReadoutUnit: Listening..." << std::endl;
    DEBUG << "MPI_transport::MPIReadoutUnit: s " << MPI_transport::EMCONSUMER_ID << ", r " << MPI_transport::Configuration->rankID
        << ", id " << MPI_transport::EVENT_REQUEST << std::endl;
    
    waitOrExitSignal[1] = MPI::COMM_WORLD.Irecv(size_id_bu, 3, MPI::INT, MPI_transport::EMCONSUMER_ID, MPI_transport::EVENT_REQUEST);
    int index;
    MPI::Status status;
    index = MPI::Request::Waitany(2,waitOrExitSignal,status);
    
    //if exit
    if (index == 0)
        return false;

    size = size_id_bu[0];
    eventID = size_id_bu[1];
    destinationBU = size_id_bu[2];

    DEBUG << "MPI_transport::MPIReadoutUnit: Received!" << std::endl;
    // data is gathered from the gpu generator on the protocol side.

    return true;
}

void MPI_transport::ReadoutUnit::pullRequestReceive(int& eventID, int& size, int& destinationBU){
    DEBUG << "MPI_transport::MPIReadoutUnit: pullRequestReceive s any, r " << MPI_transport::Configuration->rankID
        << ", id " << MPI_transport::PULL_REQUEST << std::endl;

    MPI::COMM_WORLD.Recv(size_id_bu, 3, MPI::INT, MPI::ANY_SOURCE, MPI_transport::PULL_REQUEST);
    
    // DEBUG << "MPI_transport::MPIReadoutUnit: Received!" << std::endl;

    size = size_id_bu[0];
    eventID = size_id_bu[1];
    destinationBU = size_id_bu[2];
}

/**
 * Prepares the sendEvent
 */
void MPI_transport::ReadoutUnit::prepareSendEvent(FragmentComposition*& fc, int destinationBU){
    DEBUG << "MPI_transport::MPIReadoutUnit: destinationBU " << destinationBU << ", r "
        << MPI_transport::Configuration->builderID(destinationBU) << std::endl;

    // Fragment composition packet description
    // 
    // 000-0
    // ^        reserved
    //  ^       packet type (0 - fc)
    //          Rest to zeros

    DEBUG << "MPI_transport::MPIReadoutUnit: prepareSendEvent s " << MPI_transport::Configuration->rankID
          << ", r " << MPI_transport::Configuration->builderID(destinationBU)
          << ", id " << 0 << std::endl;

    // TODO: This is a memory leak.
    //       It's still not understood why.
    //       
    // operator= segfaults (?)
    // 
    // int requestno = MPI::Request::Waitany(initCredits, fragmentRequests);
    // fragmentRequests[requestno] = MPI::COMM_WORLD.Isend(&(fc[0]), sizeof(FragmentComposition), MPI::CHAR,
    //     MPICore::MPIConfig->builderID(destinationBU), shift + MPICore::EVENT_MESSAGES_SIZE + MPICore::EVENT_FRAGMENT_COMPOSITION);
    // 
    // 
    // Therefore, TODO: Make design with Isend and Irecv work.
    MPI::COMM_WORLD.Send(fc, sizeof(FragmentComposition), MPI::CHAR, MPI_transport::Configuration->builderID(destinationBU), 0);
}

/**
 * (Async) Sends the events to a particular Builder Unit.
 */
void MPI_transport::ReadoutUnit::setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU){
    // Find a free request.
    int slot;
    for (slot=0; slot<activeRequests.size(); ++slot)
        if (activeRequests[slot] == eventID) break; // Slot found! :D
    if (slot == activeRequests.size()) {
        // We don't have a slot already assigned to us
        for (slot=0; slot<activeRequests.size(); ++slot)
            if (activeRequests[slot] == -1) break;
        if (slot == activeRequests.size()){
            ERROR << "MPI_transport::MPIReadoutUnit: setupSendEvent can't send because there are no free slots." << std::endl;
            return;
        }
        activeRequests[slot] = eventID;
        activeRequestsList.push_back(slot);
    }

    // Event fragment packet description
    // 
    // 01x-xyyyy
    // ^        reserved
    //  ^       packet type (1 - event fragment)
    //   ^      event id (18 bits)
    //      ^   fragment id (4 bits)

    int packet_type = 1;
    int event_id = eventID & 0x3FFFF;
    int fragment_id = requestIndex[slot] & 0xF;
    int id = (packet_type << 30) | (event_id << 4) | fragment_id;

    DEBUG << "MPI_transport::MPIReadoutUnit: setupSendEvent s " << MPI_transport::Configuration->rankID
          << ", r " << MPI_transport::Configuration->builderID(destinationBU)
          << ", id " << id
          << ", size " << size << std::endl;

    requests[slot][ requestIndex[slot] ] = MPI::COMM_WORLD.Isend(dataPointer, size, MPI::CHAR, MPI_transport::Configuration->builderID(destinationBU), id);
    
    requestIndex[slot]++;
}

bool MPI_transport::ReadoutUnit::sendAnyEvent(int& eventID){
    bool eventSent = false;
    std::list<int>::iterator it;

    // Check against the activeRequests.
    // Test against the oldest first, so as to [try to] keep a finish order
    // (this is useful for the RUMaster -> Generator communication)
    for (it = activeRequestsList.begin(); it != activeRequestsList.end(); it++){
        // We test an event receival
        int eid = activeRequests[*it];
        eventSent = sendEvent(eid);
        if (eventSent) {
            eventID = eid;
            break;
        }
    }

    return eventSent;
}

bool MPI_transport::ReadoutUnit::sendEvent(int eventID){
    // Check that eventID is in our list,
    // and test if it was sent. Return true and do
    // some cleanup in that case.
    bool found = false;
    bool eventSent = false;
    std::list<int>::iterator it;
    int eventSlot;
    
    for (it = activeRequestsList.begin(); it != activeRequestsList.end(); it++){
        found = true;
        if (activeRequests[*it] == eventID){
            eventSent = MPI::Request::Testall(requestIndex[*it], requests[*it]);
            if (eventSent)
                break;
        }
    }

    if (eventSent){
        // Do some cleanup
        eventSlot = *it;
        activeRequestsList.erase(it);
        activeRequests[eventSlot] = -1;
        requestIndex[eventSlot] = 0;
    }

    return eventSent;
}

void MPI_transport::ReadoutUnit::discardEvent(){
    std::list<int>::iterator it = activeRequestsList.begin();
    int eventSlot;

    DEBUG << "Event " << *it << " discarded" << std::endl;

    // Do some cleanup
    eventSlot = *it;
    activeRequestsList.erase(it);
    activeRequests[eventSlot] = -1;
    requestIndex[eventSlot] = 0;
}
