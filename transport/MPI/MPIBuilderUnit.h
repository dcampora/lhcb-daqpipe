
/**
 *      MPI BU
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef MPIBUILDERUNIT 
#define MPIBUILDERUNIT 1

#include <stdlib.h>
#include <ctime>
#include <vector>
#include <list>
#include <string>

#include "../../common/GenStructures.h"
#include "../../common/Shmem.h"
#include "../common/BUTransportCore.h"
#include "../config/ConfigurationCore.h"
#include "MPICore.h"

namespace MPI_transport {

class BuilderUnit : public BUTransportCore {
private:
    std::vector<MPI::Request*> fragmentRequests;
    std::vector<int> currentFragmentRequest;
    std::list<int> inactiveSlots;
    int currentSlot;
    std::vector<FragmentComposition*> activeFragments;

    // m credits * n-1 nodes * k requests each
    MPI::Request** requests;
    MPI::Request exitRequest;
    
    // m credits * k requests each
    std::vector<int> transmissionSize;
    int metaBufferElement, dataBufferElement;

    std::vector<int> activeRequests;

    int* size_id;
    time_t tstart;
    int initialCredits;

    std::list<int> activeRequestsList;
    std::vector<int> requestIndex;
    std::vector<std::vector<int> > requestReadoutIndex;

    // Descriptors
    int connEMListener;
    bool printit;

public:
    BuilderUnit() : BUTransportCore() {}
    ~BuilderUnit();

    virtual void initialize(int initialCredits);
    virtual void sendCredits(int& numberOfCredits);
    virtual void pullRequest(int* size_id_bu, int& readoutno);
    virtual void receiveSizeAndID(int& eventSize, int& eventID);
    virtual void discardEvent();
    virtual void prepareReceiveEvent(FragmentComposition* fc);
    virtual void prepareReceiveEvent(FragmentComposition* fc, int eid);
    virtual bool prepareReceiveEventComplete(int& fragmentReceived);
    virtual void setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype = Core::DATA);
    virtual bool receiveEvent(int& finishedEventID, int& tSize);
    virtual bool checkExitMessage(void);
};

}
#endif
