
/**
 *      MPI EM Consumer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

// class EventManagerCoreConsumerWorkerMPI : public EventManagerCoreConsumerWorker
// {
// public:
//     EventManagerCoreConsumerWorkerMPI(SharedQueue& assignedCredits) : 
//         EventManagerCoreConsumerWorker(assignedCredits) {}

//     void send(int destination);
// };

#ifndef MPIEMCONSUMER 
#define MPIEMCONSUMER 1

#include "mpi.h"
#include "stdlib.h"
#include "MPICore.h"
#include "../common/EMTransportCoreConsumer.h"
#include "../../common/Logging.h"

namespace MPI_transport {

class EMConsumer : public EMTransportCoreConsumer {
private:
    MPI::Request* RURequests;

public:
    EMConsumer();
    ~EMConsumer();
    void sendToRUs(int*& size_id_bu);
    void sendToBU(int*& size_id, int destinationBU);
	void sendExit(void);
};

}

#endif
