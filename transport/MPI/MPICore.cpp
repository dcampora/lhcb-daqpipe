
/**
 *      MPICore - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "MPICore.h"

void MPI_transport::ConfigurationLoader::initialize(int argc, char *argv[]){
    // There is no multithreaded MPI support on 
    // the openib BTL, so we cannot use MPI_THREAD_MULTIPLE here
    threadSupport = MPI::Init_thread(argc, argv, MPI_THREAD_SINGLE);

    // MPI::COMM_WORLD.Set_errhandler(MPI::ERRORS_THROW_EXCEPTIONS);
    rankID = MPI::COMM_WORLD.Get_rank();
    DEBUG << "Rank id " << rankID  << std::endl;
    numprocs = MPI::COMM_WORLD.Get_size();

    // TODO: Configurable
    // sqName = "/sharedqueue";

    logicalNames[MPI_transport::EVENT_MANAGER_LISTENER] = "eml";
    logicalNames[MPI_transport::EVENT_MANAGER_CONSUMER] = "emc";
    logicalNames[MPI_transport::BUILDER_UNIT] = "bu";
    logicalNames[MPI_transport::READOUT_UNIT] = "ru";
}

void MPI_transport::ConfigurationLoader::parseConfiguration(){
    numnodes = (int) (numprocs / (float) numProcsPerNode) - 1; // Ruling out the EM
    MPI::Get_processor_name(name, _len);

    procID = rankID % numProcsPerNode;
    emlistenerID = MPI_transport::EMLISTENER_ID;
    emconsumerID = MPI_transport::EMCONSUMER_ID;

    Core::Configuration->name = name;
    Core::Configuration->numnodes = numnodes;

    ru_startid = MPI_transport::READOUT_UNIT;
    ru_endid = MPI_transport::READOUT_UNIT + numReadoutsPerNode - 1;
    bu_startid = ru_endid + 1;
    bu_endid = bu_startid + numBuildersPerNode - 1;

    numTotalReadouts = numnodes * numReadoutsPerNode;
    
    if (logToFile)
        logfile.open(std::string(logdir + getLogicalName() + std::string(".log")).c_str());
    Logger::logger(0) << std::setprecision(2) << std::fixed;

    initializeIPs();
    differentiateShmemNames();
}

void MPI_transport::ConfigurationLoader::differentiateShmemNames(){
    for (std::vector<std::string*>::iterator it = buShmemBuffers.begin(); it != buShmemBuffers.end(); it++)
        *(*it) += builderSuffix();

    for (std::vector<std::string*>::iterator it = ruShmemBuffers.begin(); it != ruShmemBuffers.end(); it++)
        *(*it) += readoutSuffix();
}

bool MPI_transport::ConfigurationLoader::isEMListener(){
    return rankID == emlistenerID;
}

bool MPI_transport::ConfigurationLoader::isEMConsumer(){
    return rankID == emconsumerID;
}

bool MPI_transport::ConfigurationLoader::isRU(){
    return !isEMListener() && !isEMConsumer() &&
        rankID >= numProcsPerNode &&
        procID >= ru_startid && procID <= ru_endid;
}

bool MPI_transport::ConfigurationLoader::isBU(){
    return !isEMListener() && !isEMConsumer() &&
        rankID >= numProcsPerNode &&
        procID >= bu_startid && procID <= bu_endid;
}

/**
 * Only meant for Builder Unit or Readout Unit
 */
bool MPI_transport::ConfigurationLoader::is(int id){
    DEBUG << "MPI_transport::ConfigurationLoader::is: " << id << std::endl;
    
    if (id == MPI_transport::BUILDER_UNIT) return isBU();
    else if (id == MPI_transport::READOUT_UNIT) return isRU();
    
    ERROR << "Unexpected ConfigurationLoader::is with id " << id << std::endl;
    return false;
}

void MPI_transport::ConfigurationLoader::finalize(){
    MPI::Finalize();
}

int MPI_transport::ConfigurationLoader::builderID(int builderNumber){
    return bu_startid + (builderNumber % numBuildersPerNode) +
    // return numReadoutsPerNode + (builderNumber % numBuildersPerNode) +
        (builderNumber / numBuildersPerNode) * numProcsPerNode + numProcsPerNode;
}

int MPI_transport::ConfigurationLoader::readoutID(int readoutNumber){
    return ru_startid + (readoutNumber % numReadoutsPerNode) +
    // return  + (readoutNumber % numReadoutsPerNode) +
        (readoutNumber / numReadoutsPerNode) * numProcsPerNode + numProcsPerNode;
}

int MPI_transport::ConfigurationLoader::builderNumber(int builderID){
    return ((builderID - numProcsPerNode) / numProcsPerNode) * numBuildersPerNode +
       ((builderID - numProcsPerNode - bu_startid) % numProcsPerNode) + numProcsPerNode;
    // return (builderID - numProcsPerNode) / numProcsPerNode * numBuildersPerNode +
    //     (builderID - bu_startid) % numProcsPerNode;
}

int MPI_transport::ConfigurationLoader::readoutNumber(int readoutID){
    return ((readoutID - numProcsPerNode) / numProcsPerNode) * numReadoutsPerNode +
      ((readoutID - numProcsPerNode - ru_startid) % numProcsPerNode);
    // return ((readoutID - numProcsPerNode) / numProcsPerNode) * numReadoutsPerNode +
    //     ((readoutID - ru_startid) % numProcsPerNode);
}

bool MPI_transport::ConfigurationLoader::readoutInSameNode(int readoutNumber){
    return nodeNumber(rankID) == nodeNumber(readoutID(readoutNumber));
}

bool MPI_transport::ConfigurationLoader::builderInSameNode(int builderNumber){
    // I'm a RU, I want to know if the builder builderNumber is on the same node as me.
    // Simple. Simplest!
    return nodeNumber(rankID) == nodeNumber(builderID(builderNumber));
}

int MPI_transport::ConfigurationLoader::nodeID(){
    return rankID;
}

int MPI_transport::ConfigurationLoader::nodeNumber(int _rankID){
    return _rankID / numProcsPerNode;
}

std::string MPI_transport::ConfigurationLoader::getLogicalName(){
    std::string post = std::string("_") + name;

    if (isEMListener()) return logicalNames[MPI_transport::EVENT_MANAGER_LISTENER] + post;
    else if (isEMConsumer()) return logicalNames[MPI_transport::EVENT_MANAGER_CONSUMER] + post;
    else if (is(MPI_transport::READOUT_UNIT)) return logicalNames[MPI_transport::READOUT_UNIT] + readoutSuffix() + post;
    else if (is(MPI_transport::BUILDER_UNIT)) return logicalNames[MPI_transport::BUILDER_UNIT] + builderSuffix() + post;
    else return std::string("p") + Tools::toString<int>(rankID);
}

std::string MPI_transport::ConfigurationLoader::builderSuffix(){
    if (numBuildersPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(builderNumber(rankID));
    }
}

std::string MPI_transport::ConfigurationLoader::readoutSuffix(){
    if (numReadoutsPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(readoutNumber(rankID));
    }
}
