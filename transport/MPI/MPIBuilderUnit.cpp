
/**
 *      MPI BU - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "MPIBuilderUnit.h"

MPI_transport::BuilderUnit::~BuilderUnit(){
    for (int i=0; i<initialCredits; ++i){
        free(fragmentRequests[i]);
        for (int j=0; j<MPI_transport::Configuration->numTotalReadouts * (MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS); ++j)
            free(requests[i][j]);
    }
    free(size_id);
}

void MPI_transport::BuilderUnit::initialize(int initialCredits){
    initialCredits = initialCredits;

    // Reserve pointers and requests for non-blocking recvs
    fragmentRequests.resize(initialCredits, (MPI::Request*) malloc(MPI_transport::Configuration->numTotalReadouts * sizeof(MPI::Request)));
    requests = (MPI::Request**) malloc(initialCredits * sizeof(MPI::Request**));
    
    for (int i=0; i<initialCredits; ++i){
        // Initialize with MPI::REQUEST_NULL for Testall/Waitall to work
        requests[i] = (MPI::Request*) malloc(MPI_transport::Configuration->numTotalReadouts * (MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS) * sizeof(MPI::Request));
        for (int j=0; j<MPI_transport::Configuration->numTotalReadouts; ++j){
            fragmentRequests[i][j] = MPI::REQUEST_NULL;
            for (int k=0; k<MAX_RU_METADATA_FRAGMENTS+MAX_RU_DATA_FRAGMENTS; ++k)
                requests[i][j*(MAX_RU_METADATA_FRAGMENTS+MAX_RU_DATA_FRAGMENTS) + k] = MPI::REQUEST_NULL;
        }
    }

    // activeRequest queues
    activeRequests.resize(initialCredits, -1);
    activeFragments.resize(initialCredits, 0);
    currentFragmentRequest.resize(initialCredits, 0);
    requestIndex.resize(initialCredits, 0);   
    transmissionSize.resize(initialCredits, 0);
    requestReadoutIndex.resize(initialCredits);
    for (int i=0; i<requestReadoutIndex.size(); ++i)
        requestReadoutIndex[i].resize(MPI_transport::Configuration->numTotalReadouts, 0);

    DEBUG << "MPIBuilderUnit: Reserved fragments, meta and data for " << MPI_transport::Configuration->numTotalReadouts << " readouts;"
          << " data buffer: " << Core::Configuration->BUDataBufferSize << ", meta buffer: " << Core::Configuration->BUMetaBufferSize << " (B)" << std::endl;

    // Reserve size and id for sync recvs
    size_id = (int*) malloc(2 * sizeof(int));
    tstart = time(NULL);
    printit = 1;
    
    //post recv of exit request
    static char buffer[] = "exit";
    exitRequest = MPI::COMM_WORLD.Irecv(buffer,sizeof(buffer),MPI_CHAR,MPI_transport::Configuration->emconsumerID,MPI_transport::EXIT_SIGNAL);
}

void MPI_transport::BuilderUnit::sendCredits(int& numberOfCredits){
    DEBUG << "MPIBuilderUnit: sendCredits " << numberOfCredits << " credits, s " << MPI_transport::Configuration->rankID << ", r " <<
        MPI_transport::EVENT_MANAGER_LISTENER << ", id " << MPI_transport::CREDIT_ANNOUNCE << std::endl;
    
    MPI::COMM_WORLD.Send(&numberOfCredits, 1, MPI::INT, MPI_transport::EVENT_MANAGER_LISTENER, MPI_transport::CREDIT_ANNOUNCE);
}

void MPI_transport::BuilderUnit::pullRequest(int* size_id_bu, int& readoutno){
    DEBUG << "MPIBuilderUnit: pullRequest s " << MPI_transport::Configuration->rankID << ", r " << MPI_transport::Configuration->readoutID(readoutno)
        << ", id " << MPI_transport::PULL_REQUEST << std::endl;

    //TODO here it might create a memory leak as the Request is nevers checked
    MPI::COMM_WORLD.Isend(size_id_bu, 3, MPI::INT, MPI_transport::Configuration->readoutID(readoutno), MPI_transport::PULL_REQUEST);
}

void MPI_transport::BuilderUnit::receiveSizeAndID(int& eventSize, int& eventID){
    DEBUG << "MPIBuilderUnit: receiveSizeAndID, s " << MPI_transport::Configuration->rankID << ", r " <<
        MPI_transport::EMCONSUMER_ID << ", id " << MPI_transport::EVENT_ASSIGN << std::endl;
    
    MPI::COMM_WORLD.Recv(size_id, 2, MPI::INT, MPI_transport::EMCONSUMER_ID, MPI_transport::EVENT_ASSIGN);
    
    eventSize = size_id[0];
    eventID = size_id[1];
}

/**
 * Prepares the receiveEvent. In OpenMPI, this means having to find a slot for it.
 * @param fc      
 * @param eventID 
 */
void MPI_transport::BuilderUnit::prepareReceiveEvent(FragmentComposition* fc){
    prepareReceiveEvent(fc, 0);
}

void MPI_transport::BuilderUnit::prepareReceiveEvent(FragmentComposition* fc, int eventID){
    // Find a slot
    int slot;
    for (slot=0; slot<activeRequests.size(); ++slot)
        if (activeRequests[slot] == -1) break;
    if (slot == activeRequests.size()){
        ERROR << "MPIBuilderUnit: prepareReceiveEvent can't listen because there are no free slots. "
            << activeRequests.size() << " active" << std::endl;
        return;
    }
    DEBUG << "MPIBuilderUnit: prepareReceiveEvent listening on slot " << slot << std::endl;

    activeRequestsList.push_back(slot);
    currentFragmentRequest[slot] = 0;
    activeFragments[slot] = fc;

    for (int i=0; i<MPI_transport::Configuration->numTotalReadouts; ++i){
        
        // Fragment composition packet description
        // 
        // 000-0
        // ^        reserved
        //  ^       packet type (0 - fc)
        //          Rest to zeros

        DEBUG << "MPIBuilderUnit: prepareReceiveEvent s " << MPI_transport::Configuration->readoutID(i)
              << ", r " << MPI_transport::Configuration->rankID
              << ", id " << 0 << std::endl;

        fragmentRequests[slot][i] = MPI::COMM_WORLD.Irecv(fc + i, sizeof(FragmentComposition), MPI::CHAR,
            MPI_transport::Configuration->readoutID(i), 0 );
    }

    // inactiveSlots.push_back(slot);
    currentSlot = slot;
}

bool MPI_transport::BuilderUnit::prepareReceiveEventComplete(int& fragmentReceived){

    // Wait for any fragment to arrive
    // DEBUG << "MPI_transport::BuilderUnit: prepareReceiveEvent waiting on all..." << std::endl;
    bool rx = MPI::Request::Testany(MPI_transport::Configuration->numTotalReadouts, fragmentRequests[currentSlot], fragmentReceived);

    if (rx && fragmentReceived != MPI_UNDEFINED) {
        // It must be coming from the same event,
        // or it must be the first we take
        int currentEvent = (*(activeFragments[currentSlot] + fragmentReceived)).eventID;

        if (activeRequests[currentSlot] == -1 ||
            activeRequests[currentSlot] == currentEvent){
            
            DEBUG << "MPIBuilderUnit: Received fragment " << fragmentReceived << std::endl;
            activeRequests[currentSlot] = currentEvent;
            return true;
        }
    }

    return false;
}

/**
 * Sets up the Irecvs for this eventID.
 * @param memory  
 * @param size    
 * @param eventID 
 */
void MPI_transport::BuilderUnit::setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype){
    // Find assigned slot
    int slot;
    for (slot=0; slot<activeRequests.size(); ++slot)
        if (activeRequests[slot] == eventID) break; // Slot found! :D
    if (slot == activeRequests.size()){
        ERROR << "MPIBuilderUnit: setupReceiveEvent: No slot was found" << std::endl;
        return;
    }

    // Event fragment packet description
    // 
    // 01x-xyyyy
    // ^        reserved
    //  ^       packet type (1 - event fragment)
    //   ^      event id (18 bits)
    //      ^   fragment id (4 bits)

    int packet_type = 1;
    int event_id = eventID & 0x3FFFF;
    int fragment_id = requestReadoutIndex[slot][readoutIndex] & 0xF;
    int id = (packet_type << 30) | (event_id << 4) | fragment_id;

    DEBUG << "MPIBuilderUnit: Irecv: r " << MPI_transport::Configuration->rankID
          << ", s " << MPI_transport::Configuration->readoutID(readoutIndex)
          << ", id " << id
          << ", size " << size << std::endl;

    requests[slot][ requestIndex[slot] ] = MPI::COMM_WORLD.Irecv(dataPointer, size, MPI::CHAR,
        MPI_transport::Configuration->readoutID(readoutIndex), id);

    requestIndex[slot]++;
    requestReadoutIndex[slot][readoutIndex]++;

    if (datatype == Core::DATA){
        transmissionSize[slot] += size;
    }
}

/**
 * In MPI, this means "Testall" on all open comms.
 * @param  tSize returns the transmissionSize of the finished event.
 * @return                  true if finished event, false otherwise.
 */
bool MPI_transport::BuilderUnit::receiveEvent(int& finishedEventID, int& tSize){
    std::list<int>::iterator it;
    bool eventReceived;
    int slot;

    for (it = activeRequestsList.begin(); it != activeRequestsList.end(); it++){
        eventReceived = MPI::Request::Testall(requestIndex[*it], requests[*it]);

        // Debug: Check all tests
        // if (Core::Configuration->logLevel >= Logger::_DEBUG && printit){
        //     printit = 0;
        //     std::cout << "MPIBuilderUnit::Individual tests:" << std::endl;
        //     for (int i=0; i<requestIndex[*it]; ++i){
        //         std::cout << MPI::Request::Testall(1, &(requests[*it][i])) << ", ";
        //     }
        //     std::cout << std::endl;
        // }

        if (eventReceived)
            break;
    }

    if (eventReceived){
        printit = 1;

        DEBUG << "MPIBuilderUnit: Event received!" << std::endl;
        slot = *it;
        finishedEventID = activeRequests[slot];
        tSize = transmissionSize[slot];

        activeRequestsList.erase(it);
        activeRequests[slot] = -1;
        requestIndex[slot] = 0;
        transmissionSize[slot] = 0;
        for (int i=0; i<requestReadoutIndex[slot].size(); ++i)
            requestReadoutIndex[slot][i] = 0;
        return true;
    }

    return false;
}

void MPI_transport::BuilderUnit::discardEvent(){
    int slot;
    std::list<int>::iterator it = activeRequestsList.begin();

    DEBUG << "MPIBuilderUnit: Event " << *it << " discarded" << std::endl;
    
    // Cleanup
    slot = *it;
    activeRequestsList.erase(it);
    activeRequests[slot] = -1;
    requestIndex[slot] = 0;
    transmissionSize[slot] = 0;
    for (int i=0; i<requestReadoutIndex[slot].size(); ++i)
        requestReadoutIndex[slot][i] = 0;
}

bool MPI_transport::BuilderUnit::checkExitMessage(void)
{
    MPI::Status status;
    return exitRequest.Test(status);
}
