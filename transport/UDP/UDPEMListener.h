
/**
 *      EM transport core listener
 *
 *      author  -   Adam Otto
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <ifaddrs.h>
#include "../common/EMTransportCoreListener.h"
#include "../../common/Logging.h"
#include "UDPCore.h"
#include "../../em/EMCoreListener.h"
#ifndef UDPEMLISTENER
#define UDPEMLISTENER 1


namespace UDP{
class EMListener:public EMTransportCoreListener {
private:
	int * numberOfCredits;
	int sockfd, rv; 
    ssize_t numbytes; // listen on sock_fd, new connection on new_fd
    int tmp;
    std::vector<int> new_fd;
    int * bf;  //buffer size
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr; // client's address information
	socklen_t sin_size;
	struct sigaction sa;
	char s[INET6_ADDRSTRLEN];

public:

    EMListener();
    ~EMListener();
    /**
     * Asynchronous: Setups the listen in the transport.
     *               ie. For MPI, this starts several non-blocking concurrent listens.
     * Synchronous: It may be left in blank.
     */
    virtual void setupListen();

    /**
     * Waits for a credit request from a BU node. Both params are
     * return params.
     * 
     * @param node                      node making the request.
     * @param numberOfCreditsRequested 
     */
    virtual void listenForCreditRequest(int& node, int& numberOfCreditsRequested);
};
};
#endif
