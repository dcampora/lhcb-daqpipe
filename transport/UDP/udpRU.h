
/**
 *      RU transport core
 *
 *      author  -   Adam Otto
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */

#ifndef UDPRU
#define UDPRU 1
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <ifaddrs.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <sys/sendfile.h>
#include <unistd.h>
#include <fcntl.h>
#include "../common/RUTransportCore.h"
#include "UDPCore.h"



namespace UDP{
class ReadoutUnit:public RUTransportCore{
private:
	struct addrinfo hintsRU, *servinfoRU, *p_RU, hintsRUtoBU, *servinfo_RUtoBU, *p_RUtoBU, hintsRUtoBU2, *servinfo_RUtoBU2, *p_RUtoBU2;
    struct sockaddr_in sa_RUtoBU, sa_RUtoBU2;
	int sockfdRU, new_fdRU, rvRU, numbytesRU, rv_RUtoBU,rv_RUtoBU2;
    int sockRUtoBU;
    int sockRUtoBU2;
	int * bf;
    char * bf_dP;
    int send_bytes;
    int num_of_parts,err;
	char bf_RUtoBU[52];
	int * size_id_BU;
	struct sockaddr_storage their_addr_RU; // client's address information
    struct linger so_linger;
	socklen_t sin_size_RU;
    std::vector<int> requestIndex;
    std::vector<int> activeRequests;
    std::list<int> activeRequestsList;
    char S_toRU[INET6_ADDRSTRLEN];
    int seq, tmp;
    bool check;

public:
	ReadoutUnit();
    ~ReadoutUnit();
    virtual void initialize(int initialCredits);

    virtual bool EventListen(int& size, int& eventID, int& destinationBU);

    virtual void pullRequestReceive(int& eventID, int& size, int& destinationBU);

    virtual void prepareSendEvent(FragmentComposition* &fc, int destinationBU);

    virtual void setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU);

    virtual bool sendAnyEvent(int& eventID) ;

    virtual bool sendEvent(int eventID) ;

    virtual void discardEvent();
};
};
#endif
