
/**
 *      UDPCore
 *
 *      author  -   Adam OTTO
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */

#ifndef UDPCORE
#define UDPCORE 1

//#include "tcpBU.h"
#include "../../common/Logging.h"
#include "../config/ConfigurationCore.h"
#include "../../common/Tools.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <limits.h>
#include "../../extern-deps/jsoncpp/dist/json/json.h"

#include "../../zmq_handshake/zmq_handshake.h"

namespace UDP{

enum messages {
	CREDIT_REQUEST = 10,
	EVENT_REQUEST,
	EVENT_ASSIGN,
	PULL_REQUEST,
	EVENT_SEND = 100, // From 100 onwards reserved for event IDs

	EVENT_FRAGMENT_COMPOSITION = 101,
	EVENT_SEND_FRAGMENT = 102,
};

enum ids {
	EVENT_MANAGER_LISTENER = 0,
	EVENT_MANAGER_CONSUMER = 1,
	READOUT_UNIT = 2,
	BUILDER_UNIT = 3
};

enum config {
	EMLISTENER_ID = 0,
	EMCONSUMER_ID = 1,
	EVENT_MESSAGES_SIZE=15,
};

class ConfigurationLoader : public Core::ConfigurationLoader
{
private:
	int _len;

public:
	/*std::map<int, std::string> mapIDIP;
	std::map<int, std::string> mapIDPort;*/
	std::map<int, std::string> BUMapIDIP;
	std::map<std::string, int> BUMapIPID;
	std::map<int, int> BUMapIDPort_fc;
	std::map<int, int> BUMapIDPort_SendEvent;
	std::map<int, int> BUMapIDPort_SizeID;
	std::map<int, std::string> RUMapIDIP;
	std::map<std::string, int> RUMapIPID;
	std::map<int, int> RUMapIDPort;
	std::map<int, std::string> EMLMapIDIP;
	std::map<std::string, int> EMLMapIPID;
	std::map<int, int> EMLMapIDPort;
	std::map<int, std::string> EMCMapIDIP;
	std::map<std::string, int> EMCMapIPID;
	std::map<int, int> EMCMapIDPort;

	std::string sync_server_ip;
	std::string sync_server_port;
	std::string pub_server_port;

	int tmp_port;
	int emlistenerID, emconsumerID, rankID, procID, numprocs, threadSupport,
		concurrentRUtoBUSends, numnodes;
	char name[HOST_NAME_MAX];
	// std::string sqName;
	std::map<int, std::string> logicalNames;

	ConfigurationLoader() : Core::ConfigurationLoader() {}
	// ~MPIConfigurationLoader(){}

	void initialize(int argc, char *argv[]);
	void parseConfiguration();
	bool is(int id);
	bool isEMListener();
	bool isEMConsumer();
	bool isBU();
	int builderNumber(int builderID);
	int readoutNumber(int readoutID);
	int builderID(int builderNumber);
	int readoutID(int readoutNumber);

	bool readoutInSameNode(int readoutNumber);
	bool builderInSameNode(int builderNumber);
	int nodeID();
	int nodeNumber(int _rankID);
	void finalize();
	std::string getLogicalName();
	std::string builderSuffix();
	std::string readoutSuffix();

	int nodeIP(int nodeID);
	int nodePort(int nodeID);
	void initializeIPs();
	void differentiateShmemNames();
};

#ifndef EXTERN_MPI
extern ConfigurationLoader* Configuration;
#endif

}

#endif
