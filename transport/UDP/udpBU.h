
/**
 *      BU transport core
 *
 *      author  -   Adam Otto
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */

#ifndef UDPBU
#define UDPBU 1

#include <stdlib.h>
#include <ctime>
#include <vector>
#include <list>
#include <string>
#include "UDPCore.h"
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <time.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include "../../common/GenStructures.h"
#include "../../common/Shmem.h"
#include "../common/BUTransportCore.h"
#include "../config/ConfigurationCore.h"

namespace UDP{

class BuilderUnit:public BUTransportCore {

private:

	 	int sockfd, sockBUSender, sockBUListener,  sockRUListener,new_BUsock_SID, new_BUsock_dP, sockfdBU;//socket to send credits;
	    struct addrinfo hints_BUSender, *servinfo_BUSender, *p_BUSender;
	    struct addrinfo hints_BUReceiver, *servinfo_BUReceiver, *p_BUReceiver;
        struct addrinfo hints_BUReceiver2, *servinfo_BUReceiver2, *p_BUReceiver2;
		struct sockaddr_storage connectorBU_addr, connectorBU_addr2;
        struct sockaddr_storage their_addr_BU; // client's address information
        struct linger so_linger;
        struct sockaddr_in sa_BU, sa_BUpl;
        struct addrinfo hintsBU, *servinfoBU, *p_BU;
		socklen_t sinBU_size, sinBU_size2, sin_size_BU;
	    int rv_BUSender,rv_BUReceiver, rv_BUReceiver1, rv_BUReceiver2, rv_BUReceiver3;
        int bf_BUSender[2];
	    char BUdataBuffer[52];
        char sBU[INET6_ADDRSTRLEN], sBU2[INET6_ADDRSTRLEN],S_toBU[INET6_ADDRSTRLEN];
        int * BU_sizeID;
        char * BU_dataPointer;
	    std::vector<int> transmissionSize;
	    int metaBufferElement, dataBufferElement;
	    std::vector<int> activeRequests;
        int sockBUListener2;
	    int* size_id;
        time_t tstart;
	    int initialCredits, seq, total_parts, tmp, rvBU, numbytesBU;
        FragmentComposition *fc_ptr;

        int sockBUpl;


        std::list<int> activeRequestsList;
        std::vector<int> requestIndex;
        std::vector<std::vector<int> > requestReadoutIndex;


public:

        BuilderUnit();
        ~BuilderUnit();

    virtual void initialize(int initialCredits);

    virtual void sendCredits(int& numberOfCredits);

    virtual void pullRequest(int* size_id_bu, int& readoutno);

    virtual void receiveSizeAndID(int& eventSize, int& eventID);

    virtual void prepareReceiveEvent(FragmentComposition* fc);

    virtual bool prepareReceiveEventComplete(int& fragmentReceived);

    virtual void setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID);

    virtual bool receiveEvent(int& finishedEventID, int& transmissionSize);

    virtual void discardEvent();

    virtual bool checkExitMessage(void);
};
};
#endif
