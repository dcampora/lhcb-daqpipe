
/**
 *      MPI EM Consumer
 *
 *      author  -   Adam Otto
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */

#include "UDPEMConsumer.h"

UDP::EMConsumer::EMConsumer(){
	sockRUSender=0;
    sockBUSender=0;

};

UDP::EMConsumer::~EMConsumer(){};


void UDP::EMConsumer::sendToRUs(int* &size_id_bu){
	//	sockRUSender.resize(Core::Configuration->numnodes,0);
	


		
	for (int q=0; q<UDP::Configuration->numnodes; q++){

		if(sockRUSender==0){

			memset((char *)&sa, 0, sizeof(sa));

			if ((sockRUSender = socket(AF_INET,SOCK_DGRAM,0)) == -1)perror("[EMConsumer]: socket");	

			sa.sin_family = AF_INET;

		}	

		DEBUG << "UDPEMConsumer: s " << UDP::Configuration->rankID << ", r " << UDP::Configuration->readoutID(q)
		<< ", id " << UDP::EVENT_REQUEST << std::endl;

		DEBUG<<"[EMConsumer] Preparing sending to     "<<q<<" of "<<UDP::Configuration->numnodes
		<<" RUs of IP: "<<UDP::Configuration->RUMapIDIP[UDP::Configuration->readoutID(q)].c_str()<<std::endl;

		inet_pton(AF_INET, UDP::Configuration->RUMapIDIP[UDP::Configuration->readoutID(q)].c_str(), &(sa.sin_addr));

		sa.sin_port=htons(UDP::Configuration->RUMapIDPort[UDP::Configuration->readoutID(q)]);	

	
		if ((sendto(sockRUSender, size_id_bu, 3*sizeof(int), 0,(struct sockaddr *)&sa, sizeof(sa)) == -1))perror("[EMConsumer]	sendto ");

		memset((char *)&sa.sin_addr, 0, sizeof(sa.sin_addr));  
		memset((char *)&sa.sin_port, 0, sizeof(sa.sin_port));

	}
}



void UDP::EMConsumer::sendToBU(int* &size_id, int destinationBU){
	DEBUG<<"[EMConsumer] Preparing sending to "<< (UDP::Configuration->builderNumber(destinationBU)+1)<<" BU of "<<UDP::Configuration->numnodes
	<<" BUs of IP: "<<UDP::Configuration->BUMapIDIP[destinationBU].c_str()<<std::endl;

	if(sockBUSender==0){
		memset((char *)&sa, 0, sizeof(sa));

		if ((sockBUSender = socket(AF_INET,SOCK_DGRAM,0)) == -1)perror("[RU]: socket"); 

		sa.sin_family = AF_INET;
	}

	sa.sin_port=htons(UDP::Configuration->BUMapIDPort_SizeID[destinationBU]);
	inet_pton(AF_INET, UDP::Configuration->BUMapIDIP[destinationBU].c_str(), &(sa.sin_addr));

	if ((sendto(sockBUSender, size_id, 2*sizeof(int), 0,(struct sockaddr *)&sa, sizeof(sa))) == -1)perror("[EMConsumer] sendto ");

	memset((char *)&sa.sin_addr, 0, sizeof(sa.sin_addr));
	memset((char *)&sa.sin_port, 0, sizeof(sa.sin_port));

	DEBUG<<"[EMconsumer]Sended to "<<destinationBU<<" BU."<<std::endl;
//close(sockBUSender);
}

void UDP::EMConsumer::sendExit(void)
{
	std::cerr << "UDP::EMConsumer don't support send exit, cannot exit properly !" << std::endl;
	abort();
}

