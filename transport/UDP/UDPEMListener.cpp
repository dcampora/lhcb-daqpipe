
/**
 *      MPI EM - impl
 *
 *      author  -   Adam Otto
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */

#include "UDPEMListener.h"
#include "UDPCore.h"
/**
 * TODO: Modify the listen to make it controlled from the Master.
 */

UDP::EMListener::EMListener(){};
UDP::EMListener::~EMListener(){};

void UDP::EMListener::setupListen(){

	memset(&hints, 0, sizeof hints);

	int yes =1;
	tmp =0;

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;// use UDP/IP
	hints.ai_flags = AI_PASSIVE;

	bf = (int *) malloc(2*sizeof(int)); //buffer size

	if ((rv = getaddrinfo(NULL, Tools::toString(UDP::Configuration->EMLMapIDPort[UDP::Configuration->rankID]).c_str(), &hints, &servinfo)) != 0) {//tmp  port
	        
	        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
	    }

	    for(p = servinfo; p != NULL; p = p->ai_next) {

	        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
	            perror("server: socket");
	            continue;
	        }

	        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(int)) == -1) {
	            perror("setsockopt");
	            continue;
	        }

	        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
	            close(sockfd);
	            perror("server: bind");
	            continue;
	        }

	        break;
	    }

    if (p == NULL)  {

        fprintf(stderr, "server: failed to bind\n");

        }

    freeaddrinfo(servinfo); // all done with this structure

	fcntl(sockfd , F_SETFL, O_NONBLOCK); //nonblocking
}

void UDP::EMListener::listenForCreditRequest(int& node, int& numberOfCreditsRequested){
		
	sin_size = sizeof their_addr;	

	while(1){
		if((numbytes = recvfrom(sockfd, bf, 2*sizeof(int), 0,(struct sockaddr *)&their_addr,&sin_size)) < 0)continue;

		else{

			inet_ntop(their_addr.ss_family,(&((struct sockaddr_in *)&their_addr)->sin_addr) ,s, sizeof s);   

			memcpy(&numberOfCreditsRequested, bf, sizeof(int));
			memcpy(&node,(bf+1),sizeof(int));	

			DEBUG<<"[EMListener]Got connection. Received:"<<numbytes<<" in bf: "<<*bf<<" ,ncredits:"<<numberOfCreditsRequested
			<<"from builderID: "<<node<< "of IP:"<<UDP::Configuration->BUMapIDIP[node].c_str()<<std::endl; 

			return; 
		}
	}
}
