
/**
*      MPI BU - impl
*
*      author  -   Adam Otto    
*      email   -   aotto@cern.ch
*
*      November, 2014
*      CERN
*/

#include "udpBU.h"

/*MPIBuilderUnit::~MPIBuilderUnit(){
for (int i=0; i<initialCredits; ++i){
free(&fragmentRequests[i]);
for (int j=0; j<UDP::Configuration->numnodes * (MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS); ++j)
free(&requests[i][j]);
}
free(size_id);
}*/
UDP::BuilderUnit::BuilderUnit(){};
UDP::BuilderUnit::~BuilderUnit(){free(size_id);}

void UDP::BuilderUnit::initialize(int initialCredits){
    int yes = 1;
    initialCredits = initialCredits;
    activeRequests.resize(initialCredits, -1);
    requestIndex.resize(initialCredits, 0);   
    transmissionSize.resize(initialCredits, 0);
    requestReadoutIndex.resize(initialCredits);
    sockBUSender=0;
    sockBUpl=0;
    sockBUListener2=0;

    for (int i=0; i<requestReadoutIndex.size(); ++i)
        requestReadoutIndex[i].resize(UDP::Configuration->numnodes, 0);

    DEBUG << "BuilderUnit: Reserved fragments, meta and data for " << UDP::Configuration->numnodes << " nodes;"
    << " data buffer: " << Core::Configuration->BUDataBufferSize << ", meta buffer: " << Core::Configuration->BUMetaBufferSize << " (B)" << std::endl;

    // Reserve size and id for sync recvs
    size_id = (int*) malloc(2 * sizeof(int));
    tstart = time(NULL);
    
    // BU Receiver Events.########################################################################################################################################################
   
    hints_BUReceiver.ai_family = AF_INET;
    hints_BUReceiver.ai_socktype = SOCK_DGRAM;
    hints_BUReceiver.ai_flags = AI_PASSIVE; // use my IP


    if ((rv_BUReceiver = getaddrinfo(NULL, (Tools::toString(UDP::Configuration->BUMapIDPort_fc[UDP::Configuration->rankID])).c_str(), &hints_BUReceiver, &servinfo_BUReceiver)) != 0) {
        fprintf(stderr, "[BUListener] getaddrinfo: %s\n", gai_strerror(rv_BUReceiver));

    }

    for(p_BUReceiver = servinfo_BUReceiver; p_BUReceiver != NULL; p_BUReceiver = p_BUReceiver->ai_next) {
        if ((sockBUListener = socket(p_BUReceiver->ai_family, p_BUReceiver->ai_socktype,p_BUReceiver->ai_protocol)) == -1) {
            perror("[BUListener]server: socket");
            continue;
        }

        if (setsockopt(sockBUListener, SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(int)) == -1) {
            perror("[BUListener]setsockopt");
            continue;
        }

        if (bind(sockBUListener, p_BUReceiver->ai_addr, p_BUReceiver->ai_addrlen) == -1) {
            close(sockBUListener);
            perror("[BUListener]server: bind");
            continue;
        }

        break;
    }

    if (p_BUReceiver== NULL)  {
        fprintf(stderr, "[BUListener]server: failed to bind\n");

    }

    freeaddrinfo(servinfo_BUReceiver); // all done with this structure
    fcntl(sockBUListener , F_SETFL, O_NONBLOCK);

    // SETTING UP SECOND LISTENER##############################################################################################
    hints_BUReceiver2.ai_family = AF_INET;
    hints_BUReceiver2.ai_socktype = SOCK_DGRAM;
    hints_BUReceiver2.ai_flags = AI_PASSIVE; // use my IP


    if ((rv_BUReceiver2 = getaddrinfo(NULL, (Tools::toString(UDP::Configuration->BUMapIDPort_SendEvent[UDP::Configuration->rankID])).c_str(), &hints_BUReceiver2, &servinfo_BUReceiver2)) != 0) {
        
        fprintf(stderr, "[BUListener] getaddrinfo: %s\n", gai_strerror(rv_BUReceiver2));

    }

    for(p_BUReceiver2 = servinfo_BUReceiver2; p_BUReceiver2 != NULL; p_BUReceiver2 = p_BUReceiver2->ai_next) {
        if ((sockBUListener2 = socket(p_BUReceiver2->ai_family, p_BUReceiver2->ai_socktype,p_BUReceiver2->ai_protocol)) == -1) {
            perror("[BUListener]server: socket");
            continue;
        }
        
        if (setsockopt(sockBUListener2, SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(int)) == -1) {
            perror("[BUListener]setsockopt");
            continue;
        }

        if (bind(sockBUListener2, p_BUReceiver2->ai_addr, p_BUReceiver2->ai_addrlen) == -1) {
            perror("[BUListener]server: bind");
            continue;
        }

        break;
    }

    if (p_BUReceiver2== NULL)  {
        fprintf(stderr, "[BUListener]server: failed to bind\n");

    }

    fcntl(sockBUListener2 , F_SETFL, O_NONBLOCK);
    freeaddrinfo(servinfo_BUReceiver2);
    


    //############BU RECEIV SIZE AND ID ##################################
    memset(&hintsBU, 0, sizeof hintsBU);
    hintsBU.ai_family = AF_INET;
    hintsBU.ai_socktype = SOCK_DGRAM;// use TCP/IP
    hintsBU.ai_flags = AI_PASSIVE; // use my IP


    if ((rvBU = getaddrinfo(NULL,(Tools::toString(UDP::Configuration->BUMapIDPort_SizeID[UDP::Configuration->rankID])).c_str(), &hintsBU, &servinfoBU)) != 0) {//tmp  port to be decided
        fprintf(stderr, "[BU]getaddrinfo: %s\n", gai_strerror(rvBU));
    }

    for(p_BU = servinfoBU; p_BU != NULL; p_BU = p_BU->ai_next) {
        if ((sockfdBU = socket(p_BU->ai_family, p_BU->ai_socktype,p_BU->ai_protocol)) == -1) {
            perror("[RU]server: socket");
            continue;
        }

        if (setsockopt(sockfdBU, SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(int)) == -1) {
            perror("[RU]:setsockopt");
            continue;
        }

        if (bind(sockfdBU, p_BU->ai_addr, p_BU->ai_addrlen) == -1) {
            perror("[RU]server: bind");
            continue;
        }

        break;
    }

    if (p_BU == NULL)  {
        fprintf(stderr, "[RU] Server: failed to bind\n");
    }
    freeaddrinfo(servinfoBU); // all done with this structure
    sleep(5); //sleep otherwise some packets are send before receiving side is deployed
}

void UDP::BuilderUnit::sendCredits(int& numberOfCredits){

    DEBUG<<"[BU]BUILDER UNIT ID "<<UDP::Configuration->builderNumber(UDP::Configuration->rankID)<<"RANK ID "<<UDP::Configuration->rankID<<std::endl;

    if(sockBUSender==0){
        memset((char *)&sa_BU, 0, sizeof(sa_BU));
        if ((sockBUSender = socket(AF_INET,SOCK_DGRAM,0)) == -1)perror("[RU]: socket");   
        sa_BU.sin_family = AF_INET;
        sa_BU.sin_port=htons(UDP::Configuration->EMLMapIDPort[0]);
        inet_pton(AF_INET, UDP::Configuration->EMLMapIDIP[0].c_str(), &(sa_BU.sin_addr));
    }

    int node =  UDP::Configuration->rankID;
    int *bf_ptr = &bf_BUSender[0];
    
    memcpy(bf_ptr, &numberOfCredits, sizeof(numberOfCredits));
    memcpy(bf_ptr+1,&node,sizeof(node));
    
    if((sendto(sockBUSender, bf_ptr, 2*sizeof(int), 0,(struct sockaddr *)&sa_BU,sizeof(sa_BU))) == -1) {
        perror("[BUSender]: sendto");
    }   
}

void UDP::BuilderUnit::pullRequest(int* size_id_bu, int& readoutno){
    DEBUG << "[BU]: pullRequest source rankID " << UDP::Configuration->rankID << " to readout ID " << UDP::Configuration->readoutID(readoutno)
    << ", id " << UDP::PULL_REQUEST<< " of IP : "<< UDP::Configuration->RUMapIDIP[UDP::Configuration->readoutID(readoutno)].c_str()<< std::endl;

    if(sockBUpl==0){
    
        memset((char *)&sa_BUpl, 0, sizeof(sa_BUpl));

        if ((sockBUpl= socket(AF_INET,SOCK_DGRAM,0)) == -1)perror("[RU]: socket");   

        sa_BUpl.sin_family = AF_INET;
    }



    sa_BUpl.sin_port=htons(UDP::Configuration->RUMapIDPort[UDP::Configuration->readoutID(readoutno)]);        
    inet_pton(AF_INET, UDP::Configuration->RUMapIDIP[UDP::Configuration->readoutID(readoutno)].c_str(), &(sa_BUpl.sin_addr));


    if ((sendto(sockBUpl, size_id_bu, 3*sizeof(int), 0,(struct sockaddr *)&sa_BUpl,sizeof(sa_BUpl))) == -1) {
        perror("[BU]pl: : send");
    }

    memset((char *)&sa_BUpl.sin_addr, 0, sizeof(sa_BUpl.sin_addr));
    memset((char *)&sa_BUpl.sin_port, 0, sizeof(sa_BUpl.sin_port));

    DEBUG<<"[BU] pullRequest sended. eventSize : "<<size_id_bu[0]<< " eventID :  "<< size_id_bu[1] << " destination BU :  "<< size_id_bu[2]<<std::endl;
}





void UDP::BuilderUnit::receiveSizeAndID(int& eventSize, int& eventID){
    int sum =0;
    int yes_fc=1;

    DEBUG << " Waiting for Size and ID from EMConsumer"<<std::endl;

    sin_size_BU = sizeof their_addr_BU;

    if ((numbytesBU = recvfrom(sockfdBU, size_id,(2 * sizeof(int)), 0,(struct sockaddr *)&their_addr_BU,&sin_size_BU)) < 0){
        perror("[RU Listener] recv"); 
    }

    inet_ntop(their_addr_BU.ss_family,(&((struct sockaddr_in *)&their_addr_BU)->sin_addr) ,S_toBU, sizeof S_toBU);

    DEBUG<<"[BU] Server: got connection from EMConsumer :"<<S_toBU<<std::endl;

    eventSize = size_id[0];
    eventID = size_id[1];

    DEBUG<<"[BU]Size : "<<eventSize<<"eventID: "<<eventID<<std::endl;
}


void UDP::BuilderUnit::prepareReceiveEvent(FragmentComposition* fc){
    int slot;
    tmp = 0 ;
    fc_ptr = fc;

    DEBUG<<"[BUListener]prepareReceiveEvent, numnodes: "<< UDP::Configuration->numnodes <<std::endl;


    for (slot=0; slot<activeRequests.size(); ++slot) 
        if(activeRequests[slot] == -1) break;

    if (slot == activeRequests.size()){

        ERROR << "MPIBuilderUnit: prepareReceiveEvent can't listen because there are no free slots. "
        << activeRequests.size() << " active" << std::endl;

        return;
    }

    DEBUG << "BuilderUnit: prepareReceiveEvent listening on slot " << slot << std::endl;
}

bool UDP::BuilderUnit::prepareReceiveEventComplete(int& fragmentReceived){
    
    int slot;
    bool ret_val;
    
    DEBUG<<"[BUListener]prepareReceiveEvent, numnodes: "<< UDP::Configuration->numnodes <<std::endl;
    


    sinBU_size = sizeof connectorBU_addr;
    activeRequestsList.push_back(slot);

    while(1){
        for(int i=tmp; i<UDP::Configuration->numnodes; ++i){
            
            int sum_fc=0;

            if((rv_BUReceiver2=recvfrom(sockBUListener, (fc_ptr+i), sizeof(BUdataBuffer), 0,(struct sockaddr *)&connectorBU_addr,&sinBU_size))<0){
                continue;
            }
            else{

                inet_ntop(connectorBU_addr.ss_family,(&((struct sockaddr_in *)&connectorBU_addr)->sin_addr) ,sBU, sizeof sBU);

                fragmentReceived = i;
                tmp+=1;

                DEBUG << "[BUListener]Connection ID:  "<< i <<"   Server: prepareReceiveEventComplete got connection from " << sBU 
                <<" and received "<<rv_BUReceiver2<<" bytes of FragmentComposition"<<std::endl;

                ret_val=1;
                return ret_val;
            }
        }   

        tmp=0;
    }

}

/**
* Sets up the Irecvs for this eventID.
* @param memory
* @param size
* @param eventID
*/
void UDP::BuilderUnit::setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID){

    int slot=0;
    int sum = 0;

    DEBUG<<"[BUListener] setupReceiveEvent. size    "<<size<<"  from "<<readoutIndex<<" readoutIndex"<<std::endl;
    
    sinBU_size2 = sizeof connectorBU_addr2;



    while(sum<size){

        if((rv_BUReceiver3 = recvfrom(sockBUListener2,dataPointer+sum,size-sum,0,(struct sockaddr *)&connectorBU_addr2,&sinBU_size2))==-1)break;

        sum+=rv_BUReceiver3;

    }

    inet_ntop(connectorBU_addr.ss_family,(&((struct sockaddr_in *)&connectorBU_addr2)->sin_addr) ,sBU2, sizeof sBU2);
    DEBUG<<"[BUListener] Server: got connection from "<< sBU2<<" and received "<<sum<<" bytes"<<std::endl;
    
    int shift = readoutIndex * 0x01000000;

    DEBUG << "BuilderUnit: Irecv: r " << UDP::Configuration->rankID << ", s " << UDP::Configuration->readoutID(readoutIndex)
    << ", id " << shift + eventID * UDP::EVENT_MESSAGES_SIZE + UDP::EVENT_SEND_FRAGMENT + requestReadoutIndex[slot][readoutIndex]
    << ", size " << size << std::endl;


    requestIndex[slot]++;
    requestReadoutIndex[slot][readoutIndex]++;
    transmissionSize[slot] += sum;
}

bool UDP::BuilderUnit::receiveEvent(int& finishedEventID, int& tSize){ 

    int slot=0;

    finishedEventID = activeRequests[slot];
    tSize = transmissionSize[slot];

    activeRequests[slot] = -1;
    requestIndex[slot] = 0;
    transmissionSize[slot] = 0;

    return true;

}

void UDP::BuilderUnit::discardEvent(){

    int slot;
    activeRequests[slot] = -1;
    requestIndex[slot] = 0;
    transmissionSize[slot] = 0;

}

bool UDP::BuilderUnit::checkExitMessage(void)
{
	return false;
}
