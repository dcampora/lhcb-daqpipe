
/**
 *      EM transport core consumer
 *
 *      author  -   Adam Otto
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */

#ifndef UDPEMCONSUMER
#define UDPEMCONSUMER 1
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/uio.h>
#include "../common/EMTransportCoreConsumer.h"
#include "../../common/Logging.h"
#include "UDPCore.h"

namespace UDP{

class EMConsumer : public EMTransportCoreConsumer  {

private:

		int sockBUSender;
        int sockRUSender;
	    struct addrinfo hintsBU, *servinfoBU, *p_BU;
	    struct sockaddr_in sa;
	    int rv_toRU, rv_toBU;
        int * prov_bf_1;
        int * prov_bf_2;
	    char s[INET6_ADDRSTRLEN];
        int total_bytes, send_bytes;

public:
    EMConsumer();
    ~EMConsumer();

    virtual void sendToRUs(int* &size_id_bu);

    virtual void sendToBU(int* &size_id, int destinationBU);
    virtual void sendExit(void);
};
};
#endif
