
/**
 *      MPI RU
 *
 *      author  -   Adam Otto
 *      email   -   aotto@cern.ch
 *
 *      November, 2014
 *      CERN
 */

#include "udpRU.h"

	UDP::ReadoutUnit::ReadoutUnit(){};
	UDP::ReadoutUnit::~ReadoutUnit(){};


void UDP::ReadoutUnit::initialize(int initialCredits){
	int yes = 1;
	activeRequests.resize(initialCredits, -1);
	requestIndex.resize(initialCredits, 0);
	sockRUtoBU=0;
	sockRUtoBU2=0;
	tmp =0;
	check = true;
	memset(&hintsRU, 0, sizeof hintsRU);
	hintsRU.ai_family = AF_INET;
	hintsRU.ai_socktype = SOCK_DGRAM;// use UDP/IP
	hintsRU.ai_flags = AI_PASSIVE; // use my IP

	if ((rvRU = getaddrinfo(NULL,Tools::toString(UDP::Configuration->RUMapIDPort[UDP::Configuration->rankID]).c_str(), &hintsRU, &servinfoRU)) != 0) {//tmp  port to be decided
		fprintf(stderr, "[RU]getaddrinfo: %s\n", gai_strerror(rvRU));
	}

	for(p_RU = servinfoRU; p_RU != NULL; p_RU = p_RU->ai_next) {
		if ((sockfdRU = socket(p_RU->ai_family, p_RU->ai_socktype,
			p_RU->ai_protocol)) == -1) {
			perror("[RU]server: socket");
			continue;
		}

		if (setsockopt(sockfdRU, SOL_SOCKET, SO_REUSEADDR, &yes,
			sizeof(int)) == -1) {
			perror("[RU]:setsockopt");
			continue;
		}

		if (bind(sockfdRU, p_RU->ai_addr, p_RU->ai_addrlen) == -1) {
			perror("[RU]server: bind");
			continue;
		}

		break;
	}
	if (p_RU == NULL){

		fprintf(stderr, "[RU] Server: failed to bind\n");

	}

	freeaddrinfo(servinfoRU); // all done with this structure


	size_id_BU = (int*) malloc(3 * sizeof(int));
}

/**
 * Listens for a message from the EventManager.
 */
bool UDP::ReadoutUnit::EventListen(int& size, int& eventID, int& destinationBU){

	sin_size_RU=sizeof their_addr_RU;

	if((numbytesRU = recvfrom(sockfdRU, size_id_BU,(3 * sizeof(int)), 0,(struct sockaddr*)&their_addr_RU,&sin_size_RU)) < 0)perror("[RU Listener] recv");

	inet_ntop(their_addr_RU.ss_family,(&((struct sockaddr_in *)&their_addr_RU)->sin_addr) ,S_toRU, sizeof S_toRU);

	DEBUG<<"[RU] Server: got connection from "<<S_toRU<<std::endl;

	size = size_id_BU[0];

	eventID = size_id_BU[1];

	destinationBU = size_id_BU[2];

	DEBUG<<"[RU]Size : "<<size<<"eventID: "<<eventID<<" destinationBU: "<<destinationBU<<std::endl;

	return true;
}

void UDP::ReadoutUnit::pullRequestReceive(int& eventID, int& size, int& destinationBU){

	DEBUG << "MPIReadoutUnit: pullRequestReceive s any, r " << UDP::Configuration->rankID
	<< ", id " << UDP::PULL_REQUEST << std::endl;

	if(check){

		sin_size_RU=sizeof their_addr_RU;
		fcntl(sockfdRU, F_SETFL, O_NONBLOCK);
		check = false ;

	}

	while(1){	
		for (int i = tmp; i < UDP::Configuration->numnodes; ++i)
		{    		             
		 	if ((numbytesRU = recvfrom(sockfdRU, size_id_BU,3 * sizeof(int), 0,(struct sockaddr *)&their_addr_RU,&sin_size_RU)) < 0){continue; 
		 	}
		 	else{
				 size = size_id_BU[0];
				 eventID = size_id_BU[1];
				 destinationBU = UDP::Configuration->builderID(size_id_BU[2]);
				 tmp+=1;
				 inet_ntop(their_addr_RU.ss_family,(&((struct sockaddr_in *)&their_addr_RU)->sin_addr) ,S_toRU, sizeof S_toRU);
				 DEBUG<<"[RU] Server: got connection from "<<S_toRU<<std::endl;

				 DEBUG<<"[RU] pullRequestReceive Size : "<<size<<"eventID: "<<eventID<<" destinationBU: "<<destinationBU<<std::endl;

				 return;
				}
		}

		tmp=0;
	}
}
/**
 * Prepares the sendEvent
 */
void UDP::ReadoutUnit::prepareSendEvent(FragmentComposition* &fc, int destinationBU){

	DEBUG<<"[RU] metablocks :"<<fc[0].metablocks<<"  datablocks: "<<fc[0].datablocks<<"	total number of parts: "<<(fc[0].metablocks+fc[0].datablocks)<<std::endl;
	DEBUG<<"[RU] Preparing send of EVents. FC: "<<fc<<", destBU: "<<destinationBU<<"  of IP: "<<UDP::Configuration->BUMapIDIP[destinationBU].c_str()<<std::endl;
	if(sockRUtoBU==0){
		memset((char *)&sa_RUtoBU, 0, sizeof(sa_RUtoBU));
		if ((sockRUtoBU = socket(AF_INET,SOCK_DGRAM,0)) == -1)perror("[RU]: socket");	
		sa_RUtoBU.sin_family = AF_INET;
	
	}
	sa_RUtoBU.sin_port=htons(UDP::Configuration->BUMapIDPort_fc[destinationBU]);
	inet_pton(AF_INET, UDP::Configuration->BUMapIDIP[destinationBU].c_str(), &(sa_RUtoBU.sin_addr));

	if ((send_bytes = sendto(sockRUtoBU, fc, 52, 0,(struct sockaddr *)&sa_RUtoBU,sizeof(sa_RUtoBU))) == -1) {
								perror("[RU] send:");
							}
	memset((char *)&sa_RUtoBU.sin_addr, 0, sizeof(sa_RUtoBU.sin_addr));	
	memset((char *)&sa_RUtoBU.sin_port, 0, sizeof(sa_RUtoBU.sin_port));

	DEBUG<<"[RU]Sending "<<send_bytes<<" bytes... fc to following BU: "<<UDP::Configuration->BUMapIDIP[destinationBU].c_str()<<std::endl;
}

/**
 * (Async) Sends the events to a particular Builder Unit.
 */
void UDP::ReadoutUnit::setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU){
	int slot;
	int sum = 0;
	
	if(sockRUtoBU2==0){
		memset((char *)&sa_RUtoBU2, 0, sizeof(sa_RUtoBU2));
		if ((sockRUtoBU2 = socket(AF_INET,SOCK_DGRAM,0)) == -1)perror("[RU]: socket");	
		sa_RUtoBU2.sin_family = AF_INET;						
	}


	DEBUG<<"[RU] setupSendEvent socket "<<sockRUtoBU2<<" to 	"<<UDP::Configuration->builderNumber(destinationBU)<<" BU "<<" size "<<size<<std::endl;

	sa_RUtoBU2.sin_port=htons(UDP::Configuration->BUMapIDPort_SendEvent[destinationBU]);
	inet_pton(AF_INET, UDP::Configuration->BUMapIDIP[destinationBU].c_str(), &(sa_RUtoBU2.sin_addr));
	int total_bytes = 0;


	while(total_bytes<size){
	if((size - total_bytes)>=(65507)){// maximum udp message size
			if((send_bytes=sendto(sockRUtoBU2, dataPointer+total_bytes,(65507), 0,(struct sockaddr *)&sa_RUtoBU2,sizeof(sa_RUtoBU2)))==-1)perror("[RU] sendto 1 :");
					
				total_bytes+=send_bytes;		

			}
			else{

				if((send_bytes=sendto(sockRUtoBU2, dataPointer+total_bytes,(size-total_bytes), 0,(struct sockaddr *)&sa_RUtoBU2,sizeof(sa_RUtoBU2)))==-1)perror("[RU] sendto 2 :");
			
				total_bytes+=send_bytes;
			}
	}




	memset((char *)&sa_RUtoBU2.sin_addr, 0, sizeof(sa_RUtoBU2.sin_addr));	
	memset((char *)&sa_RUtoBU2.sin_port, 0, sizeof(sa_RUtoBU2.sin_port));


	DEBUG<<"[RU] SENDED to "<< UDP::Configuration->builderNumber(destinationBU)<<"	BU "<<total_bytes<<" BYTES."<<std::endl;
}

bool UDP::ReadoutUnit::sendAnyEvent(int& eventID){
	bool eventSent = true;
	return eventSent;
}

bool UDP::ReadoutUnit::sendEvent(int eventID){

	bool eventSent=true;
	return eventSent;
}

void UDP::ReadoutUnit::discardEvent(){
   
return;
}