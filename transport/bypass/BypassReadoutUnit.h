
/**
 *      Bypass memory communicator - Readout Unit
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#ifndef BYPASSREADOUTUNIT
#define BYPASSREADOUTUNIT 1

#include "../common/RUTransportCore.h" 

class BypassReadoutUnit : public RUTransportCore {
public:
    virtual void initialize(int initialCredits){}
    virtual bool EventListen(int& size, int& eventID, int& destinationBU){
        size = 1024;
        eventID = 0;
        destinationBU = 0;
		return true;
    }
    virtual void prepareSendEvent(FragmentComposition*& fragmentCompositionPointer, int destinationBU){}
    virtual void setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU){}
    virtual bool sendAnyEvent(int& eventID){
        eventID = 0;
        return true;
    }
    virtual bool sendEvent(int eventID){ return true; }
};

#endif
