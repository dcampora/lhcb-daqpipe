
/**
 *      Bypass memory communicator - Builder Unit
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#ifndef BYPASSBUILDERUNIT
#define BYPASSBUILDERUNIT 1

#include <cstdlib>
#include "../../common/GenStructures.h"
#include "../common/BUTransportCore.h"

class BypassBuilderUnit : public BUTransportCore {
private:
    FragmentComposition _fc;

public:
    virtual void initialize(int initialCredits){
        INFO << "BypassBuilderUnit: initialize" << std::endl;
        _fc.eventID = -1;
        _fc.metablocks = 0;
        _fc.datablocks = 0;
    }
    virtual void sendCredits(int& numberOfCredits){}
    virtual void receiveSizeAndID(int& eventSize, int& eventID){}
    virtual void prepareReceiveEvent(FragmentComposition* fc){
        memcpy(fc, &_fc, sizeof(FragmentComposition));

        INFO << "BypassBuilderUnit prepareReceiveEvent: #metablocks " << fc->metablocks << std::endl;
    }
    virtual void setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID){}
    virtual bool receiveEvent(int& finishedEventID, int& tSize){
        finishedEventID = 0;
        tSize = 0;
        return true;
    }
};

#endif
