/**
 *      RDMATransfer
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef RDMATRANSFER_H
#define RDMATRANSFER_H

#include "Verbs_lib.h"
#include "connection.h"
#include <infiniband/verbs.h>
#include <iostream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include "../../common/Logging.h"
#include "../../common/Tools.h"
#include "../../common/GenStructures.h"
#include <pthread.h>
#include "VerbsTransfer.h"

/** @file */

/**
 * The RDMATransfer class is used to make wirte and read (read not implemented yet)
 * RDMA transfer over and established verbs connection.
 * This class takes care of all the memory registration.
 * This class is thread safe and the payload and the memory region are protected against
 * accidental overwriting caused by retransmission.
 * For better performances is possible to add statically registered memory regions.
 * While updating the payload the code searchs for a suitable mr in the given list.
 * If is impossible to find an good mery region in the list an new temporary mr will be allocated.
 *
 */
class RDMATransfer : public VerbsTransfer
{
    ibv_mr _peer_mr;
    std::vector <ibv_mr*> _static_local_mr;
    bool static_mr_use;

    void update_mr(struct connection *conn);
public:
    RDMATransfer(void *payload = NULL, int size = 0) : VerbsTransfer(payload, size), static_mr_use(false){
    }

    ~RDMATransfer(){ }

    /**
     * @brief add_static_mr
     * adds a preregistered mr to the list of static regions
     * @param mr memory region to add
     */
    void add_static_mr(ibv_mr *mr);

    /**
     * @brief remove_static_mr
     * removes a mr from the list of static regions
     * @param mr memory region to remove
     * @return 0 success \n
     *        -1 object busy \n
     *        -2 mr not found \n
     */
    int remove_static_mr(ibv_mr *mr);

    /**
     * @brief setPayload
     * @param payload
     * @return true on success
     */
    bool setPayload(void *payload);

    /**
     * @brief peer_mr
     * @return peer_mr()
     */
    ibv_mr peer_mr() const;

    /**
     * @brief set_peer_mr
     * @param peer_mr
     */
    void set_peer_mr(ibv_mr &peer_mr);

    /**
     * @brief searchStaticMR
     * @return
     */
    ibv_mr *searchStaticMR();

    /**
     * @brief rdma_write
     * performs a RDMA write operation
     * @param conn destination connection
     * @return true on success
     */
    bool rdma_write(connection *conn);

    /**
     * @brief rdma_write
     * performs a RDMA write operation
     * @param payload
     * @param size
     * @param conn
     * @return true on success
     */
    bool rdma_write(void *payload, int size, connection *conn);

    /**
     * @brief rdma_write
     * performs a RDMA write operation
     * @param payload
     * @param size
     * @param peer_mr
     * @param conn
     * @return true on success
     */
    bool rdma_write(void *payload, int size, ibv_mr &peer_mr, connection *conn);

    /**
     * @brief rdma_write
     * performs a RDMA write operation
     * @param peer_mr
     * @param conn
     * @return true on success
     */
    bool rdma_write(ibv_mr &peer_mr, connection *conn);
};

#endif // RDMATRANSFER_H
