/**
 *      Verbs RUPollCqBU - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsRUPollcqBU.h"


context *VerbsRUPollCqBU::ctx() const
{
    return _ctx;
}

void VerbsRUPollCqBU::setCtx(context *ctx)
{
    _ctx = ctx;
}
VerbsRUPollCqBU::VerbsRUPollCqBU()
{
}

VerbsRUPollCqBU::VerbsRUPollCqBU(context *ctx, NamedPipe<Message *> *memory_region_shmem, NamedPipe<Message *> *pull_request_shmem,
                                 NamedPipe<RDMATransfer *> *completed_rdma_shmem,
                                 NamedPipe<Message *> *completed_send_msg){
    _ctx = ctx;
    _memory_region_shmem = memory_region_shmem;
    _pull_request_shmem = pull_request_shmem;
    _completed_rdma_shmem = completed_rdma_shmem;
    _completed_send_msg = completed_send_msg;
}

void VerbsRUPollCqBU::run(){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    connection *conn;
    Message *msg;
    RDMATransfer *rdma_msg;
    static int ev_conter = 0;
    const int ack_number = 20;

    while(true){
        DEBUG << "VerbsRUPollCqBU::run polling loop iteration" << std::endl;
        check_nz(ibv_get_cq_event(_ctx->comp_channel, &cq, &ctx_ret), "verbsEMListnerPolCq::run ibv_get_cq_event", true);
        while (ibv_poll_cq(cq, 1, &wc)){
            ev_conter++;
            DEBUG << "VerbsRUPollCqBU::run MSG received form qp " << wc.qp_num << std::endl;
            conn = _ctx->connections_qp_num[wc.qp_num];
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "VerbsRUPollCqBU::run status is " << ibv_wc_status_str(wc.status) << std::endl;
                if(wc.opcode == IBV_WC_RDMA_WRITE){
                    rdma_msg = (RDMATransfer *)wc.wr_id;
                    ERROR << "VerbsRUPollCqBU::run RDMA error" << std::endl;
                }else{
                    msg = (Message *) wc.wr_id;
                    if(msg->type() == MSG_END){
                        ERROR << "VerbsRUPollCqBU::run END_MSG failed" << std::endl;
                    }
                    ERROR << "VerbsRUPollCqBU::run msg error" << std::endl;
                }
            }else if(wc.opcode & IBV_WC_RECV){
                msg = (Message *) wc.wr_id;
               if(msg->type() == MSG_MR){
                   DEBUG << "VerbsRUPollCqBU::run memory region received" << std::endl;
                   while(_memory_region_shmem->write(msg) == -1) usleep(1);
//                   msg->release_mr();
//                   msg->release_payload();
               } else if(msg->type() == MSG_PULL_REQUEST){
                   DEBUG << "VerbsRUPollCqBU::run pull request received" << std::endl;
                   while(_pull_request_shmem->write(msg) == -1) usleep(1);
               }else{
                   ERROR << "VerbsRUPollCqBU::run unknown message type" << std::endl;
                   msg->release_payload();
                   msg->release_mr();
                   while(!(msg->receive_message(_ctx->connections_id[msg->sender_id()]))) usleep(1);
               }
            }else if(wc.opcode == IBV_WC_SEND){
                msg = (Message *) wc.wr_id;
                DEBUG << "VerbsRUPollCqBU::run message sent" << std::endl;
                msg->release_mr();
                msg->release_payload();
                while(_completed_send_msg->write(msg) == -1) usleep(1);
            }else if(wc.opcode == IBV_WC_RDMA_WRITE){
                rdma_msg = (RDMATransfer *)wc.wr_id;
                rdma_msg->release_mr();
                rdma_msg->release_payload();
                DEBUG << "VerbsRUPollCqBU::run RDMA write over" << std::endl;
                while(_completed_rdma_shmem->write(rdma_msg) == -1) usleep(1);
            }else{
                WARNING << "VerbsRUPollCqBU::run Unexpected event dropping" << std::endl;
            }
        }
        if(ev_conter >= ack_number){
            ibv_ack_cq_events(cq, ev_conter);
            ev_conter = 0;
        }

        check_nz(ibv_req_notify_cq(cq, 0), "VerbsRUPollCqBU::run ibv_req_notify_cq", true);
    }
}
