/**
 *      Verbs RUPollCqBU
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSRUPOLLCQBU_H
#define VERBSRUPOLLCQBU_H
#include "../../common/NamedPipe.h"
#include "../../common/NamedPipe.h"
#include "../../common/Logging.h"
#include "../../common/GenStructures.h"
#include "../../common/Timer.h"
#include "Verbs_lib.h"
#include "VerbsCore.h"
#include "rdmatransfer.h"

class VerbsRUPollCqBU
{
    context *_ctx;

    NamedPipe<Message *> *_memory_region_shmem;

    NamedPipe<Message *> *_pull_request_shmem;

    NamedPipe<RDMATransfer *> *_completed_rdma_shmem;


    NamedPipe<Message *> *_completed_send_msg;

public:
    VerbsRUPollCqBU();
    VerbsRUPollCqBU(context *ctx = NULL, NamedPipe<Message *> *memory_region_shmem = NULL,
                    NamedPipe<Message*> *pull_request_shmem = NULL,
                    NamedPipe<RDMATransfer *> *completed_rdma_shmem = NULL,
                    NamedPipe<Message *> *completed_send_msg = NULL);
    context *ctx() const;
    void setCtx(context *ctx);

    void run();
};

#endif // VERBSRUPOLLCQBU_H
