/**
 *      Verbs BUPolqcEML
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSBUPOLCQEML_H
#define VERBSBUPOLCQEML_H
#include "../../common/NamedPipe.h"
#include "../../common/Logging.h"
#include "../../common/GenStructures.h"
#include "../../common/Timer.h"
#include "Verbs_lib.h"
#include "VerbsCore.h"
#include <map>
#include <queue>

class VerbsBUPolCqcEML
{
private:
    //NamedPipe <int> *_completed_event_shmem;
    context *_ctx;
public:
    VerbsBUPolCqcEML();
    VerbsBUPolCqcEML(context *ctx = NULL);
    ~VerbsBUPolCqcEML(){}
    void run();
    context *ctx() const;
    void setCtx(context *ctx);
};

#endif // VERBSBUPOLCQEML_H
