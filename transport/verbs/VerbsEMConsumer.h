/**
 *      Verbs EMConsumer
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

// class EventManagerCoreConsumerWorkerMPI : public EventManagerCoreConsumerWorker
// {
// public:
//     EventManagerCoreConsumerWorkerMPI(SharedQueue& assignedCredits) : 
//         EventManagerCoreConsumerWorker(assignedCredits) {}

//     void send(int destination);
// };

#ifndef VERBSEMCONSUMER
#define VERBSEMCONSUMER 1

#include "stdlib.h"
#include "VerbsCore.h"
#include "../common/EMTransportCoreConsumer.h"
#include "../../common/Logging.h"
#include "Verbs_lib.h"
#include "message.h"
#include "../../common/pthreadHelper.h"
#include "../../common/Shmem.h"
#include "VerbsEMCPolcqRU.h"

namespace Verbs {

class EMConsumer : public EMTransportCoreConsumer {
private:

    context *ru_ctx;
    context *bu_ctx;

    std::vector <Message*> send_mess;
    void *send_messages_buff;

    NamedPipe<Message *> *_free_send_msg_shmem;

    VerbsEMCPolCqRU *ru_poller;

public:
    EMConsumer();
    ~EMConsumer();
    void initialize();
    void sendToRUs(int*& size_id_bu);
    void sendToBU(int*& size_id, int destinationBU);
    void sendExit(void);
};

}
#endif
