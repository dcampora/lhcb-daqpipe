/**
 *      connection
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef CONNECTION_H
#define CONNECTION_H
#include "message.h"
#include <infiniband/verbs.h>
#include <iostream>
#include <map>
#include <stdio.h>
#include <stdlib.h>


struct connection {
    int connection_number;
    struct rdma_cm_id *id;
    struct ibv_qp *qp;
    struct ibv_cq *cq;
    struct ibv_pd *pd;

    // not the best but ok
    struct ibv_mr *rdma_meta_mr;
    struct ibv_mr *rdma_data_mr;

    struct ibv_mr peer_meta_mr;
    struct ibv_mr peer_data_mr;

    class Message * recv_msg;
    class Message * send_msg;

    struct sockaddr *client_addr;

    int total_packets;

    // for benchmarking
    int packet_left;

    void *rdma_meta_buff;
    void *rdma_data_buff;
};

#endif // CONNECTION_H
