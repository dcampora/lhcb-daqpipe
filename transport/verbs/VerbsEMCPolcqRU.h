/**
 *      Verbs EMCPolcqRU
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSEMCPOLCQRU_H
#define VERBSEMCPOLCQRU_H
#include "../../common/NamedPipe.h"
#include "../../common/Logging.h"
#include "../../common/GenStructures.h"
#include "../../common/Timer.h"
#include "Verbs_lib.h"
#include "VerbsCore.h"
#include <map>
#include <queue>

class VerbsEMCPolCqRU
{
private:
    NamedPipe<Message *> *_free_send_msg_shmem;
    //NamedPipe <int> *_completed_event_shmem;
    context *_ctx;

    Timer tcompletion, twait, tcopy, ttotal, tother;
    bool print_timer;
public:
    VerbsEMCPolCqRU();
    VerbsEMCPolCqRU(context *ctx = NULL,
                   NamedPipe<Message *> *free_send_msg_shmem = NULL);
    ~VerbsEMCPolCqRU(){}
    void run();
    NamedPipe<Message *> *free_send_msg_shmem() const;
    void setFree_send_msg_shmem(NamedPipe<Message *> *free_send_msg_shmem);
    context *ctx() const;
    void setCtx(context *ctx);
};

#endif // VERBSEMCPOLCQRU_H
