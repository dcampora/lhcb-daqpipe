/**
 *      Verbs Core
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSCORE
#define VERBSCORE 1

//#include "VerbsBuilderUnit.h"
#include "../../common/Logging.h"
#include "../config/ConfigurationCore.h"
#include "../../common/Tools.h"
#include "Verbs_lib.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <limits.h>

#include "../../extern-deps/jsoncpp/dist/json/json.h"

#include "../../zmq_handshake/zmq_handshake.h"

namespace Verbs {

enum messages {
    CREDIT_REQUEST = 10,
    EVENT_REQUEST,
    EVENT_ASSIGN,
    EVENT_SEND = 100, // From 100 onwards reserved for event IDs

    EVENT_FRAGMENT_COMPOSITION = 101,
    EVENT_SEND_FRAGMENT = 102,
};

enum ids {
    EVENT_MANAGER_LISTENER = 0,
    EVENT_MANAGER_CONSUMER = 1,
    READOUT_UNIT = 2,
    BUILDER_UNIT = 3
};

enum config {
    EMLISTENER_ID = 0,
    EMCONSUMER_ID = 1,
    EVENT_MESSAGES_SIZE=15,
};

class ConfigurationLoader : public Core::ConfigurationLoader
{
private:
    int _len;

public:
    // maps for association between ip port and device number
    std::map<int, unsigned int> BUMapIDIP;
    std::map<unsigned int, int> BUMapIPID;
    std::map<int, uint16_t> BUMapIDPort;
    std::map<int, unsigned int> RUMapIDIP;
    std::map<unsigned int, int> RUMapIPID;
    std::map<int, uint16_t> RUMapIDPort;
    std::map<int, unsigned int> EMLMapIDIP;
    std::map<unsigned int, int> EMLMapIPID;
    std::map<int, uint16_t> EMLMapIDPort;
    std::map<int, unsigned int> EMCMapIDIP;
    std::map<unsigned int, int> EMCMapIPID;
    std::map<int, uint16_t> EMCMapIDPort;
    //std::map<struct connection*, int> mapConnID;

    std::string sync_server_ip;
    std::string sync_server_port;
    std::string pub_server_port;

    int emlistenerID, emconsumerID, rankID, procID, numprocs, threadSupport,
        concurrentRUtoBUSends, numnodes;
    char name[HOST_NAME_MAX];
    // std::string sqName;
    std::map<int, std::string> logicalNames;

    ConfigurationLoader() : Core::ConfigurationLoader() {}
    // ~MPIConfigurationLoader(){}

    void initialize(int argc, char *argv[]);
    void parseConfiguration();
    bool is(int id);
    bool isEMListener();
    bool isEMConsumer();
    bool isRU();
    bool isBU();
    int builderNumber(int builderID);
    int readoutNumber(int readoutID);
    int builderID(int builderNumber);
    int readoutID(int readoutNumber);

    //std::ifstream devices_ip_config_file;

    bool readoutInSameNode(int readoutNumber);
    bool builderInSameNode(int builderNumber);
    int nodeID();
    int nodeNumber(int _rankID);
    void finalize();
    std::string getLogicalName();
    std::string builderSuffix();
    std::string readoutSuffix();

    int nodeIP(int nodeID);
    int nodePort(int nodeID);
    void initializeIPs();
    void differentiateShmemNames();
};

#ifndef EXTERN_MPI
extern ConfigurationLoader* Configuration;
#endif

}

#endif
