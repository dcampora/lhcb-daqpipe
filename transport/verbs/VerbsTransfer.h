/**
 *      Verbs Transfer
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSTRANSFER_H
#define VERBSTRANSFER_H

#include <pthread.h>

#include <infiniband/verbs.h>

#include <iostream>
#include "../../common/Logging.h"
#include "../../common/Tools.h"
#include "../../common/GenStructures.h"

/**
 * @brief The VerbsTransfer class
 * This class can be used to implement verbs transfer like send messages or RDMA read/write implementing a
 * thread safe way to protect the payload and the memory region from being overwritten accidentaly.
 * This class should not be used directly.
 */

class VerbsTransfer
{
protected:
    void *_payload;
    int _size;
    ibv_mr *_local_mr;

    int _sender_id;

    pthread_mutex_t *_mr_mutex;

    bool _protect_payload;
    bool _protect_mr;
public:
    VerbsTransfer(void *payload = NULL, int size = 0) : _local_mr(NULL), _protect_mr(false),
        _protect_payload(false), _payload(payload), _size(size){
        _mr_mutex = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
        pthread_mutex_init(_mr_mutex, NULL);
    }

    ~VerbsTransfer();

    /**
     * @brief protect_mr
     * @return protect mr if true the message is locked
     */
    bool protect_mr() const ;

    /**
     * @brief release_mr
     * unlock the mr of the message. This should be done only if the payload is safely stored
     * or no longer needed
     */
    void release_mr() ;

    /**
     * @brief release_payload();
     * unlock the payload of the message. This should be done only if the payload is safely stored
     * or no longer needed
     */
    void release_payload();

    /**
     * @brief sender_id
     * @return the id of the sender
     */
    int sender_id() const;


    /**
     * @brief payload
     * @return message payload
     */
    void *payload() const ;

    /**
     * @brief size
     * @return size
     */
    int size() const ;

    // TODO lock this
    /**
     * @brief setSize
     * @param size
     */
    void setSize(int size) ;

};

#endif // VERBSTRANSFER_H
