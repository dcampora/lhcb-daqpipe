/**
 *      Verbs lib - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "Verbs_lib.h"

void check_alloc(const void* array, const char *message, bool fatal){
    if(array==NULL){
        perror(message);
        if(fatal){
            exit(EXIT_FAILURE);
        }
    }
}

void check_nz(int value, const char *message, bool fatal){
    if(value != 0){
        perror(message);
        if(fatal){
            exit(EXIT_FAILURE);
        }
    }
}

void build_context(struct ibv_context *verbs, context *ctx, int connection_number) {
    if (ctx->srq == NULL){
        struct ibv_device_attr dev_attr;
        ctx->ctx = verbs;
        ibv_query_device(verbs, &dev_attr);
        DEBUG << "build_context: max sge " << dev_attr.max_sge << std::endl;
        DEBUG << "build_context: max sge_rd " << dev_attr.max_sge_rd << std::endl;
        DEBUG << "build_context: max mr " << dev_attr.max_mr << std::endl;
        DEBUG << "build_context: max_mr_size " << dev_attr.max_mr_size << std::endl;
        DEBUG << "build_context: max qp " << dev_attr.max_qp << std::endl;
        DEBUG << "build_context: max_srq_sge " << dev_attr.max_srq_sge << std::endl;


        DEBUG << "build_context: ctx->ctx " << ctx->ctx << std::endl;
        ctx->pd = ibv_alloc_pd(ctx->ctx);
        check_alloc(ctx->pd, "build_context: ibv_alloc_pd ctx->pd", true);

        struct ibv_srq_init_attr srq_attr;
        memset(&srq_attr, 0, sizeof (srq_attr));
        srq_attr.attr.max_wr = 50;
        srq_attr.attr.max_sge = 3;
        ctx->srq = ibv_create_srq(ctx->pd, &srq_attr);
        check_alloc(ctx->srq, "build_context: ibv_create_srq:", true);

        ctx->comp_channel = ibv_create_comp_channel(ctx->srq->context);
        check_alloc(ctx->comp_channel, "build_context: ibv_alloc_pd ctx->comp_channel", true);
    }

    ctx->connections_id[connection_number]->pd = ctx->pd;
    DEBUG << "build_context: connection number: " << connection_number << " verbs: " << verbs << std::endl;
    ctx->connections_id[connection_number]->cq = ibv_create_cq(verbs, 10, NULL, ctx->comp_channel, 0); /* cqe=10 is arbitrary */
    check_alloc(ctx->connections_id[connection_number]->cq, "build_context: ibv_create_cq ctx->cq", true);
    //ctx->cq = cq;
    DEBUG << "build_context: ctx cq " << ctx->connections_id[connection_number]->cq << std::endl;

    check_nz(ibv_req_notify_cq(ctx->connections_id[connection_number]->cq, 0), "build_context: ibv_req_notify_cq", true);

}

void build_qp_attr(struct ibv_qp_init_attr *qp_attr, struct connection *conn) {
    memset(qp_attr, 0, sizeof(*qp_attr));

    qp_attr->send_cq = conn->cq;
    qp_attr->recv_cq = conn->cq;
    qp_attr->qp_type = IBV_QPT_RC;

    qp_attr->cap.max_send_wr = 100;
    qp_attr->cap.max_recv_wr = 100;
    qp_attr->cap.max_send_sge = 3;
    qp_attr->cap.max_recv_sge = 3;
}

int on_connect_request(struct rdma_cm_id *id, struct context *ctx, std::map <unsigned int, int> &mapIPID){
    struct ibv_qp_init_attr qp_attr;
    struct rdma_conn_param cm_params;
    struct connection *conn;
    std::map<unsigned int,int>::iterator it;
    int connection_id = 0;


    //conn = (struct connection*) malloc(sizeof(struct connection));
    conn = new connection;
    check_alloc(conn, "on_connect_request: malloc conn", true);
    conn->send_msg = new Message;
    conn->recv_msg = new Message;

    it = mapIPID.find(((struct sockaddr_in *) &id->route.addr.dst_addr)->sin_addr.s_addr);
    DEBUG << "on_connect_request: client port " << ((struct sockaddr_in *) &id->route.addr.dst_addr)->sin_port << std::endl;
    if(it == mapIPID.end()){
        ERROR << "on_connect_request: Connection from undefined device" << std::endl;
        return(-1);
    }else{
        connection_id = it->second;
    }

    ctx->connections_id[connection_id] = conn;

    DEBUG << "on_connect_request: received connection request number " << connection_id << std::endl;

    DEBUG << "on_connect_request: building context" << std::endl;
    build_context(id->verbs, ctx, connection_id);
    build_qp_attr(&qp_attr, ctx->connections_id[connection_id]);

    DEBUG << "on_connect_request: create qp" << std::endl;

    conn->qp = ibv_create_qp(ctx->pd, &qp_attr);
    check_alloc(conn->qp, "on_connect_request: rdma_create_qp", true);

    ctx->connections_qp_num[conn->qp->qp_num] = conn;

    conn->connection_number = connection_id;
    id->context = conn;
    id->qp = conn->qp;
    DEBUG << "on_connect_request: conn qp handle " << conn->qp->handle << std::endl;


    memset(&cm_params, 0, sizeof(cm_params));
    check_nz(rdma_accept(id, &cm_params), "on_connect_request: rdma_accept", true);

    return 0;
}

void printf_mr(const struct ibv_mr *mr){
    DEBUG << "memory region" << std::endl << "context: " << mr->context << std::endl << "pd: " << mr->pd << std::endl
          <<  "addr:  " << mr->addr << std::endl << "length: " << mr->length << std::endl
          << "handle: " << mr->handle << std::endl << "lkey: " << mr->lkey << std::endl <<  "rkey: " << mr->rkey << std::endl;
}

int on_connection(void *context){
    struct connection *conn = (struct connection *)context;

    return 0;
}

int on_addr_resolved(struct rdma_cm_id *id, struct context *ctx, std::map <unsigned int, int> &mapIPID) {
    struct ibv_qp_init_attr qp_attr;
    struct connection *conn;
    std::map<unsigned int,int>::iterator it;
    int connection_id = 0;

    conn = new connection;
    check_alloc(conn, "on_addr_resolved: malloc conn", true);
    conn->send_msg = new Message;
    conn->recv_msg = new Message;

    it = mapIPID.find(((struct sockaddr_in *) &id->route.addr.dst_addr)->sin_addr.s_addr);
    if(it == mapIPID.end()){
        ERROR << "on_addr_resolved: Connection from undefined device" << std::endl;
        return(-1);
    }else{
        connection_id = it->second;
    }

    ctx->connections_id[connection_id] = conn;
    DEBUG << "on_addr_resolved: address resolved." << std::endl;

    build_context(id->verbs, ctx, connection_id);
    build_qp_attr(&qp_attr, ctx->connections_id[connection_id]);

    conn->qp = ibv_create_qp(ctx->pd, &qp_attr);
    check_alloc(conn->qp, "on_addr_resolved: ibv_create_qp", true);

    ctx->connections_qp_num[conn->qp->qp_num] = conn;

    id->context = conn;

    conn->connection_number = connection_id;

    conn->id = id;
    id->qp = conn->qp;

    check_nz(rdma_resolve_route(id, TIMEOUT_IN_MS), "on_addr_resolved: rdma_resolve_route", true);

    return 0;
}

int on_route_resolved(struct rdma_cm_id *id){
    struct rdma_conn_param cm_params;

    DEBUG << "route resolved." << std::endl;
    memset(&cm_params, 0, sizeof(cm_params));
    cm_params.rnr_retry_count = 5;
    cm_params.retry_count = 5;
    check_nz(rdma_connect(id, &cm_params), "on_route_resolved: rdma_connect", true);

    return 0;
}

int on_disconnect(struct rdma_cm_id *id, context *ctx){
    struct connection *conn = (struct connection *)id->context;
    double time_interval;
    double total_time_interval;
    double bandwidth;
    double total_bandwidth;

    rdma_destroy_qp(id);

    ibv_destroy_cq(conn->cq);

    ibv_dereg_mr(conn->rdma_data_mr);
    ibv_dereg_mr(conn->rdma_meta_mr);

    free(conn->send_msg);
    free(conn->recv_msg);

    free(conn->rdma_data_buff);
    free(conn->rdma_meta_buff);

    rdma_destroy_id(id);

    return 0;
}

int server_event_connection(struct rdma_cm_event *event, struct context *ctx, std::map <unsigned int, int> &mapIPID){

    int ret_val = 0;

    DEBUG << "server_event_connection:  event->id " << event->id << std::endl;
    if (event->event == RDMA_CM_EVENT_CONNECT_REQUEST) {
        DEBUG << "cserver_event_connection: onnection request" << std::endl;
        ret_val = on_connect_request(event->id, ctx, mapIPID);
        // set correct node type
    }else if (event->event == RDMA_CM_EVENT_ESTABLISHED){
        DEBUG << "server_event_connection: connection established" << std::endl;
        (ctx->number_of_connections)++;
        ret_val = on_connection(event->id->context);
    }else{
        perror("server_event_connection: on_event");
        DEBUG << "event type: " << event->event << std::endl;
        exit(-1);
    }

    return ret_val;
}

context *build_context_server(int connection_number, uint16_t port, std::map <unsigned int, int> &mapIPID){
    struct sockaddr_in addr;
    struct rdma_cm_event *event = NULL;
    struct rdma_cm_id *listener = NULL;
    struct rdma_event_channel *ec = NULL;
    struct rdma_cm_event event_copy;
    struct context *ctx = NULL;

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    /* init rdma connection */
    ec = rdma_create_event_channel();
    check_alloc(ec, "build_context_server: rdma_create_event_channel", true);
    check_nz(rdma_create_id(ec, &listener, NULL, RDMA_PS_TCP), "build_context_server: rdma_create_id", true);
    check_nz(rdma_bind_addr(listener, (struct sockaddr *)&addr), "build_context_server: rdma_bind_addr", true);
    check_nz(rdma_listen(listener, 10), "build_context_server: rdma_listen", true);

    /* check port configuration */
    port = ntohs(rdma_get_src_port(listener));

    DEBUG << "build_context_server: server port: " << port << std::endl;

    ctx = new context;
    check_alloc(ctx, "build_context_server: malloc ctx", true);
    ctx->number_of_connections = 0;


    DEBUG << "build_context_server: begin" << std::endl;

    while(ctx->number_of_connections < connection_number){
        if(rdma_get_cm_event(ec, &event) == 0){

            memcpy(&event_copy, event, sizeof(*event));
            rdma_ack_cm_event(event);

            // TODO fix this
            server_event_connection(&event_copy, ctx, mapIPID);

            DEBUG << "build_context_server: connection " << ctx->number_of_connections << "/" << connection_number << std::endl;
        }
    }

    DEBUG << "build_context_server: building context done" << std::endl;

    return ctx;
}

int client_event_connection(struct rdma_cm_event *event, struct context *ctx, std::map <unsigned int, int> &mapIPID){
    int ret_val = 0;

    if (event->event == RDMA_CM_EVENT_ADDR_RESOLVED){
        DEBUG << "client_event_connection: on_addr_resolved" << std::endl;
        ret_val = on_addr_resolved(event->id, ctx, mapIPID);
    }else if (event->event == RDMA_CM_EVENT_ROUTE_RESOLVED){
        ret_val = on_route_resolved(event->id);
        DEBUG << "client_event_connection: port route resolved" << std::endl;
    }else if (event->event == RDMA_CM_EVENT_ESTABLISHED){
        DEBUG << "client_event_connection: connection established" << std::endl;
        ret_val = on_connection(event->id->context);
    }else{
        perror("client_event_connection: on_event");
        ERROR << "event type: " << event->event << std::endl;
        exit(EXIT_FAILURE);
    }

    return ret_val;
}


context *build_context_client(std::map <int, uint16_t> &mapIDPort, std::map <unsigned int, int> &mapIPID){

    struct sockaddr_in dest_addr;
    struct rdma_cm_event *event = NULL;
    struct rdma_cm_id *conn = NULL;
    struct rdma_event_channel *ec = NULL;
    struct rdma_cm_event event_copy;
    struct context *ctx = NULL;

    ctx = new context;
    check_alloc(ctx, "build_context_client: main malloc ctx", true);

    DEBUG << "build_context_client: begin" << std::endl;
    for(std::map<unsigned int,int>::iterator it = mapIPID.begin() ; it != mapIPID.end() ; it++){
        /* init addr values */
        memset(&dest_addr, 0, sizeof(dest_addr));
        dest_addr.sin_family = AF_INET;
        dest_addr.sin_port = htons(mapIDPort[it->second]);
        dest_addr.sin_addr.s_addr = it->first;

        DEBUG << "build_context_client: connecting to " << inet_ntoa(dest_addr.sin_addr) << ":" << dest_addr.sin_port << std::endl;
        /* init rdma connection */
        ec = rdma_create_event_channel();
        check_alloc(ec, "build_context_client: rdma_create_event_channel", true);
        check_nz(rdma_create_id(ec, &conn, NULL, RDMA_PS_TCP), "build_context_client: rdma_create_id", true);
//        check_nz(rdma_resolve_addr(conn, (struct sockaddr*)&src_addr, (struct sockaddr*)&dest_addr, TIMEOUT_IN_MS), "main rdma_resolve_addr", true);
        check_nz(rdma_resolve_addr(conn, NULL, (struct sockaddr*)&dest_addr, TIMEOUT_IN_MS), "build_context_client: rdma_resolve_addr", true);
        do{
            if(rdma_get_cm_event(ec, &event) == 0){

                memcpy(&event_copy, event, sizeof(*event));
                rdma_ack_cm_event(event);

                // TODO fix this
                DEBUG << "build_context_client: handling event" << std::endl;
                if(event_copy.event != 8){
                    client_event_connection(&event_copy, ctx, mapIPID);
                } else {
                    INFO << "build_context_client: server " << inet_ntoa(dest_addr.sin_addr) << ":" << dest_addr.sin_port << " not ready" << std::endl;
                    sleep(1);
                    on_route_resolved(event_copy.id);

                }
            }
        }while(event_copy.event != RDMA_CM_EVENT_ESTABLISHED);


        /* check port configuration */
    }

    DEBUG << "build_context_client: building context done" << std::endl;

    return ctx;

}
