/**
 *      Verbs RURDMASender - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsRURDMASender.h"



NamedPipe<Message *> *VerbsRURDMASender::completed_send_msg() const
{
    return _completed_send_msg;
}

void VerbsRURDMASender::setCompleted_send_msg(NamedPipe<Message *> *completed_send_msg)
{
    _completed_send_msg = completed_send_msg;
}

NamedPipe<RDMATransfer *> *VerbsRURDMASender::completed_rdma_shmem() const
{
    return _completed_rdma_shmem;
}

void VerbsRURDMASender::setCompleted_rdma_shmem(NamedPipe<RDMATransfer *> *completed_rdma_shmem)
{
    _completed_rdma_shmem = completed_rdma_shmem;
}

NamedPipe<RDMATransfer *> *VerbsRURDMASender::free_rdma_shmem() const
{
    return _free_rdma_shmem;
}

void VerbsRURDMASender::setFree_rdma_shmem(NamedPipe<RDMATransfer *> *free_rdma_shmem)
{
    _free_rdma_shmem = free_rdma_shmem;
}

NamedPipe<int> *VerbsRURDMASender::completed_events_id() const
{
    return _completed_events_id;
}

void VerbsRURDMASender::setCompleted_events_id(NamedPipe<int> *completed_events_id)
{
    _completed_events_id = completed_events_id;
}

NamedPipe<std::pair<int, int> *> *VerbsRURDMASender::ID_num_frag_shmem() const
{
    return _ID_num_frag_shmem;
}

void VerbsRURDMASender::setID_num_frag_shmem(NamedPipe<std::pair<int, int> *> *ID_num_frag_shmem)
{
    _ID_num_frag_shmem = ID_num_frag_shmem;
}

NamedPipe<std::pair<RDMATransfer *, int> *> *VerbsRURDMASender::rdma_transfer_ID_shmem() const
{
    return _rdma_transfer_ID_shmem;
}

void VerbsRURDMASender::setRdma_transfer_ID_shmem(NamedPipe<std::pair<RDMATransfer *, int> *> *rdma_transfer_ID_shmem)
{
    _rdma_transfer_ID_shmem = rdma_transfer_ID_shmem;
}

context *VerbsRURDMASender::ctx() const
{
    return _ctx;
}

void VerbsRURDMASender::setCtx(context *ctx)
{
    _ctx = ctx;
}

VerbsRURDMASender::VerbsRURDMASender(context *bu_ctx,
                                     NamedPipe<Message *> *completed_send_msg,
                                     NamedPipe<RDMATransfer *> *completed_rdma_shmem,
                                     NamedPipe<RDMATransfer *> *free_rdma_shmem,
                                     NamedPipe<int> *completed_events_id,
                                     NamedPipe<std::pair<int, int> *> *ID_num_frag_shmem,
                                     NamedPipe<std::pair<RDMATransfer *, int> *> *rdma_transfer_ID_shmem){

    _ctx = bu_ctx;
    _completed_send_msg = completed_send_msg;
    _completed_rdma_shmem = completed_rdma_shmem;
    _free_rdma_shmem = free_rdma_shmem ;
    _completed_events_id = completed_events_id ;
    _ID_num_frag_shmem = ID_num_frag_shmem ;
    _rdma_transfer_ID_shmem = rdma_transfer_ID_shmem ;
}

void VerbsRURDMASender::run(){
    std::pair<int, int> *tmp_ID_num_frag;
    std::pair<RDMATransfer *, int> *tmp_rdma_transfer_ID;
    RDMATransfer *tmp_rdma_transfer;
    int non_sent_number;

//    if(Core::Configuration->shmemTransportEnabled || Core::Configuration->bypassShmemTransport){
        non_sent_number = 1;
//    }else {
//        non_sent_number = 0;
//    }

    while(true){
        // TODO read the queue until is empty
        while(_completed_rdma_shmem->read(tmp_rdma_transfer) == -1) usleep(1);
        while(_ID_num_frag_shmem->read(tmp_ID_num_frag) != -1){
            DEBUG << "VerbsRURDMASender::run event " << tmp_ID_num_frag->first << " has "
                  << tmp_ID_num_frag->second << " fragments" << std::endl;
            _map_id_no_rmda[tmp_ID_num_frag->first] = tmp_ID_num_frag->second;
            delete[] tmp_ID_num_frag;
            //_map_id_no_rmda.insert(*tmp_ID_num_frag);
        }

        while(_rdma_transfer_ID_shmem->read(tmp_rdma_transfer_ID) != -1){
            DEBUG << "VerbsRURDMASender::run drma_transfer " << tmp_rdma_transfer_ID->first <<
                     " contains event " << tmp_rdma_transfer_ID->second << std::endl;
            _map_rdma_id[tmp_rdma_transfer_ID->first] = tmp_rdma_transfer_ID->second;
            delete[] tmp_rdma_transfer_ID;
            //_map_rdma_id.insert(*tmp_rdma_transfer_ID);
        }

        //while(_completed_rdma_shmem->read(tmp_rdma_transfer) != -1){
            DEBUG << "VerbsRURDMASender::run completed RDMA received" << std::endl;
            std::map<RDMATransfer *, int>::iterator rdma_id_it;
            std::map<int, int>::iterator id_no_rdma_it;
            int eventID;
            rdma_id_it = _map_rdma_id.find(tmp_rdma_transfer);
            if(rdma_id_it == _map_rdma_id.end()){
                ERROR << "VerbsRURDMASender::run tmp_rdma_transfer " << tmp_rdma_transfer << " is unassigned" << std::endl;
            }else{
                eventID = rdma_id_it->second;
                id_no_rdma_it = _map_id_no_rmda.find(eventID);
                if(id_no_rdma_it == _map_id_no_rmda.end()){
                    ERROR << "VerbsRURDMASender::run Event ID not found" << std::endl;
                }else{
                    if(id_no_rdma_it->second > non_sent_number){
                        id_no_rdma_it->second = id_no_rdma_it->second - 1;
                        DEBUG << "VerbsRURDMASender::run event " << eventID << " " << id_no_rdma_it->second
                              << " fragments missing" << std::endl;
                    }else{
                        Message *tmp_msg;
                        connection *conn;

                        DEBUG << "VerbsRURDMASender::run event " << eventID << "sent" << std::endl;

                        conn = _ctx->connections_id[rdma_id_it->first->sender_id()];
                        while(_completed_events_id->write(eventID) == -1) usleep(1);

                        while(_completed_send_msg->read(tmp_msg) == -1) usleep(1);

                        while(tmp_msg->protect_mr()){
                            WARNING << "VerbsRURDMASender::run message not ready" << std::endl;
                            usleep(1);
                        }

                        //tmp_msg->setSize();
                        memcpy(tmp_msg->payload(), &eventID, sizeof(eventID));
                        tmp_msg->setType(MSG_END);
                        while(!tmp_msg->send_message(conn)) usleep(1);
                        DEBUG << "VerbsRURDMASender::run MSG_END sent" << std::endl;
                        _map_id_no_rmda.erase(id_no_rdma_it);
                        // TODO house keeping add shemem support on MPIReadoutUnit and poller
                        // implement shmem for rdma queue in RU
                    }
                    _map_rdma_id.erase(rdma_id_it);
                }
            }
            tmp_rdma_transfer->release_payload();
            tmp_rdma_transfer->release_mr();
            while(_free_rdma_shmem->write(tmp_rdma_transfer) == -1) usleep(1);
        //}
    }
}
