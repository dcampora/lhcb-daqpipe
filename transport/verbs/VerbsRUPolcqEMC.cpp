/**
 *      Verbs RUPolcqEMC - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsRUPolcqEMC.h"

context *VerbsRUPolCqEMC::ctx() const
{
    return _ctx;
}

void VerbsRUPolCqEMC::setCtx(context *ctx)
{
    _ctx = ctx;
}

NamedPipe<int*> *VerbsRUPolCqEMC::event_assign_shmem() const
{
    return _event_assign_shmem;
}

void VerbsRUPolCqEMC::setEvent_assign_shmem(NamedPipe<int*> *event_assign_shmem)
{
    _event_assign_shmem = event_assign_shmem;
}
VerbsRUPolCqEMC::VerbsRUPolCqEMC(){
}

VerbsRUPolCqEMC::VerbsRUPolCqEMC(context *ctx,
                                 NamedPipe<int *> *event_assign_shmem){
    _ctx = ctx;
    _event_assign_shmem = event_assign_shmem;
}

void VerbsRUPolCqEMC::run(){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    connection *conn;
    Message *msg;
    static int ev_conter = 0;
    const int ack_number = 13;



    print_timer = false;
    while(true){
        ttotal.start();
        twait.start();
        check_nz(ibv_get_cq_event(_ctx->comp_channel, &cq, &ctx_ret), "VerbsRUPolCqEMC::run ibv_get_cq_event", true);
        twait.stop();
        while (ibv_poll_cq(cq, 1, &wc)){
            tother.start();
            ev_conter++;
            msg = (Message *) wc.wr_id;
            conn = _ctx->connections_qp_num[wc.qp_num];
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "VerbsRUPolCqEMC::run status is " << ibv_wc_status_str(wc.status) << std::endl;
            }else if(wc.opcode == IBV_WC_RECV){
                if(msg->type() == MSG_EVENT_ASSIGN){
                    int *tmp_payload;
                    tmp_payload = (int*)malloc(3*sizeof(int));
                    DEBUG <<  Verbs::Configuration->name << "VerbsRUPolCqEMC::run message received" << std::endl;
                    DEBUG << "VerbsRUPolCqEMC::run message sent" << std::endl;
                    memcpy(tmp_payload, msg->payload(), 3*sizeof(int));
                    msg->release_mr();
                    msg->release_payload();
                    while(!msg->receive_message(conn)) usleep(1);
                    while(_event_assign_shmem->write(tmp_payload) == -1) usleep(1);
                }else{
                    WARNING << "VerbsRUPolCqEMC::run unknown message received" << std::endl;
                }
            }else{
                WARNING << "VerbsRUPolCqEMC::run unexpected event dropping" << std::endl;
            }
            tother.stop();
        }
        if(ev_conter >= ack_number){
            ibv_ack_cq_events(cq, ev_conter);
            ev_conter = 0;
        }
        check_nz(ibv_req_notify_cq(cq, 0), "VerbsRUPolCqEMC::run ibv_req_notify_cq", true);

        ttotal.stop();
//        if (Core::Configuration->logActivated[Logger::_PROFILE] && print_timer){
//            // Timer ttotal, twait, torder, tmeta, tfragment, tsetup;
//            PROFILE << "VerbsBUPolCqRU::run " << Core::Configuration->name << " timers: " << std::endl
//                << std::setw(15) << "ttotal " << ttotal.get() << std::endl
//                << std::setw(15) << "tcompletion " << tcompletion.get() << " (" << 100.f * tcompletion.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "twait " << twait.get() << " (" << 100.f * twait.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "tcopy " << tcopy.get() << " (" << 100.f * tcopy.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "tother " << tother.get() << " (" << 100.f * tother.get() / ttotal.get() << "\%)" << std::endl
//                << std::endl;
//            print_timer = false;
//        }
        //getchar();
    }
}
