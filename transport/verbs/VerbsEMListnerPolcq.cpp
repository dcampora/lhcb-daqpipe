/**
 *      Verbs EMListnerPolcq - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsEMListnerPolcq.h"

VerbsEMListnerPolCq::VerbsEMListnerPolCq(context *ctx){
    _ctx = ctx;
}
VerbsEMListnerPolCq::VerbsEMListnerPolCq(context *ctx, NamedPipe<Message *> *no_credit_shmem){
    _ctx = ctx;
    _no_credit_shmem = no_credit_shmem;
}
VerbsEMListnerPolCq::VerbsEMListnerPolCq(NamedPipe <Message *> *no_credit_shmem){
    _no_credit_shmem = no_credit_shmem;
}

inline void VerbsEMListnerPolCq::set_context(context *ctx){
    _ctx = ctx;
}

 inline void VerbsEMListnerPolCq::set_no_credit_shmem(NamedPipe <Message *> *no_credit_shmem){
    _no_credit_shmem = no_credit_shmem;
 }

inline context *VerbsEMListnerPolCq::get_context(){
    return _ctx;
}

inline NamedPipe <Message *> *VerbsEMListnerPolCq::get_no_credit_shmem(){
    return _no_credit_shmem;
}

void VerbsEMListnerPolCq::run(){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    connection *conn;
    Message *msg;
    int *payload;
    static int ev_conter = 0;
    const int ack_number = 18;


    while(true){
        DEBUG << "VerbsEMListnerPolCq::run polling loop iteration" << std::endl;
        check_nz(ibv_get_cq_event(_ctx->comp_channel, &cq, &ctx_ret), "verbsEMListnerPolCq::run ibv_get_cq_event", true);
        DEBUG << "VerbsEMListnerPolCq::run cq got" << std::endl;
        while (ibv_poll_cq(cq, 1, &wc)){
            ev_conter++;
            DEBUG << "VerbsEMListnerPolCq::run poll qc loop" << std::endl;
            DEBUG << "VerbsEMListnerPolCq::run MSG received form qp " << wc.qp_num << std::endl;
            msg = (Message *) wc.wr_id;
            conn = _ctx->connections_qp_num[wc.qp_num];
            DEBUG << "VerbsEMListnerPolCq::run conn qp num " << conn->qp->qp_num << std::endl;
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "VerbsEMListnerPolCq::run status is " << ibv_wc_status_str(wc.status) << std::endl;
                while(_no_credit_shmem->write(NULL)) usleep(1);
                msg->release_mr();
                msg->release_payload();
                while(!msg->receive_message(conn)){
                    usleep(1);
                    WARNING << "VerbsEMListnerPolCq::run post receive failed" << std::endl;
                }

            }else if(wc.opcode & IBV_WC_RECV){
                payload = (int *) msg->payload();
                DEBUG << "VerbsEMListnerPolCq::run message type " << msg->type() << std::endl;
                if(msg->type() == MSG_CREDIT_ANNOUNCE){
                    //TODO change type of num_credit_id in int*
                    while(_no_credit_shmem->write(msg)) usleep(1);
                }
//                msg->release_mr();
//                msg->release_payload();
//                if(msg->receive_message(conn)) {
//                    DEBUG << "post rcv ok" << std::endl;
//                }
            } else{
                WARNING << "VerbsEMListnerPolCq::run Unexpected event dropping" << std::endl;
            }
            ibv_ack_cq_events(cq, 1);
            check_nz(ibv_req_notify_cq(cq, 0), "VerbsEMListnerPolCq::run ibv_req_notify_cq", true);
        }

        if(ev_conter >= ack_number){
            ibv_ack_cq_events(cq, ev_conter);
            ev_conter = 0;
        }
        DEBUG << "VerbsEMListnerPolCq::run poll qc loop end" << std::endl;
        //getchar();
    }
}
