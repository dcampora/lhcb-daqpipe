/**
 *      Verbs RU
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSREADOUTUNIT
#define VERBSREADOUTUNIT 1

#include <list>
#include "../common/RUTransportCore.h"
#include "VerbsCore.h"
#include "Verbs_lib.h"
#include "VerbsRUPollcqBU.h"
#include "VerbsRUPolcqEMC.h"
#include "../../common/pthreadHelper.h"
#include "../../common/Shmem.h"
#include "../../common/NamedPipe.h"
#include "../../common/NamedPipe.h"
#include "rdmatransfer.h"
#include "VerbsRURDMASender.h"
#include "../../common/Timer.h"

namespace Verbs {

class ReadoutUnit : public RUTransportCore {
private:
    std::vector<int> requestIndex;
    std::vector<int> activeRequests;
    std::list<int> activeRequestsList;
    int* size_id_bu;

    // verbs connection
    context *em_listner_ctx;
    context *em_consumer_ctx;
    context *bu_ctx;


    NamedPipe<int*> *_event_assign_shmem;
    NamedPipe<Message *> *_memory_region_shmem;
    NamedPipe<Message *> *_pull_request_shmem;
    NamedPipe<Message *> *_completed_send_msg;
    NamedPipe<RDMATransfer *> *_completed_rdma_shmem;
    NamedPipe<RDMATransfer *> *_free_rdma_shmem;
    NamedPipe<int> *_completed_events_id;
    NamedPipe<std::pair<int, int> *> *_ID_num_frag_shmem;
    NamedPipe<std::pair<RDMATransfer *, int> *> *_rdma_transfer_ID_shmem;

    class VerbsRUPollCqBU *BUpoller;
    class VerbsRURDMASender *RDMASender;
    class VerbsRUPolCqEMC *EMCPoller;

    void *recv_messages_buff;
    void *send_messages_buff;

    std::vector <Message *> send_mess;
    std::vector <Message *> recv_mess;
    std::vector <Message *> emc_receive_mess;
    std::vector <RDMATransfer *> rdma_sends;

    int *emc_data_buffer;

    std::vector <ibv_mr*> rdma_send_mr;

    std::queue <Message *> free_send_msg;

    std::queue <RDMATransfer *> free_rdma_sends;
    std::map <int, int> map_id_no_rmda;
    std::map <RDMATransfer *, int> map_rdma_id;
    std::vector <int> completed_send_event;

    Timer tperiod, ttotal, tread_receive, tread_free, twrite_rdma_id, twrite_rdma, t_stuck;

public:
    ReadoutUnit() : RUTransportCore() {}
    ~ReadoutUnit(){
        free(recv_messages_buff);
    }

    virtual void initialize(int initialCredits, std::map<std::string, void *> &pmems);
    virtual bool EventListen(int& size, int& eventID, int& destinationBU);
    virtual void pullRequestReceive(int& eventID, int& size, int& destinationBU);
    virtual void prepareSendEvent(FragmentComposition* &fragmentCompositionPointer, int destinationBU);
    virtual void setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU);
    virtual bool sendAnyEvent(int& eventID);
    virtual bool sendEvent(int eventID);
};

}
#endif
