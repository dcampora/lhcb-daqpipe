/**
 *      Verbs BUPolcqRU - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsBUPolcqRU.h"

NamedPipe<int> *VerbsBUPolCqRU::completed_event_shmem() const
{
    return _completed_event_shmem;
}

void VerbsBUPolCqRU::setComplete_event_shmem(NamedPipe<int> *complete_event_shmem)
{
    _completed_event_shmem = complete_event_shmem;
}

VerbsBUPolCqRU::VerbsBUPolCqRU(){
}

VerbsBUPolCqRU::VerbsBUPolCqRU(context *ctx,
                               NamedPipe<Message *> *fragment_composition_shmem,
                               NamedPipe<Message *> *free_send_msg_shmem,
                               NamedPipe<int> *completed_event_shmem,
                               NamedPipe<int> *event_received_shmem){
    _ctx = ctx;
    _fragment_composition_shmem = fragment_composition_shmem;
    _free_send_msg_shmem = free_send_msg_shmem;
    _completed_event_shmem = completed_event_shmem;
    _event_received_shmem = event_received_shmem;
}

NamedPipe<Message *> *VerbsBUPolCqRU::fragment_composition_shmem() const
{
    return _fragment_composition_shmem;
}

void VerbsBUPolCqRU::setFragment_composition_shmem(NamedPipe<Message *> *fragment_composition_shmem)
{
    _fragment_composition_shmem = fragment_composition_shmem;
}

context *VerbsBUPolCqRU::ctx() const
{
    return _ctx;
}

void VerbsBUPolCqRU::setCtx(context *ctx)
{
    _ctx = ctx;
}

void VerbsBUPolCqRU::run(){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    connection *conn;
    Message *msg;
    static int ev_conter = 0;
    const int ack_number = 27;
    std::map<int, int>::iterator map_it;

    fragments = (Message **) malloc(Verbs::Configuration->initialCredits*Verbs::Configuration->numnodes*sizeof(Message *));

    check_alloc(fragments, "VerbsBUPolCqRU::run fragments malloc", true);


    int fragment_id = -1;
    frag_recv = 0;
    for(int i=0 ; i<Verbs::Configuration->initialCredits ; i++){
        avail_cretits_queue.push(i);
    }

    next_credit = 0;

    print_timer = false;
    while(true){
        ttotal.start();
        DEBUG << "VerbsBUPolCqRU::run polling loop iteration" << std::endl;
        twait.start();
        check_nz(ibv_get_cq_event(_ctx->comp_channel, &cq, &ctx_ret), "VerbsBUPolCqRU::run ibv_get_cq_event", true);
        twait.stop();
        while (ibv_poll_cq(cq, 1, &wc)){
            tother.start();
            ev_conter++;
            msg = (Message *) wc.wr_id;
            conn = _ctx->connections_qp_num[wc.qp_num];
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "VerbsBUPolCqRU::run status is " << ibv_wc_status_str(wc.status) << std::endl;
            }else if(wc.opcode & IBV_WC_RECV){
                DEBUG << "VerbsBUPolCqRU::run MSG received from " << msg->sender_id() << std::endl;
                if(msg->type() == MSG_FRAGMENT_COMPOSITION){
                    DEBUG << "VerbsBUPolCqRU::run fragment composition received from " << msg->sender_id() << std::endl;
                    while(_fragment_composition_shmem->write(msg) == -1){
                        usleep(1);
                    }
                } else if (msg->type() == MSG_END){
                    int eventID;

                    eventID = *(int*)msg->payload();
                    DEBUG << "VerbsBUPolCqRU::run MSG_END received" << std::endl;
                    DEBUG << "VerbsBUPolCqRU::run event received from RU " << msg->sender_id() << std::endl;
                    map_it = map_id_fragment_rec.find(eventID);
                    if(map_it != map_id_fragment_rec.end()){
                        (map_it->second)++;
                    }else{
                        std::pair<std::map<int, int>::iterator,bool> tmp_pair;
//                        map_id_fragment_rec[eventID] = 1;
                        tmp_pair = map_id_fragment_rec.insert(std::pair<int, int>(eventID, 1));
                        map_it = tmp_pair.first;
                    }
                    //if((map_it->second == Verbs::Configuration->numnodes) || ((map_it->second == Verbs::Configuration->numnodes - 1) &&
                    //                                                     (Core::Configuration->bypassShmemTransport || Core::Configuration->shmemTransportEnabled))){
                    if((map_it->second == Verbs::Configuration->numnodes)){
                        while(_event_received_shmem->write(eventID) == -1) usleep(1);
                        DEBUG << "VerbsBUPolCqRU::run event received" << std::endl;
                        map_id_fragment_rec.erase(map_it);
                    }
                    msg->release_mr();
                    msg->release_payload();
                    while(!msg->receive_message(conn)) usleep(1);
                }

            }else if(wc.opcode == IBV_WC_SEND){
                DEBUG << "VerbsBUPolCqRU::run message sent" << std::endl;
                msg->release_mr();
                msg->release_payload();
                while(_free_send_msg_shmem->write(msg) == -1) usleep(1);
            }else{
                WARNING << "VerbsBUPolCqRU::run Unexpected event dropping" << std::endl;
            }
            tother.stop();
        }
        if(ev_conter >= ack_number){
            ibv_ack_cq_events(cq, ev_conter);
            ev_conter = 0;
        }
        check_nz(ibv_req_notify_cq(cq, 0), "VerbsBUPolCqRU::run ibv_req_notify_cq", true);

        ttotal.stop();
//        if (Core::Configuration->logActivated[Logger::_PROFILE] && print_timer){
//            // Timer ttotal, twait, torder, tmeta, tfragment, tsetup;
//            PROFILE << "VerbsBUPolCqRU::run " << Core::Configuration->name << " timers: " << std::endl
//                << std::setw(15) << "ttotal " << ttotal.get() << std::endl
//                << std::setw(15) << "tcompletion " << tcompletion.get() << " (" << 100.f * tcompletion.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "twait " << twait.get() << " (" << 100.f * twait.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "tcopy " << tcopy.get() << " (" << 100.f * tcopy.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "tother " << tother.get() << " (" << 100.f * tother.get() / ttotal.get() << "\%)" << std::endl
//                << std::endl;
//            print_timer = false;
//        }
        //getchar();
    }
}
