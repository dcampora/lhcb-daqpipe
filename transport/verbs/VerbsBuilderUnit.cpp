
/**
 *      Verbs BU - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsBuilderUnit.h"

Verbs::BuilderUnit::~BuilderUnit(){
    free(size_id);
}

void Verbs::BuilderUnit::initialize(int initialCredits, void *sh_meta_pmem, void *sh_data_pmem){
    connection *conn;
    void *buff;
    initialCredits = initialCredits;

    t_print.start();
    number_of_prepare = 0;

    int num_rcv_mess_per_node;
    int num_snd_mess_per_node;

    switch (Verbs::Configuration->protocol) {
    case Core::PUSH:
        DEBUG << "Verbs::VerbsBuilderUnit::initialize PUSH protocol selected" << std::endl;
        num_rcv_mess_per_node =  2*Verbs::Configuration->initialCredits;
        num_snd_mess_per_node = (MAX_RU_FRAGMENTS)*Verbs::Configuration->initialCredits;
        break;
    case Core::PULL:
        DEBUG << "Verbs::VerbsBuilderUnit::initialize PULL protocol selected" << std::endl;
        num_rcv_mess_per_node =  2*Verbs::Configuration->initialCredits;
        num_snd_mess_per_node = (MAX_RU_FRAGMENTS + Verbs::Configuration->numReadoutsPerNode)
                *Verbs::Configuration->initialCredits;
        break;
    default:
        ERROR << "Verbs::VerbsBuilderUnit::initialize unknown protocol requested aborting" << std::endl;
        exit(EXIT_FAILURE);
        break;
    }

    const size_t max_recv_message_size = sizeof(FragmentComposition);
    const size_t max_send_message_size = sizeof(ibv_mr)>(3*sizeof(int)) ? sizeof(ibv_mr) : 3*sizeof(int);
    const size_t max_event_assing_size = 2*sizeof(int);

    //assuming 1 bu per node
    const int no_rcv_mess = num_rcv_mess_per_node * (Verbs::Configuration->numnodes ) ;
    const int no_snd_mess = num_snd_mess_per_node * (Verbs::Configuration->numnodes );
    const int num_event_assing = Verbs::Configuration->initialCredits;


    // activeRequest queues
    DEBUG << "Verbs::VerbsBuilderUnit: Reserved fragments, meta and data for " << Verbs::Configuration->numnodes << " nodes;"
          << " data buffer: " << Core::Configuration->BUDataBufferSize << ", meta buffer: " << Core::Configuration->BUMetaBufferSize << " (B)" << std::endl;

    // Reserve size and id for sync recvs
//    size_id = (int*) malloc(2 * sizeof(int));
    tstart = time(NULL);


    em_listner_ctx = build_context_client(Verbs::Configuration->EMLMapIDPort,
                                           Verbs::Configuration->EMLMapIPID);
    DEBUG << "em_listner_ctx->pd " << em_listner_ctx->pd << std::endl;

    if(Verbs::Configuration->protocol == Core::PULL){
        em_consumer_ctx = build_context_client(Verbs::Configuration->EMCMapIDPort,
                                               Verbs::Configuration->EMCMapIPID);

        event_assing.resize(num_event_assing);

        event_assing_buff = malloc(num_event_assing*max_event_assing_size);
        check_alloc(event_assing_buff, "VerbsEMListener::setupListen event_assing_buff malloc", true);


        for(int k = 0 ;  k < event_assing.size() ; k++){
            Message *tmp_msg;

            tmp_msg = new Message;

            if(tmp_msg->setPayload((char*)event_assing_buff + k*max_event_assing_size)){
                DEBUG << "setPayload ok" << std::endl;
            }

            tmp_msg->setSize(max_event_assing_size);

            while(!tmp_msg->receive_message(
                      em_consumer_ctx->connections_id[Verbs::Configuration->emconsumerID])) usleep(1);

            event_assing[k] = tmp_msg;
        }

        std::string tmp_string = "shmem-Verbs::VerbsBuilderUnit::requested_events_shmem";

        _requested_events_shmem = new NamedPipe <Message*> (tmp_string, num_event_assing);

        EMCPoller = new VerbsBUPolqcEMC(em_consumer_ctx, _requested_events_shmem);
        startThreadWithReference<VerbsBUPolqcEMC>(*EMCPoller);
    }

    ru_ctx = build_context_server(Verbs::Configuration->numTotalReadouts, Verbs::Configuration->BUMapIDPort[Verbs::Configuration->nodeID()],
             Verbs::Configuration->RUMapIPID);

    recv_mess.resize(no_rcv_mess);
    send_mess.resize(no_snd_mess);
    //rdma_mr.resize(no_snd_mess, NULL);

    rdma_mr_chache.resize(Verbs::Configuration->numnodes);

    recv_messages_buff = malloc(no_rcv_mess*max_recv_message_size);
    check_alloc(recv_messages_buff, "VerbsEMListener::setupListen recv_messages_buff malloc", true);

    send_messages_buff = malloc(no_snd_mess*max_send_message_size);
    check_alloc(send_messages_buff, "VerbsEMListener::setupListen send_messages_buff malloc", true);

    std::string tmp_string = "shmem-Verbs::VerbsBuilderUnit::fragment_shmem";
    _fragment_composition_shmem = new NamedPipe <Message*> (tmp_string, Verbs::Configuration->numnodes);

    tmp_string = "shmem-Verbs::VerbsBuilderUnit::free_send_msg_shmem";
    _free_send_msg_shmem = new NamedPipe <Message *> (tmp_string, send_mess.size());

    tmp_string = "shmem-Verbs::VerbsBuilderUnit::complete_event_shmem";
    _complete_event_shmem = new NamedPipe <int> (tmp_string, Verbs::Configuration->initialCredits);

    tmp_string = "shmem-Verbs::VerbsBuilderUnit::event_received_shmem";
    _event_received_shmem = new NamedPipe <int> (tmp_string, Verbs::Configuration->initialCredits);

    int index = 0;
    for(std::map <int, struct connection *>::iterator it = ru_ctx->connections_id.begin() ; it != ru_ctx->connections_id.end() ; it++){
        connection *conn;
        conn = it->second;
        for(int i=0 ; i < num_rcv_mess_per_node ; i++){
            Message *tmp_msg;
            tmp_msg = new Message;
            if(tmp_msg->setPayload((char*)recv_messages_buff + index*max_recv_message_size)){
                DEBUG << "setPayload ok" << std::endl;
            }
            tmp_msg->setSize(max_recv_message_size);
            while(!(tmp_msg->receive_message(conn))) usleep(1);
            recv_mess[index] = tmp_msg;
            index++;
        }
        //        conn->recv_msg->receive_message(conn);
    }

    index = 0;
    for(std::vector<Message *>::iterator it = send_mess.begin() ; it != send_mess.end() ; it++){
        *it = new Message((char*)send_messages_buff + index*max_send_message_size, max_send_message_size);
        while(_free_send_msg_shmem->write(*it) == -1) usleep(1);
        index++;
    }


    RUPoller = new VerbsBUPolCqRU (ru_ctx, _fragment_composition_shmem, _free_send_msg_shmem,
                                   _complete_event_shmem, _event_received_shmem);


    startThreadWithReference<VerbsBUPolCqRU>(*RUPoller);

    for(std::map <int, struct connection *>::iterator it = ru_ctx->connections_id.begin() ; it != ru_ctx->connections_id.end() ; it++ ){
        DEBUG << "connection pd: " << (void*) it->second->pd << std::endl;
    }
    DEBUG << "context pd:  " << (void*) ru_ctx->pd << std::endl;

    ibv_mr *tmp_mr;
    tmp_mr = ibv_reg_mr(ru_ctx->pd, sh_meta_pmem,Core::Configuration->RUMetaBufferSize,
            IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(tmp_mr, "Verbs::VerbsBuilderUnit::initialize sh_meta_pmem rdma_mr ibv_reg_mr", true);
    rdma_mr.push_back(tmp_mr);
    tmp_mr = ibv_reg_mr(ru_ctx->pd, sh_data_pmem,Core::Configuration->RUDataBufferSize,
            IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(tmp_mr, "Verbs::VerbsBuilderUnit::initialize sh_data_pmem rdma_mr ibv_reg_mr", true);
    rdma_mr.push_back(tmp_mr);

    DEBUG << "ru_ctx->pd " << ru_ctx->pd << std::endl;
    DEBUG << "em_listner_ctx->pd " << em_listner_ctx->pd << std::endl;

    conn = em_listner_ctx->connections_id[Verbs::Configuration->emlistenerID];
    //buff = malloc(sizeof(int));
    conn->send_msg->setSize(sizeof(int));
    //conn->send_msg->setPayload(buff);
    conn->send_msg->setType(MSG_CREDIT_ANNOUNCE);

    EMLPoller = new VerbsBUPolCqcEML(em_listner_ctx);
    startThreadWithReference<VerbsBUPolCqcEML>(*EMLPoller);

    //em_listner_ctx = create_context(VerbsCore::VerbsConfig->mapIDIP[VerbsCore::VerbsConfig->emlistenerID],
            //VerbsCore::VerbsConfig->mapIDPort[VerbsCore::VerbsConfig->emlistenerID]);

   DEBUG << "bu inizialized finished" << std::endl;

}

void Verbs::BuilderUnit::sendCredits(int& numberOfCredits){
    INFO << "size of mr " << sizeof(int) << std::endl;
    DEBUG << "Verbs::VerbsBuilderUnit: sendCredits " << numberOfCredits << " credits, s " << Verbs::Configuration->rankID << ", r " <<
        Verbs::EVENT_MANAGER_LISTENER << ", id " << Verbs::CREDIT_REQUEST << std::endl;
    
    connection *conn;


    struct ibv_cq *cq;
    struct ibv_wc wc;
    conn =  em_listner_ctx->connections_id[Verbs::Configuration->emlistenerID];


    //check if the message is free
    while(conn->send_msg->protect_mr()) usleep(1);

    conn->send_msg->setPayload(&numberOfCredits);

    DEBUG << "Verbs::VerbsBuilderUnit::sendCredits adding " << numberOfCredits << "credits" << std::endl;
    while(!conn->send_msg->send_message(conn)) usleep(1);

}

void Verbs::BuilderUnit::pullRequest(int* size_id_bu, int& readoutno){
    Message *tmp_msg;
    DEBUG << "Verbs::VerbsBuilderUnit: pullRequest s " << Verbs::Configuration->rankID << ", r " <<
             Verbs::Configuration->readoutID(readoutno) << std::endl;

    while(_free_send_msg_shmem->read(tmp_msg) == -1) usleep(1);

    tmp_msg->setType(MSG_PULL_REQUEST);
//    tmp_msg->setSize(3*sizeof(int));
    memcpy(tmp_msg->payload(), size_id_bu, 3*sizeof(int));

    tmp_msg->send_message(ru_ctx->connections_id[Verbs::Configuration->readoutID(readoutno)]);
}

void Verbs::BuilderUnit::receiveSizeAndID(int& eventSize, int& eventID){
    Message *tmp_msg;
    int *payload;
    DEBUG << "Verbs::VerbsBuilderUnit: receiveSizeAndID, s " << Verbs::Configuration->rankID << ", r " <<
             Verbs::EMCONSUMER_ID << ", id " << Verbs::EVENT_ASSIGN << std::endl;

    while (_requested_events_shmem->read(tmp_msg) == -1) usleep(1);

    payload = (int*) tmp_msg->payload();

    eventSize = payload[0];
    eventID = payload[1];

    tmp_msg->release_mr();
    tmp_msg->release_payload();

    while(!tmp_msg->receive_message(em_consumer_ctx->connections_id[tmp_msg->sender_id()])) usleep(1);

    DEBUG << "Verbs::VerbsBuilderUnit: Received size and id from Event Manager" << std::endl;
}

/**
 * Prepares the receiveEvent. In OpenMPI, this means having to find a slot for it.
 * @param fc
 * @param eventID
 */

// TODO: Refactor, FragmentComposition pointer (fc) is not used at the moment.
void Verbs::BuilderUnit::prepareReceiveEvent(FragmentComposition* fc){
    // Find a slot
    int slot;
    int eventID;

//    receiving_fc[fc->eventID] = fc;
    receiving_fc = fc;
    DEBUG << "Verbs::VerbsBuilderUnit::prepareReceiveEvent saving fc:" << (void*) fc << std::endl;

}

bool Verbs::BuilderUnit::prepareReceiveEventComplete(int& fragmentReceived){
    bool ret_val;
    Message *tmp_msg;

    int slot;
    int eventID;
    int sender_id;

    if(_fragment_composition_shmem->read(tmp_msg) == 0){
        fragmentReceived = Verbs::Configuration->readoutNumber(tmp_msg->sender_id());
        sender_id = tmp_msg->sender_id();
        DEBUG << "Verbs::VerbsBuilderUnit::prepareReceiveEventComplete received fc from r" << sender_id
              << " fragment " << fragmentReceived << " of event "
              << ((FragmentComposition*)tmp_msg->payload())->eventID << std::endl;
        memcpy(receiving_fc + fragmentReceived, tmp_msg->payload(), sizeof(FragmentComposition));
        tmp_msg->release_mr();
        tmp_msg->release_payload();
        while(!tmp_msg->receive_message(ru_ctx->connections_id[sender_id])) usleep(1);
        ret_val = true;
    }else{
        ret_val = false;
    }

    return ret_val;
}

/**
 * Sets up the Irecvs for this eventID.
 * @param memory
 * @param size
 * @param eventID
 */

// TODO: Refactor at some point (dataPointer and size are useless right now)
void Verbs::BuilderUnit::setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype){
    t_total.start();
    number_of_prepare++;
    static std::map<char *, int> data_pointer_counter;
    std::vector<ibv_mr *>::iterator it;
    ibv_mr *tmp_mr;
    char * tmp_dataPointer;
    bool found = false;

    tmp_dataPointer = dataPointer;

    t_mr_search.start();
    for(it = rdma_mr.begin() ; it != rdma_mr.end() ; it++){
        if(!((tmp_dataPointer < (*it)->addr) || (tmp_dataPointer + size) > ((char*) (*it)->addr +  (*it)->length))){
            DEBUG << " Verbs::VerbsBuilderUnit::setupReceiveEvent mr found in static_mr list" << std::endl;
            found = true;
            break;
        }
    }

    if(!found){
        ERROR << "Verbs::VerbsBuilderUnit::setupReceiveEvent unknown data_pointer aborting" << std::endl;
        exit(EXIT_FAILURE);
    }
    t_mr_search.stop();

    t_free_msg.start();
    Message *snd_mess;
    while(_free_send_msg_shmem->read(snd_mess) == -1) {
        DEBUG << "_free_send_msg_shmem->read(snd_mess) == -1" << std::endl ;
        usleep(1);
    }
    t_free_msg.stop();

    t_msg_prepare.start();
    snd_mess->setType(MSG_MR);
    DEBUG << "Verbs::VerbsBuilderUnit::setupReceiveEvent sending mr to " << Verbs::Configuration->readoutID(readoutIndex) << std::endl ;
    memcpy(snd_mess->payload(), *it, sizeof(ibv_mr));
    tmp_mr = (ibv_mr*)snd_mess->payload();
    tmp_mr->addr = dataPointer;
    tmp_mr->length = size;
//    snd_mess->setSize(sizeof(ibv_mr));

    if(!snd_mess->send_message(ru_ctx->connections_id[Verbs::Configuration->readoutID(readoutIndex)])){
        ERROR << "Verbs::VerbsBuilderUnit::setupReceiveEvent Verbs::VerbsBuilderUnit::setupReceiveEvent[" << readoutIndex << "] failed" << std::endl;
    }

    t_msg_prepare.stop();

    t_map.start();
    std::map<int, int>::iterator map_it;
    map_it = eventID_size_map.find(eventID);
    if(map_it != eventID_size_map.end()){
        if (datatype == Core::DATA){
            map_it->second += size;
        }
    }else{
        DEBUG << "event id " << eventID << " assigned" << std::endl;
        eventID_size_map[eventID] = (datatype == Core::DATA) ? size : 0;
    }
    t_map.stop();

    t_total.stop();

    if(t_print.getElapsedTime() > 5){
        if (Core::Configuration->logActivated[Logger::_PROFILE]){
            PROFILE << "Verbs::VerbsBuilderUnit::setupReceiveEvent " << Core::Configuration->name << " timers: " << std::endl
                    << std::setw(15) << "ttotal " << t_total.get() << std::endl
                    << std::setw(15) << "t_mr_search " << t_mr_search.get() << " (" << 100.f * t_mr_search.get() / t_total.get() << "\%)" << std::endl
                    << std::setw(15) << "t_free_msg " << t_free_msg.get() << " (" << 100.f * t_free_msg.get() / t_total.get() << "\%)" << std::endl
                    << std::setw(15) << "t_msg_prepare " << t_msg_prepare.get() << " (" << 100.f * t_msg_prepare.get() / t_total.get() << "\%)" << std::endl
                    << std::setw(15) << "t_map " << t_map.get() << " (" << 100.f * t_map.get() / t_total.get() << "\%)" << std::endl
                    << "number fo prepares " << number_of_prepare << " time per prepare " << ((double) number_of_prepare)/t_total.get() << std::endl
                    << std::endl;


        }
        t_print.flush();
        t_print.start();
    }

}

bool Verbs::BuilderUnit::checkExitMessage(void)
{
    return false;
}

/**
 * In MPI, this means "Testall" on all open comms.
 * @param  tSize returns the transmissionSize of the finished event.
 * @return                  true if finished event, false otherwise.
 */
// read from shamem_queue for event ack
bool Verbs::BuilderUnit::receiveEvent(int& finishedEventID, int& tSize){
    std::list<int>::iterator it;
    bool eventReceived = false;
    int rec_evID;

    if(_event_received_shmem->read(rec_evID) != -1){
        eventReceived = true;
    }

    if (eventReceived){
        DEBUG << "Verbs::VerbsBuilderUnit: Event " << rec_evID << " received!" << std::endl;

        //finishedEventID = receiving_fc->eventID;
        finishedEventID = rec_evID;
        std::map<int, int>::iterator it;
        it = eventID_size_map.find(rec_evID);
        if(it != eventID_size_map.end()){
            tSize = it->second;
            eventID_size_map.erase(it);
            DEBUG << "Verbs::VerbsBuilderUnit: Event " << rec_evID << " found!" << std::endl;
        }else{
            ERROR << "Verbs::VerbsBuilderUnit::receiveEvent eventID: " << rec_evID << " unknown" <<std::endl;
            tSize = 0;
        }
    }

    return eventReceived;
}
