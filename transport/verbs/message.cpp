/**
 *      Message - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "message.h"

int Message::type() const
{
    return _type;
}

void Message::setType(int type)
{
    _type = type;
}

Message::~Message(){
    if(_type_mr != NULL){
        ibv_dereg_mr(_type_mr);
    }
}

bool Message::setPayload(void *payload) {
    bool ret_val;
    pthread_mutex_lock(_mr_mutex);
    if(!_protect_payload){
        _protect_payload = true;
        _payload = payload;
        _null_payload = _payload == NULL;
        if(!_null_payload){
            if(_local_mr != NULL){
                if(_payload != _local_mr->addr){
                    DEBUG << "Message::setPayload payload change unregistering mr" << std::endl;
                    if(_local_mr != NULL){
                        ibv_dereg_mr(_local_mr);
                    }
                    _local_mr = NULL;
                }
            }
        }
        ret_val = true;
    }else{
        ret_val = false;
    }
    pthread_mutex_unlock(_mr_mutex);
    return ret_val;
}

bool Message::send_message(void *payload, int size, struct connection *conn){
    if(!this->setPayload(payload)){
        return false;
    }
    _size = size;

    return this->send_message(conn);
}


void Message::update_mr(struct connection *conn){
    if(!_null_payload){
        if(_local_mr == NULL){
            DEBUG << "Message::update_mr _local_mr NULL creating new mr" << std::endl;
            _local_mr = ibv_reg_mr(conn->pd,_payload,_size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
            check_alloc(_local_mr, "Message::update_mr _local_mr ibv_reg_mr", true);
        }else if(_local_mr->pd != conn->pd){
            DEBUG << "Message::update_mr _local_mr out of date updating" << std::endl;
            ibv_dereg_mr(_local_mr);
            _local_mr = ibv_reg_mr(conn->pd,_payload,_size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
            check_alloc(_local_mr, "Message::update_mr _mr ibv_reg_mr", true);
        }else{
            DEBUG << "Message::update_mr _mr up to date" << std::endl;
        }
    }

    if(_type_mr == NULL){
        DEBUG << "Message::update_mr _type_mr NULL creating new mr" << std::endl;
        _type_mr = ibv_reg_mr(conn->pd, &_type, sizeof(_type), IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
        check_alloc(_type_mr, "Message::update_mr _type_mr ibv_reg_mr", true);
    }else if(_type_mr->pd != conn->pd){
        DEBUG << "Message::update_mr _type_mr out of date updating" << std::endl;
        ibv_dereg_mr(_type_mr);
        _type_mr = ibv_reg_mr(conn->pd, &_type, sizeof(_type), IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
        check_alloc(_type_mr, "Message::update_mr _type_mr ibv_reg_mr", true);
    }else{
        DEBUG << "Message::update_mr _type_mr up to date" << std::endl;
    }

}

bool Message::send_message(struct connection *conn){
    struct ibv_send_wr wr, *bad_wr = NULL;
    struct ibv_sge sge[2];

    bool ret_val;

    pthread_mutex_lock(_mr_mutex);

    if(_protect_mr){
        ret_val = false;
    }else{

        this->update_mr(conn);

        memset(&wr, 0, sizeof(wr));

        wr.wr_id = (uintptr_t)this;
        wr.opcode = IBV_WR_SEND;
        wr.sg_list = sge;
        if(_null_payload){
            wr.num_sge = 1;
        }else{
            wr.num_sge = 2;
        }
        wr.send_flags = IBV_SEND_SIGNALED;

        sge[0].addr = (uintptr_t)&_type;
        sge[0].length = sizeof(_type);
        sge[0].lkey = _type_mr->lkey;

        if(!_null_payload){
            sge[1].addr = (uintptr_t)_payload;
            sge[1].length = _size;
            sge[1].lkey = _local_mr->lkey;
        }

        DEBUG << "Message::send_message posting send" << std::endl;
        check_nz(ibv_post_send(conn->qp, &wr, &bad_wr), "Message::send_message ibv_post_send", true);
        DEBUG << "Message::send_message send posted" << std::endl;

        _protect_mr = true;
    }

    pthread_mutex_unlock(_mr_mutex);
    return true;
}

bool Message::receive_message(void *payload, int size,struct connection *conn){
    if(!this->setPayload(payload)){
        return false;
    }
    _size = size;

    return this->receive_message(conn);
}

bool Message::receive_message(struct connection *conn){
    struct ibv_recv_wr wr, *bad_wr = NULL;
    struct ibv_sge sge[2];
    bool ret_val;

    pthread_mutex_lock(_mr_mutex);
    if(_protect_mr){
        ret_val = false;
    }else{

        this->update_mr(conn);

        wr.wr_id = (uintptr_t)this;
        wr.next = NULL;
        wr.sg_list = sge;
        wr.num_sge = 2;

        sge[0].addr = (uintptr_t)&_type;
        sge[0].length = sizeof(_type);
        sge[0].lkey = _type_mr->lkey;

        sge[1].addr = (uintptr_t)_payload;
        sge[1].length = _size;
        sge[1].lkey = _local_mr->lkey;

        check_nz(ibv_post_recv(conn->qp, &wr, &bad_wr), "Message::receive_message ibv_post_receive", true);

        _protect_mr = true;
        _sender_id = conn->connection_number;

        DEBUG << "Message::receive_message sender_id " << this->sender_id() << std::endl;

        ret_val = true;
    }
    pthread_mutex_unlock(_mr_mutex);

    return ret_val;
}
