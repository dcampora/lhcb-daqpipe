/**
 *      Verbs EMListener
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef EMVERBS
#define EMVERBS 1

#include <vector>
#include <stdlib.h>

#include "../common/EMTransportCoreListener.h"
#include "../../common/Logging.h"
#include "VerbsCore.h"

#include "../../em/EMCoreListener.h"
#include "Verbs_lib.h"
#include "message.h"
#include "VerbsEMListnerPolcq.h"
#include "../../common/pthreadHelper.h"
#include "../../common/NamedPipe.h"

namespace Verbs {

class EMListener : public EMTransportCoreListener
{
private:
    int* numberOfCredits;

    // verbs connection
    context *ru_ctx;
    context *bu_ctx;

    VerbsEMListnerPolCq *poller;
    NamedPipe <Message *> *credit_shmem;
    std::map <void *, Message *> payload_mess;

//    const int num_rcv_mess_per_node = 1;
//    const size_t max_message_size = sizeof(int);
//    //assuming 1 bu per node
//    const int no_rcv_mess = num_rcv_mess_per_node * verbsCore::VerbsConfig->numnodes;

    void *messages_buff;
    std::vector <Message *> recv_mess;




public:
    EMListener(){}
    ~EMListener(){ free(numberOfCredits); free(messages_buff);}

    virtual void setupListen();
    virtual void listenForCreditRequest(int& node, int& numberOfCreditsRequested);
};

}
#endif
