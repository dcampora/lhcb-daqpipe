/**
 *      Message
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include "Verbs_lib.h"
#include "connection.h"
#include <infiniband/verbs.h>
#include <iostream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include "../../common/Logging.h"
#include "../../common/Tools.h"
#include "../../common/GenStructures.h"
#include "cache.h"
#include <pthread.h>
#include "VerbsTransfer.h"

/** @file */

/**
 * @brief The message_type enum
 * Enumeration used for message types
 */
enum message_type{
    MSG_RDMA,
    MSG_CREDIT_ANNOUNCE,
    MSG_FRAGMENT_COMPOSITION,
    MSG_MR,
    MSG_EVENT_ASSIGN,
    MSG_PULL_REQUEST,
    MSG_NUM_PACK,
    MSG_END,
    MSG_ERROR
};

/**
 * @brief The Message class
 * Class used for sending and receiving messages over and established verbs connection.
 * This class takes care of all the memory region registration.
 * This class is thread safe and the payload and the memory region are protected against
 * accidental overwriting caused by retransmission.
 */
class Message : public VerbsTransfer {
private:
//    enum message_type _type;
//    message_payload _payload;

    int _type;
    ibv_mr *_type_mr;

    bool _null_payload;

    void update_mr(struct connection *conn);

public:
    Message(void *payload = NULL, int size = 0) : VerbsTransfer(payload, size), _type_mr(NULL){
        _null_payload = _payload == NULL;
    }

    ~Message();

    /**
     * @brief send_message
     * Send the given message.
     * After sending a message the object is locked and must be unlocked before sending a new message.
     * @param payload payload of the message
     * @param size size of the payload
     * @param conn connection to the destination device
     * @return true on success
     */
    bool send_message(void *payload, int size,struct connection *conn);

    /**
     * @brief send_message
     * Send the given message.
     * After sending a message the object is locked and must be unlocked before sending a new message.
     * @param conn connection to the destination device
     * @return true on success
     */
    bool send_message(struct connection *conn);

    /**
     * @brief receive_message
     * Receive a message from the given connection.
     * After receiving a message the object is locked and must be unlocked before receiving a new message.
     * @param payload payload of the message
     * @param size size of the payload
     * @param conn connection to the source device
     * @return true on success
     */
    bool receive_message(void *payload, int size,struct connection *conn);

    /**
     * @brief receive_message
     * Receive a message from the given connection.
     * After receiving a message the object is locked and must be unlocked before receiving a new message.
     * @param conn connection to the source device
     * @return true on success
     */
    bool receive_message(struct connection *conn);

    /**
     * @brief setPayload
     * @param payload
     * @return true on success
     */
    bool setPayload(void *payload);

    /**
     * @brief type
     * @return message type
     */
    int type() const;

    /**
     * @brief setType
     * @param type
     */
    void setType(int type);
};

#endif // MESSAGE_H
