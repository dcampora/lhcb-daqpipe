/**
 *      Verbs BUPolqcEML - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsBUPolqcEML.h"

context *VerbsBUPolCqcEML::ctx() const
{
    return _ctx;
}

void VerbsBUPolCqcEML::setCtx(context *ctx)
{
    _ctx = ctx;
}
VerbsBUPolCqcEML::VerbsBUPolCqcEML(){
}

VerbsBUPolCqcEML::VerbsBUPolCqcEML(context *ctx){
    _ctx = ctx;
}

void VerbsBUPolCqcEML::run(){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    connection *conn;
    Message *msg;
    static int ev_conter = 0;
    const int ack_number = 13;



    while(true){
        check_nz(ibv_get_cq_event(_ctx->comp_channel, &cq, &ctx_ret), "VerbsBUPolCqcEML::run ibv_get_cq_event", true);
        while (ibv_poll_cq(cq, 1, &wc)){
            ev_conter++;
            msg = (Message *) wc.wr_id;
            conn = _ctx->connections_qp_num[wc.qp_num];
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "VerbsBUPolCqcEML::run status is " << ibv_wc_status_str(wc.status) << std::endl;
            }else if(wc.opcode == IBV_WC_SEND){
                DEBUG << "VerbsBUPolCqcEML::run message sent" << std::endl;
                msg->release_mr();
                msg->release_payload();
            }else{
                WARNING << "VerbsBUPolCqcEML::run unexpected event dropping" << std::endl;
            }
        }
        if(ev_conter >= ack_number){
            ibv_ack_cq_events(cq, ev_conter);
            ev_conter = 0;
        }
        check_nz(ibv_req_notify_cq(cq, 0), "VerbsBUPolCqcEML::run ibv_req_notify_cq", true);
    }
}
