/**
 *      Verbs RU - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsReadoutUnit.h"

void Verbs::ReadoutUnit::initialize(int initialCredits, std::map<std::string, void *> &pmems){
    activeRequests.resize(initialCredits, -1);
    requestIndex.resize(initialCredits, 0);


    int num_rcv_mess_per_node;
    int num_snd_mess_per_node;
    switch (Verbs::Configuration->protocol) {
    case Core::PUSH:
        num_rcv_mess_per_node = (MAX_RU_FRAGMENTS)*Verbs::Configuration->initialCredits;
        num_snd_mess_per_node = 2*Verbs::Configuration->initialCredits;
        break;
    case Core::PULL:
        num_rcv_mess_per_node = (MAX_RU_FRAGMENTS+1)*Verbs::Configuration->initialCredits;
        num_snd_mess_per_node = 2*Verbs::Configuration->initialCredits;
        break;
    default:
        ERROR << "Verbs::VerbsReadoutUnit::initialize unknown protocol requested aborting" << std::endl;
        exit(EXIT_FAILURE);
        break;
    }

    const size_t max_send_message_size = sizeof(FragmentComposition);
    const size_t max_recv_message_size = sizeof(ibv_mr);

    const int no_rcv_mess = (Verbs::Configuration->numnodes )* num_rcv_mess_per_node;
    const int no_snd_mess =(Verbs::Configuration->numnodes )* num_snd_mess_per_node;

    for (int i=0; i<Verbs::Configuration->numprocs; ++i){
        int pid = i % Verbs::Configuration->numProcsPerNode;

    }
    tperiod.start();

    //TODO server non ready issue
    if(Core::Configuration->protocol == Core::PUSH){
        em_consumer_ctx = build_context_client(Verbs::Configuration->EMCMapIDPort,
                                               Verbs::Configuration->EMCMapIPID);
    }

    sleep(1);

    do{
        DEBUG << "RU building context" << std::endl;
        bu_ctx = build_context_client(Verbs::Configuration->BUMapIDPort,
                                      Verbs::Configuration->BUMapIPID);
        usleep(1);
    }while (bu_ctx == NULL);

    //just fragment composition for now
    recv_messages_buff = malloc(no_rcv_mess*max_recv_message_size);
    check_alloc(recv_messages_buff, "VerbsEMListener::setupListen recv_messages_buff malloc", true);

    send_messages_buff = malloc(no_snd_mess*max_send_message_size);
    check_alloc(send_messages_buff, "VerbsEMListener::setupListen send_messages_buff malloc", true);


    recv_mess.resize(no_rcv_mess);
    send_mess.resize(no_snd_mess);
    rdma_sends.resize((MAX_RU_FRAGMENTS)*Verbs::Configuration->numnodes*Verbs::Configuration->initialCredits);

    if(Core::Configuration->protocol == Core::PUSH){
        emc_receive_mess.resize(Verbs::Configuration->numnodes*Verbs::Configuration->numBuildersPerNode*
                                Verbs::Configuration->initialCredits);

        std::string tmp_string = "shmem-Verbs::VerbsReadoutUnit::event_assign_shmem";
        _event_assign_shmem = new NamedPipe<int*>(tmp_string, emc_receive_mess.size());
    }

    std::string tmp_string = "shmem-Verbs::VerbsReadoutUnit::memory_region_shmem";
    _memory_region_shmem = new NamedPipe <Message *> (tmp_string,  rdma_sends.size());

    tmp_string = "shmem-Verbs::VerbsReadoutUnit::completed_rdma_shmem";
    _completed_rdma_shmem = new NamedPipe <RDMATransfer *> (tmp_string, rdma_sends.size());

    tmp_string = "shmem-Verbs::VerbsReadoutUnit::free_rdma_shmem";
    _free_rdma_shmem = new NamedPipe <RDMATransfer *> (tmp_string, rdma_sends.size());

    tmp_string = "shmem-Verbs::VerbsReadoutUnit::completed_msg_shmem";
    _completed_send_msg = new NamedPipe <Message *> (tmp_string, send_mess.size());

    tmp_string = "shmem-Verbs::VerbsReadoutUnit::completed_events_id";
    _completed_events_id = new NamedPipe<int> (tmp_string,
                                                       Verbs::Configuration->numnodes*Verbs::Configuration->initialCredits);

    tmp_string = "shmem-Verbs::VerbsReadoutUnit::ID_num_frag_shmem";
    _ID_num_frag_shmem = new  NamedPipe<std::pair<int, int> *>(tmp_string,
                                                                       Verbs::Configuration->numnodes*Verbs::Configuration->initialCredits);
    tmp_string = "shmem-Verbs::VerbsReadoutUnit::rdma_transfer_ID_shmem";
    _rdma_transfer_ID_shmem = new NamedPipe<std::pair<RDMATransfer *, int> *>
            (tmp_string,  Verbs::Configuration->numnodes*(MAX_RU_FRAGMENTS)*Verbs::Configuration->initialCredits);

    // TODO finish this
    if(Verbs::Configuration->protocol == Core::PULL){
        tmp_string = "shmem-Verbs::VerbsReadoutUnit::pull_request_shmem";
        _pull_request_shmem = new NamedPipe <Message *> (tmp_string,
                                                                 Verbs::Configuration->initialCredits*Verbs::Configuration->initialCredits);
    }

    int index = 0;
    for(std::map <int, struct connection *>::iterator it = bu_ctx->connections_id.begin() ; it != bu_ctx->connections_id.end() ; it++){
        connection *conn;
        Message *tmp_msg_rcv;
        conn = it->second;
        for(int i=0 ; i < num_rcv_mess_per_node ; i++){
            tmp_msg_rcv = new Message;

            if(tmp_msg_rcv->setPayload((char*)recv_messages_buff + index*max_recv_message_size)){
                DEBUG << "Verbs::ReadoutUnit::initialize setPayload ok" << std::endl;
            }
            tmp_msg_rcv->setSize(max_recv_message_size);
            while (!tmp_msg_rcv->receive_message(conn)) usleep(1);
            recv_mess[index++] = tmp_msg_rcv;
        }
    }

    index = 0;
    for(std::vector<Message *>::iterator it = send_mess.begin() ; it != send_mess.end() ; it++){
        *it = new Message((char*)send_messages_buff + index*max_send_message_size, max_send_message_size);
//        free_send_msg.push(*it);
        while(_completed_send_msg->write(*it) == -1) usleep(1);
        index++;
    }

    ibv_mr *tmp_mr;
    tmp_mr = ibv_reg_mr(bu_ctx->pd, pmems[Core::Configuration->RUMeta1Name],Core::Configuration->RUMetaBufferSize,
            IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(tmp_mr, "Verbs::VerbsReadoutUnit::initialize meta_1 rdma_send_mr ibv_reg_mr", true);
    rdma_send_mr.push_back(tmp_mr);

    tmp_mr = ibv_reg_mr(bu_ctx->pd, pmems[Core::Configuration->RUMeta2Name],Core::Configuration->RUMetaBufferSize,
            IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(tmp_mr, "Verbs::VerbsReadoutUnit::initialize meta_2 rdma_send_mr ibv_reg_mr", true);
    rdma_send_mr.push_back(tmp_mr);

    tmp_mr = ibv_reg_mr(bu_ctx->pd, pmems[Core::Configuration->RUData1Name],Core::Configuration->RUDataBufferSize,
            IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
//    check_alloc(tmp_mr, "Verbs::VerbsReadoutUnit::initialize data_1 rdma_send_mr ibv_reg_mr", true);
    check_alloc(tmp_mr, Verbs::Configuration->getLogicalName().c_str(), true);
    rdma_send_mr.push_back(tmp_mr);

    tmp_mr = ibv_reg_mr(bu_ctx->pd, pmems[Core::Configuration->RUData2Name],Core::Configuration->RUDataBufferSize,
            IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    check_alloc(tmp_mr, "Verbs::VerbsReadoutUnit::initialize meta_2 rdma_send_mr ibv_reg_mr", true);
    rdma_send_mr.push_back(tmp_mr);

    for(std::vector<RDMATransfer *>::iterator it = rdma_sends.begin() ; it != rdma_sends.end() ; it++){
        *it = new RDMATransfer;
        for(std::vector<ibv_mr *>::iterator vec_it = rdma_send_mr.begin() ; vec_it != rdma_send_mr.end() ; vec_it++){
            (*it)->add_static_mr(*vec_it);
            DEBUG << "Verbs::ReadoutUnit::initialize adding static mr" << std::endl;
        }
        while(_free_rdma_shmem->write(*it) == -1) usleep(1);
    }

    if(Core::Configuration->protocol == Core::PUSH){
        emc_data_buffer = (int *)malloc(3*emc_receive_mess.size()*sizeof(int));
        check_alloc(emc_data_buffer, "Verbs::VerbsReadoutUnit::initialize emc_data_buffer malloc", true);

        connection *conn;
        conn = em_consumer_ctx->connections_id[Verbs::Configuration->emconsumerID];
        for(int k = 0 ; k < emc_receive_mess.size() ; k++){
            emc_receive_mess[k] = new Message(emc_data_buffer + 3*k, 3*sizeof(int));
            DEBUG << "Verbs::ReadoutUnit::initialize receiving msg number" << k << std::endl;
            while(!(emc_receive_mess[k]->receive_message(conn))) usleep(1);
        }

        EMCPoller = new VerbsRUPolCqEMC(em_consumer_ctx, _event_assign_shmem);

        startThreadWithReference(*EMCPoller);
    }

    BUpoller = new VerbsRUPollCqBU(bu_ctx, _memory_region_shmem, _pull_request_shmem,
                                   _completed_rdma_shmem, _completed_send_msg);

    startThreadWithReference(*BUpoller);

    RDMASender = new VerbsRURDMASender(bu_ctx, _completed_send_msg, _completed_rdma_shmem, _free_rdma_shmem,
                                       _completed_events_id, _ID_num_frag_shmem, _rdma_transfer_ID_shmem);

    startThreadWithReference(*RDMASender);


//    em_consumer_ctx = build_context_client(verbsCore::VerbsConfig->EMCMapIDPort[verbsCore::EMCONSUMER_ID],
//            verbsCore::VerbsConfig->EMCMapIPID);

    
    DEBUG << "ru inizialized finished" << std::endl;
}

/**
 * Listens for a message from the EventManager.
 */
bool Verbs::ReadoutUnit::EventListen(int& size, int& eventID, int& destinationBU){
    connection *conn;
    Message *msg;
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    int *tmp_payload;

    static int ev_conter = 0;
    const int ack_number = 1;

    //conn =  em_consumer_ctx->connections_id[verbsCore::VerbsConfig->emconsumerID];

    DEBUG << "Verbs::VerbsReadoutUnit: Listening..." << std::endl;
    DEBUG << "Verbs::VerbsReadoutUnit: s " << Verbs::EMCONSUMER_ID << ", r " << Verbs::Configuration->rankID
        << ", id " << Verbs::EVENT_REQUEST << std::endl;

//    t_stuck.start();
//    while(_event_assign_shmem->read(tmp_payload) == -1){

//        if(t_stuck.getElapsedTime() > 5){
//            ERROR << "Verbs::VerbsReadoutUnit::listen: " << verbsCore::VerbsConfig->getLogicalName() << " listen stucked" << std::endl;
//            t_stuck.stop();
//            t_stuck.start();
//        }
//        usleep(1);
//    }
//    t_stuck.stop();

    while(_event_assign_shmem->read(tmp_payload) == -1) usleep(1);

    size = tmp_payload[0];
    destinationBU = tmp_payload[2];
    eventID = tmp_payload[1];

    free(tmp_payload);

    DEBUG << "Verbs::VerbsReadoutUnit: Received!" << std::endl;
    DEBUG << "Verbs::VerbsReadoutUnit: event size: " << (int) size << " eventID " << (int) eventID
          << " dest BU " << (int) destinationBU << std::endl;
    // data is gathered from the gpu generator on the protocol side.
    return true;
}


void Verbs::ReadoutUnit::pullRequestReceive(int& eventID, int& size, int& destinationBU){
    Message *tmp_msg;
    int *payload;

    while(_pull_request_shmem->read(tmp_msg) == -1) usleep(1);

    payload = (int*) tmp_msg->payload();

    size = payload[0];
    eventID = payload[1];
    destinationBU = payload[2];

    tmp_msg->release_payload();
    tmp_msg->release_mr();

    tmp_msg->receive_message(bu_ctx->connections_id[tmp_msg->sender_id()]);
}

/**
 * Prepares the sendEvent
 */
void Verbs::ReadoutUnit::prepareSendEvent(FragmentComposition *&fc, int destinationBU){
    int eventID = fc->eventID;
    connection *conn;

    DEBUG << "Verbs::VerbsReadoutUnit: destinationBU " << destinationBU << ", r "
        << Verbs::Configuration->builderID(destinationBU) << std::endl;

    DEBUG << "Verbs::VerbsReadoutUnit: prepareSendEvent s " << Verbs::Configuration->rankID << ", r " << Verbs::Configuration->builderID(destinationBU)
        << ", id " << eventID * Verbs::EVENT_MESSAGES_SIZE + Verbs::EVENT_FRAGMENT_COMPOSITION << std::endl;

    conn = bu_ctx->connections_id[Verbs::Configuration->builderID(destinationBU)];

//    if(!verbsCore::VerbsConfig->readoutInSameNode(destinationBU)){
        Message *tmp_msg;
        std::pair<int, int> *frag_ID_pair;
        while(_completed_send_msg->read(tmp_msg) == -1) usleep(1);

        while(tmp_msg->protect_mr()){
            WARNING << "message not ready" << std::endl;
            usleep(1);
        }

        tmp_msg->setSize(sizeof(FragmentComposition));
        tmp_msg->setType(MSG_FRAGMENT_COMPOSITION);
        memcpy(tmp_msg->payload(), fc, sizeof(FragmentComposition));
        while(!tmp_msg->send_message(conn)) usleep(1);

        frag_ID_pair = new std::pair<int, int>(eventID,fc->metablocks + fc->datablocks);
        while(_ID_num_frag_shmem->write(frag_ID_pair) == -1) usleep(1);
//    }

    if (Core::Configuration->logLevel >= 4){
        INFO << "Verbs::VerbsReadoutUnit::prepareSendEvent FragmentComposition Sent to " << conn->connection_number << std::endl;
        DEBUG << "fc event id " << fc->eventID << std::endl;
        Logger::printFC( fc );
    }



    DEBUG << "Verbs::VerbsReadoutUnit::prepareSendEvent fc sent" << std::endl;

    DEBUG << "Verbs::VerbsReadoutUnit::prepareSendEvent post send done" << std::endl;
}

/**
 * (Async) Sends the events to a particular Builder Unit.
 */
//void Verbs::VerbsReadoutUnit::setupSendEvent(char* dataPointer, char* receivePointer, int size, int eventID, int destinationBU){
void Verbs::ReadoutUnit::setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU){
    // Find a free request.    static char * tmp_dataPointer;
    char * tmp_dataPointer;

    tmp_dataPointer = dataPointer;

    ttotal.start();

    std::pair<RDMATransfer *, int> *RDMA_ID;
    Message *tmp_msg;
    RDMATransfer *tmp_rdma;
    tread_receive.start();
    while(_memory_region_shmem->read(tmp_msg) == -1) { usleep(1);}
    DEBUG << "Verbs::VerbsReadoutUnit::setupSendEvent memory region received from " << tmp_msg->sender_id() << std::endl;
    tread_receive.stop();

    tread_free.start();
    while(_free_rdma_shmem->read(tmp_rdma) == -1) usleep(1);
    tread_free.stop();

    DEBUG << "Verbs::VerbsReadoutUnit::setupSendEvent rdma transfer " << (void*)tmp_rdma << " assigned to event " << eventID << std::endl;
    twrite_rdma_id.start();
    RDMA_ID = new std::pair<RDMATransfer *, int>(tmp_rdma, eventID);
    while(_rdma_transfer_ID_shmem->write(RDMA_ID) == -1) usleep(1);
    twrite_rdma_id.stop();

    twrite_rdma.start();
    if(tmp_msg->sender_id() != Verbs::Configuration->builderID(destinationBU)){
        ERROR << "destination BU != MR" << std::endl;
    }
    DEBUG << "payload: " << (void*)tmp_dataPointer << " size: " << size << std::endl;
    if (size < 0) {
        WARNING << "negative size " << size << " setting size to 0" << std::endl;
        size = 0;
    }
    tmp_rdma->setSize(size);
    tmp_rdma->setPayload(tmp_dataPointer);
    tmp_rdma->set_peer_mr(*(ibv_mr*)tmp_msg->payload());

    DEBUG << "tmp_rdma peer addr " << tmp_rdma->peer_mr().addr << std::endl;

    if(tmp_rdma->rdma_write(bu_ctx->connections_id[tmp_msg->sender_id()])){
        DEBUG << "Verbs::VerbsReadoutUnit::setupSendEvent rdma_send over" << std::endl;
    }else{
        ERROR << "Verbs::VerbsReadoutUnit::setupSendEvent rdma_send failed" << std::endl;
    }


    tmp_msg->release_mr();
    tmp_msg->release_payload();

    tmp_msg->receive_message(bu_ctx->connections_id[tmp_msg->sender_id()]);
    twrite_rdma.stop();


    ttotal.stop();
         if (tperiod.getElapsedTime() > 10){
            if (Core::Configuration->logActivated[Logger::_PROFILE]){
                PROFILE << "Verbs::VerbsReadoutUnit::setupSendEvent " << Core::Configuration->name << " timers: " << std::endl
                        << std::setw(15) << "ttotal " << ttotal.get() << std::endl
                        << std::setw(15) << "tread_receive " << tread_receive.get() << " (" << 100.f * tread_receive.get() / ttotal.get() << "\%)" << std::endl
                        << std::setw(15) << "tread_free " << tread_free.get() << " (" << 100.f * tread_free.get() / ttotal.get() << "\%)" << std::endl
                        << std::setw(15) << "twrite_rdma_id " << twrite_rdma_id.get() << " (" << 100.f * twrite_rdma_id.get() / ttotal.get() << "\%)" << std::endl
                        << std::setw(15) << "twrite_rdma " << twrite_rdma.get() << " (" << 100.f * twrite_rdma.get() / ttotal.get() << "\%)" << std::endl << std::endl;
            }
            tperiod.flush();
            tperiod.start();
        }


}

bool Verbs::ReadoutUnit::sendAnyEvent(int& eventID){
    bool eventSent = false;
    std::list<int>::iterator it;

    // Check against the activeRequests.
    // Test against the oldest first, so as to [try to] keep a finish order
    // (this is useful for the RUMaster -> Generator communication)
    for (it = activeRequestsList.begin(); it != activeRequestsList.end(); it++){
        // We test an event receival
        int eid = activeRequests[*it];
        eventSent = sendEvent(eid);
        if (eventSent) {
            eventID = eid;
            break;
        }
    }

    return eventSent;
}

bool Verbs::ReadoutUnit::sendEvent(int eventID){
    // Check that eventID is in our list,
    // and test if it was sent. Return true and do
    // some cleanup in that case.
    bool found = false, eventSent = false;
    std::list<int>::iterator it;
    int eventSlot;
    int tmp_ID;

    DEBUG << "Verbs::VerbsReadoutUnit::sendEvent begin" << std::endl;

    if(_completed_events_id->read(tmp_ID) == 0){
        if(eventID == tmp_ID){
            eventSent = true;
        }else{
            completed_send_event.push_back(tmp_ID);
        }
    }

    if(!eventSent){
        for(std::vector<int>::iterator it=completed_send_event.begin() ; it != completed_send_event.end() ; it ++){
            if(*it == eventID){
                eventSent = true;
                // TODO not so efficient
                completed_send_event.erase(it);
                DEBUG << "Verbs::VerbsReadoutUnit::sendEvent event completed found" << std::endl;
                DEBUG << "Verbs::VerbsReadoutUnit::sendEvent " << completed_send_event.size() << " event waiting" << std::endl;
                break;

            }
        }
    }




    if (eventSent){
        DEBUG << "EVENT FOUND!!!!!!!!!!!" << std::endl;
    }

    return eventSent;
}
