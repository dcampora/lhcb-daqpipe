/**
 *      Verbs RUPolcqEMC
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSRUPOLCQEMC_H
#define VERBSRUPOLCQEMC_H
#include "../../common/NamedPipe.h"
#include "../../common/Logging.h"
#include "../../common/GenStructures.h"
#include "../../common/Timer.h"
#include "Verbs_lib.h"
#include "VerbsCore.h"
#include <map>
#include <queue>

class VerbsRUPolCqEMC
{
private:
    //NamedPipe<message *> *_free_send_msg_shmem;
    NamedPipe <int*> *_event_assign_shmem;
    context *_ctx;

    Timer tcompletion, twait, tcopy, ttotal, tother;
    bool print_timer;
public:
    VerbsRUPolCqEMC();
    VerbsRUPolCqEMC(context *ctx = NULL,
                   NamedPipe<int*> *event_assign_shmem = NULL);
    ~VerbsRUPolCqEMC(){}
    void run();
    context *ctx() const;
    void setCtx(context *ctx);
    NamedPipe<int *> *event_assign_shmem() const;
    void setEvent_assign_shmem(NamedPipe<int*> *event_assign_shmem);
};

#endif // VERBSRUPOLCQEMC_H
