/**
 *      cache - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "cache.h"

template <class Key, class Value>
int Cache<Key, Value>::cache_search(Key const& key, Value &value){
    typename std::map <Key, Value>::iterator cache_it;
    int ret_val;

    cache_it = _cache_map.find(key);
    if(cache_it != _cache_map.end()){
        _hit_number++;
        value = cache_it->second;
        ret_val = 0;
    }else{
        ret_val = -1;
        _miss_number++;
    }
    _cache_calls++;

    return ret_val;

}

template <class Key, class Value>
void Cache<Key, Value>::cache_add(Key const& key, Value const &value){
    //hashes[((uint64_t) key << 1)% 128]++;
    _cache_map[key] = value;
}


template <class Key, class Value>
double Cache<Key, Value>::hit_rate(){
//    for(int k = 0; k< 128 ; k++){
//        ERROR << "hashes[" << k << "]=" << hashes[k] << std::endl;
//    }
    return _hit_number/((double)_cache_calls);
}

template <class Key, class Value>
double Cache<Key, Value>::miss_rate(){
    return _miss_number/((double)_cache_calls);
}

template <class Key, class Value>
void  Cache<Key, Value>::reset_counters(){
    _miss_number = 0;
    _hit_number = 0;
    _cache_calls = 0;
}

template <class Key, class Value>
void Cache<Key, Value>::cache_remove(Key const& key){
    _cache_map.erase(key);
}

template <class Key, class Value>
void Cache<Key, Value>::cache_update(Key const& key, Value const &value){
    Value tmp_value;
    if(this->cache_search(key, tmp_value) == 0){
        tmp_value = value;
    }

}
