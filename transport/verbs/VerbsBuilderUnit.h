/**
 *      Verbs BU
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSBUILDERUNIT
#define VERBSBUILDERUNIT 1

#include <stdlib.h>
#include <ctime>
#include <vector>
#include <list>
#include <string>
#include <stdio.h>

#include "../../common/Timer.h"
#include "../../common/GenStructures.h"
#include "../../common/Shmem.h"
#include "../common/BUTransportCore.h"
#include "../config/ConfigurationCore.h"
#include "VerbsCore.h"
#include "VerbsBUPolqcEML.h"
#include "VerbsBUPolqcEMC.h"
#include "VerbsBUPolcqRU.h"
#include "Verbs_lib.h"
#include "rdmatransfer.h"
#include "../../common/pthreadHelper.h"
#include "cache.h"

namespace Verbs {

class BuilderUnit : public BUTransportCore {
private:
    // m credits * k requests each
    std::vector<int> transmissionSize;
    int metaBufferElement, dataBufferElement;

    std::vector<int> activeRequests;

    int* size_id;
    time_t tstart;
    int initialCredits;

    std::list<int> activeRequestsList;
    std::vector<int> requestIndex;
    std::vector<std::vector<int> > requestReadoutIndex;

    std::map<int, int> eventID_size_map;

    // verbs connection
    context *em_listner_ctx;
    context *em_consumer_ctx;
    context *ru_ctx;

    class VerbsBUPolCqcEML *EMLPoller;
    class VerbsBUPolqcEMC *EMCPoller;
    class VerbsBUPolCqRU *RUPoller;
    NamedPipe<Message *> *_fragment_composition_shmem;
    NamedPipe<Message *> *_free_send_msg_shmem;
    NamedPipe<Message *> *_requested_events_shmem;
    NamedPipe<int> *_complete_event_shmem;
    NamedPipe<int> *_event_received_shmem;
    void *recv_messages_buff;
    void *send_messages_buff;
    void *event_assing_buff;
    std::vector <struct ibv_mr*> rdma_mr;
    std::vector <Message *> send_mess;
    std::vector <Message *> recv_mess;
    std::vector <Message *> event_assing;

//    std::map <int, FragmentComposition *> receiving_fc;
    FragmentComposition *receiving_fc;

    std::vector <Cache <void *, struct ibv_mr*> > rdma_mr_chache;


    Timer t_mr_search, t_free_msg, t_msg_prepare, t_total, t_map, t_print;
    int number_of_prepare;
public:
    BuilderUnit() : BUTransportCore() {}
    ~BuilderUnit();

    virtual void initialize(int initialCredits, void *sh_meta_pmem, void *sh_data_pmem);
    virtual void sendCredits(int& numberOfCredits);
    virtual void pullRequest(int *size_id_bu, int &readoutno);
    virtual void receiveSizeAndID(int& eventSize, int& eventID);
    virtual void prepareReceiveEvent(FragmentComposition* fc);
    virtual bool prepareReceiveEventComplete(int& fragmentReceived);
    virtual void setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype = Core::DATA);
    virtual bool receiveEvent(int& finishedEventID, int& tSize);
    virtual bool checkExitMessage(void);
};

};
#endif
