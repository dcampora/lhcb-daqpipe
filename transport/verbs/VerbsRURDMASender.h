/**
 *      Verbs RURDMASender
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSRURDMASENDER_H
#define VERBSRURDMASENDER_H

#include <stdlib.h>
#include <vector>
#include <list>
#include <string>
#include <stdio.h>
#include <utility>

#include "../../common/NamedPipe.h"
#include "../../common/NamedPipe.h"
#include "../../common/GenStructures.h"
#include "../../common/Shmem.h"
#include "../config/ConfigurationCore.h"
#include "VerbsCore.h"
#include "VerbsBUPolcqRU.h"
#include "Verbs_lib.h"
#include "rdmatransfer.h"
#include "../../common/pthreadHelper.h"

class VerbsRURDMASender
{
    NamedPipe<Message *> *_completed_send_msg;
    NamedPipe<RDMATransfer *> *_completed_rdma_shmem;
    NamedPipe<RDMATransfer *> *_free_rdma_shmem;
    NamedPipe<int> *_completed_events_id;
    NamedPipe<std::pair<int, int> *> *_ID_num_frag_shmem;
    NamedPipe<std::pair<RDMATransfer *, int> *> *_rdma_transfer_ID_shmem;


    std::map <int, int> _map_id_no_rmda;
    std::map <RDMATransfer *, int> _map_rdma_id;
    //std::vector <int> _completed_send_event;

    context *_ctx;
public:
    VerbsRURDMASender(context *bu_ctx = NULL,
                      NamedPipe<Message *> *completed_send_msg = NULL,
                      NamedPipe<RDMATransfer *> *completed_rdma_shmem = NULL,
                      NamedPipe<RDMATransfer *> *free_rdma_shmem = NULL,
                      NamedPipe<int> *completed_events_id = NULL,
                      NamedPipe<std::pair<int, int> *> *ID_num_frag_shmem = NULL,
                      NamedPipe<std::pair<RDMATransfer *, int> *> *rdma_transfer_ID_shmem = NULL);


    void run();
    NamedPipe<Message *> *completed_send_msg() const;
    void setCompleted_send_msg(NamedPipe<Message *> *completed_send_msg);
    NamedPipe<RDMATransfer *> *completed_rdma_shmem() const;
    void setCompleted_rdma_shmem(NamedPipe<RDMATransfer *> *completed_rdma_shmem);
    NamedPipe<RDMATransfer *> *free_rdma_shmem() const;
    void setFree_rdma_shmem(NamedPipe<RDMATransfer *> *free_rdma_shmem);
    NamedPipe<int> *completed_events_id() const;
    void setCompleted_events_id(NamedPipe<int> *completed_events_id);
    NamedPipe<std::pair<int, int> *> *ID_num_frag_shmem() const;
    void setID_num_frag_shmem(NamedPipe<std::pair<int, int> *> *ID_num_frag_shmem);
    NamedPipe<std::pair<RDMATransfer *, int> *> *rdma_transfer_ID_shmem() const;
    void setRdma_transfer_ID_shmem(NamedPipe<std::pair<RDMATransfer *, int> *> *rdma_transfer_ID_shmem);
    context *ctx() const;
    void setCtx(context *ctx);
};

#endif // VERBSRURDMASENDER_H
