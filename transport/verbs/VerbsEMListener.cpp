/**
 *      Verbs EMListener - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsEMListener.h"

/**
 * TODO: Modify the listen to make it controlled from the Master.
 */

void Verbs::EMListener::setupListen(){
    DEBUG << "VerbsEMListener: setupListen" << std::endl;
    const int num_rcv_mess_per_node = Verbs::Configuration->initialCredits*Verbs::Configuration->numBuildersPerNode;
    const size_t max_message_size = sizeof(int);
    //assuming 1 bu per node
    const int no_rcv_mess = num_rcv_mess_per_node * Verbs::Configuration->numnodes;

    numberOfCredits = (int*) malloc(Verbs::Configuration->numnodes * Core::Configuration->numBuildersPerNode * sizeof(int));

    for (int i=0; i<Verbs::Configuration->numnodes; ++i){

        for (int j=0; j<Core::Configuration->numBuildersPerNode; ++j){
            DEBUG << "VerbsEMListener: startListen s " << Verbs::Configuration->builderID(i*Core::Configuration->numBuildersPerNode + j)
                << ", r " << Verbs::Configuration->rankID << ", id " << Verbs::CREDIT_REQUEST << std::endl;
            
//            requests[i*Core::Configuration->numBuildersPerNode + j] =
//                MPI::COMM_WORLD.Irecv(&numberOfCredits[i*Core::Configuration->numBuildersPerNode + j], 1, MPI::INT,
//                verbsCore::MPIConfig->builderID(i*Core::Configuration->numBuildersPerNode + j), verbsCore::CREDIT_REQUEST);
        }
    }

    DEBUG << Verbs::Configuration->name << "VerbsEMListener::setupListen building context bu_ctx" << std::endl;
    bu_ctx = build_context_server(Verbs::Configuration->numTotalBuilders,
                                 Verbs::Configuration->EMLMapIDPort[Verbs::EMLISTENER_ID], Verbs::Configuration->BUMapIPID);

    recv_mess.resize(no_rcv_mess);

    messages_buff = malloc(no_rcv_mess*max_message_size);
    check_alloc(messages_buff, "VerbsEMListener::setupListen messages_buff malloc", true);

//    char *tmp_buff_location = messages_buff;
    int index = 0;
    for(std::map <int, struct connection *>::iterator it = bu_ctx->connections_id.begin() ; it != bu_ctx->connections_id.end() ; it++){
        connection *conn;
        conn = it->second;
        for(int i=0 ; i < num_rcv_mess_per_node ; i++){
            Message *tmp_mess;
            tmp_mess = new Message;

            if(tmp_mess->setPayload((char*)messages_buff + index*max_message_size)){
                DEBUG << "setPayload ok" << std::endl;
            }
            payload_mess[tmp_mess->payload()] = tmp_mess;
            tmp_mess->setSize(max_message_size);
            while(!tmp_mess->receive_message(conn)) usleep(1);
            recv_mess[index]=tmp_mess;
            index++;
        }
    }


    std::string tmp_string = "shmem-VerbsEMListener::credit_shmem";
    credit_shmem = new NamedPipe <Message *> (tmp_string,
                                                      Verbs::Configuration->numTotalBuilders * Verbs::Configuration->initialCredits );

    DEBUG << "VerbsEMListener running poller" << std::endl;
    poller = new VerbsEMListnerPolCq (bu_ctx, credit_shmem);

    startThreadWithReference<VerbsEMListnerPolCq>(*poller);



//     ru_ctx = build_context_server(verbsCore::VerbsConfig->numnodes,
//                                  verbsCore::VerbsConfig->EMCMapIDPort[verbsCore::VerbsConfig->emconsumerID], verbsCore::VerbsConfig->RUMapIPID);

//     ru_ctx = build_context_server(verbsCore::VerbsConfig->numnodes,
//                                  verbsCore::VerbsConfig->EMLMapIDPort[verbsCore::VerbsConfig->emlistenerID], verbsCore::VerbsConfig->RUMapIPID);


//    std::string name = "/listen-queue";
//    int size = 10000;
//    NamedPipe* locksh_queue = new NamedPipe<int>(name, size);

//    A* a = new A(locksh_queue);
//    startThreadWithPointer<A>(a);

//    int i = 20;
//    locksh_queue->write(i);

//    // a->run()
//    int b;
//    while(locksh_queue->read(b) == -1) usleep(1);
}

void Verbs::EMListener::listenForCreditRequest(int& node, int& numberofcreditsrequested){
    // since my listeners are ordered, and the builderid can be gotten by
    // Verbscore::Verbsconfig->builderid(i), there's no need to listen for the status.
    //num_credit_id tmp_num_id;
    Message *tmp_mess;
    //todo handling message error


//    for(std::map <int, struct connection *>::iterator it = bu_ctx->connections_id.begin() ; it != bu_ctx->connections_id.end() ; it++){
//        connection *conn;
//        conn = it->second;
//        buff = malloc(sizeof(int));
//        conn->recv_msg->setsize(sizeof(int));
//        debug << "here" << std::endl;
//        if(conn->recv_msg->setpayload(buff)){
//            debug << "setpayload ok" << std::endl;
//        }
//        //conn->recv_msg->receive_message(conn);
//        debug << "conn qp num " << conn->qp->qp_num << std::endl;
//    }

    do{
        DEBUG << "wait for credits" << std::endl;
        while(credit_shmem->read(tmp_mess)) usleep(1);

        DEBUG << "credits received" << std::endl;
//    int requestno = MPI::Request::Waitany(verbsCore::MPIConfig->numnodes * Core::Configuration->numBuildersPerNode, requests);
    
//    // Get the node and number of credits requested
//    numberOfCreditsRequested = numberOfCredits[requestno];
//    node = requestno;

//    DEBUG << "VerbsEMListener: Received a " << numberOfCreditsRequested << "-credit request from node "
//          << node << std::endl;

//    // Restore the listener
//    DEBUG << "VerbsEMListener: restored listener s " << verbsCore::VerbsConfig->builderID(requestno)
//        << ", r " << verbsCore::VerbsConfig->rankID << ", id " << verbsCore::CREDIT_REQUEST << std::endl;
//    requests[requestno] = MPI::COMM_WORLD.Irecv(&numberOfCredits[requestno], 1, MPI::INT, verbsCore::MPIConfig->builderID(requestno), verbsCore::CREDIT_REQUEST);
    }while(tmp_mess == NULL);
    node = Verbs::Configuration->builderNumber(tmp_mess->sender_id());
    numberofcreditsrequested = *(int *) tmp_mess->payload();
    DEBUG << "VerbsEMListener: Received a " << numberofcreditsrequested << "-credit request from node " << node << std::endl;
    tmp_mess->release_mr();
    tmp_mess->release_payload();
    while(!tmp_mess->receive_message(bu_ctx->connections_id[tmp_mess->sender_id()])){
        usleep(1);
        WARNING << "VerbsEMListener::listenForCreditRequest receive_message failed" << std::endl;
    }
}
