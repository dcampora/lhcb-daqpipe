/**
 *      Verbs lib
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBS_LIB_H
#define VERBS_LIB_H

#include "rdmatransfer.h"
#include "message.h"
#include "connection.h"
#include <time.h>
#include <stdint.h>
#include <netdb.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <infiniband/verbs.h>
#include <unistd.h>
#include <rdma/rdma_cma.h>
#include <pthread.h>
#include <iostream>
#include <map>
#include <arpa/inet.h>
#include "../../common/Logging.h"
#include "../../common/Tools.h"
#include "../../common/GenStructures.h"

/** @file */
const int TIMEOUT_IN_MS = 500; /* ms */

/**
 * @brief The context struct
 *
 * struct used to handle the connections established by a server or a client
 */
struct context {
    struct ibv_context *ctx; /**< ibv_context of the device*/
    struct ibv_pd *pd;/**< ibv_pd of the device*/
    struct ibv_srq *srq;/**< ibv_srq common to all the open connections*/
    struct ibv_comp_channel *comp_channel;/**< completion channel for the ibv_srq*/

    std::map <int, struct connection *> connections_id;/**< map id and connection struct of all the open connections*/
    std::map <int, struct connection *> connections_qp_num;/**< map qp number and connection struct of all the open connections*/
    int number_of_connections;/**< number of opne connections*/

    context() : ctx(NULL), pd(NULL), srq(NULL), comp_channel(NULL){}
};


/**
 * @brief check_alloc
 * check memory allocation errors
 * @param array variable to be checked
 * @param message message to print if array is NULL
 * @param fatal exit on error
 */
void check_alloc(const void* array, const char *Message, bool fatal);

/**
 * @brief check_nz
 * check return value errors
 * @param value value to be checked
 * @param message message to print if value != 0
 * @param fatal exit on error
 */
void check_nz(int value, const char *Message, bool fatal);

/**
 * build the context struct allocating memory if necessary.
 *
 * @param verbs ibv_context of the current connection
 * @param ctx returned initialized context
 * @param connection_number connection number id
 */
void build_context(struct ibv_context *verbs, struct context *ctx, int connection_number) ;

/**
 * build ibv_qp_init_attr struct.
 *
 * @param qp_attr returned ibv_qp_init_attr initialized
 * @param conn current connection struct
 */
void build_qp_attr(struct ibv_qp_init_attr *qp_attr, struct connection *conn) ;

/**
 *  rmdacm on connect request event handler.
 *
 *  @param id id of the current event
 *  @param ctx context in use
 *  @param mapIPID map of IP and ID
 *  @return 0 on success
 */
int on_connect_request(struct rdma_cm_id *id, context *ctx, std::map<unsigned int, int> &mapIPID);

/**
 * @brief printf_mr
 * print an ibv_mr.
 * @param mr mr to be printed
 */
void printf_mr(const struct ibv_mr *mr);

/**
 * @brief on_connection
 * rdmacm on_connection event handler just a place holder returning 0.
 * @param context context struct in use
 * @return 0
 */
int on_connection(void *context);

/**
 * @brief on_addr_resolved
 * rdmacm on_addr_resolved event handler.
 * @param id id of the current event
 * @param ctx context in use
 * @param mapIPID map of IP and ID
 * @return 0 on success
 */
int on_addr_resolved(struct rdma_cm_id *id, context *ctx, std::map<unsigned int, int> &mapIPID);

/**
 * @brief on_route_resolved
 * rdmacm on_route_resolved event handler.
 * @param id id of the current event
 * @return 0 on success
 */
int on_route_resolved(struct rdma_cm_id *id);

/**
 * @brief on_disconnect
 * rdmacm on_disconnect event handler.
 * @param id id of the current event
 * @param ctx context in use
 * @return
 */
int on_disconnect(struct rdma_cm_id *id, context *ctx);

/**
 * @brief server_event_connection
 * server rdmacm event handler
 * @param event current event
 * @param ctx current context
 * @param mapIPID map of IP and ID
 * @return 0 on success
 */
int server_event_connection(struct rdma_cm_event *event, struct context *ctx, std::map <unsigned int, int> &mapIPID);

/**
 * @brief build_context_server
 * build the context for a server whith multiple clients connecting
 * @param connection_number number of clients connecting
 * @param port port used for listening
 * @param mapIPID map of IP and ID of the clients
 * @return new allocated and initialized context
 */
context *build_context_server(int connection_number, uint16_t port, std::map <unsigned int, int> &mapIPID);

/**
 * @brief client_event_connection
 * client rdmacm event handler
 * @param event current event
 * @param ctx current context
 * @param mapIPID map of IP and ID
 * @return 0 on success
 */
int client_event_connection(struct rdma_cm_event *event, struct context *ctx, std::map <unsigned int, int> &mapIPID);

/**
 * @brief build_context_client
 * build the context for a client connecting to multiple servers
 * @param mapIDPort map of ID and port of the servers
 * @param mapIPID map of IP and ID of the servers
 * @return new allocated and initialized context
 */
context *build_context_client(std::map<int, uint16_t> &mapIDPort, std::map<unsigned int, int> &mapIPID);

#endif
