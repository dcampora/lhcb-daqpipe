/**
 *      Verbs BUPolcqRU
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSBUPOLCQRU_H
#define VERBSBUPOLCQRU_H
#include "../../common/NamedPipe.h"
#include "../../common/Logging.h"
#include "../../common/GenStructures.h"
#include "../../common/Timer.h"
#include "Verbs_lib.h"
#include "VerbsCore.h"
#include <map>
#include <queue>

class VerbsBUPolCqRU
{
private:
    struct num_frag_cretid_id{
        int num_frag;
        int credit_id;
    };

    NamedPipe<Message *> *_fragment_composition_shmem;
    NamedPipe<Message *> *_free_send_msg_shmem;
    NamedPipe<int> *_completed_event_shmem;
    NamedPipe<int> *_event_received_shmem;
    //NamedPipe <int> *_completed_event_shmem;
    context *_ctx;

//    std::map<int, message **> map_ID_fragment;
//    std::map<int, int> map_ID_num_fragment;
    Message **fragments;
    std::map<int, int> ID_credit;
    std::map<int, num_frag_cretid_id> map_id_num_frag_credit;
    // TODO map for more tha 1 credit
    //std::map<int, int> ID_rmda_completed;
    std::map <int, int> map_id_fragment_rec;
    //int fragment_id;
    int frag_recv;
    int next_credit;
    std::queue<int> avail_cretits_queue;

    Timer tcompletion, twait, tcopy, ttotal, tother;
    bool print_timer;
public:
    VerbsBUPolCqRU();
    VerbsBUPolCqRU(context *ctx = NULL,
                   NamedPipe <Message *> *fragment_composition_shmem = NULL,
                   NamedPipe<Message *> *free_send_msg_shmem = NULL,
                   NamedPipe<int> *completed_event_shmem = NULL,
                   NamedPipe<int> *event_received_shmem = NULL);
    ~VerbsBUPolCqRU(){}
    void run();
    NamedPipe<Message *> *fragment_composition_shmem() const;
    void setFragment_composition_shmem(NamedPipe<Message *> *fragment_composition_shmem);
    context *ctx() const;
    void setCtx(context *ctx);
    NamedPipe<int> *completed_event_shmem() const;
    void setComplete_event_shmem(NamedPipe<int> *completed_event_shmem);
};

#endif // VERBSBUPOLCQRU_H
