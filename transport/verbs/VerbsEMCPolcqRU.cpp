/**
 *      Verbs EMCPolcqRU - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsEMCPolcqRU.h"


NamedPipe<Message *> *VerbsEMCPolCqRU::free_send_msg_shmem() const
{
    return _free_send_msg_shmem;
}

void VerbsEMCPolCqRU::setFree_send_msg_shmem(NamedPipe<Message *> *free_send_msg_shmem)
{
    _free_send_msg_shmem = free_send_msg_shmem;
}

context *VerbsEMCPolCqRU::ctx() const
{
    return _ctx;
}

void VerbsEMCPolCqRU::setCtx(context *ctx)
{
    _ctx = ctx;
}
VerbsEMCPolCqRU::VerbsEMCPolCqRU(){
}

VerbsEMCPolCqRU::VerbsEMCPolCqRU(context *ctx,
                                 NamedPipe<Message *> *free_send_msg_shmem){
    _ctx = ctx;
    _free_send_msg_shmem = free_send_msg_shmem;
}

void VerbsEMCPolCqRU::run(){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    connection *conn;
    Message *msg;
    static int ev_conter = 0;
    const int ack_number = 12;



    print_timer = false;
    while(true){
        ttotal.start();
        twait.start();
        check_nz(ibv_get_cq_event(_ctx->comp_channel, &cq, &ctx_ret), "VerbsEMCPolCqRU::run ibv_get_cq_event", true);
        twait.stop();
        while (ibv_poll_cq(cq, 1, &wc)){
            tother.start();
            ev_conter++;
            msg = (Message *) wc.wr_id;
            conn = _ctx->connections_qp_num[wc.qp_num];
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "VerbsEMCPolCqRU::run status is " << ibv_wc_status_str(wc.status) << " on qp "
                      << wc.qp_num << std::endl;
            }else if(wc.opcode == IBV_WC_SEND){
                DEBUG << "VerbsEMCPolCqRU::run message sent" << std::endl;
                msg->release_mr();
                msg->release_payload();
                while(_free_send_msg_shmem->write(msg) == -1) usleep(1);
            }else{
                WARNING << "VerbsEMCPolCqRU::run unexpected event dropping" << std::endl;
            }
            tother.stop();
        }
        if(ev_conter >= ack_number){
            ibv_ack_cq_events(cq, ev_conter);
            ev_conter = 0;
        }
        check_nz(ibv_req_notify_cq(cq, 0), "VerbsEMCPolCqRU::run ibv_req_notify_cq", true);

        ttotal.stop();
//        if (Core::Configuration->logActivated[Logger::_PROFILE] && print_timer){
//            // Timer ttotal, twait, torder, tmeta, tfragment, tsetup;
//            PROFILE << "VerbsBUPolCqRU::run " << Core::Configuration->name << " timers: " << std::endl
//                << std::setw(15) << "ttotal " << ttotal.get() << std::endl
//                << std::setw(15) << "tcompletion " << tcompletion.get() << " (" << 100.f * tcompletion.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "twait " << twait.get() << " (" << 100.f * twait.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "tcopy " << tcopy.get() << " (" << 100.f * tcopy.get() / ttotal.get() << "\%)" << std::endl
//                << std::setw(15) << "tother " << tother.get() << " (" << 100.f * tother.get() / ttotal.get() << "\%)" << std::endl
//                << std::endl;
//            print_timer = false;
//        }
        //getchar();
    }
}
