/**
 *      Verbs EMListnerPolcq
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBEMLISTNERPOLCQ_H
#define VERBEMLISTNERPOLCQ_H

#include "../../common/NamedPipe.h"
#include "../../common/Logging.h"
#include "Verbs_lib.h"
#include "VerbsCore.h"

class VerbsEMListnerPolCq
{
private:
    NamedPipe <Message *> *_no_credit_shmem;
    context *_ctx;
public:
    VerbsEMListnerPolCq(){}
    VerbsEMListnerPolCq(context *ctx);
    VerbsEMListnerPolCq(context *ctx, NamedPipe <Message *> *no_credit_shmem);
    VerbsEMListnerPolCq(NamedPipe <Message *> *no_credit_shmem);
    ~VerbsEMListnerPolCq(){}
    inline void set_context(context *ctx);
    inline void set_no_credit_shmem(NamedPipe <Message *> *no_credit_shmem);
    inline context *get_context();
    inline NamedPipe <Message *> *get_no_credit_shmem();
    void run();
};

#endif // VERBEMLISTNERPOLCQ_H
