/**
 *      Verbs BUPolqcEMC - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsBUPolqcEMC.h"

VerbsBUPolqcEMC::VerbsBUPolqcEMC(context *ctx){
    _ctx = ctx;
}
VerbsBUPolqcEMC::VerbsBUPolqcEMC(context *ctx, NamedPipe<Message *> *no_credit_shmem){
    _ctx = ctx;
    _event_assign_shmem = no_credit_shmem;
}
VerbsBUPolqcEMC::VerbsBUPolqcEMC(NamedPipe <Message *> *no_credit_shmem){
    _event_assign_shmem = no_credit_shmem;
}

inline void VerbsBUPolqcEMC::set_context(context *ctx){
    _ctx = ctx;
}

 inline void VerbsBUPolqcEMC::set_event_assign_shmem(NamedPipe <Message *> *no_credit_shmem){
    _event_assign_shmem = no_credit_shmem;
 }

inline context *VerbsBUPolqcEMC::get_context(){
    return _ctx;
}

inline NamedPipe <Message *> *VerbsBUPolqcEMC::get_event_assign_shmem(){
    return _event_assign_shmem;
}

void VerbsBUPolqcEMC::run(){
    struct ibv_cq *cq;
    struct ibv_wc wc;
    void *ctx_ret = NULL;
    connection *conn;
    Message *msg;
    int *payload;
    static int ev_conter = 0;
    const int ack_number = 18;


    while(true){
        DEBUG << "VerbsBUPolCqRU::run polling loop iteration" << std::endl;
        check_nz(ibv_get_cq_event(_ctx->comp_channel, &cq, &ctx_ret), "verbsEMListnerPolCq::run ibv_get_cq_event", true);
        DEBUG << "VerbsBUPolCqRU::run cq got" << std::endl;
        while (ibv_poll_cq(cq, 1, &wc)){
            ev_conter++;
            DEBUG << "VerbsBUPolCqRU::run poll qc loop" << std::endl;
            DEBUG << "VerbsBUPolCqRU::run MSG received form qp " << wc.qp_num << std::endl;
            msg = (Message *) wc.wr_id;
            conn = _ctx->connections_qp_num[wc.qp_num];
            DEBUG << "VerbsBUPolCqRU::run conn qp num " << conn->qp->qp_num << std::endl;
            if(wc.status != IBV_WC_SUCCESS){
                ERROR << "VerbsBUPolCqRU::run status is " << ibv_wc_status_str(wc.status) << std::endl;
                msg->release_mr();
                msg->release_payload();
                while(!msg->receive_message(conn)){
                    usleep(1);
                    WARNING << "VerbsBUPolCqRU::run post receive failed" << std::endl;
                }
            }else if(wc.opcode & IBV_WC_RECV){
                payload = (int *) msg->payload();
                DEBUG << "VerbsBUPolCqRU::run message type " << msg->type() << std::endl;
                if(msg->type() == MSG_EVENT_ASSIGN){
                    //TODO change type of num_credit_id in int*
                    while(_event_assign_shmem->write(msg) == -1) usleep(1);
                }
            }else{
                msg->release_payload();
                msg->release_mr();
                WARNING << "VerbsBUPolCqRU::run Unexpected event dropping" << std::endl;
            }
            ibv_ack_cq_events(cq, 1);
            check_nz(ibv_req_notify_cq(cq, 0), "vVerbsBUPolCqRU::run ibv_req_notify_cq", true);
        }

        if(ev_conter >= ack_number){
            ibv_ack_cq_events(cq, ev_conter);
            ev_conter = 0;
        }
        DEBUG << "VerbsBUPolCqRU::run poll qc loop end" << std::endl;
        //getchar();
    }
}
