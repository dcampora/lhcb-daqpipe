/**
 *      cache
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef CACHE_H
#define CACHE_H
#include <map>
#include <iostream>
#include <stdlib.h>
#include <stdint.h>
#include "../../common/Logging.h"

template <class Key, class Value>
class Cache
{
    typename std::map<Key, Value> _cache_map;
    int _max_cache_size;
    int _hit_number;
    int _miss_number;
    int _cache_calls;
    int hashes[128];

public:
    int cache_search(Key const& key, Value &value);
    void cache_add(Key const& key, Value const &value);
    void cache_remove(Key const& key);
    void cache_update(Key const& key, Value const &value);
    double hit_rate();
    double miss_rate();
    void reset_counters();
    Cache() : _miss_number(0), _hit_number(0), _cache_calls(0){
        for(int k = 0; k< 128 ; k++){
            hashes[k] = 0;
        }
    }
};

#include "cache_impl.h"

#endif // CACHE_H
