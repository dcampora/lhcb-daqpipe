/**
 *      Verbs Transfer - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsTransfer.h"

bool VerbsTransfer::protect_mr() const {
    return _protect_mr;
}

void VerbsTransfer::release_mr() {
    pthread_mutex_lock(_mr_mutex);
    _protect_mr = false;
    pthread_mutex_unlock(_mr_mutex);
}

void VerbsTransfer::release_payload(){
    pthread_mutex_lock(_mr_mutex);
    _protect_payload = false;
    pthread_mutex_unlock(_mr_mutex);
}

int VerbsTransfer::sender_id() const{
    return _sender_id;
}

VerbsTransfer::~VerbsTransfer(){
    pthread_mutex_lock(_mr_mutex);
    if(_local_mr != NULL){
        ibv_dereg_mr(_local_mr);
    }

   pthread_mutex_destroy(_mr_mutex);
   free(_mr_mutex);
}

void *VerbsTransfer::payload() const {
    return _payload;
}

int VerbsTransfer::size() const {
    return _size;
}

// TODO lock this
void VerbsTransfer::setSize(int size) {
    _size = size;
}
