/**
 *      Verbs EMConsumer - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsEMConsumer.h"

Verbs::EMConsumer::EMConsumer(){
}

Verbs::EMConsumer::~EMConsumer(){
}

// only push implemented so far
void Verbs::EMConsumer::initialize(){
    size_t max_send_message_size;
    int no_send_mess;
    int no_dest_units;

    std::map<unsigned int, int> *DestMapIPID;


//    bu_ctx = build_context_server(verbsCore::VerbsConfig->numnodes*verbsCore::VerbsConfig->numBuildersPerNode,
//                                  verbsCore::VerbsConfig->EMCMapIDPort[verbsCore::EMCONSUMER_ID], verbsCore::VerbsConfig->BUMapIPID);

    switch (Core::Configuration->protocol) {
    case Core::PUSH:
        DEBUG << "Verbs::VerbsConsumer::initialize PUSH protocol selected" << std::endl;
        DestMapIPID = &(Verbs::Configuration->RUMapIPID);
        no_send_mess = Verbs::Configuration->numTotalReadouts * Verbs::Configuration->initialCredits;
        no_dest_units = Verbs::Configuration->numTotalReadouts;
        max_send_message_size = 3*sizeof(int);
        break;
    case Core::PULL:
        DEBUG << "Verbs::VerbsConsumer::initialize PULL protocol selected" << std::endl;
        DestMapIPID = &(Verbs::Configuration->BUMapIPID);
        no_send_mess = Verbs::Configuration->numTotalBuilders * Verbs::Configuration->initialCredits;
        no_dest_units = Verbs::Configuration->numTotalBuilders;
        max_send_message_size = 2*sizeof(int);
        break;
    default:
        ERROR << "Verbs::VerbsConsumer::initialize unknown protocol requested aborting" << std::endl;
        exit(EXIT_FAILURE);
        break;
    }

    ru_ctx = build_context_server(no_dest_units,
                                  Verbs::Configuration->EMCMapIDPort[Verbs::EMCONSUMER_ID], *DestMapIPID);
    send_mess.resize(no_send_mess);

    std::string tmp_str = "shmem-Verbs::VerbsConsumer_free_send_msg_shmem";
    _free_send_msg_shmem = new NamedPipe <Message *> (tmp_str, no_send_mess);

    send_messages_buff = malloc(max_send_message_size*no_send_mess);
    check_alloc(send_messages_buff,"Verbs::VerbsConsumer::initialize send_messages_buff malloc", true);

    int i = 0;
    for(std::vector<Message*>::iterator it = send_mess.begin() ; it != send_mess.end() ; it++){
        *it = new Message ((char*)send_messages_buff+i++*max_send_message_size, max_send_message_size);
        while(_free_send_msg_shmem->write(*it) == -1) usleep(1);
    }

    ru_poller = new VerbsEMCPolCqRU(ru_ctx, _free_send_msg_shmem);

    startThreadWithReference(*ru_poller);

    sleep(5);
}

void Verbs::EMConsumer::sendExit(void)
{
     //currently not supported, do nothing
}

void Verbs::EMConsumer::sendToRUs(int*& size_id_bu){
    Message *send_mess;
    // INFO << "VerbsEMConsumer: sending to RUs! :D" << std::endl;
    DEBUG << "VerbsEMConsumer: sending to RUs! :D" << std::endl;

    // Iterate in RUs and send packets of {size, id, bu}
    for (int i=0; i<Verbs::Configuration->numTotalReadouts; ++i){
        DEBUG << "VerbsEMConsumer: s " << Verbs::Configuration->rankID << ", r " << Verbs::Configuration->readoutID(i)
            << ", id " << Verbs::EVENT_REQUEST << std::endl;
        
        while(_free_send_msg_shmem->read(send_mess) == -1) usleep(1);

        send_mess->setType(MSG_EVENT_ASSIGN);
        memcpy(send_mess->payload(), size_id_bu, 3*sizeof(int));
        send_mess->setSize(3*sizeof(int));
        while(!(send_mess->send_message(ru_ctx->connections_id[Verbs::Configuration->readoutID(i)]))) usleep(1);

        DEBUG << "Verbs::VerbsConsumer: event size: " << size_id_bu[0] << " eventID " << size_id_bu[1] << " dest BU "
              << size_id_bu[2] << "readout_ID " << Verbs::Configuration->readoutID(i) << std::endl;
    }
    

    DEBUG << "VerbsEMConsumer: All sent!" << std::endl;
}

// not needed in push
void Verbs::EMConsumer::sendToBU(int*& size_id, int destinationBU){
    Message *send_mess;
    DEBUG << "VerbsEMConsumer: sending to BUs! :D" << std::endl;

    // Iterate in BUs and send packets of {size, id}
    while(_free_send_msg_shmem->read(send_mess) == -1) usleep(1);

    send_mess->setType(MSG_EVENT_ASSIGN);
    memcpy(send_mess->payload(), size_id, 2*sizeof(int));
    send_mess->setSize(2*sizeof(int));
    while(!(send_mess->send_message(ru_ctx->connections_id[Verbs::Configuration->builderID(destinationBU)])))
        usleep(1);

    DEBUG << "VerbsEMConsumer: sendToBU: s " << Verbs::Configuration->rankID
          << ", r " << Verbs::Configuration->builderID(destinationBU)
          << ", id " << Verbs::EVENT_ASSIGN << std::endl;

}
