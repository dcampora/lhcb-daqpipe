/**
 *      RDMATransfer - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "rdmatransfer.h"

bool RDMATransfer::setPayload(void *payload)
{
    bool ret_val;
    bool found = false;
    pthread_mutex_lock(_mr_mutex);

    DEBUG << "RDMATransfer::setPayload" << std::endl;
    if(!_protect_payload){
        _protect_payload = true;
        _payload = payload;
        ret_val = true;
    }else{
        DEBUG << "RDMATransfer::setPayload payload busy" << std::endl;
        ret_val = false;
    }

    pthread_mutex_unlock(_mr_mutex);
    return ret_val;
}

ibv_mr RDMATransfer::peer_mr() const
{
    return _peer_mr;
}

void RDMATransfer::set_peer_mr(ibv_mr &peer_mr)
{
    _peer_mr = peer_mr;
}

ibv_mr *RDMATransfer::searchStaticMR(){
    ibv_mr *ret_val = NULL;
    for(std::vector<ibv_mr*>::iterator it = _static_local_mr.begin() ; it != _static_local_mr.end() ; it++){
        if(!((_payload < (*it)->addr) || ((char*)_payload + _size) > ((char*) (*it)->addr +  (*it)->length))){
            DEBUG << "RDMATransfer::searchStaticMR mr found in static_mr list" << std::endl;
            ret_val = *it;
            break;
        }
    }
    return ret_val;
}

void RDMATransfer::update_mr(struct connection *conn){
    ibv_mr *static_mr;
    static_mr = searchStaticMR();
    if(_local_mr == NULL){
        DEBUG << "RDMATransfer::update_mr _local_mr NULL creating new mr" << std::endl;
        if (static_mr != NULL){
            DEBUG << "RDMATransfer::update_mr found static" << std::endl;
           _local_mr = static_mr;
           static_mr_use = true;
        }else{
            _local_mr = ibv_reg_mr(conn->pd,_payload,_size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
           static_mr_use = false;
        }
        check_alloc(_local_mr, "RDMATransfer::update_mr _local_mr ibv_reg_mr", true);
    }else if((_local_mr->pd != conn->pd)){
        DEBUG << "RDMATransfer::update_mr _local_mr out of date updating" << std::endl;
        if (!static_mr_use){
            ibv_dereg_mr(_local_mr);
        }
        _local_mr = ibv_reg_mr(conn->pd,_payload,_size, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
        check_alloc(_local_mr, "RDMATransfer::update_mr _local_mr ibv_reg_mr", true);
    }else if(static_mr != NULL){
       DEBUG << "RDMATransfer::update_mr _local_mr found" << std::endl;
       if(!static_mr_use){
           ibv_dereg_mr(_local_mr);
       }

       _local_mr = static_mr;
    }else{
        DEBUG << "RDMATransfer::update_mr _local_mr up to date" << std::endl;
    }
}

void RDMATransfer::add_static_mr(ibv_mr *mr){
    DEBUG << "RDMATransfer::add_static_m mr: " << (void*) mr << std::endl;
    _static_local_mr.push_back(mr);
}

int RDMATransfer::remove_static_mr(ibv_mr *mr){
    int ret_val;

    bool found=false;

    pthread_mutex_lock(_mr_mutex);
    if(!_protect_payload || !static_mr_use){
        for(std::vector<ibv_mr*>::iterator it = _static_local_mr.begin() ; it != _static_local_mr.end() ; it++){
            if(*it == mr){
                if(*it == _local_mr){
                    _local_mr = NULL;
                }
                _static_local_mr.erase(it);
                found = true;
                break;
            }
        }
        if(found){
            ret_val = 0;
        }else{
            ret_val = -2;
        }
    }else{
        ret_val = -1;
    }

    pthread_mutex_unlock(_mr_mutex);

    return ret_val;
}

bool RDMATransfer::rdma_write(connection *conn){
    struct ibv_send_wr wr, *bad_wr = NULL;
    struct ibv_sge sge;

    bool ret_val;

    pthread_mutex_lock(_mr_mutex);

    if(_protect_mr){
        ret_val = false;
    }else{

        DEBUG << "RDMATransfer::rdma_write rdma write in progress" << std::endl;
        if(_size > _peer_mr.length){
            WARNING << "RDMATransfer::rdma_write requested size to big aborting" << std::endl;
            ret_val = false;
        }else{

            update_mr(conn);

            memset(&wr, 0, sizeof(wr));

            wr.wr_id = (uintptr_t)this;
            wr.opcode = IBV_WR_RDMA_WRITE;
            wr.sg_list = &sge;
            wr.num_sge = 1;
            wr.send_flags = IBV_SEND_SIGNALED;
            wr.wr.rdma.remote_addr = (uintptr_t) _peer_mr.addr;
            wr.wr.rdma.rkey = _peer_mr.rkey;

            sge.addr = (uintptr_t)_payload;
            sge.length = _size;
            sge.lkey = _local_mr->lkey;

            check_nz(ibv_post_send(conn->qp, &wr, &bad_wr), "RDMATransfer::rdma_write ibv_post_send", true);

            _protect_mr = true;
            _sender_id = conn->connection_number;

            DEBUG << "RDMATransfer::rdma_write remote addr:" << (void*) _peer_mr.addr << " size " << _size << std::endl;
            ret_val = true;
        }
    }
    pthread_mutex_unlock(_mr_mutex);

    return ret_val;
}

bool RDMATransfer::rdma_write(void *payload, int size, connection *conn){
    if(!setPayload(payload)){
        return false;
    }

    _size = size;

    return rdma_write(conn);
}

bool RDMATransfer::rdma_write(void *payload, int size, ibv_mr &peer_mr, connection *conn){
    _peer_mr = peer_mr;

    return rdma_write(payload, size, conn);

}

bool RDMATransfer::rdma_write(ibv_mr &peer_mr, connection *conn){
    _peer_mr = peer_mr;

    return rdma_write(conn);
}
