/**
 *      Verbs BUPolqcEMC
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#ifndef VERBSBUPOLCQEMC_H
#define VERBSBUPOLCQEMC_H

#include "../../common/NamedPipe.h"
#include "../../common/Logging.h"
#include "Verbs_lib.h"
#include "VerbsCore.h"

class VerbsBUPolqcEMC
{
private:
    NamedPipe <Message *> *_event_assign_shmem;
    context *_ctx;
public:
    VerbsBUPolqcEMC(){}
    VerbsBUPolqcEMC(context *ctx);
    VerbsBUPolqcEMC(context *ctx, NamedPipe <Message *> *no_credit_shmem);
    VerbsBUPolqcEMC(NamedPipe <Message *> *no_credit_shmem);
    ~VerbsBUPolqcEMC(){}
    inline void set_context(context *ctx);
    inline void set_event_assign_shmem(NamedPipe <Message *> *no_credit_shmem);
    inline context *get_context();
    inline NamedPipe <Message *> *get_event_assign_shmem();
    void run();
};

#endif // VERBSBUPOLCQEMC_H
