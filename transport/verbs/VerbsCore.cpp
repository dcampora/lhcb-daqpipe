/**
 *      Verbs Core - impl
 *
 *      author  -   Flavio Pisani
 *      email   -   flavio.pisani@cern.ch
 *
 *      May, 2014
 *      CERN
 */

#include "VerbsCore.h"

void Verbs::ConfigurationLoader::initialize(int argc, char *argv[]){
    // There is no multithreaded MPI support on 
    // the openib BTL, so we cannot use MPI_THREAD_MULTIPLE here
    //threadSupport = MPI::Init_thread(argc, argv, MPI_THREAD_SINGLE);

    // MPI::COMM_WORLD.Set_errhandler(MPI::ERRORS_THROW_EXCEPTIONS);
    //rankID = MPI::COMM_WORLD.Get_rank();
    //numprocs = MPI::COMM_WORLD.Get_size();
    // TODO: Update. Kind of hack
    numProcsPerNode = 4;
    if(interface == Core::ETHERNET){
        numprocs = config_root.get("number of ethernet nodes", 0).asInt() * numProcsPerNode;
    }else if(interface == Core::INFINIBAND){
        numprocs = config_root.get("number of infiniband nodes", 0).asInt() * numProcsPerNode;
    }

    // TODO: Configurable
    // sqName = "/sharedqueue";

    logicalNames[Verbs::EVENT_MANAGER_LISTENER] = "eml";
    logicalNames[Verbs::EVENT_MANAGER_CONSUMER] = "emc";
    logicalNames[Verbs::BUILDER_UNIT] = "bu";
    logicalNames[Verbs::READOUT_UNIT] = "ru";

}

void Verbs::ConfigurationLoader::parseConfiguration(){
    numnodes = (int) (numprocs / (float) numProcsPerNode);
    gethostname(name, HOST_NAME_MAX);

    sync_server_ip = config_root.get("sync server ip", "").asString();
    sync_server_port = config_root.get("sync server port", "").asString();
    pub_server_port = config_root.get("pub server port", "").asString();

    rankID = zmq_handshake_sub(sync_server_ip, pub_server_port, sync_server_port);
    if (rankID < 0){
        ERROR <<"Verbs::ConfigurationLoader::parseConfiguration zmq_handshake failed aborting" << std::endl;
        exit(EXIT_FAILURE);
    }


    procID = rankID % numProcsPerNode;
    emlistenerID = Verbs::EMLISTENER_ID;
    emconsumerID = Verbs::EMCONSUMER_ID;

    Core::Configuration->name = name;
    Core::Configuration->numnodes = numnodes;

    ru_startid = Verbs::READOUT_UNIT;
    ru_endid = Verbs::READOUT_UNIT + numReadoutsPerNode - 1;
    bu_startid = ru_endid + 1;
    bu_endid = bu_startid + numBuildersPerNode - 1;

    numTotalReadouts = numnodes * numReadoutsPerNode;
    numTotalBuilders = numnodes * numBuildersPerNode;

    if (logToFile)
        logfile.open(std::string(logdir + getLogicalName() + std::string(".log")).c_str());
    Logger::logger(0) << std::setprecision(2) << std::fixed;

    initializeIPs();
    differentiateShmemNames();

}
void Verbs::ConfigurationLoader::differentiateShmemNames(){
    for (std::vector<std::string*>::iterator it = buShmemBuffers.begin(); it != buShmemBuffers.end(); it++)
        *(*it) += builderSuffix();

    for (std::vector<std::string*>::iterator it = ruShmemBuffers.begin(); it != ruShmemBuffers.end(); it++)
        *(*it) += readoutSuffix();
}

void Verbs::ConfigurationLoader::initializeIPs(){
    // IPs and ports
    std::map<int, unsigned int> nodeIP;
    std::string ip_string;
    int index = 0;
    Json::Value hosts;
    Json::Value int_type;
    Json::Value ip_list;

    switch (interface) {
    case Core::INFINIBAND:
        int_type = config_root["infiniband capabitilies"];
        break;
    case Core::ETHERNET:
        int_type = config_root["ethernet capabitilies"];
        break;
    default:
        break;
        ERROR << "VerbsConfigurationLoader::initializeIPs: Undefined interface type aborting" << std::endl;
        exit(EXIT_FAILURE);
    }
    hosts = int_type["hosts"];
    for (int k = 0 ; k<hosts.size() ; k++){
        ip_list = int_type[hosts[k].asString().c_str()]["ip list"];
        if(ip_list.size() <= 1){
            nodeIP[k] = inet_addr(ip_list[0].asString().c_str());
        }else{
            WARNING << "VerbsConfigurationLoader::initializeIPs: multiple interfaces found" << std::endl;
            for(int j=0 ; j < ip_list.size() ; j++){
                WARNING << "addr " << j << " " << ip_list[j].asString() << std::endl;
            }
            WARNING << "Using the first addr in the list " << ip_list[0].asString() << std::endl;
            nodeIP[k] = inet_addr(ip_list[0].asString().c_str());
        }
    }

    for (int i=0; i<numprocs; ++i){
        int pid = i % numProcsPerNode;

        if (i == emlistenerID){
            EMLMapIDIP[i] = nodeIP[0];
            EMLMapIPID[nodeIP[0]] = i;
            EMLMapIDPort[i] = startingPort;
        }
        else if (i == emconsumerID){
            EMCMapIDIP[i] = nodeIP[0];
            EMCMapIPID[nodeIP[0]] = i;
            EMCMapIDPort[i] = startingPort + 1;
            DEBUG << "emconsumer port " << EMCMapIDPort[i] << std::endl;
        }
        else if((pid != emconsumerID) && (pid != emlistenerID)) {
            int nodeno = nodeNumber(i);

            if (pid < Verbs::READOUT_UNIT + numReadoutsPerNode){
                int readoutno = readoutNumber(i);
                RUMapIDIP[i] = nodeIP[nodeno];
                RUMapIPID[nodeIP[nodeno]] = i;
                RUMapIDPort[i] = startingPort + ru_startid + (readoutno % numReadoutsPerNode);
                DEBUG << "RU node " << nodeno << " id " << i << " port " << RUMapIDPort[i] << std::endl;
            }
            else if (pid >= Verbs::BUILDER_UNIT){
                int builderno = builderNumber(i);
                BUMapIDIP[i] = nodeIP[nodeno];
                BUMapIPID[nodeIP[nodeno]] = i;
                BUMapIDPort[i] = startingPort + bu_startid + (builderno % numBuildersPerNode);
                DEBUG << "BU node " << nodeno << " id " << i << " port " << BUMapIDPort[i] << std::endl;
            }
        }
    }

    for(std::map<int, unsigned int>::iterator it=BUMapIDIP.begin() ; it != BUMapIDIP.end() ; it++){
        DEBUG << "BU id " << it->first << " IP " << it->second << ":" << BUMapIDPort[it->first] <<std::endl;
    }

    for(std::map<int, unsigned int>::iterator it=RUMapIDIP.begin() ; it != RUMapIDIP.end() ; it++){
        DEBUG << "RU id " << it->first << " IP " << it->second << ":" << RUMapIDPort[it->first] <<std::endl;
    }
}

bool Verbs::ConfigurationLoader::isEMListener(){
    return rankID == emlistenerID;
}

bool Verbs::ConfigurationLoader::isEMConsumer(){
    return rankID == emconsumerID;
}

bool Verbs::ConfigurationLoader::isRU(){
    return procID >= ru_startid && procID <= ru_endid;
}

bool Verbs::ConfigurationLoader::isBU(){
    return procID >= bu_startid && procID <= bu_endid;
}

bool Verbs::ConfigurationLoader::is(int id){
    DEBUG << "Verbs::ConfigurationLoader::is: " << id << std::endl;

    if (id == Verbs::BUILDER_UNIT) return isBU();
    else if (id == Verbs::READOUT_UNIT) return isRU();
    return id == procID;
}

void Verbs::ConfigurationLoader::finalize(){
}

int Verbs::ConfigurationLoader::builderID(int builderNumber){
    return bu_startid + (builderNumber % numBuildersPerNode) +
        (builderNumber / numBuildersPerNode) * numProcsPerNode;
}

int Verbs::ConfigurationLoader::readoutID(int readoutNumber){
    return ru_startid + (readoutNumber % numReadoutsPerNode) +
        (readoutNumber / numReadoutsPerNode) * numProcsPerNode;
}

int Verbs::ConfigurationLoader::builderNumber(int builderID){
    return (builderID / numProcsPerNode) * numBuildersPerNode +
        ((builderID - bu_startid) % numProcsPerNode);
}

int Verbs::ConfigurationLoader::readoutNumber(int readoutID){
    return (readoutID / numProcsPerNode) * numReadoutsPerNode +
        ((readoutID - ru_startid) % numProcsPerNode);
}

bool Verbs::ConfigurationLoader::readoutInSameNode(int readoutNumber){
    return nodeNumber(rankID) == nodeNumber(readoutID(readoutNumber));
}

bool Verbs::ConfigurationLoader::builderInSameNode(int builderNumber){
    // I'm a RU, I want to know if the builder builderNumber is on the same node as me.
    // Simple. Simplest!
    return nodeNumber(rankID) == nodeNumber(builderID(builderNumber));
}

int Verbs::ConfigurationLoader::nodeID(){
    return rankID;
}

int Verbs::ConfigurationLoader::nodeNumber(int _rankID){
    return _rankID / numProcsPerNode;
}

std::string Verbs::ConfigurationLoader::getLogicalName(){
    std::string post = std::string("_") + name;

    if (isEMListener()) return logicalNames[Verbs::EVENT_MANAGER_LISTENER] + post;
    else if (isEMConsumer()) return logicalNames[Verbs::EVENT_MANAGER_CONSUMER] + post;
    else if (is(Verbs::READOUT_UNIT)) return logicalNames[Verbs::READOUT_UNIT] + readoutSuffix() + post;
    else if (is(Verbs::BUILDER_UNIT)) return logicalNames[Verbs::BUILDER_UNIT] + builderSuffix() + post;
    else return std::string("p") + Tools::toString<int>(rankID);
}

std::string Verbs::ConfigurationLoader::builderSuffix(){
    if (numBuildersPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(builderNumber(rankID));
    }
}

std::string Verbs::ConfigurationLoader::readoutSuffix(){
    if (numReadoutsPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(readoutNumber(rankID));
    }
}
