
/**
 *      MPI BU - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "tcpBU.h"


    TCP::BuilderUnit::BuilderUnit(){};
    TCP::BuilderUnit::~BuilderUnit(){free(size_id);}

void TCP::BuilderUnit::initialize(int initialCredits,  void *sh_meta_pmem, void *sh_data_pmem){
    //setting up receiving sockets
        int yes = 1;
        initialCredits = initialCredits;
        activeRequests.resize(initialCredits, -1);
        requestIndex.resize(initialCredits, 0);   
        transmissionSize.resize(initialCredits, 0);
        requestReadoutIndex.resize(initialCredits);
        new_BUsock.resize(Core::Configuration->numnodes,0);
        new_BUsock2.resize(Core::Configuration->numnodes,0);
        sockBUpl.resize(Core::Configuration->numnodes,0);
        sockBUSender=0;
       /* conn_tmp.resize(Core::Configuration->numnodes);
        thread.resize(Core::Configuration->numnodes,0);*/ //threading

        for (int i=0; i<requestReadoutIndex.size(); ++i)
            requestReadoutIndex[i].resize(TCP::Configuration->numnodes, 0);

    DEBUG << "MPIBuilderUnit: Reserved fragments, meta and data for " << TCP::Configuration->numnodes << " nodes;"
          << " data buffer: " << Core::Configuration->BUDataBufferSize << ", meta buffer: " << Core::Configuration->BUMetaBufferSize << " (B)" << std::endl;

    // Reserve size and id for sync recvs
        size_id = (int*) malloc(2 * sizeof(int));
        tstart = time(NULL);
    
        memset(&hints_BUReceiver, 0, sizeof hints_BUReceiver);
        hints_BUReceiver.ai_family = AF_INET;
        hints_BUReceiver.ai_socktype = SOCK_STREAM;
        hints_BUReceiver.ai_flags = AI_PASSIVE; // use my IP
      

        if ((rv_BUReceiver = getaddrinfo(NULL, TCP::Configuration->BUMapIDPort_fc[TCP::Configuration->rankID].c_str(), &hints_BUReceiver, &servinfo_BUReceiver)) != 0) {
            fprintf(stderr, "[BUListener] getaddrinfo: %s\n", gai_strerror(rv_BUReceiver));
          
        }

        for(p_BUReceiver = servinfo_BUReceiver; p_BUReceiver != NULL; p_BUReceiver = p_BUReceiver->ai_next) {
            if ((sockBUListener = socket(p_BUReceiver->ai_family, p_BUReceiver->ai_socktype,
                    p_BUReceiver->ai_protocol)) == -1) {
                perror("[BUListener]server: socket");
                continue;
            }

            if (setsockopt(sockBUListener, SOL_SOCKET, SO_REUSEADDR, &yes,
                    sizeof(int)) == -1) {
                perror("[BUListener]setsockopt");
                continue;
            }

            if (bind(sockBUListener, p_BUReceiver->ai_addr, p_BUReceiver->ai_addrlen) == -1) {
                close(sockBUListener);
                perror("[BUListener]server: bind");
                continue;
            }

            break;
        }

        if (p_BUReceiver== NULL)  {
            fprintf(stderr, "[BUListener]server: failed to bind\n");
           
        }

        freeaddrinfo(servinfo_BUReceiver); // all done with this structure

        if (listen(sockBUListener, 65000) == -1) {
            perror("[BUListener]listen");
            
        }

// SETTING UP SECOND LISTENER##############################################################################################
        memset(&hints_BUReceiver2, 0, sizeof hints_BUReceiver2);
        hints_BUReceiver2.ai_family = AF_INET;
        hints_BUReceiver2.ai_socktype = SOCK_STREAM;
        hints_BUReceiver2.ai_flags = AI_PASSIVE; // use my IP
      

        if ((rv_BUReceiver2 = getaddrinfo(NULL, TCP::Configuration->BUMapIDPort_SendEvent[TCP::Configuration->rankID].c_str(), &hints_BUReceiver2, &servinfo_BUReceiver2)) != 0) {
            fprintf(stderr, "[BUListener] getaddrinfo: %s\n", gai_strerror(rv_BUReceiver2));
          
        }

        for(p_BUReceiver2 = servinfo_BUReceiver2; p_BUReceiver2 != NULL; p_BUReceiver2 = p_BUReceiver2->ai_next) {
            if ((sockBUListener2 = socket(p_BUReceiver2->ai_family, p_BUReceiver2->ai_socktype,
                    p_BUReceiver2->ai_protocol)) == -1) {
                perror("[BUListener]server: socket");
                continue;
            }

            if (setsockopt(sockBUListener2, SOL_SOCKET, SO_REUSEADDR, &yes,
                    sizeof(int)) == -1) {
                perror("[BUListener]setsockopt");
                continue;
            }

            if (bind(sockBUListener2, p_BUReceiver2->ai_addr, p_BUReceiver2->ai_addrlen) == -1) {
              //  close(sockBUListener2);
                perror("[BUListener]server: bind");
                continue;
            }

            break;
        }

        if (p_BUReceiver2== NULL)  {
            fprintf(stderr, "[BUListener]server: failed to bind\n");
           
        }

        freeaddrinfo(servinfo_BUReceiver2); // all done with this structure

        if (listen(sockBUListener2, 65000) == -1) {
            perror("[BUListener]listen");
            
        }


///////////////////////////////////////////////////////BU RECV SIZE AND ID/////////////////////////////////////////////////////
                memset(&hintsBU, 0, sizeof hintsBU);
                hintsBU.ai_family = AF_INET;
                hintsBU.ai_socktype = SOCK_STREAM;// use TCP/IP
                hintsBU.ai_flags = AI_PASSIVE; // use my IP

                
                if ((rvBU = getaddrinfo(NULL, TCP::Configuration->BUMapIDPort_SizeID[TCP::Configuration->rankID].c_str(), &hintsBU, &servinfoBU)) != 0) {//tmp  port to be decided
                        fprintf(stderr, "[BU]getaddrinfo: %s\n", gai_strerror(rvBU));
                    }

                    for(p_BU = servinfoBU; p_BU != NULL; p_BU = p_BU->ai_next) {
                        if ((sockfdBU = socket(p_BU->ai_family, p_BU->ai_socktype,
                                p_BU->ai_protocol)) == -1) {
                            perror("[RU]server: socket");
                            continue;
                        }

                        if (setsockopt(sockfdBU, SOL_SOCKET, SO_REUSEADDR, &yes,
                                sizeof(int)) == -1) {
                            perror("[RU]:setsockopt");
                            continue;
                        }

                        if (bind(sockfdBU, p_BU->ai_addr, p_BU->ai_addrlen) == -1) {
                           // close(sockfdRU);
                            perror("[RU]server: bind");
                            continue;
                        }

                        break;
                        }
                    if (p_BU == NULL)  {
                            fprintf(stderr, "[RU] Server: failed to bind\n");
                        }
                        freeaddrinfo(servinfoBU); // all done with this structure
                    if (listen(sockfdBU , 65000) == -1) { 
                            perror("[BU]listen");

                        }
                size_id = (int*) malloc(2 * sizeof(int));
                bf_ptr = (int *) malloc(2 * sizeof(int));
}

void TCP::BuilderUnit::sendCredits(int& numberOfCredits){
     DEBUG<<"[BU]BUILDER UNIT ID "<<TCP::Configuration->builderNumber(TCP::Configuration->rankID)<<"RANK ID "<<TCP::Configuration->rankID<<std::endl;
   
        if(sockBUSender==0){
        //int yes_c = 1;
        memset(&hints_BUSender, 0, sizeof hints_BUSender);
        hints_BUSender.ai_family = AF_INET;
        hints_BUSender.ai_socktype = SOCK_STREAM;
        if ((rv_BUSender = getaddrinfo(TCP::Configuration->EMLMapIDIP[0].c_str(), TCP::Configuration->EMLMapIDPort[0].c_str(), &hints_BUSender, &servinfo_BUSender)) != 0) { //provisional
        fprintf(stderr, "[BUSender]getaddrinfo: %s\n", gai_strerror(rv_BUSender));

        }
        for(p_BUSender = servinfo_BUSender; p_BUSender != NULL; p_BUSender = p_BUSender->ai_next) {
        if ((sockBUSender= socket(p_BUSender->ai_family, p_BUSender->ai_socktype,
                        p_BUSender->ai_protocol)) == -1) {
                    perror("[BUSender]client: socket");
                    continue;
              }

          while(connect(sockBUSender, p_BUSender->ai_addr, p_BUSender->ai_addrlen) == -1) {
               //close(sockBUSender);
               perror("[BUSender]Client: connect");
               continue;
                }

                break;
            }
        if (p_BUSender == NULL) {
                fprintf(stderr, "[BUSender]Client: failed to connect\n");
                
                        }

          //    if (setsockopt(sockBUSender,IPPROTO_TCP, TCP_CORK,(char *)&yes_c,sizeof(int))==-1)perror("[RU]:setsockopt");

    freeaddrinfo(servinfo_BUSender);
  }
    int node =  TCP::Configuration->rankID;
    bf_ptr[0] = numberOfCredits;
    bf_ptr[1] = node;

	if ((send(sockBUSender, bf_ptr, 2*sizeof(int), 0)) == -1) {
            perror("[BUSender]: send");
	    }


DEBUG<<"sended "<<numberOfCredits<<" credits  from node "<<node<<" to EM listener. "<<std::endl;


}

void TCP::BuilderUnit::pullRequest(int* size_id_bu, int& readoutno){
    DEBUG << "[BU]: pullRequest source rankID " << TCP::Configuration->rankID << " to readout ID " << TCP::Configuration->readoutID(readoutno)
        << ", id " << TCP::PULL_REQUEST<< " of IP : "<< TCP::Configuration->RUMapIDIP[TCP::Configuration->readoutID(readoutno)].c_str()<< std::endl;

        if(sockBUpl[readoutno]==0){//checking if socket is already open, if not, connects
        //int yes_c = 1;
        memset(&hints_BUpl, 0, sizeof hints_BUpl);
        hints_BUpl.ai_family = AF_INET;
        hints_BUpl.ai_socktype = SOCK_STREAM;
        if ((rv_BUpl = getaddrinfo(TCP::Configuration->RUMapIDIP[TCP::Configuration->readoutID(readoutno)].c_str(), TCP::Configuration->RUMapIDPort[TCP::Configuration->readoutID(readoutno)].c_str(), &hints_BUpl, &servinfo_BUpl)) != 0) { //provisional
        fprintf(stderr, "[BU] pl: getaddrinfo: %s\n", gai_strerror(rv_BUpl));
        }
        for(p_BUpl = servinfo_BUpl; p_BUpl != NULL; p_BUpl = p_BUpl->ai_next) {
        if ((sockBUpl[readoutno]= socket(p_BUpl->ai_family, p_BUpl->ai_socktype,
                        p_BUpl->ai_protocol)) == -1) {
                    perror("[BU] pl: client: socket");
                    continue;
              } 
          while(connect(sockBUpl[readoutno], p_BUpl->ai_addr, p_BUpl->ai_addrlen) == -1) {

               perror("[BU] pullRequest: Client: connect");
               continue;
                }

                break;
            }
        if (p_BUpl == NULL) {
                fprintf(stderr, "[BU]pl: Client: failed to connect\n");
                
                        }

          //    if (setsockopt(sockBUSender,IPPROTO_TCP, TCP_CORK,(char *)&yes_c,sizeof(int))==-1)perror("[RU]:setsockopt");

    freeaddrinfo(servinfo_BUpl);
  }

    if ((send(sockBUpl[readoutno], size_id_bu, 3*sizeof(int), 0)) == -1) {
            perror("[BU]pl: : send");}

        DEBUG<<"[BU] pullRequest sended. eventSize : "<<size_id_bu[0]<< " eventID :  "<< size_id_bu[1] << " destination BU :  "<< size_id_bu[2]<<std::endl;

}

void TCP::BuilderUnit::receiveSizeAndID(int& eventSize, int& eventID){
            int sum =0;
            int yes_fc=1;
    if(new_fdBU==0){
            sin_size_BU = sizeof their_addr_BU;

            new_fdBU = accept(sockfdBU, (struct sockaddr *)&their_addr_BU, &sin_size_BU);
            if (new_fdBU == -1) {perror("[BU Listener] accept");}


            //fcntl(new_fdRU , F_SETFL, O_NONBLOCK); //nonblocking optio
       //  if (setsockopt(new_fdBU,IPPROTO_TCP, TCP_CORK,(char *)&yes_fc,sizeof(int))==-1)perror("[RU]:setsockopt");
                        
            inet_ntop(their_addr_BU.ss_family,(&((struct sockaddr_in *)&their_addr_BU)->sin_addr) ,S_toBU, sizeof S_toBU);
            DEBUG<<"[BU] Server: got connection from EMConsumer "<<S_toBU<<std::endl;

         }
             
             //close(sockfdRU);

             if ((numbytesBU = recv(new_fdBU, size_id,(2 * sizeof(int)), 0)) < 0) {perror("[RU Listener] recv"); }//break; }
             
            eventSize = size_id[0];
            eventID = size_id[1];

            DEBUG<<"[BU]Size : "<<eventSize<<"eventID: "<<eventID<<std::endl;


}



void TCP::BuilderUnit::prepareReceiveEvent(FragmentComposition* fc){
    int slot;
    tmp = 0 ;
    fc_ptr = fc;
    DEBUG<<"[BUListener]prepareReceiveEvent, numnodes: "<< TCP::Configuration->numnodes <<std::endl;


    for (slot=0; slot<activeRequests.size(); ++slot) 
    if(activeRequests[slot] == -1) break;
    
    if (slot == activeRequests.size()){
        ERROR << "MPIBuilderUnit: prepareReceiveEvent can't listen because there are no free slots. "
            << activeRequests.size() << " active" << std::endl;
        return;
    }
    DEBUG << "MPIBuilderUnit: prepareReceiveEvent listening on slot " << slot << std::endl;

    
    




/*         
    DEBUG<<"[BU] metablocks : "<<fc[i].metablocks<<" datablocks: "<<fc[i].datablocks<<" total number of parts: "<<(fc[i].metablocks+fc[i].datablocks)<<std::endl;
    num_of_parts[i]=(fc[i].metablocks+fc[i].datablocks);   */ //datablock threading
    
}





bool TCP::BuilderUnit::prepareReceiveEventComplete(int& fragmentReceived){
int slot;
bool ret_val;
while(1){
for (int i=tmp; i<TCP::Configuration->numTotalReadouts; ++i){

            DEBUG << "MPIBuilderUnit: prepareReceiveEvent s " << TCP::Configuration->readoutID(i) << ", r " << TCP::Configuration->rankID
                << ", id " << TCP::EVENT_MESSAGES_SIZE + TCP::EVENT_FRAGMENT_COMPOSITION << std::endl;


            activeRequestsList.push_back(slot);
            if(new_BUsock[i]==0){
            fcntl(new_BUsock[i] , F_SETFL, O_NONBLOCK);
            sinBU_size = sizeof connectorBU_addr;
            new_BUsock[i]= accept(sockBUListener, (struct sockaddr *)&connectorBU_addr, &sinBU_size);
            if (new_BUsock[i] == -1) {perror("[BUListener]: accept");}
            inet_ntop(connectorBU_addr.ss_family,(&((struct sockaddr_in *)&connectorBU_addr)->sin_addr) ,sBU, sizeof sBU);
            DEBUG << "[BUListener] Server: prepareReceiveEventComplete got connection from  \n" << sBU <<std::endl;
            }  

                if((rv_BUReceiver2=recv(new_BUsock[i], (fc_ptr+i), sizeof(BUdataBuffer), 0))<0){continue; //perror("recv");//
                
                }else{  
                        fragmentReceived = i;
                        tmp+=1;
                        DEBUG << "[BUListener]Connection ID:  "<< i <<"   Server: prepareReceiveEventComplete got connection from " << sBU 
                        <<" and received "<<rv_BUReceiver2<<" bytes of FragmentComposition"<<std::endl;
                         ret_val=1;

                        return ret_val;//ret_val;
                }

                
    }
 tmp =0;
}
}

void TCP::BuilderUnit::setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype){
       
    int slot=0;
    /*int yes=1;
    int no=0;*/ //used for setting tcp socket options
    int sum = 0;

    DEBUG<<"[BUListener] setupReceiveEvent. size "<<size<<"from "<<readoutIndex<<"readoutIndex"<<std::endl;

      // for (int j=0; j</*TCP::Configuration->numnodes*/1; ++j){
            sinBU_size2 = sizeof connectorBU_addr2;

           if(new_BUsock2[readoutIndex]==0){            

           // fcntl(new_BUsock2[readoutIndex] , F_SETFL, O_NONBLOCK);
            new_BUsock2[readoutIndex]= accept(sockBUListener2, (struct sockaddr *)&connectorBU_addr2, &sinBU_size2);
            if(new_BUsock2[readoutIndex] == -1)perror("[BUListener]: accept");
            

            

            inet_ntop(connectorBU_addr.ss_family,(&((struct sockaddr_in *)&connectorBU_addr2)->sin_addr) ,sBU2, sizeof sBU2);
            DEBUG<<"[BUListener] Server: got connection from "<< sBU2<<std::endl;
            }

 //if (setsockopt(new_BUsock2[readoutIndex],IPPROTO_TCP, TCP_NODELAY,(char *) &no,sizeof(int)) == -1)perror("[RU]:setsockopt");


/*int tcp_win = getsock_tcp_windowsize( new_BUsock2[readoutIndex], size );
printf("[BUListener] TCP WINDOW SIZE is %d \n", tcp_win);*/ //ipperf tcp window  resizing function
//structures for datablock threading
/*            conn_tmp[seq].new_socket=new_BUsock2[(readoutIndex*5)+seq];
            conn_tmp[seq].buffer=dataPointer;
            conn_tmp[seq].size= size;
            pthread_create(&thread[seq], NULL,receiver, &conn_tmp[seq]);*/


while(sum<size){

if((rv_BUReceiver3 = recv(new_BUsock2[readoutIndex], dataPointer+sum,size-sum,0))<0)perror("[BU] recv event:");
sum+=rv_BUReceiver3;
}



//splcing through kernel
/*pipe(pfd);
while(sum<size){
if((rv_BUReceiver3=splice(new_BUsock2[readoutIndex], NULL,pfd[1], NULL, size-sum,SPLICE_F_MORE))==-1)perror("[BU] splice:");
read(pfd[0],dataPointer+sum,size-sum);
sum+=rv_BUReceiver3;
}
close(pfd[1]);
close(pfd[0]);*/



    DEBUG<<"[BU] received "<<sum<<" bytes "<<std::endl;




    int shift = readoutIndex * 0x01000000;

    activeRequests[slot]=eventID; 
    requestIndex[slot]++;
    requestReadoutIndex[slot][readoutIndex]++; 
    transmissionSize[slot] += sum;
    //datablocks threading -> thread per sended block
 //  if(seq==(total_parts-1)){for(int f = 0 ; f < total_parts; f++ )pthread_join(thread[f], NULL);}

  //  seq+=1;

}

bool TCP::BuilderUnit::receiveEvent(int& finishedEventID, int& tSize){ 
        int slot=0;
        finishedEventID = activeRequests[slot];
        tSize = transmissionSize[slot];
  //  printf(" tSize : %d   \n", tSize);
    //if (seq == (total_parts-1)){for(int f = 0 ; f < total_parts; f++ )pthread_join(thread[f], NULL);}
     //   for(int f = 0 ; f < total_parts; f++ )pthread_join(thread[f], NULL);
        activeRequests[slot] = -1;
        requestIndex[slot] = 0;
        transmissionSize[slot] = 0;
        
        return true;
}


void TCP::BuilderUnit::discardEvent(){
    int slot;
     activeRequests[slot] = -1;
    requestIndex[slot] = 0;
    transmissionSize[slot] = 0;
}

bool TCP::BuilderUnit::checkExitMessage(void)
{
	return false;
}
