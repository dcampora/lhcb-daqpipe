
/**
 *      RU transport core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef TCPRU
#define TCPRU 1
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <ifaddrs.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <sys/sendfile.h>
#include <unistd.h>
#include <fcntl.h>
#include "../common/RUTransportCore.h"
#include "TCPCore.h"
#include "c_utils.h" //usefull functions for threading

namespace TCP {
//#define SFV_FD_SELF (-2)
class ReadoutUnit : public RUTransportCore{
private:
	struct addrinfo hintsRU, *servinfoRU, *p_RU, hintsRUtoBU, *servinfo_RUtoBU, *p_RUtoBU, hintsRUtoBU2, *servinfo_RUtoBU2, *p_RUtoBU2;
	int sockfdRU, new_fdRU, rvRU, numbytesRU, rv_RUtoBU,rv_RUtoBU2,tmp;
    std::vector<int> new_fdpl;
    std::vector<int> sockRUtoBU;
    std::vector<int> sockRUtoBU2;
	int * bf;
    char * bf_dP;
    int send_bytes;
    int num_of_parts,err;
	char bf_RUtoBU[52];
	int * size_id_BU;
	struct sockaddr_storage their_addr_RU; // client's address information
    struct linger so_linger;
	socklen_t sin_size_RU;
    std::vector<int> requestIndex;
    std::vector<int> activeRequests;
    std::list<int> activeRequestsList;
    char S_toRU[INET6_ADDRSTRLEN];
  /*  std::vector<pthread_t> thread;
    int seq,tmp;
    std::vector<senderData>  sendtmp;*/ //threading

  //  int devnull;
    //off_t baddr;
/*    int pfd[2], pfd_fc[2];
    struct iovec iov, iov_fc;*/ // splicing

public:
	ReadoutUnit();
    ~ReadoutUnit();

    virtual void initialize(int initialCredits, std::map<std::string, void *> &pmems);

    virtual bool EventListen(int& size, int& eventID, int& destinationBU);


    virtual void pullRequestReceive(int& eventID, int& size, int& destinationBU);

    virtual void prepareSendEvent(FragmentComposition* &fc, int destinationBU);


    virtual void setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU);


    virtual bool sendAnyEvent(int& eventID) ;


    virtual bool sendEvent(int eventID) ;

    virtual void discardEvent();
};
};
#endif
