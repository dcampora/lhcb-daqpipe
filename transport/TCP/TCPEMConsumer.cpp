
/**
 *      MPI EM Consumer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "TCPEMConsumer.h"

TCP::EMConsumer::EMConsumer(){
	sockRUSender.resize(Core::Configuration->numnodes,0);
	sockBUSender.resize(Core::Configuration->numnodes,0);
	/*thread.resize(Core::Configuration->numnodes,0);
	send_tmp.resize(Core::Configuration->numnodes);*/
};

TCP::EMConsumer::~EMConsumer(){};


void TCP::EMConsumer::sendToRUs(int* &size_id_bu){

	for (int q=0; q<TCP::Configuration->numnodes; q++){ //socket per readout unit
		//pipe(pfd);
		
		DEBUG << "MPIEMConsumer: s " << TCP::Configuration->rankID << ", r " << TCP::Configuration->readoutID(q)
            << ", id " << TCP::EVENT_REQUEST << std::endl;
		if(sockRUSender[q]==0){
		memset(&hintsEMCtoRU, 0, sizeof hintsEMCtoRU);
	    hintsEMCtoRU.ai_family = AF_UNSPEC;
	    hintsEMCtoRU.ai_socktype = SOCK_STREAM;

	    
	    if ((rv_toRU = getaddrinfo(TCP::Configuration->RUMapIDIP[TCP::Configuration->readoutID(q)].c_str(), TCP::Configuration->RUMapIDPort[TCP::Configuration->readoutID(q)].c_str(), &hintsEMCtoRU, &servinfoEMCtoRU)) != 0) {
	        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv_toRU));
	        /*exit(1);*/
	    }

	    for(p_EMCtoRU = servinfoEMCtoRU; p_EMCtoRU != NULL; p_EMCtoRU = p_EMCtoRU->ai_next) {
	        if ((sockRUSender[q] = socket(p_EMCtoRU->ai_family, p_EMCtoRU->ai_socktype,p_EMCtoRU->ai_protocol)) == -1) {
	            perror("client: socket");
	            continue;
	        }

	         while(connect(sockRUSender[q], p_EMCtoRU->ai_addr, p_EMCtoRU->ai_addrlen) == -1) {

	        	perror("[EMCOnsumer]client: connect");
	        	continue;
	        }

	        break;
	    }

	    if (p_EMCtoRU == NULL) {
	        fprintf(stderr, "client: failed to connect\n");

	    }
	    }		//threading
	    		/*send_tmp[q].new_socket= sockRUSender[q];
            	send_tmp[q].buffer= size_id_bu;
                send_tmp[q].size= 3*sizeof(int);
				pthread_create(&thread[q], NULL,sender, &send_tmp[q]);*/

        DEBUG<<"[EMConsumer] Preparing sending to "<<(q+1)<<" of "<<TCP::Configuration->numnodes
	    <<" RUs of IP: "<<TCP::Configuration->RUMapIDIP[TCP::Configuration->readoutID(q)].c_str()<<std::endl;
				
	    if ((send(sockRUSender[q], size_id_bu, 3*sizeof(int), 0)) == -1) {  	perror("send");}
	       	  

	    /*int total_bytes = 0;
		while(total_bytes<12){
		iov_emc.iov_base = size_id_bu+total_bytes;
		iov_emc.iov_len = (12 - total_bytes);
		vmsplice(pfd[1], &iov_emc, 1, 0);
		if((send_bytes=splice(pfd[0], NULL, sockRUSender[q], NULL, (12-total_bytes),SPLICE_F_MOVE))==-1)perror("[EMConsumer] splice:");
		total_bytes+=send_bytes;} //splicing
*/
	}

	 //for(int k=0;k<TCP::Configuration->numnodes; k++)pthread_join(thread[k], NULL);

	   }




void TCP::EMConsumer::sendToBU(int* &size_id, int destinationBU){
	     DEBUG<<"[EMConsumer] Preparing sending to "<< (TCP::Configuration->builderNumber(destinationBU)+1)<<" BU of "<<TCP::Configuration->numnodes
	    <<" BUs of IP: "<<TCP::Configuration->BUMapIDIP[destinationBU].c_str()<<std::endl;
	
	if(sockBUSender[TCP::Configuration->builderNumber(destinationBU)]==0){
	memset(&hintsBU, 0, sizeof hintsBU);
    hintsBU.ai_family = AF_UNSPEC;
    hintsBU.ai_socktype = SOCK_STREAM;
    if ((rv_toBU = getaddrinfo(TCP::Configuration->BUMapIDIP[destinationBU].c_str(), TCP::Configuration->BUMapIDPort_SizeID[destinationBU].c_str(), &hintsBU, &servinfoBU)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv_toBU));

    }

    for(p_BU = servinfoBU; p_BU != NULL; p_BU = p_BU->ai_next) {
        if ((sockBUSender[TCP::Configuration->builderNumber(destinationBU)] = socket(p_BU->ai_family, p_BU->ai_socktype,p_BU->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }

        while(connect(sockBUSender[TCP::Configuration->builderNumber(destinationBU)], p_BU->ai_addr, p_BU->ai_addrlen) == -1) {
        	perror("client: connect");
        	continue;
        }

        break;
    }

    if (p_BU == NULL) {
        fprintf(stderr, "client: failed to connect\n");
    	}
    }

     
    if ((send(sockBUSender[TCP::Configuration->builderNumber(destinationBU)], size_id, 2*sizeof(int), 0)) == -1)perror("send");
    DEBUG<<"[EMconsumer]Sended to BU."<<std::endl;
       }

void TCP::EMConsumer::sendExit(void)
{
	std::cerr << "sendExit not implemented for TCP, cannot exit properly !" << std::endl;
	abort();
}
