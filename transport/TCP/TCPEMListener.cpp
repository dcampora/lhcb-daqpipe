
/**
 *      MPI EM - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "TCPEMListener.h"

/**
 * TODO: Modify the listen to make it controlled from the Master.
 */

    TCP::EMListener::EMListener(){};
    TCP::EMListener::~EMListener(){};

void TCP::EMListener::setupListen(){
	//setting up listening socket
		memset(&hints, 0, sizeof hints);
		int yes =1;
		tmp =0;
		new_fd.resize(Core::Configuration->numnodes, 0);
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;// use TCP/IP
		hints.ai_flags = AI_PASSIVE;
		bf = (int *) malloc(2*sizeof(int)); //buffer size
		if ((rv = getaddrinfo(NULL, TCP::Configuration->EMLMapIDPort[TCP::Configuration->rankID].c_str(), &hints, &servinfo)) != 0) {//tmp  port
		        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		    }

		    for(p = servinfo; p != NULL; p = p->ai_next) {
		        if ((sockfd = socket(p->ai_family, p->ai_socktype,
		                p->ai_protocol)) == -1) {
		            perror("server: socket");
		            continue;
		        }

		        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
		                sizeof(int)) == -1) {
		            perror("setsockopt");
		            continue;
		        }

		        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
		            close(sockfd);
		            perror("server: bind");
		            continue;
		        }

		        break;
		    	}
		    if (p == NULL)  {
		            fprintf(stderr, "server: failed to bind\n");
		        }
		        freeaddrinfo(servinfo); // all done with this structure
		    if (listen(sockfd, 65000) == -1) {
		            perror("listen");

		        }


		 }

void TCP::EMListener::listenForCreditRequest(int& node, int& numberOfCreditsRequested){
		//nonblocking socket
		while(1){

		for(int u = tmp; u < Core::Configuration->numnodes; u++){
		if(new_fd[u]==0){
		 sin_size = sizeof their_addr;
	    

		 new_fd[u] = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);

		 if (new_fd[u]== -1) {perror("[EMListener] accept");/* break;*/}

	   	 fcntl(new_fd[u] , F_SETFL, O_NONBLOCK); //nonblocking
		 } 

	     if((numbytes = recv(new_fd[u], bf, 2*sizeof(int), 0)) < 0){/*perror("[EMListener]recv"); */continue;//tmp+=1;break;}//it returns always error since it is nonblocking socket configuration
	     }else{

	     memcpy(&numberOfCreditsRequested,bf,sizeof(int));
	     memcpy(&node,(bf+1),sizeof(int));	
	     tmp+=1;

	     DEBUG<<"[EMListener]Got connection. Received: "<<numbytes<<" bytes  in bf: "<<*bf<<" ,ncredits: "<<numberOfCreditsRequested
	     <<" from builderID: "<<node<< " of IP: "<<TCP::Configuration->BUMapIDIP[node].c_str()<<std::endl; 
	     
	     return;
	     }

	     }
		tmp=0;
	     
	     }

}
