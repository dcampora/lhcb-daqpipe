
/**
 *      MPI RU
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "tcpRU.h"

	TCP::ReadoutUnit::ReadoutUnit(){};
    TCP::ReadoutUnit::~ReadoutUnit(){};


void TCP::ReadoutUnit::initialize(int initialCredits, std::map<std::string, void *> &pmems){

				printf("[RU] Initializing listener. Initial credits: %d \n", initialCredits );
				int yes = 1;
				activeRequests.resize(initialCredits, -1);
    			requestIndex.resize(initialCredits, 0);
    			sockRUtoBU.resize(TCP::Configuration->numnodes,0);
    			sockRUtoBU2.resize(TCP::Configuration->numnodes,0);
    			new_fdpl.resize(TCP::Configuration->numnodes,0);
    			memset(&hintsRU, 0, sizeof hintsRU);
	    		hintsRU.ai_family = AF_INET;
	    		hintsRU.ai_socktype = SOCK_STREAM;// use TCP/IP
	    		hintsRU.ai_flags = AI_PASSIVE; // use my IP

	    		if ((rvRU = getaddrinfo(NULL, TCP::Configuration->RUMapIDPort[TCP::Configuration->rankID].c_str(), &hintsRU, &servinfoRU)) != 0) {//tmp  port to be decided
	    		        fprintf(stderr, "[RU]getaddrinfo: %s\n", gai_strerror(rvRU));
	    		    }

	    		    for(p_RU = servinfoRU; p_RU != NULL; p_RU = p_RU->ai_next) {
	    		        if ((sockfdRU = socket(p_RU->ai_family, p_RU->ai_socktype,
	    		                p_RU->ai_protocol)) == -1) {
	    		            perror("[RU]server: socket");
	    		            continue;
	    		        }

	    		        if (setsockopt(sockfdRU, SOL_SOCKET, SO_REUSEADDR, &yes,
	    		                sizeof(int)) == -1) {
	    		            perror("[RU]:setsockopt");
	    		            continue;
	    		        }

	    		        if (bind(sockfdRU, p_RU->ai_addr, p_RU->ai_addrlen) == -1) {
	    		           // close(sockfdRU);
	    		            perror("[RU]server: bind");
	    		            continue;
	    		        }

	    		        break;
	    		    	}
	    		    if (p_RU == NULL)  {
	    		            fprintf(stderr, "[RU] Server: failed to bind\n");
	    		        }
	    		        freeaddrinfo(servinfoRU); // all done with this structure
	    		    if (listen(sockfdRU , 65000) == -1) { 

	    		        }
	    		size_id_BU = (int*) malloc(3 * sizeof(int));
	    		tmp=0;
	
	DEBUG<<"[RU] Event Listener : Socket n. "<< sockfdRU<<"allocated"<<std::endl;
}

/**
 * Listens for a message from the EventManager.
 */
bool TCP::ReadoutUnit::EventListen(int& size, int& eventID, int& destinationBU){

			int sum =0;
			int yes_fc=1;
			if(new_fdRU==0){ //checking if socket is already open
			 sin_size_RU = sizeof their_addr_RU;

			new_fdRU = accept(sockfdRU, (struct sockaddr *)&their_addr_RU, &sin_size_RU);
			if (new_fdRU == -1) {perror("[RU Listener] accept");}
			//fcntl(new_fdRU , F_SETFL, O_NONBLOCK);
		//	if (setsockopt(new_fdRU,IPPROTO_TCP, TCP_CORK,(char *)&yes_fc,sizeof(int))==-1)perror("[RU Listener]:setsockopt");
	 	        
		    inet_ntop(their_addr_RU.ss_family,(&((struct sockaddr_in *)&their_addr_RU)->sin_addr) ,S_toRU, sizeof S_toRU);
	   	 	DEBUG<<"[RU] Server: got connection from "<<S_toRU<<std::endl;

	   	 }
		     
	   	 	
			while(sum<(3*sizeof(int))){
		     if ((numbytesRU = recv(new_fdRU, size_id_BU+sum,(3 * sizeof(int))-sum, 0)) < 0) {perror("[RU Listener] recv"); }//break; }
		     sum+=numbytesRU;
				 }		 
		    // for(int c =0; c < 3; c++ )printf("[RU] size_id_BU[%d] %d\n",c, size_id_BU[c]);

		     
			size = size_id_BU[0];
		    eventID = size_id_BU[1];
		    destinationBU = size_id_BU[2];
		     DEBUG<<"[RU]Size : "<<size<<"eventID: "<<eventID<<" destinationBU: "<<destinationBU<<std::endl;
		return true;
}


void TCP::ReadoutUnit::pullRequestReceive(int& eventID, int& size, int& destinationBU){
    DEBUG << "MPIReadoutUnit: pullRequestReceive s any, r " << TCP::Configuration->rankID
        << ", id " << TCP::PULL_REQUEST << std::endl;
		while(1){	
			for (int i = tmp; i < TCP::Configuration->numnodes; ++i)
			{
				
			if(new_fdpl[i]==0){//checking if socket is already open for a specific connection
			 sin_size_RU = sizeof their_addr_RU;

			new_fdpl[i] = accept(sockfdRU, (struct sockaddr *)&their_addr_RU, &sin_size_RU);
			if (new_fdpl[i] == -1) {perror("[RU Listener] accept");}

			fcntl(new_fdpl[i] , F_SETFL, O_NONBLOCK); //socket in nonblocking mode
			//if (setsockopt(new_fdpl[i],IPPROTO_TCP, TCP_CORK,(char *)&yes_fc,sizeof(int))==-1)perror("[RU]:setsockopt");
	    		        
		    inet_ntop(their_addr_RU.ss_family,(&((struct sockaddr_in *)&their_addr_RU)->sin_addr) ,S_toRU, sizeof S_toRU);
	   	 	DEBUG<<"[RU] Server: got connection from "<<S_toRU<<std::endl;

	   	 }
		     
		     if ((numbytesRU = recv(new_fdpl[i], size_id_BU,3 * sizeof(int), 0)) < 0){continue; 
		     }else{
		     size = size_id_BU[0];
    		 eventID = size_id_BU[1];
             destinationBU = TCP::Configuration->builderID(size_id_BU[2]);
             tmp+=1;
             DEBUG<<"[RU] pullRequestReceive Size : "<<size<<"eventID: "<<eventID<<" destinationBU: "<<destinationBU<<std::endl;
             return;
		 }
	}
    
    tmp=0;

    }
    // DEBUG << "MPIReadoutUnit: Received!" << std::endl;

    
    
}
/**
 * Prepares the sendEvent
 */
void TCP::ReadoutUnit::prepareSendEvent(FragmentComposition* &fc, int destinationBU){
    DEBUG<<"[RU] metablocks :"<<fc[0].metablocks<<"  datablocks: "<<fc[0].datablocks<<"	total number of parts: "<<(fc[0].metablocks+fc[0].datablocks)<<std::endl;
			DEBUG<<"[RU] Preparing send of EVents. FC: "<<fc<<", destBU: "<<destinationBU<<"  of IP: "<<TCP::Configuration->BUMapIDIP[destinationBU].c_str()<<std::endl;
			num_of_parts = (fc[0].metablocks+fc[0].datablocks);
/*			seq=0;  // leftovers from threading 
			thread.resize(num_of_parts, 0);
			sendtmp.resize(num_of_parts);*/

			if(sockRUtoBU[TCP::Configuration->builderNumber(destinationBU)]==0){
			memset(&hintsRUtoBU, 0, sizeof( hintsRUtoBU));
			hintsRUtoBU.ai_family = AF_INET;
			hintsRUtoBU.ai_socktype = SOCK_STREAM;
			//pipe(pfd_fc); // used for splicing in order to make zero-copy from user space to network send space
			if ((rv_RUtoBU = getaddrinfo(TCP::Configuration->BUMapIDIP[destinationBU].c_str(), TCP::Configuration->BUMapIDPort_fc[destinationBU].c_str(), &hintsRUtoBU, &servinfo_RUtoBU)) != 0) { //provisional
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv_RUtoBU));
		    }
			for(p_RUtoBU = servinfo_RUtoBU; p_RUtoBU != NULL; p_RUtoBU = p_RUtoBU->ai_next) {
			if ((sockRUtoBU[TCP::Configuration->builderNumber(destinationBU)]= socket(p_RUtoBU->ai_family, p_RUtoBU->ai_socktype,
			                p_RUtoBU->ai_protocol)) == -1) {
			            perror("[RU] client: socket");
			            continue;
			      }

			  while(connect(sockRUtoBU[TCP::Configuration->builderNumber(destinationBU)], p_RUtoBU->ai_addr, p_RUtoBU->ai_addrlen) == -1) {
			      // close(sockRUtoBU);
			       perror("[RU] client: connect");
			       continue;
			        }

			        break;
			    }
			if (p_RUtoBU == NULL) {
			        fprintf(stderr, "[RU] client: failed to connect\n");
			    }
				freeaddrinfo(servinfo_RUtoBU);	
			}


			/*
//Zero copy in sending direction
			int total_bytes_fc = 0;
			while(total_bytes_fc<52){
			iov_fc.iov_base = fc+total_bytes_fc;
			iov_fc.iov_len = 52 - total_bytes_fc;
			vmsplice(pfd_fc[1], &iov_fc, 1, 0);
			if((send_bytes=splice(pfd_fc[0], NULL, sockRUtoBU[TCP::Configuration->builderNumber(destinationBU)], NULL, 52-total_bytes_fc,SPLICE_F_MOVE))==-1)perror("[RU] splice:");
			total_bytes_fc+=send_bytes;

			}*/


			    if ((send_bytes = send(sockRUtoBU[TCP::Configuration->builderNumber(destinationBU)], fc, 52, 0)) == -1) {
			    	    	perror("[RU] send FragmentComposition:");
			    	    }
	
		   DEBUG<<"[RU]Sending "<<send_bytes<<" bytes... fc to following BU: "<<TCP::Configuration->BUMapIDIP[destinationBU].c_str()<<std::endl;

		   //seq =0; // leftover from datablocks threading
}


void TCP::ReadoutUnit::setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU){
    		int slot;
    		int sum = 0;
    		

    		//seq+=1; // leftover from datablocks threading
  
    		if(sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)]==0){
    		int yes =1;
    		int no=0;
    		
    		//pipe(pfd); //used for splicing
    	
   			memset(&hintsRUtoBU2, 0, sizeof( hintsRUtoBU2));
			hintsRUtoBU2.ai_family = AF_INET;
			hintsRUtoBU2.ai_socktype = SOCK_STREAM;
			
			if ((rv_RUtoBU2 = getaddrinfo(TCP::Configuration->BUMapIDIP[destinationBU].c_str(), TCP::Configuration->BUMapIDPort_SendEvent[destinationBU].c_str(), &hintsRUtoBU2, &servinfo_RUtoBU2)) != 0) { //provisional
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv_RUtoBU2));
		
		    }
			for(p_RUtoBU2 = servinfo_RUtoBU2; p_RUtoBU2 != NULL; p_RUtoBU2 = p_RUtoBU2->ai_next) {
			if ((sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)]= socket(p_RUtoBU2->ai_family, p_RUtoBU2->ai_socktype,
			                p_RUtoBU2->ai_protocol)) == -1) {
			            perror("[RU] client: socket");
			            continue;
			      }
//leftovers from testing different tcp socket options

/*			   	if (setsockopt(sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)],IPPROTO_TCP, TCP_NODELAY,(char *)&yes,sizeof(int))==-1){ perror("[RU]:setsockopt");
	    		            continue;
	    		        }*/
	    		/*if (setsockopt(sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)],SOL_SOCKET, SO_LINGER,&so_linger,sizeof(so_linger)) == -1){perror("[RU]:setsockopt");
	    		continue;}
*/

			   while(connect(sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)], p_RUtoBU2->ai_addr, p_RUtoBU2->ai_addrlen) == -1) {
			      // close(sockRUtoBU);
			       perror("[RU] client: connect");
			       continue;
			        }

			if (p_RUtoBU2 == NULL) {
			        fprintf(stderr, "[RU] client: failed to connect\n");

			    }
	}
				freeaddrinfo(servinfo_RUtoBU2);	
}
DEBUG<<"[RU] setupSendEvent socket "<<sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)]<<" to  "<<TCP::Configuration->builderNumber(destinationBU)<<" BU "<<" size "<<size<<std::endl;
   


   
while(size>sum){
if((send_bytes=send(sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)], dataPointer+sum,size-sum, 0))==-1)perror("[RU] send event:");
sum+=send_bytes;
}

//threading for sending datablocks
/*sendtmp[seq].new_socket=sockRUtoBU2[(TCP::Configuration->builderNumber(destinationBU)*5)+seq];
sendtmp[seq].size=size;
sendtmp[seq].buffer= dataPointer;
//printf("SEQUENCE: %d socket: %d size: %d dP: %d \n ",seq,sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)],size,dataPointer);
err=pthread_create(&thread[seq],NULL,sender,&sendtmp[seq]);*/



//splicing
/*int total_bytes = 0;
while(total_bytes<size){
iov.iov_base = dataPointer+total_bytes;
iov.iov_len = size - total_bytes;
vmsplice(pfd[1], &iov, 1, 0);
if((send_bytes=splice(pfd[0], NULL, sockRUtoBU2[TCP::Configuration->builderNumber(destinationBU)], NULL, size-total_bytes,SPLICE_F_MOVE))==-1)perror("[RU] splice:");
total_bytes+=send_bytes;

}
close(pfd[1]);
close(pfd[0]);*/






//DEBUG<<"[RU] SENDED to "<< TCP::Configuration->builderNumber(destinationBU)<<"	BU "<<total_bytes<<" BYTES."<<std::endl;
DEBUG<<"[RU] SENDED to "<< TCP::Configuration->builderNumber(destinationBU)<<"	BU "<<sum<<" BYTES."<<std::endl;







//threading
//if (seq == (num_of_parts-1)){for(int f = 0 ; f < num_of_parts; f++ )pthread_join(thread[f], NULL);}
//seq++;


}

bool TCP::ReadoutUnit::sendAnyEvent(int& eventID){
 	bool eventSent = true;
 	//threading datablocks
 	//if (seq == (num_of_parts-1)){for(int f = 0 ; f < num_of_parts; f++ )pthread_join(thread[f], NULL);}
 	//exit(0);
    return eventSent;
}

bool TCP::ReadoutUnit::sendEvent(int eventID){

  	bool eventSent=true;

  //if (seq == (num_of_parts-1)){for(int f = 0 ; f < num_of_parts; f++ )pthread_join(thread[f], NULL);} // <-- joining in case of threading per datablock

    
 //for(int f = 0 ; f < num_of_parts; f++ )pthread_join(thread[f], NULL); //<-- joining in case of threading per event

    return eventSent;
}


void TCP::ReadoutUnit::discardEvent(){
   
return;
}

