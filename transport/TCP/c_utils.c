#include "c_utils.h"
#ifdef __cplusplus
/*extern "C" {*/
#endif

void* receiver(void* cd){
    struct connectionData* data = (struct connectionData*) cd;
    char *  buff =  data ->buffer;
    int nfd = data -> new_socket;
    int size_tmp = data->size;
    int amount_bytes= 0;
    int sum = 0;
    while(sum<size_tmp){if((amount_bytes=recv(nfd,buff+sum, size_tmp-sum, 0)) > 0){sum+=amount_bytes;}

    }
    
    return 0;   
}

void* sender(void* sd){
    struct senderData* data = (struct senderData*) sd;
    char *  buff =  data ->buffer;
    int nfd = data -> new_socket;
    int size_tmp = data->size;
    int amount_bytes= 0;
    int sum = 0;
   // printf("SIZE: %d SOCKET: %d ", size_tmp, nfd);
    while(sum<size_tmp){if((amount_bytes=send(nfd, buff+sum, size_tmp-sum, 0)) > 0){sum+=amount_bytes;}}
    return 0;

}

void* splicer(void* sd){
    struct senderData* data = (struct senderData*) sd;
    struct iovec iov;
    char *  buff =  data ->buffer;
    int nfd = data -> new_socket;
    int size_tmp = data->size;
    int pfd_tmp[2];
    int amount_bytes= 0;
    int total_bytes = 0;
   // printf("socket: %d size: %d dP: %d \n ",nfd,size_tmp,buff);
    pipe(pfd_tmp);

    while(total_bytes<size_tmp){
    iov.iov_base = buff+total_bytes;
    iov.iov_len = (size_tmp - total_bytes);
    vmsplice(pfd_tmp[1], &iov, 1, 0);

    if((amount_bytes=splice(pfd_tmp[0], NULL, nfd, NULL, (size_tmp - total_bytes),SPLICE_F_MOVE))<0){perror("splice:");}
    total_bytes+=amount_bytes;
    //printf("%d bytes to send \n total bytes sended: %d. \n %d more bytes to send. \n",size_tmp, total_bytes,(size_tmp-total_bytes) );


    }
            //splice(pfd[0], NULL, devnull, NULL, total_bytes,SPLICE_F_MOVE);
    close(pfd_tmp[1]);
    close(pfd_tmp[0]);

    return NULL;

}


char * define_IP(){
    struct ifaddrs * ifAddrStruct=NULL;
    struct ifaddrs * ifa=NULL;
    void * tmpAddrPtr=NULL;
    char * addressBuffer =(char *)malloc(INET_ADDRSTRLEN);
    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa ->ifa_addr->sa_family==AF_INET && strcmp(ifa->ifa_name, "INTERFACE") == 0) { // check it is IP4 is a valid IP4 Address
            tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            break;
        }
    }
    return addressBuffer;
}



#ifdef __cplusplus
/*} end extern "C" */
#endif