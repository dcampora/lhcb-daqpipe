
/**
 *      EM transport core consumer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef TCPEMCONSUMER
#define TCPEMCONSUMER 1
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/uio.h>
#include "../common/EMTransportCoreConsumer.h"
#include "../../common/Logging.h"
#include "TCPCore.h"
#include "c_utils.h"


namespace TCP { 
class EMConsumer : public EMTransportCoreConsumer  {
private:

		std::vector<int>  sockBUSender;
        std::vector<int>  sockRUSender;
        //std::vector<pthread_t>  thread;
	    struct addrinfo hintsBU, *servinfoBU, *p_BU;
	    struct addrinfo hintsEMCtoRU, *servinfoEMCtoRU, *p_EMCtoRU;
        //std::vector<senderData> send_tmp;
	    int rv_toRU, rv_toBU;
        int * prov_bf_1;
        int * prov_bf_2;
	    char s[INET6_ADDRSTRLEN];
        int total_bytes, pfd[2], send_bytes;
        struct iovec iov_emc;

public:
    EMConsumer();
    ~EMConsumer();

    

    virtual void sendToRUs(int* &size_id_bu);

    virtual void sendToBU(int* &size_id, int destinationBU);
    virtual void sendExit(void);
};
};
#endif
