
/**
 *      EM transport core listener
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <ifaddrs.h>
#include "../common/EMTransportCoreListener.h"
#include "../../common/Logging.h"
#include "TCPCore.h"
#include "../../em/EMCoreListener.h"
//#include <pthread.h>
#ifndef TCPEMLISTENER
#define TCPEMLISTENER 1

namespace TCP {

class EMListener:public EMTransportCoreListener {
private:
	int * numberOfCredits;
	int sockfd, rv; 
    ssize_t numbytes; // listen on sock_fd, new connection on new_fd
    int tmp;
    std::vector<int> new_fd;
    int * bf;  //buffer size
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr; // client's address information
	socklen_t sin_size;
	struct sigaction sa;
	char s[INET6_ADDRSTRLEN];

public:
    EMListener();
    ~EMListener();

    virtual void setupListen();


    virtual void listenForCreditRequest(int& node, int& numberOfCreditsRequested);
};
};
#endif
