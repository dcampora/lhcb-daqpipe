
/**
 *      TCP - impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "TCPCore.h"

void TCP::ConfigurationLoader::initialize(int argc, char *argv[]){
    // There is no multithreaded MPI support on 
    // the openib BTL, so we cannot use MPI_THREAD_MULTIPLE here
    numprocs = config_root.get("number of ethernet nodes", 0).asInt() * numProcsPerNode;

    // TODO: Configurable
    // sqName = "/sharedqueue";

    logicalNames[TCP::EVENT_MANAGER_LISTENER] = "eml";
    logicalNames[TCP::EVENT_MANAGER_CONSUMER] = "emc";
    logicalNames[TCP::BUILDER_UNIT] = "bu";
    logicalNames[TCP::READOUT_UNIT] = "ru";

    // TODO: Update. Kind of hack
    numProcsPerNode = 4;
}

void TCP::ConfigurationLoader::parseConfiguration(){
    numnodes = (int) (numprocs / (float) numProcsPerNode);
    gethostname(name, HOST_NAME_MAX);

    sync_server_ip = config_root.get("sync server ip", "").asString();
    sync_server_port = config_root.get("sync server port", "").asString();
    pub_server_port = config_root.get("pub server port", "").asString();

    rankID = zmq_handshake_sub(sync_server_ip, pub_server_port, sync_server_port);
    if (rankID < 0){
        ERROR <<"TCP::ConfigurationLoader::parseConfiguration zmq_handshake failed aborting" << std::endl;
        exit(EXIT_FAILURE);
    }

    procID = rankID % numProcsPerNode;
    emlistenerID = TCP::EMLISTENER_ID;
    emconsumerID = TCP::EMCONSUMER_ID;

    Core::Configuration->name = name;
    Core::Configuration->numnodes = numnodes;
    numTotalReadouts = numnodes * numReadoutsPerNode;
    numTotalBuilders = numnodes * numBuildersPerNode;

    if (logToFile)
        logfile.open(std::string(logdir + getLogicalName() + std::string(".log")).c_str());
    
    Logger::logger(0) << std::setprecision(2) << std::fixed;

    initializeIPs();
    differentiateShmemNames();

}


void TCP::ConfigurationLoader::differentiateShmemNames(){
    for (std::vector<std::string*>::iterator it = buShmemBuffers.begin(); it != buShmemBuffers.end(); it++)
        *(*it) += builderSuffix();

    for (std::vector<std::string*>::iterator it = ruShmemBuffers.begin(); it != ruShmemBuffers.end(); it++)
        *(*it) += readoutSuffix();
}

void TCP::ConfigurationLoader::initializeIPs(){
    // IPs and ports

    std::map<int, std::string> nodeIP;
    std::string ip_string;
    int index = 0;
    Json::Value hosts;
    Json::Value int_type;
    Json::Value ip_list;

    switch (interface) {
    case Core::INFINIBAND:
        int_type = config_root["infiniband capabitilies"];
        break;
    case Core::ETHERNET:
        int_type = config_root["ethernet capabitilies"];
        break;
    default:
        break;
        ERROR << "ConfigurationLoader::initializeIPs: Undefined interface type aborting" << std::endl;
        exit(EXIT_FAILURE);
    }
    hosts = int_type["hosts"];
    for (int k = 0 ; k<hosts.size() ; k++){
        ip_list = int_type[hosts[k].asString().c_str()]["ip list"];
        if(ip_list.size() <= 1){
            nodeIP[k] = ip_list[0].asString();
        }else{
            WARNING << "ConfigurationLoader::initializeIPs: multiple interfaces found" << std::endl;
            for(int j=0 ; j < ip_list.size() ; j++){
                WARNING << "addr " << j << " " << ip_list[j].asString() << std::endl;
            }
            WARNING << "Using the first addr in the list " << ip_list[0].asString() << std::endl;
            nodeIP[k] = ip_list[0].asString();
        }
    }

/*    for (int k=0 ; k<hosts.size() ; k++){
        ERROR << "ip " << k << " " << nodeIP[k] << std::endl;
    }*/
    int tmp = 0 ;
    for (int i=tmp; i<numprocs; ++i){
        int pid = i % numProcsPerNode;

        if (i == emlistenerID){
            EMLMapIDIP[i] = nodeIP[0];
            EMLMapIPID[nodeIP[0]] = i;
            EMLMapIDPort[i] = Tools::toString(startingPort);
            tmp = i;
            DEBUG << "EML  id "<< i <<" port " << EMLMapIDPort[i] << std::endl;
        }
        else if (i == emconsumerID){
            EMCMapIDIP[i] = nodeIP[0];
            EMCMapIPID[nodeIP[0]] = i;
            tmp_port=startingPort + 1;
            EMCMapIDPort[i] = Tools::toString(tmp_port);
            tmp = i ;
            DEBUG << "EMC  id "<< i <<" port " << EMCMapIDPort[i]<<std::endl;
        }
        else if((pid != emconsumerID) && (pid != emlistenerID)) {
            int nodeno = nodeNumber(i);

            if (pid < TCP::READOUT_UNIT + numReadoutsPerNode){
                int readoutno = readoutNumber(i);
                RUMapIDIP[i] = nodeIP[nodeno];
                RUMapIPID[nodeIP[nodeno]] = i;
                tmp_port=startingPort + i;
                RUMapIDPort[i] = Tools::toString(tmp_port);
                tmp = i;
                DEBUG << "RU  id " << i << " port " << RUMapIDPort[i] << std::endl;
            }
            else if (pid >= TCP::BUILDER_UNIT){
                int builderno = builderNumber(i);
                BUMapIDIP[i] = nodeIP[nodeno];
                BUMapIPID[nodeIP[nodeno]] = i;
                tmp_port=startingPort + i ;
                BUMapIDPort_fc[i] = Tools::toString(tmp_port);
                tmp_port=startingPort + i + 1;
                BUMapIDPort_SendEvent[i] = Tools::toString(tmp_port);
                tmp_port=startingPort + i  +2 ;
                BUMapIDPort_SizeID[i] = Tools::toString(tmp_port);
                tmp = i + 3 ;
                DEBUG << "BU  id " << i << " port fc:" << BUMapIDPort_fc[i]<< std::endl;
                DEBUG << "BU  id " << i << " port SendEvent:" << BUMapIDPort_SendEvent[i]<< std::endl;
                DEBUG << "BU  id " << i << " port SizeID:" << BUMapIDPort_SizeID[i]<<std::endl;
            }
        }
    }

    for(std::map<int, std::string>::iterator it=BUMapIDIP.begin() ; it != BUMapIDIP.end() ; it++){
        DEBUG << "BU id " << it->first << " IP " << it->second << " fc:" << BUMapIDPort_fc[it->first]<<" SendEvent:"<< BUMapIDPort_SendEvent[it->first]<< " SizeID:"<<BUMapIDPort_SizeID[it->first] <<std::endl;
    }

    for(std::map<int, std::string>::iterator it=RUMapIDIP.begin() ; it != RUMapIDIP.end() ; it++){
        DEBUG << "RU id " << it->first << " IP " << it->second << ":" << RUMapIDPort[it->first] <<std::endl;
    }
}



bool TCP::ConfigurationLoader::isEMListener(){
    return rankID == emlistenerID;
}

bool TCP::ConfigurationLoader::isEMConsumer(){
    return rankID == emconsumerID;
}

bool TCP::ConfigurationLoader::isBU(){
    return procID >= TCP::BUILDER_UNIT;
}

bool TCP::ConfigurationLoader::is(int id){
    if (id == TCP::BUILDER_UNIT) return isBU();
    return id == procID;
}

void TCP::ConfigurationLoader::finalize(){
}

int TCP::ConfigurationLoader::builderID(int builderNumber){
    return TCP::BUILDER_UNIT + (builderNumber % numBuildersPerNode) +
        (builderNumber / numBuildersPerNode) * numProcsPerNode;
}

int TCP::ConfigurationLoader::readoutID(int readoutNumber){
    return TCP::READOUT_UNIT + readoutNumber * numProcsPerNode;
}

int TCP::ConfigurationLoader::builderNumber(int builderID){
    return (builderID / numProcsPerNode) * numBuildersPerNode + 
        ((builderID - TCP::BUILDER_UNIT) % numBuildersPerNode);
}

int TCP::ConfigurationLoader::readoutNumber(int readoutID){
    return readoutID / numProcsPerNode;
}

bool TCP::ConfigurationLoader::readoutInSameNode(int readoutNumber){
    return nodeNumber(rankID) == nodeNumber(readoutID(readoutNumber));
}

bool TCP::ConfigurationLoader::builderInSameNode(int builderNumber){
    // I'm a RU, I want to know if the builder builderNumber is on the same node as me.
    // Simple. Simplest!
    return nodeNumber(rankID) == nodeNumber(builderID(builderNumber));
}

int TCP::ConfigurationLoader::nodeID(){
    return rankID;
}

int TCP::ConfigurationLoader::nodeNumber(int _rankID){
    return _rankID / numProcsPerNode;
}

std::string TCP::ConfigurationLoader::getLogicalName(){
    std::string post = std::string("_") + name;

    if (isEMListener()) return logicalNames[TCP::EVENT_MANAGER_LISTENER] + post;
    else if (isEMConsumer()) return logicalNames[TCP::EVENT_MANAGER_CONSUMER] + post;
    else if (is(TCP::READOUT_UNIT)) return logicalNames[TCP::READOUT_UNIT] + post;
    else if (is(TCP::BUILDER_UNIT)) return logicalNames[TCP::BUILDER_UNIT] + builderSuffix() + post;
    else return std::string("p") + Tools::toString<int>(rankID);
}

std::string TCP::ConfigurationLoader::builderSuffix(){
    if (numBuildersPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(builderNumber(rankID));
    }
}
std::string TCP::ConfigurationLoader::readoutSuffix(){
    if (numReadoutsPerNode==1)
        return "";
    else {
        return std::string("_") + Tools::toString<int>(readoutNumber(rankID));
    }
}
