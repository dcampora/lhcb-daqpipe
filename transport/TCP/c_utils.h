#ifndef C_UTILS
#define C_UTILS 1

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <ifaddrs.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <sys/mman.h>
#include "../../common/GenStructures.h"

struct connectionData{
	int new_socket, size;
	char * buffer;
};
struct senderData{
	int new_socket,size;
	char * buffer;
};

void* receiver(void* cd);
void* sender(void* sd);
void* splicer(void* sd);
char * define_IP();




#endif