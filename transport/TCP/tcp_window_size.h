#ifndef TCP_WINDOW_SIZE
#define TCP_WINDOW_SIZE 1

#include "util.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>

/** Added for daemonizing the process */
    #include <syslog.h>


    #include <netdb.h>

    #include <netinet/in.h>
    #include <netinet/tcp.h>


    #include <arpa/inet.h> 

int setsock_tcp_windowsize( int inSock, int inTCPWin, int inSend );
int getsock_tcp_windowsize( int inSock, int inSend );

#endif