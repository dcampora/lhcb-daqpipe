
/**
 *      BU transport core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef TCPBU
#define TCPBU 1

#include <stdlib.h>
#include <ctime>
#include <vector>
#include <list>
#include <string>
#include "TCPCore.h"
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <time.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include "../../common/GenStructures.h"
#include "../../common/Shmem.h"
#include "../common/BUTransportCore.h"
#include "../config/ConfigurationCore.h"
#include "c_utils.h"

namespace TCP{

class BuilderUnit : public BUTransportCore {
private:
	 	int sockfd, sockBUSender, sockBUListener, sockBUListener2, sockRUListener,new_BUsock_SID, new_BUsock_dP;//socket to send credits;
	    struct addrinfo hints_BUSender, *servinfo_BUSender, *p_BUSender;
	    struct addrinfo hints_BUReceiver, *servinfo_BUReceiver, *p_BUReceiver;
        struct addrinfo hints_BUReceiver2, *servinfo_BUReceiver2, *p_BUReceiver2;
		struct sockaddr_storage connectorBU_addr, connectorBU_addr2; // client's address information
        struct addrinfo hintsBU, *servinfoBU, *p_BU;
        int sockfdBU, new_fdBU, rvBU, numbytesBU;
        struct addrinfo hints_BUpl, *servinfo_BUpl, *p_BUpl;
        int rv_BUpl, tmp;
        std::vector<int> sockBUpl;
        //struct linger so_linger;
		socklen_t sinBU_size, sinBU_size2,sin_size_BU;
        struct sockaddr_storage their_addr_BU;
	    int rv_BUSender,rv_BUReceiver, rv_BUReceiver1, rv_BUReceiver2, rv_BUReceiver3;
        int bf_BUSender[2];
	    char BUdataBuffer[52];
        std::vector<int> new_BUsock;
        std::vector<int> new_BUsock2;
        char sBU[INET6_ADDRSTRLEN], sBU2[INET6_ADDRSTRLEN], S_toBU[INET6_ADDRSTRLEN];
        int * BU_sizeID;
        char * BU_dataPointer;
	    std::vector<int> transmissionSize;
	    int metaBufferElement, dataBufferElement;
	    std::vector<int> activeRequests;
	    int* size_id;
        int *bf_ptr;
        time_t tstart;
	    int initialCredits, seq, total_parts;
        std::vector<int> num_of_parts;
        std::vector<pthread_t> thread;
        std::vector<connectionData> conn_tmp;
        FragmentComposition *fc_ptr;
     //   struct tpacket_req req;

//        int pfd[2];

        std::list<int> activeRequestsList;
        std::vector<int> requestIndex;
        std::vector<std::vector<int> > requestReadoutIndex;
        // Descriptors

public:
        BuilderUnit() ;
        ~BuilderUnit();

    virtual void initialize(int initialCredits, void *sh_meta_pmem, void *sh_data_pmem);

    virtual void sendCredits(int& numberOfCredits);

    virtual void receiveSizeAndID(int& eventSize, int& eventID);

    virtual void pullRequest(int* size_id_bu, int& readoutno);

    virtual void prepareReceiveEvent(FragmentComposition* fc);

    virtual bool prepareReceiveEventComplete(int& fragmentReceived);

    virtual void setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype = Core::DATA);

    virtual bool receiveEvent(int& finishedEventID, int& transmissionSize);

    virtual void discardEvent();
	
    virtual bool checkExitMessage(void);
};
};
#endif
