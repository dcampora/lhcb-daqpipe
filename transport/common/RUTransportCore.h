
/**
 *      RU transport core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef RUTRANSPORTCORE 
#define RUTRANSPORTCORE 1

#include <stdlib.h>
#include <map>
#include <string>
#include "../../common/GenStructures.h"
#include "../config/ConfigurationCore.h"

class RUTransportCore {
public:
    RUTransportCore(){}

    /**
     * Placeholder for initialization.
     * 
     * @param initialCredits 
     */
    virtual void initialize(int initialCredits){}

    // TODO: Move the second parameter to singleton config, delete this method
    virtual void initialize(int initialCredits, std::map<std::string, void*> &pmems){
        this->initialize(initialCredits);
    }

    /**
     * Waits for an event triggered on the EM.
     * 
     * @param size          
     * @param eventID       
     * @param destinationBU 
     * @post size_id_bu is filled with size, id and destinationBU.
     */
    virtual bool EventListen(int& size, int& eventID, int& destinationBU) = 0;

    /**
     * Receive a pull request from any node
     * @param eventID       
     * @param destinationBU 
     */
    virtual void pullRequestReceive(int& eventID, int& size, int& destinationBU){}

    /**
     * Prepares the sendEvent by passing the FragmentComposition.
     * 
     * @param fc                         fragment composition, ie. how many
     *                                   meta and data fragments will be sent
     * @param destinationBU              
     */
    virtual void prepareSendEvent(FragmentComposition*& fc, int destinationBU) = 0;

    /**
     * In a non-reliable data transmission world, discards an event.
     */
    virtual void discardEvent(){}
    
    /**
     * Synchronous: Setups the send mechanism.
     * Asynchronous: Sends events.
     * 
     * @param dataPointer   
     * @param size          
     * @param eventID       
     * @param destinationBU 
     */
    virtual void setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU) = 0;

    /**
     * Synchronous transmission: Sends one of the setup sends.
     * Asynchronous transmission: Tests any send for completion.
     *
     * @param eventID Overwritten in case an event finished transmitting,
     *                with the eventID that finished.
     * @return        true if sent. false otherwise.
     */
    virtual bool sendAnyEvent(int& eventID) = 0;

    /**
     * This function differs in it sends the *specific eventID*
     * on the method definition.
     * 
     * Synchronous transmission: Sends the setup sends for that eid.
     * Asynchronous transmission: Tests the send for that eid to complete.
     *
     * @param eventID the eid which should be sent
     * @return        true if sent. false otherwise.
     */
    virtual bool sendEvent(int eventID) = 0;
	
	/**
	 * Register some segments to be used by lowlevel driver like IB
	**/
	virtual void registerSegment (void *ptr, size_t size) {};
};

#endif
