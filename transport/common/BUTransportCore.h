
/**
 *      BU transport core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef BUTRANSPORTCORE 
#define BUTRANSPORTCORE 1

#include "../config/ConfigurationCore.h"
#include "../../common/GenStructures.h"

class BUTransportCore {
public:
    BUTransportCore(){}

    /**
     * Placeholder for initialization.
     * 
     * @param initialCredits
     */
    virtual void initialize(int initialCredits){}

    /**
     * Overload for initialization.
     * 
     * @param initialCredits 
     * @param sh_meta_pmem   
     * @param sh_data_pmem   
     */
    virtual void initialize(int initialCredits, void *sh_meta_pmem, void *sh_data_pmem){
        this->initialize(initialCredits);
    }

    /**
     * Sends credits to Event Manager.
     * 
     * @param numberOfCredits
     */
    virtual void sendCredits(int& numberOfCredits) = 0;

    /**
     * Requests a PULL from the RUs.
     * 
     * @param eventID   
     * @param readoutID 
     */
    virtual void pullRequest(int* size_id_bu, int& readoutno){}

    /**
     * Receives the event size and ID from the Event Manager.
     * 
     * @param eventSize 
     * @param eventID   
     */
    virtual void receiveSizeAndID(int& eventSize, int& eventID) = 0;

    /**
     * Prepare the Receive mechanism, by transmitting the FragmentComposition.
     *
     * Synchronous: Setups the prepareReceiveEventComplete mechanism.
     * Asynchronous: Sends FragmetnCompositions.
     * @param fcArray       
     */
    virtual void prepareReceiveEvent(FragmentComposition* fc) = 0;
    
    /**
     * Overload for when eid is known.
     * 
     * @param fc  
     * @param eid 
     */
    virtual void prepareReceiveEvent(FragmentComposition* fc, int eid){}

    /**
     * Tests the completion of any FragmentComposition reception.
     * 
     * @param  fragmentReceived 
     * @return                  
     */
    virtual bool prepareReceiveEventComplete(int& fragmentReceived){ return true; }
    
    /**
     * In a non-reliable data transmission world, discards an event.
     */
    virtual void discardEvent(){}

    /**
     * Setup communication to receive events.
     * 
     * @pre Memory is not reserved. Size is total size, and
     *      it's distributed evenly across sending nodes.
     * @param dataPointer memory pointer
     * @param size total size of the event
     * @param readoutIndex
     * @param eventID
     */
    virtual void setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID, Core::fragmentType datatype) = 0;

    /**
     * Synchronous: Receives an event.
     * Asynchronous: Waits for a receive to complete.
     *
     * @param finishedEventID eventID finished
     * @param transmissionSize size of finished transmission
     * @return true if received, false otherwise
     */
    virtual bool receiveEvent(int& finishedEventID, int& transmissionSize) = 0;
    
    /**
     * Check if an exit message as been receive from the EM.
    **/
    virtual bool checkExitMessage(void) = 0;

    /**
     * Help for support of lowlevel driver (IB/libfabric) to pin the pages and register the segment
	 * to the board with an ID.
	 * By default ignored.
    **/
    virtual void registerSegment(void * ptr,size_t size) {};
};

#endif
