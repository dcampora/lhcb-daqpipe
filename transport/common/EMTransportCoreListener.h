
/**
 *      EM transport core listener
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMTRANSPORTCORELISTENER 
#define EMTRANSPORTCORELISTENER 1

class EMCoreListener;

class EMTransportCoreListener {
public:
    EMTransportCoreListener(){}

    /**
     * Asynchronous: Setups the listen in the transport.
     *               ie. For MPI, this starts several non-blocking concurrent listens.
     * Synchronous: It may be left in blank.
     */
    virtual void setupListen(){}

    /**
     * Waits for a credit request from a BU node. Both params are
     * return params.
     * 
     * @param node                      node making the request.
     * @param numberOfCreditsRequested 
     */
    virtual void listenForCreditRequest(int& node, int& numberOfCreditsRequested) = 0;

    //for RDMA
    //virtual int *listenForCreditRequest(int& node, int* numberOfCreditsRequested) = 0;
};

#endif
