
/**
 *      EM transport core consumer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMTRANSPORTCORECONSUMER 
#define EMTRANSPORTCORECONSUMER 1

class EMTransportCoreConsumer {
public:
    EMTransportCoreConsumer(){}

    virtual void initialize(){ }
    /**
     * Sends the request to all RUs
     * 
     * @param size_id_bu  Size, ID and BU packed
     *                    as three consecutive ints
     */
    virtual void sendToRUs(int*& size_id_bu) = 0;

    /**
     * Send the request to designated BU
     * 
     * @param size_id   Size and ID packed as two
     *                  consecutive ints
     */
    virtual void sendToBU(int*& size_id, int destinationBU) = 0;
	virtual void sendExit(void) = 0;
};

#endif
