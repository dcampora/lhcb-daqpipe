
/**
 *      Configuration Core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#ifndef CONFIGURATION_CORE
#define CONFIGURATION_CORE 1

#include <string>
#include <map>
#include <pthread.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include <string.h>
#include <errno.h>

#include "../../extern-deps/jsoncpp/dist/json/json.h"
#include "../../common/Logging.h"
#include "FileStdLogger.h"
#include "../../common/Tools.h"

#define MAX_RU_METADATA_FRAGMENTS 5
#define MAX_RU_DATA_FRAGMENTS 5
#define MAX_RU_FRAGMENTS (MAX_RU_METADATA_FRAGMENTS+MAX_RU_DATA_FRAGMENTS)

namespace Core {

enum fragmentType {
    METADATA,
    DATA
};

enum transport {
    PUSH,
    PULL
};

enum interface_type{
    ETHERNET,
    INFINIBAND
};

class ConfigurationLoader {
public:
    int logLevel;
    std::map<int, std::string> mapLogLevels;
    std::map<int, bool> logActivated;
    bool logToFile, logToStd;
    std::string logdir;
    std::ofstream logfile;
    FileStdLogger discardStream;
    FileStdLogger logfilestd;
    MessageLogger* messageLogger;
    VoidLogger* discardLogger;

    // bool separateLogs;
    // std::map<int, std::ofstream> mapLogFile;

    int protocol;
    int initialCredits;
    int triggerFrequency;
    int numnodes;
    std::string name;

    std::string trigger_sqName;

    std::map<int, std::string> nodeIP;
    int startingPort;

    size_t maxBUDataBufferSize, maxRUDataBufferSize;
    size_t maxRUMetaBufferSize, maxBUMetaBufferSize;

    std::string BUMetaBufferName;
    std::string BUDataBufferName;
    size_t BUMetaBufferSize;
    size_t BUDataBufferSize;

    size_t RUDataBufferSize;
    size_t RUMetaBufferSize;
    size_t maxRUFragments;
    size_t eventFragmentSize;
    size_t dataFragmentSize;
    size_t eventSize;

    std::string RUgpuGeneratedQName1;
    std::string RUgpuGeneratedQName2;
    std::string RUProcessedQName;
    std::string RUMeta1Name;
    std::string RUMeta2Name;
    std::string RUData1Name;
    std::string RUData2Name;
    int RUQueueElements;
    
    std::map<int, std::string> buffer_IDName;
    std::map<int, int> buffer_IDSize;
    std::vector<int> IDSize;

    std::string shmemCommQTransactionName;
    std::string shmemCommQInputTransactionName;
    std::string shmemCommQBUFinishedName;
    std::string shmemCommQRUFinishedName;
    std::string shmemCommFragmentCompositionName;
    std::string shmemCommMetaBuffer;
    int shmemQueueSize, shmemFragmentQueueSize;

    std::vector<std::string*> buShmemBuffers;
    std::vector<std::string*> ruShmemBuffers;
    std::vector<std::string*> shmem_ShmemBuffers;

    int numcopycats;
    int EMSharedQueueSize;

    bool shmemTransportEnabled;
    bool bypassShmemTransport;
    bool logMetadata, logFC;

    int numBuildersPerNode, numReadoutsPerNode, numProcsPerNode;
    int numTotalReadouts;
    int numTotalBuilders;

    int bu_startid, bu_endid, ru_startid, ru_endid;

    int timeoutPrepareReceiveEvent;
    int maxRUSendAttempts;

    std::string shmPrefix;

    std::ifstream configuration_file;
    Json::Value config_root;

    interface_type interface;

    int startingEventID;
    bool unreliableTransport;
    
    int timeout;

    ConfigurationLoader();
    bool loadConfigFile(const char* file_name);
    bool isConfigurationValid();
    void activateLogLevel(int logLevel);
    void setConfiguration();
    void setReadouts(int numReadouts);
    void setBuilders(int numBuilders);

    virtual bool readoutInSameNode(int rankID) = 0;
    virtual bool builderInSameNode(int builderNumber) = 0;
    virtual int builderNumber(int builderID) = 0;
    virtual int readoutNumber(int readoutID) = 0;
    virtual int builderID(int builderNumber) = 0;
    virtual int readoutID(int readoutNumber) = 0;
    virtual int nodeID() = 0;
    virtual int nodeNumber(int rankID) = 0;
	virtual bool isEMListener(void) = 0;
	virtual bool isEMConsumer(void) = 0;

    // TODO: Add missing virtual functions
    virtual bool is(int rankID) = 0;

    virtual std::string getLogicalName() = 0;

    std::string generatePrefix();
};

#ifndef EXTERN_CONFIGURATION_CORE
extern ConfigurationLoader* Configuration;
#endif

};

#endif
