
/**
 *      Configuration Core impl
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      December, 2013
 *      CERN
 */

#include "ConfigurationCore.h"

// TODO: Load configuration from configuration file.
Core::ConfigurationLoader::ConfigurationLoader(){

    logLevel = 3;
    logActivated[10] = 0;

    mapLogLevels[10] = "PROFILE";
    mapLogLevels[5] = "DEBUG";
    mapLogLevels[4] = "INFO";
    mapLogLevels[3] = "MON";
    mapLogLevels[2] = "WARNING";
    mapLogLevels[1] = "ERROR";
    mapLogLevels[0] = "MAIN";

    logToFile = false;
    logToStd = true;
    logdir = "logs/";
    
    timeout = 0;

    messageLogger = new MessageLogger(&logfile, &logfilestd);
    discardLogger = new VoidLogger(&discardStream);

    // IPs and ports
    startingPort = 10000;

    protocol = Core::PUSH;

    interface = INFINIBAND;

    startingEventID = 0;

    // 2 seconds and 10 seconds
    // timeoutSendEvent = 2000;
    timeoutPrepareReceiveEvent = 50000;
    maxRUSendAttempts = 10000;
    
    // separateLogs = false;
    // for (std::map<int, std::string>::iterator it = mapLogLevels.begin(); it != mapLogLevels.end(); ++it){
    //     mapLogFile[it->first] = logfile;
    // }

    // Overriden by main.cpp
    eventSize = 1024;

    initialCredits = 3;
    triggerFrequency = 1;

    numnodes = 2;
    name = "machinename";
    
    maxRUFragments = MAX_RU_METADATA_FRAGMENTS + MAX_RU_DATA_FRAGMENTS;

    dataFragmentSize = 1024;
    eventFragmentSize = 1024; // 1 KB for event fragment size

    maxBUDataBufferSize = 4ull * 1024ull * 1024ull * 1024ull ; // Max of 4 G
    maxRUDataBufferSize = 4ull * 1024ull * 1024ull * 1024ull ; // Max of 4 G
    maxRUMetaBufferSize = 512ull * 1024ull * 1024ull ;  // Max of 512 M
    maxBUMetaBufferSize = 512ull * 1024ull * 1024ull ; // Max of 512 M

    // TODO: Quick hack, refactor
    buShmemBuffers.push_back(&BUMetaBufferName);
    buShmemBuffers.push_back(&BUDataBufferName);

    RUQueueElements = 10000;

    ruShmemBuffers.push_back(&RUgpuGeneratedQName1);
    ruShmemBuffers.push_back(&RUgpuGeneratedQName2);
    ruShmemBuffers.push_back(&RUProcessedQName);
    ruShmemBuffers.push_back(&RUMeta1Name);
    ruShmemBuffers.push_back(&RUMeta2Name);
    ruShmemBuffers.push_back(&RUData1Name);
    ruShmemBuffers.push_back(&RUData2Name);

    shmemQueueSize = 1000;
    shmemFragmentQueueSize = 1000;

    // In case a problem comes up with this, some reading,
    // http://www.open-mpi.org/community/lists/users/2010/08/14137.php
    EMSharedQueueSize = 10000;

    numcopycats = 4;

    shmemTransportEnabled = false;
    bypassShmemTransport = true;
    logMetadata = false;
    logFC = true;

    // Default: 1 builder + 1 readout
    //numProcsPerNode = 4;
    numProcsPerNode = 2;
    setBuilders(1);
    setReadouts(1);

    unreliableTransport = false;
}

bool Core::ConfigurationLoader::loadConfigFile(const char* file_name){
    bool ret_val;
    Json::Reader file_reader;
    std::ifstream config_file;
    config_file.open(file_name);
    if(!config_file.is_open()){
        ret_val = false;
        ERROR << "Core::ConfigurationLoader::loadConfigFile: " << file_name << ": Error opening config file" << std::endl;
    }else{
       ret_val = file_reader.parse(config_file, config_root);
    }

    return ret_val;

}

void Core::ConfigurationLoader::setReadouts(int numReadouts){
    numReadoutsPerNode = numReadouts;
    numProcsPerNode += numReadouts - 1;
}

void Core::ConfigurationLoader::setBuilders(int numBuilders){
    numBuildersPerNode = numBuilders;
    numProcsPerNode += numBuilders - 1;
}

void Core::ConfigurationLoader::setConfiguration(){
    // More things that need setting after the rest

    // Buffers of RU and BU
    // Note: dataBufferSize and dataFragmentSize must be aligned (multiple)
    // RUDataBufferSize = 1024 * 1024 * 1024; // 1 GB for data1 and data2
    // RUMetaBufferSize = 256 * 1024 * 1024; // 128 MB for meta1 and meta2
    // RUDataBufferSize = (initialCredits + 3) * Core::Configuration->eventSize * Core::Configuration->dataFragmentSize;
    // RUMetaBufferSize = (initialCredits + 20) * (Core::Configuration->eventSize / MAX_EVENTIDS_METADATA) * sizeof(Metadata);
    // BUDataBufferSize = initialCredits * RUDataBufferSize; // #credits * 1 G
    // BUMetaBufferSize = initialCredits * RUMetaBufferSize; // #credits * 256 M
    
    // Why not the maximum? :)
    // Because we don't have enough RAM :(

    RUDataBufferSize = (size_t) config_root.get("data buffer size", (long long unsigned int) maxRUDataBufferSize).asUInt64() ;
    BUDataBufferSize = (size_t) config_root.get("data buffer size", (long long unsigned int) maxBUDataBufferSize).asUInt64() ;
    RUMetaBufferSize = (size_t) config_root.get("meta buffer size", (long long unsigned int) maxRUMetaBufferSize).asUInt64() ;
    BUMetaBufferSize = (size_t) config_root.get("meta buffer size", (long long unsigned int) maxBUMetaBufferSize).asUInt64() ;

    logFC = config_root.get("log fragment composition", true).asBool();
    logMetadata = config_root.get("log metadata", false).asBool();
    startingPort = config_root.get("starting port", 6000).asUInt();

    // Generate names for shmems
    shmPrefix = generatePrefix();

    trigger_sqName = shmPrefix + "trigger";

    BUMetaBufferName = shmPrefix + "bu-meta";
    BUDataBufferName = shmPrefix + "bu-data";

    RUgpuGeneratedQName1 = shmPrefix + "gpuru-gpugenerated-meta1";
    RUgpuGeneratedQName2 = shmPrefix + "gpuru-gpugenerated-meta2";
    RUProcessedQName = shmPrefix + "gpuru-ruprocessed";
    RUMeta1Name = shmPrefix + "meta1";
    RUMeta2Name = shmPrefix + "meta2";
    RUData1Name = shmPrefix + "data1";
    RUData2Name = shmPrefix + "data2";

    shmemCommQTransactionName = shmPrefix + "shmem-transaction";
    shmemCommQInputTransactionName = shmPrefix + "shmem-inputtransaction";
    shmemCommQRUFinishedName = shmPrefix + "shmem-rufinished";
    shmemCommQBUFinishedName = shmPrefix + "shmem-bufinished";
    shmemCommFragmentCompositionName = shmPrefix + "shmem-fc";
    shmemCommMetaBuffer = shmPrefix + "shmem-meta";

    if (logToFile){
        logdir =  config_root.get("log path", logdir).asString();
        if(!Tools::mk_dir(logdir)){
            ERROR << "Core::ConfigurationLoader::setConfiguration " << logdir << " " << strerror(errno) << std::endl;
        }
        logdir += "/";
    }

}

bool Core::ConfigurationLoader::isConfigurationValid(){
    // There is a connection between the number of initialCredits and
    // the event size to keep stability
    size_t necessary_RUData = (initialCredits + 3) * Core::Configuration->eventSize * Core::Configuration->dataFragmentSize;
    size_t necessary_BUData = initialCredits * necessary_RUData;
    size_t necessary_RUMeta = (initialCredits + 20) * (Core::Configuration->eventSize / MAX_EVENTIDS_METADATA) * sizeof(Metadata);
    size_t necessary_BUMeta = initialCredits * necessary_RUMeta;
    
    if (((size_t) (initialCredits + 3) * Core::Configuration->eventSize * Core::Configuration->dataFragmentSize) > maxRUDataBufferSize ||
        ((size_t) (initialCredits + 20) * (Core::Configuration->eventSize / MAX_EVENTIDS_METADATA) * sizeof(Metadata)) > maxRUMetaBufferSize ||
        BUDataBufferSize > maxBUDataBufferSize ||
        BUMetaBufferSize > maxBUMetaBufferSize){
        
        ERROR << "main configuration check: The number of initial credits and size of events is too big" << std::endl;
        ERROR << "note: Requested buffer size is (init_credits+3)*size_of_event. Meta is  (initialCredits+20)*size_of_meta" << std::endl
            << "Requested RU meta: " << RUMetaBufferSize / (1024.f * 1024.f) << " MB (max " << maxRUMetaBufferSize / (1024.f * 1024.f)
            << "), data: " << RUDataBufferSize / (1024.f * 1024.f) << " MB (max " << maxRUDataBufferSize / (1024.f * 1024.f) << ")" << std::endl
            << "Requested BU meta: " << BUMetaBufferSize / (1024.f * 1024.f) << " MB (max " << maxBUMetaBufferSize / (1024.f * 1024.f)
            << "), data: " << BUDataBufferSize / (1024.f * 1024.f) << " MB (max " << maxBUDataBufferSize / (1024.f * 1024.f) << ")" << std::endl << std::endl;
            
        throw -1;
    }
    else {
        INFO << "main buffer configuration" << std::endl
            << "Requested RU meta: " << RUMetaBufferSize / (1024.f * 1024.f) << " MB (max " << maxRUMetaBufferSize / (1024.f * 1024.f)
            << "), data: " << RUDataBufferSize / (1024.f * 1024.f) << " MB (max " << maxRUDataBufferSize / (1024.f * 1024.f) << ")" << std::endl
            << "Requested BU meta: " << BUMetaBufferSize / (1024.f * 1024.f) << " MB (max " << maxBUMetaBufferSize / (1024.f * 1024.f)
            << "), data: " << BUDataBufferSize / (1024.f * 1024.f) << " MB (max " << maxBUDataBufferSize / (1024.f * 1024.f) << ")" << std::endl << std::endl;
    }
    return true;
}

void Core::ConfigurationLoader::activateLogLevel(int logLevel){
    logActivated[logLevel] = 1;
}

std::string Core::ConfigurationLoader::generatePrefix(){
    std::string os_prefix = "/tmp/";
    std::string test_string = "meta1";
    std::string random_string = "";
    std::string gen_prefix = "/";

    if (Tools::fileExists(os_prefix + test_string)){
        WARNING << "Colliding shm data found. Probably another instance is running." << std::endl;

        bool found = false;
        srand(time(NULL));

        while(!found){
            random_string += Tools::toString<int>(rand() % 10);
            if (!Tools::fileExists(os_prefix + random_string + test_string)){
                WARNING << " shm prefix generated: " << random_string << std::endl;
                gen_prefix += random_string + "_";
                found = true;
            }
        }
    }

    return gen_prefix;
}
