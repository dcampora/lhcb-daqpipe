
/**
 *      Shared memory communicator - Builder Unit
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      March, 2014
 *      CERN
 */

#include "ShmemBuilderUnit.h"

ShmemBuilderUnit::ShmemBuilderUnit() : BUTransportCore(){
    qBUFinishedTransactions = new NamedPipe<int>(Core::Configuration->shmemCommQBUFinishedName, Core::Configuration->shmemQueueSize);

    // Start async FragmentReader!
    fragmentIDs.resize(Core::Configuration->initialCredits, -1);
    fragments.resize(Core::Configuration->initialCredits);
    
    fragmentReader = new FragmentReader(this);
    startThreadWithReference<FragmentReader>(fragmentReader);
}

void ShmemBuilderUnit::prepareReceiveEvent(FragmentComposition* fc){
    // TODO: I effectively break shmem transport with this
    int eventID = 0;

    DEBUG << "ShmemBuilderUnit: prepareReceiveEvent eid " << eventID << std::endl;
    
    // memcpy is not needed here, but it's not the cause of slow down (rather, the shmem in general)

    // Find the ID in the fragmentIDs (active wait)
    bool found = false;
    while (!found){
        for (int i=0; i<fragmentIDs.size(); ++i){
            if (fragmentIDs[i] == eventID){
                // Found ya!
                found = true;
                memcpy(fc, &(fragments[i]), sizeof(FragmentComposition));

                // Liberate them
                fragmentIDs[i] = -1;
                break;
            }
        }
        usleep(1);
    }

    eventID_norequests[eventID] = 0;
    eventID_size[eventID] = 0;
    eventID_finished[eventID] = 0;

    DEBUG << "ShmemBuilderUnit: prepareReceiveEvent done!" << std::endl;
}

void ShmemBuilderUnit::setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID){
    DEBUG << "ShmemBuilderUnit: setupReceiveEvent eid " << eventID << std::endl;

    eventID_norequests[eventID]++;
    eventID_size[eventID] += size;
}

/**
 * ShmemBuilderUnit::receiveEvent behaves in a slightly different way than the other
 * receiveEvent functions. In this case, finishedEventID is an eventID that will be polled
 * for checking completion. It's a special case, not to have to extend the BUTransportCore
 * unnecesarily.
 * 
 * @param  finishedEventID **INPUT** eventID
 * @param  tSize           if finished, transmitted size
 * @return                 whether eventID shmem has finished or not
 */
bool ShmemBuilderUnit::receiveEvent(int& finishedEventID, int& tSize){
    int eid;
    while(qBUFinishedTransactions->read(eid) != -1){
        eventID_finished[eid]++;
    }

    DEBUG << "ShmemBuilderUnit: receiveEvent " << finishedEventID << ", req: "
        << eventID_norequests[finishedEventID] << ", finished: " << eventID_finished[finishedEventID] << std::endl;

    if (eventID_norequests[finishedEventID] == eventID_finished[finishedEventID]){
        tSize = eventID_size[finishedEventID];

        eventID_finished.erase(finishedEventID);
        eventID_size.erase(finishedEventID);
        eventID_norequests.erase(finishedEventID);

        return true;
    }
    else if (eventID_finished[finishedEventID] > eventID_norequests[finishedEventID]){
        //WARNING << "ShmemBuilderUnit: Finished more than requested" << std::endl;

        return true;
    }
    return false;

    // Generic check - Deprecated
    // for(std::map<int, int>::iterator it = eventID_norequests.begin(); it != eventID_norequests.end(); it++){
    //     // Return eid and clear if all requests are done
    //     if ( eventID_norequests[it->first] == it->second ){
    //         finishedEventID = it->first;
    //         tSize = eventID_size[it->first];

    //         eventID_size.erase(it->first);
    //         eventID_norequests.erase(it->first);
    //         eventID_finished.erase(it->first);

    //         return true;
    //     }
    // }
    // return false;
}

bool ShmemBuilderUnit::checkExitMessage(void)
{
	return false;
}
