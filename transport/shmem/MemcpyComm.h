
/**
 *      Memcpy memory communicator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

// It writes the result to two queues, to fit exactly what I want to do

#ifndef MEMCPYCOMM
#define MEMCPYCOMM 1

#include <cstring>
#include "../../common/Logging.h"
#include "../../common/GenStructures.h"
#include "../../common/NamedPipe.h"
#include "../../common/pthreadHelper.h"

class MemcpyComm {
private:
    NamedPipe<Transaction> * qtransactions;
    NamedPipe<Transaction> * qinput;
    NamedPipe<int> * qBUFinishedTransactions;
    NamedPipe<int> * qRUFinishedTransactions;

    int nthreads;

public:
    MemcpyComm(int nthreads);
    void run();
    char* getPointer(int bufferID);
};

class Copycat {
private:
    NamedPipe<Transaction> * qinput;
    NamedPipe<int> * qBUFinishedTransactions;
    NamedPipe<int> * qRUFinishedTransactions;
    pthread_t* me;

public:
    Copycat(NamedPipe<Transaction>* qinput, NamedPipe<int>* qBUFinished, NamedPipe<int>* qRUFinished) : 
        qinput(qinput), qBUFinishedTransactions(qBUFinished), qRUFinishedTransactions(qRUFinished) {}
    void run();
};

#endif
