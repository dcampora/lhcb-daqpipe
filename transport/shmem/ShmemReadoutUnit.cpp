
/**
 *      Shared memory communicator - Builder Unit
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      March, 2014
 *      CERN
 */

#include "ShmemReadoutUnit.h"

ShmemReadoutUnit::ShmemReadoutUnit() : RUTransportCore(){
    qfragments = new NamedPipe<FragmentComposition>(Core::Configuration->shmemCommFragmentCompositionName, Core::Configuration->shmemFragmentQueueSize);
    qtransactions = new NamedPipe<Transaction>(Core::Configuration->shmemCommQTransactionName, Core::Configuration->shmemQueueSize);
    qRUFinishedTransactions = new NamedPipe<int>(Core::Configuration->shmemCommQRUFinishedName, Core::Configuration->shmemQueueSize);

    openSharedMem(Core::Configuration->shmemCommMetaBuffer, Core::Configuration->BUMetaBufferSize, descriptor, pmem);
    destPointer = (char*) pmem;
}

void ShmemReadoutUnit::prepareSendEvent(FragmentComposition*& fc, int destinationBU){
    // Send through sh_queue
    DEBUG << "ShmemReadoutUnit: prepareSendEvent for eid " << fc->eventID << std::endl;
    while(qfragments->write(*fc) == -1) usleep(1);

    eventID_norequests[fc->eventID] = 0;
    eventID_finished[fc->eventID] = 0;

    DEBUG << "ShmemReadoutUnit: prepareSendEvent done!" << std::endl;
}

void ShmemReadoutUnit::setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU){
    Transaction t;
    t.id = eventID;
    t.bufferOrig = dataPointer;
    t.size = size;

    if ((destPointer + size - ((char*) pmem)) > Core::Configuration->BUMetaBufferSize)
        // It doesn't fit no more
        destPointer = (char*) pmem;

    DEBUG << "ShmemReadoutUnit: Relative pointer: " << destPointer - (char*) pmem << std::endl;
    
    t.bufferDest = destPointer;

    while(qtransactions->write(t) == -1) usleep(1);
    destPointer += size;

    eventID_norequests[eventID]++;
}

bool ShmemReadoutUnit::sendAnyEvent(int& eventID){
    // int tempEID;
    // while (qRUFinishedTransactions->read(tempEID) != -1)
    //     eventID_finished[tempEID]++;

    // for(std::map<int, int>::iterator it = eventID_norequests.begin(); it != eventID_norequests.end(); it++){
    //     if ( eventID_finished[it->first] == it->second ){
    //         // EID is finished, let's return it
    //         eventID = it->first;

    //         eventID_finished.erase(it->first);
    //         eventID_norequests.erase(it->first);

    //         return true;
    //     }
    // }
    return false;
}

bool ShmemReadoutUnit::sendEvent(int eventID){
    DEBUG << "ShmemReadoutUnit: reading..." << std::endl;
    int counter = 1;

    int tempEID;
    while (qRUFinishedTransactions->read(tempEID) != -1){
        eventID_finished[tempEID]++;
        counter++;
    }

    DEBUG << "ShmemReadoutUnit: read " << counter << " times" << std::endl;

    std::map<int, int>::iterator it = eventID_norequests.find(eventID);
    if (it != eventID_norequests.end()){
        if ( eventID_finished[it->first] == it->second ){
            eventID_finished.erase(it->first);
            eventID_norequests.erase(it->first);
            return true;
        }
    }
    return false;
}
