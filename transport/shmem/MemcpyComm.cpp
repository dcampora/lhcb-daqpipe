
/**
 *      Memcpy memory communicator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      March, 2013
 *      CERN
 */

#include "MemcpyComm.h"

MemcpyComm::MemcpyComm(int nthreads) : nthreads(nthreads) {
    DEBUG << "Memcpy: Constructor" << std::endl;
    qinput = new NamedPipe<Transaction>(Core::Configuration->shmemCommQInputTransactionName, Core::Configuration->shmemQueueSize);
    qtransactions = new NamedPipe<Transaction>(Core::Configuration->shmemCommQTransactionName, Core::Configuration->shmemQueueSize);
    qBUFinishedTransactions = new NamedPipe<int>(Core::Configuration->shmemCommQBUFinishedName, Core::Configuration->shmemQueueSize);
    qRUFinishedTransactions = new NamedPipe<int>(Core::Configuration->shmemCommQRUFinishedName, Core::Configuration->shmemQueueSize);
}

void MemcpyComm::run(){
    // Spawn threads if necessary
    // Listen to incoming requests, make transactions as they come
    Transaction t;
    Copycat* cc;
    int submitted = 0;

    // Start worker threads
    for (int i=0; i<nthreads; ++i){
        cc = new Copycat(qinput, qBUFinishedTransactions, qRUFinishedTransactions);
        startThreadWithReference<Copycat>(*cc);
    }

    while(true){
        DEBUG << "Memcpy: Listening..." << std::endl;
        while ((qtransactions->read(t)) == -1) usleep(1);

        // Copy!
        DEBUG << "Memcpy: Submitted " << submitted++ << std::endl;
        while(qinput->write(t) == -1) usleep(1);
    }
}

void Copycat::run(){
    DEBUG << "Copycat: Started worker thread!..." << std::endl;
    
    Transaction t;
    while(true){
        while(qinput->read(t) == -1) usleep(1);

        // My turn! :)
        memcpy(t.bufferDest, t.bufferOrig, t.size);
        while(qBUFinishedTransactions->write(t.id) == -1) usleep(1);
        while(qRUFinishedTransactions->write(t.id) == -1) usleep(1);

        DEBUG << "Copycat: Finished " << t.id << " (" << t.size << " B)" << std::endl;
    }
}
