
/**
 *      Shared memory communicator - Builder Unit
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      March, 2014
 *      CERN
 */

#ifndef SHMEMBUILDERUNIT
#define SHMEMBUILDERUNIT 1

#include <cstring>
#include "MemcpyComm.h"
#include "../config/ConfigurationCore.h"
#include "../common/BUTransportCore.h"
#include "../../common/NamedPipe.h"
#include "FragmentReader.h"

class ShmemBuilderUnit : public BUTransportCore {
private:
    NamedPipe<int>* qBUFinishedTransactions;

    std::map<int, int> eventID_norequests;
    std::map<int, int> eventID_size;
    std::map<int, int> eventID_finished;

    FragmentReader * fragmentReader;

public:
    std::vector<int> fragmentIDs;
    std::vector<FragmentComposition> fragments;
    
    ShmemBuilderUnit();

    virtual void initialize(int initialCredits){}
    virtual void sendCredits(int& numberOfCredits){}
    virtual void receiveSizeAndID(int& eventSize, int& eventID){}
    
    virtual void prepareReceiveEvent(FragmentComposition* fc);
    virtual void setupReceiveEvent(char* dataPointer, int size, int readoutIndex, int eventID);
    virtual bool receiveEvent(int& finishedEventID, int& tSize);
    virtual bool checkExitMessage(void);
};

#endif
