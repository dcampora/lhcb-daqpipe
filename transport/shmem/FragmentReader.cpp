
/**
 *      Memcpy memory communicator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#include "FragmentReader.h"

FragmentReader::FragmentReader(ShmemBuilderUnit* bu_instance) : 
    bu(bu_instance) {
    qfragments = new NamedPipe<FragmentComposition>(Core::Configuration->shmemCommFragmentCompositionName, Core::Configuration->shmemFragmentQueueSize);
}

void FragmentReader::run(){
    DEBUG << "FragmentReader: Started to run()" << std::endl;
    while(true){
        // Find a free slot in fragmentIDs
        for (int slot=0; slot<bu->fragmentIDs.size(); ++slot){
            if (bu->fragmentIDs[slot] == -1){
                // Read into the free slot
                DEBUG << "FragmentReader: Found free slot! (" << slot << "), awaiting to fill it up" << std::endl;
                
                while (qfragments->read(bu->fragments[slot]) == -1) usleep(1);
                bu->fragmentIDs[slot] = bu->fragments[slot].eventID;
                
                DEBUG << "FragmentReader: qfragment eid " << bu->fragmentIDs[slot] << " read!" << std::endl;
            }
        }

        usleep(1);
    }
}
