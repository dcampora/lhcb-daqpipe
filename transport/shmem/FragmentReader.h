
/**
 *      Memcpy memory communicator
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      April, 2014
 *      CERN
 */

#ifndef FRAGMENTREADER
#define FRAGMENTREADER 1

#include "../config/ConfigurationCore.h"
#include "../../common/NamedPipe.h"

class ShmemBuilderUnit;

class FragmentReader {
private:
    NamedPipe<FragmentComposition>* qfragments;
    ShmemBuilderUnit* bu;

public:
    FragmentReader(ShmemBuilderUnit* bu_instance);
    void run();
};

#endif

#include "ShmemBuilderUnit.h"
