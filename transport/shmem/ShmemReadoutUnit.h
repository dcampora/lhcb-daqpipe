
/**
 *      Shared memory communicator - Readout Unit
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      March, 2014
 *      CERN
 */

#ifndef SHMEMREADOUTUNIT
#define SHMEMREADOUTUNIT 1

#include "MemcpyComm.h"
#include "../common/RUTransportCore.h" 
#include "../config/ConfigurationCore.h"

class ShmemReadoutUnit : public RUTransportCore {
private:
    NamedPipe<Transaction>* qtransactions;
    NamedPipe<FragmentComposition>* qfragments;
    NamedPipe<int>* qRUFinishedTransactions;

    // destPointer starts pointing to the shmem loc, and is increased
    // every setupSendEvent (ring buffer)
    char* destPointer;
    void* pmem;
    int descriptor;

    std::map<int, int> eventID_norequests;
    std::map<int, int> eventID_finished;

public:
    ShmemReadoutUnit();

    virtual void initialize(int initialCredits){}
    virtual bool EventListen(int& size, int& eventID, int& destinationBU){return false;}
    virtual void prepareSendEvent(FragmentComposition*& fragmentCompositionPointer, int destinationBU);
    virtual void setupSendEvent(char* dataPointer, int size, int eventID, int destinationBU);
    virtual bool sendAnyEvent(int& eventID);
    virtual bool sendEvent(int eventID);
};

#endif
