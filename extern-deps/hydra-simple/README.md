Import of hydra-simple files
============================

The files from this directory are imported from the mpich sources :

 - src/pmi/simple/simple_pmi.c
 - src/pmi/simple/simple_pmiutil.c
 - src/pmi/simple/simple_pmiutil.h
 - src/include/pmi.h
 - src/include/mpimem.h
 - src/include/mpibase.h
 - src/pm/util/safestr2.c

Some changes have been done on those files :

 - mpimem.h : only kept the two needed function, so its mostly not the original file.
 - simple_pmi.c : replace include of mpi.h by #define MPI_MAX_PORT_NAME 256
 - In all files, replace the mpichconf.h file by pmiconfig.h

Most of this is inspired from what does the MPC (http://mpc.paratools.com/) project to use hydra.