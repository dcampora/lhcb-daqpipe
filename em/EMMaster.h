
/**
 *      Event Manager Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMMASTER
#define EMMASTER 1

#include "EMCore.h"

#include "../common/SharedQueue.h"
#include "../common/Logging.h"
#include <iostream>
#include <queue>
#include <stdlib.h>

#include "../transport/common/BUTransportCore.h"
#include "../transport/common/EMTransportCoreListener.h"
#include "../transport/common/EMTransportCoreConsumer.h"
//#include "../transport/verbs/MPIEMConsumer.h"
#include "EMCoreConsumerPush.h"
#include "EMCoreConsumerPull.h"

class EMListenerMaster
{
protected:
    EMTransportCoreListener* transport;
    std::string& sqName;

public:
    EMListenerMaster(std::string& sqName, EMTransportCoreListener* transport);
    void run();
};

#define EMListenerMasterPush EMListenerMaster
#define EMListenerMasterPull EMListenerMaster

#define EMConsumerMaster EMCoreConsumer

#define EMConsumerMasterPush EMCoreConsumerPush
#define EMConsumerMasterPull EMCoreConsumerPull

#endif
