
/**
 *      EM Core Push
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMCOREPULL 
#define EMCOREPULL 1

#include "EMCore.h"
#include "EMCoreConsumer.h"
#include "EMCoreListener.h"

class EMCoreConsumerPull : public EMCoreConsumer {
public:
    EMCoreConsumerPull(std::string& sqName, EMTransportCoreConsumer* transport, int triggerFrequency, int eventSize) : 
        EMCoreConsumer(sqName, transport, triggerFrequency, eventSize){}
    virtual void dispatch(int destination);
};

#endif
