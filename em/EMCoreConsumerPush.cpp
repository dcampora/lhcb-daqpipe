
/**
 *      EM Core Push
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "EMCoreConsumerPush.h"
 
void EMCoreConsumerPush::dispatch(int destination){
    size_id_bu[0] = generateSize();
    size_id_bu[1] = currentEventID;
    size_id_bu[2] = destination;

    size_id[0] = size_id_bu[0];
    size_id[1] = size_id_bu[1];

    DEBUG << "EMCorePush: currentEventID " << currentEventID << std::endl;

    // Next event
    currentEventID = currentEventID + 1;
    
    // Send to RUs
    transport->sendToRUs(size_id_bu);
}
