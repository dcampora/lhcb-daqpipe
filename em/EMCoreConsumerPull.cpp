
/**
 *      EM Core Push
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "EMCoreConsumerPull.h"

void EMCoreConsumerPull::dispatch(int destination){
    size_id_bu[0] = generateSize();
    size_id_bu[1] = currentEventID;
    size_id_bu[2] = destination;

    size_id[0] = size_id_bu[0];
    size_id[1] = size_id_bu[1];

    DEBUG << "EMCorePush: currentEventID " << currentEventID << " destination: " << destination << std::endl;

    // Next event, we reserve some bits for fancy things (like MPI tags)
    currentEventID = currentEventID + 1;
    
    // Send to BUs only (they start the communication)
    transport->sendToBU(size_id, size_id_bu[2]);
}
