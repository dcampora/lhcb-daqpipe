
/**
 *      EM Core Trigger
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMCORETRIGGER 
#define EMCORETRIGGER 1

#include "EMCore.h"
 
class EMCoreTrigger
{
private:
    int triggerFrequency, period, creditQueuePointer, consumed;
    NamedPipe<int>* credits;
    SharedQueue* assignedBUs;

public:
    EMCoreTrigger(std::string& sqName, SharedQueue* assignedBUs, int triggerFrequency);
    void run();
};

#endif
