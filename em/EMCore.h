
/**
 *      Event Manager Core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMCORE
#define EMCORE 1

#include "../common/SharedQueue.h"
#include "../common/NamedPipe.h"
#include "../transport/config/ConfigurationCore.h"

#define SLEEP_INTERVAL 1 // 1 us if nothing to process
    // TODO: Rearrange this somewhere (options file)

#endif
