
/**
 *      EM Core Push
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMCOREPUSH 
#define EMCOREPUSH 1

#include "EMCore.h"
#include "EMCoreConsumer.h"
#include "EMCoreListener.h"

class EMCoreConsumerPush : public EMCoreConsumer {
public:
    EMCoreConsumerPush(std::string& sqName, EMTransportCoreConsumer* transport, int triggerFrequency, int eventSize) : 
        EMCoreConsumer(sqName, transport, triggerFrequency, eventSize){

        currentEventID = Core::Configuration->startingEventID;
    }
    virtual void dispatch(int destination);
};

#endif
