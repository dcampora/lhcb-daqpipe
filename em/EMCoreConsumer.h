
/**
 *      EM Core Consumer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMCORECONSUMER 
#define EMCORECONSUMER 1

#include "EMCore.h"
#include "EMCoreTrigger.h"
#include "../transport/common/EMTransportCoreConsumer.h"
#include "../common/pthreadHelper.h"

class EMCoreConsumer
{
private:
    int eventSize;
    SharedQueue* assignedBUs;
    EMCoreTrigger* emTrigger;
    int consumeCredit();
    int fetchCredit();

protected:
    int *size_id_bu, *size_id, currentEventID;
    EMTransportCoreConsumer* transport;
    // TODO: Perhaps we want a dynamic eventSize generation
    int generateSize();

public:
    EMCoreConsumer(std::string& sqName, EMTransportCoreConsumer* transport, int triggerFrequency, int eventSize);
    virtual void dispatch(int destination) = 0;
    void run();
};

#endif
