
/**
 *      Event Manager Core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "EMCoreTrigger.h"

/**
 * Constructor
 */
EMCoreTrigger::EMCoreTrigger(std::string& sqName, SharedQueue* assignedBUs, int triggerFrequency) : 
    assignedBUs(assignedBUs), triggerFrequency(triggerFrequency), consumed(0) {
    
    credits = new NamedPipe<int>(sqName, Core::Configuration->EMSharedQueueSize);
    period = (1.0 / triggerFrequency) * 1000000.0f;
}

/**
 * Generate events every period milliseconds.
 */
void EMCoreTrigger::run(){
    while(true){
        usleep(period);

        int credit = -1;
        if (credits->read(credit) != -1){
            DEBUG << "EMCoreTrigger: Read credit from BU " << credit << std::endl;
        }
        
        if (credit != -1){
            int wres = -1;
            while(wres == -1){
                wres = assignedBUs->write(credit);
                DEBUG << "EMCoreTrigger: Written credit " << credit << std::endl;
                if (wres == -1){
                    // TODO: Increment to WARNING
                    DEBUG << "EMCoreTrigger: no assignedBUs slots available to write" << std::endl;
                }
            }
            // ++consumed;
            if((++consumed % 1000) == 0)
                DEBUG << "EMCoreTrigger: consumed " << consumed << std::endl;
        }
        else {
            // TODO: Increment to WARNING
            // DEBUG << "EMCoreTrigger: no credits" << std::endl;
        }
    }
}
