
/**
 *      EM Core Listener
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#ifndef EMCORELISTENER 
#define EMCORELISTENER 1

#include "EMCore.h"
#include "../transport/common/EMTransportCoreListener.h"

class EMCoreListener
{
private:
    EMTransportCoreListener* transport;

public:
    NamedPipe<int>* credits;
    EMCoreListener(std::string& sqName, EMTransportCoreListener* transport);
    
    void addCredits(int node, int ncredits);
    void addCreditToQueue(int node);
    void run();
};

#endif
