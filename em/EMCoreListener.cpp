
/**
 *      Event Manager Core
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include <iostream>
#include "EMCoreListener.h"
#include "../common/Timer.h"

/**
 * Constructor
 */
EMCoreListener::EMCoreListener(std::string& sqName, EMTransportCoreListener* transport) :
    transport(transport) {
    credits = new NamedPipe<int>(sqName, Core::Configuration->EMSharedQueueSize);
}

void EMCoreListener::addCredits(int node, int ncredits){
    for(int i=0; i<ncredits; ++i)
        addCreditToQueue(node);

    DEBUG << "EMCoreListener: Added " << ncredits << " credits of node " << node << " to consumer queue" << std::endl;
}

void EMCoreListener::addCreditToQueue(int node){
    int error = credits->write(node);
    while(error == -1){
        usleep(SLEEP_INTERVAL);
        error = credits->write(node);
    }
}

void EMCoreListener::run(){
    DEBUG << "EMCoreListener: started" << std::endl;
    transport->setupListen();
    
    //timer to check timeout
    Timer timeoutTimer;
    timeoutTimer.start();

    while(true){
        // Get credits
        int node, ncredits;

        transport->listenForCreditRequest(node, ncredits);
        if (node == -1)
            break;

        // Add credits to queue
        addCredits(node, ncredits);
    }
}
