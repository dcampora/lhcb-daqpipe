
/**
 *      EM Core Consumer
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "EMCoreConsumer.h"
#include "../common/Timer.h"

/**
 * Constructor
 */

    // EMCoreConsumerPush(std::string& sqName, EMTransportCoreConsumer*& transport, int triggerFrequency, int eventSize) : 
    //     EMCoreConsumer(sqName, transport, triggerFrequency, eventSize){}

EMCoreConsumer::EMCoreConsumer(std::string& sqName, EMTransportCoreConsumer* transport, int triggerFrequency, int eventSize) : 
    eventSize(eventSize), transport(transport), currentEventID(0) {

    size_id_bu = (int*) malloc(3 * sizeof(int));
    size_id = (int*) malloc(1000 * sizeof(int));
    // size_id_bu[0] = 0xBEEF;
    // size_id_bu[1] = 0xCAFE;
    // size_id_bu[2] = 0XCAFECAFE;

    DEBUG << "malloc'ed size_id and size_id_bu" << std::endl;
    
    assignedBUs = new SharedQueue(Core::Configuration->EMSharedQueueSize);

    DEBUG << "malloc'ed SharedQueue" << std::endl;

    emTrigger = new EMCoreTrigger(sqName, assignedBUs, triggerFrequency);
    startThreadWithReference<EMCoreTrigger>(*emTrigger);

    DEBUG << "EMCoreConsumer: started EM trigger" << std::endl;
}

int EMCoreConsumer::generateSize(){
    return eventSize;
}

int EMCoreConsumer::fetchCredit(){
    int destination = consumeCredit();
    while(destination == -1){
        usleep(SLEEP_INTERVAL);
        destination = consumeCredit();
    }
    return destination;
}

/**
 * Consumes credits from the credits queue.
 * @return -1 if there is no credits available.
 *         BU id otherwise.
 */
int EMCoreConsumer::consumeCredit(){
    int cread;
    if(assignedBUs->read(cread) != -1)
        return cread;
    return -1;
}

/**
 * TODO: Accumulate several events before sending.
 * Main loop, fetch credits, build event and send.
 */
void EMCoreConsumer::run(){
    transport->initialize();
    
    //timer to check timeout
    Timer timer;
    timer.start();

    while(true){
        // Fetch destination.
        int destination = fetchCredit();
        DEBUG << "EMCoreConsumer: Fetched credit from BU " << destination << std::endl;

        // Dispatch destination using specialized dispatch function.
        dispatch(destination);
        
        //check timeout
        if (Core::Configuration->timeout > 0 && timer.getElapsedTime() > Core::Configuration->timeout)
        {
            DEBUG << "EMCoreConsumer::run Reach timeout" << std::endl;
            transport->sendExit();
            break;
        }
    }
}
