
/**
 *      Event Manager Master
 *
 *      author  -   Daniel Campora
 *      email   -   dcampora@cern.ch
 *
 *      November, 2013
 *      CERN
 */

#include "EMMaster.h"

/**
 * Listener Master
 */
EMListenerMaster::EMListenerMaster(std::string& sqName, EMTransportCoreListener* transport) :
    sqName(sqName), transport(transport){}

void EMListenerMaster::run(){
    EMCoreListener* emListener = new EMCoreListener(sqName, transport);
    emListener->run();
}
