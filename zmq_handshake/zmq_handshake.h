#ifndef ZMQ_HANDSHAKE_H
#define ZMQ_HANDSHAKE_H

#include <zmq.h>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <cstring>
#include <limits.h>
#include <map>
#include <vector>
#include "../common/Tools.h"

#define BUFFER_SIZE HOST_NAME_MAX
#define SYNC_OK "SYNC_OK"
#define SYNC_ERROR "SYNC_ERR"

int zmq_handshake_sub(const std::string &server_addr,
                      const std::string &pub_port,
                      const std::string &sync_port);

int zmq_handshake_pub(int number_of_subscribers,
                      const std::string &pub_port,
                      const std::string &sync_port,
                      std::map<std::string, std::vector<int>> host_name_rank_id);

#endif // ZMQ_HANDSHAKE_H
