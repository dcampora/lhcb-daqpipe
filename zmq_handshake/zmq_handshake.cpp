#include "zmq_handshake.h"

int zmq_handshake_sub(const std::string &server_addr,
                      const std::string &pub_port,
                      const std::string &sync_port){

   int ret_val;
   std::string pub_addr;
   std::string sync_addr;
   void *context = zmq_ctx_new();
   char send_buffer[BUFFER_SIZE];
   char receive_buffer[BUFFER_SIZE];

   void *subscriber;
   void *syncclient;
   pub_addr =  "tcp://";
   pub_addr += server_addr;
   pub_addr += ":";
   pub_addr += pub_port;

   subscriber = zmq_socket(context, ZMQ_SUB);

   zmq_connect(subscriber, pub_addr.c_str());
   zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, "", 0);

   sleep(1);

   sync_addr =  "tcp://";
   sync_addr += server_addr;
   sync_addr += ":";
   sync_addr += sync_port;

   syncclient = zmq_socket(context, ZMQ_REQ);

   zmq_connect(syncclient, sync_addr.c_str());

   gethostname(send_buffer, BUFFER_SIZE);
   zmq_send(syncclient, send_buffer, BUFFER_SIZE, 0);

   zmq_recv(syncclient, receive_buffer, BUFFER_SIZE, 0);
   ret_val = atoi(receive_buffer);

   zmq_recv(subscriber, receive_buffer, BUFFER_SIZE, 0);

   if(strcmp(receive_buffer, SYNC_OK) != 0){
       ret_val = -1;
   }

   zmq_close (subscriber);
   zmq_close (syncclient);
   zmq_ctx_destroy (context);

   return ret_val;
}

int zmq_handshake_pub(int number_of_subscribers,
                      const std::string &pub_port,
                      const std::string &sync_port,
                      std::map<std::string, std::vector<int>> host_name_rank_id){

    std::string pub_addr;
    std::string sync_addr;
    void *context = zmq_ctx_new ();
    std::string send_buffer;
    char receive_buffer[BUFFER_SIZE];

    //  Socket to talk to clients
    void *publisher = zmq_socket (context, ZMQ_PUB);

    int sndhwm = 1100000;
    zmq_setsockopt (publisher, ZMQ_SNDHWM, &sndhwm, sizeof (int));

    pub_addr = "tcp://*:";
    pub_addr += pub_port;
    zmq_bind (publisher, pub_addr.c_str());

    //  Socket to receive signals
    void *syncservice = zmq_socket (context, ZMQ_REP);

    sync_addr = "tcp://*:";
    sync_addr += sync_port;

    zmq_bind (syncservice, sync_addr.c_str());

    //  Get synchronization from subscribers
    std::cout << "Waiting for " << number_of_subscribers << " subscribers" << std::endl;
    for (int subscribers = 0 ; subscribers < number_of_subscribers ; subscribers++){
        //  - wait for synchronization request
        zmq_recv(syncservice, receive_buffer, BUFFER_SIZE, 0);
        //  - send synchronization reply
        std::map<std::string, std::vector<int>>::iterator it;
        it = host_name_rank_id.find(std::string(receive_buffer));
        if (it == host_name_rank_id.end()){
            std::cerr << "unknown host " << receive_buffer << " aborting"<< std::endl;
            break;
        }
        send_buffer = Tools::toString(it->second.back());
        it->second.pop_back();
        zmq_send(syncservice, send_buffer.c_str(), BUFFER_SIZE, 0);
    }
    //  Now broadcast exactly 1M updates followed by END

    bool check = true;
    for ( auto host : host_name_rank_id){
        if(host.second.size() != 0){
            check = false;
        }
    }

    std::cout << "Broadcasting messages" << std::endl;
    if (check){
        zmq_send(publisher, SYNC_OK, strlen(SYNC_OK), 0);
    }else{
        zmq_send(publisher, SYNC_ERROR, strlen(SYNC_ERROR), 0);
    }

    zmq_close (publisher);
    zmq_close (syncservice);
    zmq_ctx_destroy (context);
    return 0;
}

//int zmq_ask_id(const std::string &pub_port,
//               const std::string &sync_port){

//    void *context = zmq_ctx_new ();
//    void *receiver = zmq_socket (context, ZMQ_PULL);
//    zmq_connect (receiver, "tcp://localhost:5557");

//    // Socket to send messages to
//    zmq_connect (sender, "tcp://localhost:5558");

//}
