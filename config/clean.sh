#!/bin/bash
# Now it will only clean apps started by user

AP_NAMES=(xterm eb_app)

for i in `cat $1`;
  do
  echo "Cleaning $i";
  #ssh $i rm -f /dev/shm/meta* /dev/shm/data* /dev/shm/sharedqueue /dev/shm/trigger /dev/shm/gpuru-* /dev/shm/bu-* /dev/shm/shmem-*;
  ssh $i rm -rf /dev/shm/*daqpipe-${USER} /tmp/shmem* /tmp/daqpipe-${USER}

  for ap_name in "${AP_NAMES[@]}"; do
    ssh $i killall -9 "${ap_name}" &
  done

done

wait
