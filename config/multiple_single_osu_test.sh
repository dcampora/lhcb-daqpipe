#!/bin/bash

mpirun -np 2 -mca btl openib,self -machinefile ./config/labs_11_12.labs /usr/mpi/gcc/openmpi-1.8.2/tests/osu-micro-benchmarks-4.3/osu_bw > r1.out &

mpirun -np 2 -mca btl openib,self -machinefile ./config/labs_11_12.labs /usr/mpi/gcc/openmpi-1.8.2/tests/osu-micro-benchmarks-4.3/osu_bw > r2.out &

wait

