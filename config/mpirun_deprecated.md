## Testing the fabric and library ##

# Debugging the BTL (transport)
mpirun --map-by ppr:1:node -mca mca_component_path /usr/mpi/gcc/openmpi-1.6.5/lib64/openmpi -mca self,btl openib -mca btl_openib_warn_default_gid_prefix 0 -mca btl_base_verbose 100 -mca btl_openib_verbose 100 -machinefile <machine_file> `pwd`/em_bu_app

# Run custom OSU test
mpirun --map-by ppr:1:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/mpi_tests/osu_bibw

## MVAPICH2 ##

# Run
# We use the wrapper mpiexec
# The application behaves slightly different (not normalized BW between nodes,
# ie. one side transmits faster than the other)
/usr/mpi/gcc/mvapich2-1.9/bin/mpiexec -env MV2_NUM_HCAS 1 -env MV2_NUM_PORTS 2 -ppn 4 -machinefile <machine_file> `pwd`/taskset_launch.sh 0 1 `pwd`/eb_app -f 400 -s 52428800

## OpenMPI ##

### Debug run ###

# To debug, open a second -X ssh session, and modify mpilaunch_debug.sh with the proper DISPLAY
./config/clean.sh <machine_file> ; mpirun --map-by ppr:2:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/mpilaunch_debug.sh xterm -sl 10000 -e gdb --args `pwd`/eb_app -f 1 -s $((100*1024)) -c 2 -d 5

# Automatically run
./config/clean.sh <machine_file> ; mpirun --map-by ppr:2:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/mpilaunch_debug.sh xterm -sl 10000 -e gdb -x `pwd`/gdb_run.command --args `pwd`/eb_app -f 100000 -s $((100*1024)) -c 2 -d 5

### Valgrind ###

# It is not very useful at the moment.
# Note: Our OpenMPI is not compiled with Memchecker.
# For more info: http://www.open-mpi.org/faq/?category=debugging#valgrind_clean
mpirun --map-by ppr:4:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/mpilaunch_debug.sh xterm -hold -e valgrind --suppressions=/usr/mpi/gcc/openmpi-1.6.5/share/openmpi/openmpi-valgrind.supp `pwd`/eb_app -f 1 -s 1024

### Run MPI application ###

# Run anywhere
./config/clean.sh <machine_file>; mpirun --map-by ppr:2:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/numactl_launch.sh `pwd`/eb_app  -f 100000 -s $((100*1024)) -c 2

# Run in the labs (cpp11)
./config/clean.sh <machine_file>; mpirun --map-by ppr:2:node -mca btl openib,self -mca btl_openib_warn_default_gid_prefix 0 -machinefile <machine_file> `pwd`/numactl_launch.sh `pwd`/config/setenv.sh `pwd`/eb_app  -f 100000 -s $((100*1024)) -c 2

# Run in farm (debug)
./config/clean.sh ./config/hltb01.labs ; /usr/lib64/openmpi/bin/mpirun -npernode 2 -mca btl tcp,self --mca btl_tcp_if_include 192.168.0.0/16 -machinefile ./config/hltb01.labs `pwd`/mpilaunch_debug.sh xterm -sl 10000 -e gdb -x `pwd`/gdb_run.command --args `pwd`/build/eb_app -f 100000 -s $((10*1024)) -c 1

# Run in the farm (best performance over TCP - 2 BUs and 1 credit)
./config/clean.sh ./config/hltb01.labs ; /usr/lib64/openmpi/bin/mpirun -npernode 2 -mca btl tcp,self --mca btl_tcp_if_include 192.168.0.0/16 -machinefile ./config/hltb01.labs  `pwd`/build/eb_app -f 100000 -s $((10*1024)) -c 1


