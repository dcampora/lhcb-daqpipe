#!/bin/bash
# Script to reserve some named chunk of memory to a socket-memory.
# Useful when requiring the whole program to be pinned in one membind,
# but only some memory on the other socket. Ie. recx buffer only.

ssh lab14 numactl --length=1G --file /dev/shm/bu-data  --membind=1 
ssh lab14 numactl --length=256M --file /dev/shm/bu-meta --membind=1

ssh lab13 numactl --length=1G --file /dev/shm/bu-data  --membind=1
ssh lab13 numactl --length=256M --file /dev/shm/bu-meta --membind=1 
