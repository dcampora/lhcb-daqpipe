#!/usr/bin/python

import os
import sys
import getopt
import subprocess
import re
from time import sleep

connectivity_test_command = "ssh -o ConnectTimeout=<timeout> <host> uname -a"

class ConnectivityTester(object):
    """Checks the connectivity of nodes (SSH)."""
    def __init__(self):
        self._timeout = '10'
        self._retries = '2'

    def testConnectivity(self, hosts):
        """Simply test which nodes are active in the host list.
        Notify the ones not active, and return the active node list."""
        print "Performing connectivity test..."
        active_hosts = []
        required_hosts = [a for a in hosts]

        # Retry several times
        r = 0
        while r < int(self._retries):
            r += 1
            print "Attempt #" + str(r)
            request_pool = []
            for i in required_hosts:
                request_pool.append(self._issueConnectivityCommand(i))
            sleep(int(self._timeout))
            for i in range(len(required_hosts)):
                if self._testConnectivity(request_pool[i], required_hosts[i]):
                    active_hosts.append(required_hosts[i])

            # Shorten the list of required_hosts and cleanup
            for i in active_hosts:
                try: required_hosts.remove(i)
                except: pass

            if required_hosts == []:
                break

        if hosts == active_hosts:
            print "All nodes are reachable and ssh-able!"
        else:
            print "Active hosts:", active_hosts

        self.hosts_real_len = len(active_hosts)
        return active_hosts

    def _issueConnectivityCommand(self, host):
        s = re.sub("<host>", host, connectivity_test_command)
        s = re.sub("<timeout>", self._timeout, s)
        return subprocess.Popen(s.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def _testConnectivity(self, p, hostname):
        # Simply poll when the timeout is over
        if p.poll() == None or p.stderr.readlines() != []:
            try: os.kill(p.pid, signal.SIGKILL)
            except: pass
            #print hostname, "is not reachable:", (p.poll() and ''.join(p.stderr.readlines())) or "timeout"
            return 0
        return 1

    def _waitClients(self):
        for iperf_instance in self.clients.values():
            iperf_instance.wait()


def lengthtify(s, length):
    if len(s) != length:
        return ''.join(['0' for i in range(length - len(s))]) + s
    return s

def genStringRange(start=1, end=10, length=2):
    s = []
    for i in range(start, end+1):
        s.append( lengthtify(str(i), length) )
    return s


def testFarm(farm = "a", nnodes = "8"):
    # Test connectivity on the subfarms which are free
    # on the d1 interface (data)
    i_nnodes = int(nnodes)
    no_subfarms = 10
    if farm == "f": no_subfarms = 6
    farms = [farm+i for i in genStringRange(end=no_subfarms)]
    
    tester = ConnectivityTester()
    out = open("alive_nodes_" + farm + ".txt", "w")
    for f in farms:
        print "Testing subfarm", f
        nodes = ["hlt"+f+i+"-d1" for i in genStringRange(end=24)]
        alive_nodes = tester.testConnectivity(nodes)
        # Pick up a max of 5 per subfarm, to maximize performance
        for i in alive_nodes[:i_nnodes]:
            out.write(i + "\n")
    out.close()


def utilization_and_exit():
    print 'test_connectivity.py -f <farms> (eg. -f bc) [-n <nnodes>]'
    sys.exit(2)


def main(argv):
    # Get params
    farms = "b"
    nnodes = "8"
    start = False

    try:
        opts, args = getopt.getopt(argv, "hf:n:",["farms=","nnodes="])
    except getopt.GetoptError, e:
        utilization_and_exit()
    for opt, arg in opts:
        if opt == '-h':
            utilization_and_exit()
        elif opt in ("-f", "--farms"):
            farms = arg
            start = True
        elif opt in ("-o", "--ofile"):
            nnodes = arg

    if not start:
        utilization_and_exit()

    testFarm(farms, nnodes)


if __name__ == "__main__":
   main(sys.argv[1:])
